    
//
//  Macro.swift
//  SnapFood
//
//  Created by Capermint Mini 3 on 9/29/16.
//  Copyright © 2016 Capermint Mini 3. All rights reserved.
//

import Foundation
import NVActivityIndicatorView
import SlideMenuControllerSwift
import AVFoundation

let IS_IPHONE4s = UIScreen.main.bounds.size.height == 480.0

let IS_IPHONE5 = UIScreen.main.bounds.size.height == 568.0

let IS_IPHONE6 = UIScreen.main.bounds.size.height == 667.0

let IS_IPHONE6P = UIScreen.main.bounds.size.height == 736.0

let IS_IPAD = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad

let SCREEN_WIDTH = UIScreen.main.bounds.size.width

let SCREEN_HEIGHT = UIScreen.main.bounds.size.height

let screenBounds = UIScreen.main.bounds

let screenScale = UIScreen.main.scale

let screenSize = CGSize(width: screenBounds.size.width * screenScale, height: screenBounds.size.height * screenScale)

let screenWidth = screenSize.width

let screenHeight = screenSize.height

let AppName = "Meditable"
var numberOfDaysRemaining : Int = 0
var expiryDate = ""
var downloadAgain = false
var downloadRefIndex = IndexPath()

//App Delegate
let appDelegate = UIApplication.shared.delegate as! MTAppDelegate

//App URL Scheme
let facebookScheme = "fb1901002226837398"

let googleScheme = ""

let twitterScheme = ""



//User Defaults
let defaults = UserDefaults.standard

let SubscriptionStartDate = "SubscriptionStartDate"
let kdownloadedMeditations = "offlineDownloadedMeditations"

//Googel API Key
let googleAPIKey = ""

//App Version
let appVersion = "1.0"

//Alert Title And Messages
let AlertViewTitle = "Meditable"

let NoInternet = "Connect to the internet for recent updates"

let ErrorMessage = "There is some error occured. Please try again later"

let QuitMeditaionMessage = "Are you sure you want to quit this meditation?"

let QuitBackgroundDownloadMessage = "Are you sure you want to quit this download?"

let NoInternetMessage = "Sorry, no Internet connectivity detected. Please reconnect and try again"

let DeleteMeditaionMessage = "Do you want to delete this saved meditaton?"

let DeleteSavedAndListMeditaionMessage = "Do you want to delete this saved meditation and remove from your device?"

let DeleteDownloadedMeditaionMessage = "Do you want to delete this downloaded meditation from your device?"
    
let LockMeditationMessage = "This is premium content. Subscribe & enjoy!"

let LikeToSaveEditedMeditation = "Would you like to save your edited meditation?"

let DeleteBackgroundTrack = "Do you want to delete this downloaded background track?"

let updateMeditation = "Would you like to overwrite this downloaded meditation with your changes?"
    
let upgradeMsg = "Your trial has expired, subscribe for membership"

let updateMeditationSuccessfully = "Would you like to overwrite this downloaded meditation with your changes?"

let signUpSuccessfull = "You have registered successfully, please verify email address to continue enjoying Meditable."

//Base URL
//let baseURLWithoutAuth = "http://madeinkhadi.com/meditable/api/beforeauth/"

//let baseURLWithAuth = "http://madeinkhadi.com/meditable/api/auth/"

let baseURLWithoutAuth = "http://getmeditable.com/meditable/api/beforeauth/"
    
let baseURLWithAuth = "http://getmeditable.com/meditable/api/auth/"
    
    
// Google Track ID

let googleTrackID = "UA-101769120-1"

//API

let kUserSignup = "signup"
let kUserLogin = "login"
let kResetPwd = "resetpassword"
let kChangePwd = "changepassword"
let kLogout = "logout"
let kSocialLogin = "sociallogin"
let kUpdateProPic = "updateprofilepicture"
let kAboutTrack = "aboutrack"
let kCategoryWithMeditationCount = "subcategorylist"
let kGetMeditationList = "meditationlist"
let kGetSubCategoryListWithTrackCount = "subcategorylistwithtrackcount"
let kGetcustommeditationcount = "custommeditationcount"
let kGetBackgroundTracks = "backgroundtrack"
let kGetDefaultmeditationtrack = "defaultmeditationtrack"
let kGetTracksList = "tracklist"
let kSaveCustomList = "savecustom"
let kEditCustomList = "editcustom"
let kGetIntroTracks = "introtrack"
let kGetEndingTracks = "endingtrack"
let kGetSilenceTracks = "silencetrack"
let kDeleteMeditation = "deletemeditation"
let kGetCustomMeditationList = "custommeditationlist"
let kGetPlanlist = "planlist"
let kAddTrackLog = "addtracklog/"
let kAutoLogin = "autologin"
let kMyActivity = "myactivity"
let kAddToAnother = "addtocustom"
let kUpdateMembership = "updatemembership"
let kPromoCode = "applypromocode"
let kcancelmembership = "cancelmembership"

    

//Subscription
let MonthlySubscriptionIdentifier = "com.meditable.monthlysubscription"
let YearlySubscriptionIdentifier = "com.meditable.yearlysubscription"
let MonthlyLimitedSubscriptionIdentifier = "com.meditable.limitedmonthlysubscription"
let YearlyLimitedSubscriptionIdentifier = "com.meditable.limitedyearlysubscription"

let MonthlyIAPMessage = "Would you like to subscribe to Meditable Premium for $9.99 /month?"
let YearlyIAPMessage = "Would you like to subscribe to Meditable Premium for $77.99 /year?"
let MonthlyLimitedIAPMessage = "Would you like to subscribe to Meditable Premium for $9.99 /month?"
let YearlyLimitedIAPMessage = "Would you like to subscribe to Meditable Premium for $77.99 /year?"

let upgradePlanToMonthly = "Would you like to upgrade to monthly subscription for $9.99?"
let upgradePlanToYearly = "Would you like to upgrade to yearly subscription for $77.99?"

let errorWhileLoading = "Error occured while loading tracks, please try again."

let SelectPlan = "Please select any plan for subscription."
let cancelSubscriptionURL = "itms-apps://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/manageSubscriptions"
//Meditation By
let MadeByYou = "Made by Me"
let MadeByMeditable = "Made by Meditable"

//iTunesConnect App Link
let appURL = "http://www.google.com"

//Dabase Path
let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    .appendingPathComponent("Meditable.sqlite")

//Add Statusbar to View
extension UIViewController {
    func addStatusBarBackgroundView(_ viewController: UIViewController) -> Void {
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size:CGSize(width: SCREEN_WIDTH, height:20))
        let view : UIView = UIView.init(frame: rect)
        view.backgroundColor = UIColor().backgroundColor
        viewController.view?.addSubview(view)
    }
    
    func addSlideMenuGestures() {
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
    }
    
    func removeSlideMenuGestures() {
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
    
    func inAppPurchase(){
        NotificationCenter.default.addObserver(forName: NSNotification.Name.mkStoreKitProductPurchased,object: nil, queue: OperationQueue.main) { (note) -> Void in
            print ("Purchased product: \(note.object!)")
            appDelegate.checkPurchasedProduct()
        }
    }
    
    func showToast(message : String) {
        let toastLabel =
            UILabel(frame:
                CGRect(x: self.view.frame.size.width/2 - 150,
                       y: self.view.frame.size.height-100,
                       width: 300,
                       height: 35))
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.black
        toastLabel.textAlignment = NSTextAlignment.center
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
        self.view.addSubview(toastLabel)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        UIView.animate(withDuration: 4.0, animations: {
            toastLabel.alpha = 0.0
        })
    }
    //For save and fetch Locally
    func saveToLocal(result : [[String : AnyObject]], title : String){
        let defaultData = NSKeyedArchiver.archivedData(withRootObject: result)
        defaults.set(defaultData, forKey: title)
        defaults.synchronize()
        
        let defaultUnarchivedData = defaults.value(forKey: title) as! NSData
        let unarchivedData = NSKeyedUnarchiver.unarchiveObject(with: defaultUnarchivedData as Data)
    }
    
    func fetchFromLocal(title : String) -> [[String : AnyObject]]{
        let defaultUnarchivedData = defaults.value(forKey: title) as! NSData
        let unarchivedData = NSKeyedUnarchiver.unarchiveObject(with: defaultUnarchivedData as Data)
        return unarchivedData as! [[String : AnyObject]]
    }
    
    func addGoogleAnalytics(screenName : String){
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: screenName)
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    func getDateMonthYear(date : Date) -> (date: Int, month: Int, year: Int){
        let currentDate = date
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: currentDate)
        
        let year =  components.year
        let month = components.month
        let day = components.day
        
        return (day!, month!, year!)
    }
    
    func getTotalTime(totalTime : String, backgroundMusicTime : String) -> (time : String, backgroundSeconds : Int, totalSeconds : Int){
        let totalArray = totalTime.components(separatedBy: ":")
        let bgArray = backgroundMusicTime.components(separatedBy: ":")
        var timeTotal = 0
        if(totalArray.count == 2){
            timeTotal = (Int("\(totalArray[0])")! * 60) + Int.init("\(totalArray[1])")!
        }else if(totalArray.count == 3){
            timeTotal = (Int("\(totalArray[0])")! * 3600) + (Int("\(totalArray[1])")! * 60) + Int.init("\(totalArray[2])")!
        }
        let backgroundSeconds = (Int("\(bgArray[0])")! * 60) + Int.init("\(bgArray[1])")!
        let totalseconds = timeTotal - backgroundSeconds
        var time = ""
        if(totalseconds >= 3600){
            var Hours = "\(Int(totalseconds) / 3600)"
            var Minuts = "\((totalseconds % 3600) / 60)"
            var Seconds = "\((totalseconds % 3600) % 60)"
            
            if Hours.characters.count == 1 {
                Hours = "0\(Hours)"
            }
            if Minuts.characters.count == 1 {
                Minuts = "0\(Minuts)"
            }
            if Seconds.characters.count == 1 {
                Seconds = "0\(Seconds)"
            }
            
            time = "\(Hours):\(Minuts):\(Seconds)"
        }else{
            var Minuts = "\((totalseconds % 3600) / 60)"
            var Seconds = "\((totalseconds % 3600) % 60)"
            
            if Minuts.characters.count == 1 {
                Minuts = "0\(Minuts)"
            }
            if Seconds.characters.count == 1 {
                Seconds = "0\(Seconds)"
            }
            
            time = "\(Minuts):\(Seconds)"
        }
        
        return (time, backgroundSeconds, totalseconds)
    }
    
    func getSeconds(totalTime : String) -> Int {
        var time = 0
        let totalArray : [String] = totalTime.components(separatedBy: ":")
        if(totalArray.count == 2){
            time = (Int("\(totalArray[0])")! * 60) + Int("\(totalArray[1])")!
        }else if(totalArray.count == 3){
            time = (Int("\(totalArray[0])")! * 3600) + (Int("\(totalArray[1])")! * 60) + Int("\(totalArray[2])")!
        }
        return time
    }
    func getTotalTimeWithoutSilenceBackGround(totalTime : String, backgroundMusicTime : String, silenceMusic : String) -> (time : String, backgroundSeconds : Int, totalSeconds : Int, silenceSeconds : Int){
        let totalArray = totalTime.components(separatedBy: ":")
        let bgArray = backgroundMusicTime.components(separatedBy: ":")
        let silenceArray = silenceMusic.components(separatedBy: ":")

        var timeTotal = 0
        if(totalArray.count == 2){
            timeTotal = (Int("\(totalArray[0])")! * 60) + Int.init("\(totalArray[1])")!
        }else if(totalArray.count == 3){
            timeTotal = (Int("\(totalArray[0])")! * 3600) + (Int("\(totalArray[1])")! * 60) + Int.init("\(totalArray[2])")!
        }
        
        let backgroundSeconds = (Int("\(bgArray[0])")! * 60) + Int.init("\(bgArray[1])")!
        let silenceSeconds = (Int("\(silenceArray[0])")! * 60) + Int.init("\(silenceArray[1])")!

        let totalseconds = timeTotal - (backgroundSeconds + silenceSeconds)
        
        var time = ""
        if(totalseconds >= 3600){
            var Hours = "\(Int(totalseconds / 3600))"
            var Minuts = "\((totalseconds % 3600) / 60)"
            var Seconds = "\((totalseconds % 3600) % 60)"
            
            if Hours.characters.count == 1 {
                Hours = "0\(Hours)"
            }
            if Minuts.characters.count == 1 {
                Minuts = "0\(Minuts)"
            }
            if Seconds.characters.count == 1 {
                Seconds = "0\(Seconds)"
            }
            
            time = "\(Hours):\(Minuts):\(Seconds)"
        }else{
            var Minuts = "\((totalseconds % 3600) / 60)"
            var Seconds = "\((totalseconds % 3600) % 60)"
            
            if Minuts.characters.count == 1 {
                Minuts = "0\(Minuts)"
            }
            if Seconds.characters.count == 1 {
                Seconds = "0\(Seconds)"
            }
            
            time = "\(Minuts):\(Seconds)"
        }
        
        return (time, backgroundSeconds, totalseconds, silenceSeconds)
    }
    
    func getTotalTrackTime(track : [[String : AnyObject]]) -> (time : String, totalSeconds : Int){
        var totalseconds : Int = 0
        for i in 0...track.count - 1{
            let time = "\(track[i]["time"]!)"
            let totalArray = time.components(separatedBy: ":")
            if(totalArray.count == 2){
                totalseconds = (Int("\(totalArray[0])")! * 60) + Int.init("\(totalArray[1])")! + totalseconds
            }else if(totalArray.count == 3){
                totalseconds = (Int("\(totalArray[0])")! * 3600) + (Int("\(totalArray[1])")! * 60) + Int.init("\(totalArray[2])")! + totalseconds
            }
        }
        var time = ""
        if(totalseconds >= 3600){
            var hours = "\(Int(totalseconds) / 3600)"
            var minutes = "\(Int(totalseconds) / 60 % 60)"
            var seconds = "\(Int(totalseconds) % 60)"
            if hours.characters.count == 1 {
                hours = "0\(hours)"
            }
            if minutes.characters.count == 1 {
                minutes = "0\(minutes)"
            }
            if seconds.characters.count == 1 {
                seconds = "0\(seconds)"
            }
            time = "\(hours):\(minutes):\(seconds)"
        }else{
            var minutes = "\(Int(totalseconds) / 60 % 60)"
            var seconds = "\(Int(totalseconds) % 60)"
            if minutes.characters.count == 1 {
                minutes = "0\(minutes)"
            }
            if seconds.characters.count == 1 {
                seconds = "0\(seconds)"
            }
            time = "\(minutes):\(seconds)"
        }
        return (time, totalseconds)
    }
    
    
    func getTotalMeditationTime(trackTime : String) -> (Int){
        let totalArray = trackTime.components(separatedBy: ":")
        var totalseconds = 0
        if(totalArray.count == 2){
            totalseconds =  (Int("\(totalArray[0])")! * 60) + Int("\(totalArray[1])")!
        }else if(totalArray.count == 3){
            totalseconds = (Int("\(totalArray[0])")! * 3600) + (Int("\(totalArray[1])")! * 60) + Int("\(totalArray[2])")!
        }
        return (totalseconds)
    }
    func setTotalMeditationTime(totalseconds : Int) -> (String){
        var Hours = "\(totalseconds / 3600)"
        var Minuts = "\((totalseconds % 3600) / 60)"
        var Seconds = "\((totalseconds % 3600) % 60)"
        
        if Hours.characters.count == 1 {
            Hours = "0\(Hours)"
        }
        if Minuts.characters.count == 1 {
            Minuts = "0\(Minuts)"
        }
        if Seconds.characters.count == 1 {
            Seconds = "0\(Seconds)"
        }
        
        let time = "\(Hours):\(Minuts):\(Seconds)"
        return (time)
    }
}

extension UIApplication {
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        
        if let slide = viewController as? SlideMenuController {
            return topViewController(slide.mainViewController)
        }
        return viewController
    }
}

extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 0.5, height: 0.5)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

//Set Star Images
func setStarImage(_ rating: String) -> UIImage {
    let strRating: NSString = rating as NSString
    if strRating.isEqual(to: "1") {
        return UIImage(named: "1Star")!
    }
    else if strRating.isEqual(to: "2") {
        return UIImage(named: "2Star")!
    }
    else if strRating.isEqual(to: "3") {
        return UIImage(named: "3Star")!
    }
    else if strRating.isEqual(to: "4") {
        return UIImage(named: "4Star")!
    }
    else if strRating.isEqual(to: "5") {
        return UIImage(named: "5Star")!
    }
    else if strRating.isEqual(to: "0") {
        return UIImage(named: "0Star")!
    }
    else {
        return UIImage(named: "0Star")!
    }
}

//Get Font Family Names
func getFontFamilyName() {
    for family: String in UIFont.familyNames {
        for names: String in UIFont.fontNames(forFamilyName: family) {
        }
    }
}


//Set Colors
extension UIColor {
    var yellowBorderColor: UIColor {
        return UIColor(red: 234/255, green: 175/255, blue: 17/255, alpha: 1)
    }

    var whiteBorderColor: UIColor {
        return UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
    }

    var backgroundColor: UIColor {
        return UIColor(red: 36/255, green: 24/255, blue: 68/255, alpha: 1)
    }
    
    var lightYellowColor: UIColor {
        return UIColor(red: 245/255, green: 215/255, blue: 110/255, alpha: 1)
    }
    
    var lightAlphaYellowColor: UIColor {
        return UIColor(red: 245/255, green: 215/255, blue: 110/255, alpha: 0.2)
    }

    var menuColor: UIColor {
        return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
    }
}

//Set Font Name
extension String {
    var themeFuturaFontName : String {
        return "FuturaBT-Medium"
    }

    var themeFuturaBoldFontName : String {
        return "FuturaBT-Bold"
    }
    
    var themeFuturaMediumItalicFontName : String {
        return "FuturaBT-MediumItalic"
    }

    var themeFuturaBoldItalicFontName : String {
        return "FuturaBT-BoldItalic"
    }
    
    func replace(_ string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: .literal
            , range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(" ", replacement: "")
    }
    
    var isEmptyField: Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines) == ""
    }
    
}

//Add Alerts
extension UIAlertController {
    func alertViewWithTitleAndMessage(_ viewController: UIViewController, message: String) -> Void {
        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: message, preferredStyle: .alert)
        let hideAction: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
        alert.addAction(hideAction)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func alertViewWithErrorMessage(_ viewController: UIViewController) {
        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: ErrorMessage, preferredStyle: .alert)
        let hideAstion: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
        alert.addAction(hideAstion)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func alertViewWithNoInternet(_ viewController: UIViewController) {
        let alert: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: NoInternet, preferredStyle: .alert)
        let hideAstion: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
        alert.addAction(hideAstion)
        viewController.present(alert, animated: true, completion: nil)
    }
}

//Convert Device Token to String
extension Data {
    func hexString() -> String {
        var bytesPointer: UnsafeBufferPointer<UInt8> = UnsafeBufferPointer(start: nil, count: 0)
        self.withUnsafeBytes { (bytes) in
            bytesPointer = UnsafeBufferPointer<UInt8>(start: UnsafePointer(bytes), count:self.count)
        }
        let hexBytes = bytesPointer.map { return String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
}

extension NSMutableAttributedString {
    func bold(_ text:String) -> NSMutableAttributedString {
        let attrs:[String:AnyObject] = [NSFontAttributeName : UIFont(name: "HelveticaNeue-Bold", size: 18)!]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }

    func normal(_ text:String) -> NSMutableAttributedString {
        let normal =  NSAttributedString(string: text)
        self.append(normal)
        return self
    }
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date)) years ago"}
        if months(from: date)  > 0 { return "\(months(from: date)) month ago"}
        if weeks(from: date)   > 0 { return "\(weeks(from: date)) weeks ago"}
        if days(from: date)    > 0 { return "\(days(from: date)) days ago"}
        if hours(from: date)   > 0 { return "\(hours(from: date)) hours ago"}
        if minutes(from: date) > 0 { return "\(minutes(from: date)) minutes ago"}
        if seconds(from: date) > 0 { return "few seconds ago"}
        return ""
    }
    
    func toString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
    
    func getMonth() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        return dateFormatter.string(from: self)
    }
    
    func toStringForEvent() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        return dateFormatter.string(from: self)
    }
    
    func trialDateFormate () -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.string(from: self)
        return dateString
    }
    func toStringMonthYear() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-yyyy"
        return dateFormatter.string(from: self)
    }
    
    func toLocalTime() -> Date {
        let timeZone = NSTimeZone.local
        let seconds: TimeInterval = Double(timeZone.secondsFromGMT(for: self as Date))
        let localDate = Date(timeInterval: seconds, since: self as Date)
        return localDate
    }
    
}

extension UIImage {
    func makeFixOrientation() -> UIImage {
        if self.imageOrientation == UIImageOrientation.up {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage;
    }
    
    func imageByCroppingImage(_ size : CGSize) -> UIImage {
        let refWidth : CGFloat = CGFloat(self.cgImage!.width)
        let refHeight : CGFloat =  CGFloat(self.cgImage!.height)
        
        let x = (refWidth - size.width) / 2
        let y = (refHeight - size.height) / 2
        
        let cropRect = CGRect(x: x, y: y, width: size.width, height: size.height)
        let imageRef = self.cgImage!.cropping(to: cropRect)
        
        let cropped : UIImage = UIImage(cgImage: imageRef!, scale: 0, orientation: self.imageOrientation)
        
        return cropped
    }
}

extension UILabel {
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label: UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
}

extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
}

extension Collection {
    func map2Dict<K, V>( map: ((Self.Iterator.Element) -> (K,    V)?))  -> [K: V] {
        var d = [K: V]()
        for e in self {
            if let kV = map(e) {
                d[kV.0] = kV.1
            }
        }
        
        return d
    }
    
    func map2Array<T>( map: ((Self.Iterator.Element) -> (T)?))  -> [T] {
        var a = [T]()
        for e in self {
            if let o = map(e) {
                a.append(o)
            }
        }
        
        return a
    }
    
    func map2Set<T>( map: ((Self.Iterator.Element) -> (T)?))  -> Set<T> {
        return Set(map2Array(map: map))
    }
}
func checkIfDatabaseExists(path: String) -> Bool {
    let fileManager: FileManager = FileManager.default
    let isDBExists: Bool = fileManager.fileExists(atPath: path)
    return isDBExists
}
extension Array {
    mutating func rearrange(from: Int, to: Int) {
        precondition(from != to && indices.contains(from) && indices.contains(to), "invalid indexes")
        insert(remove(at: from), at: to)
    }
}

func dispatchDelay(delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay, execute: closure)
}

extension AVAudioPlayer {
    
    func fadeOutNew(vol:Float, sliderVolume:Float) {
        print("vol - \(vol)")
        print("avg vol - \(vol/15.0)")
        if volume > vol {
            dispatchDelay(delay: 0.008, closure: {
                [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.volume -= (sliderVolume/15.0)/100.0
                print("now vol - \(strongSelf.volume)")
                strongSelf.fadeOutNew(vol: vol, sliderVolume: sliderVolume)
            })
        } else {
            volume = vol
        }
    }

    func fadeOut(vol:Float) {
        if volume > vol {
            dispatchDelay(delay: 0.1, closure: {
                [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.volume -= 0.01
                strongSelf.fadeOut(vol: vol)
            })
        } else {
            volume = vol
        }
    }
    func fadeIn(vol:Float) {
        if volume < vol {
            dispatchDelay(delay: 0.1, closure: {
                [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.volume += 0.01
                strongSelf.fadeIn(vol: vol)
            })
        } else {
            volume = vol
        }
    }
}
public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}
