//
//  APIManager.swift
//  ELunch
//
//  Created by capermintmini6 on 13/06/17.
//  Copyright © 2017 Capermint Mini 3. All rights reserved.
//

import Foundation
import Alamofire

public class APIManager {
    
    public class var sharedInstance: APIManager {
        struct Singleton {
            static let instance: APIManager = APIManager()
        }
        return Singleton.instance
    }
    
    init() {}
    
    func AddTrackLogs(parameters : [String : String], completion:@escaping (_ dict: Any?,_ error: NSError?) -> ()){
        if(appDelegate.reachable.isReachable){
        
                Alamofire.request(baseURLWithAuth + kAddTrackLog, method: .post, parameters: parameters).validate().responseJSON { response in
                    
                    switch response.result {
                    case .success:
                        if let json: NSDictionary = response.result.value as? NSDictionary{
                            print(json)
                            if("\(json["status"]!)" == "1"){
                                completion(json,nil)
                            }else{
                                completion(nil, NSError.init(domain: "Error", code: Int.init("\(json["status"]!)")!, userInfo: nil))
                            }
                        }else {
                            completion(nil, NSError.init(domain: "Error", code: 0, userInfo: nil))
                        }
                    case .failure(let error):
                        print (error)
                    }
                }
            }
        }
    
    func AutoLogin(parameters : [String : String], completion:@escaping (_ dict: Any?,_ error: NSError?) -> ()){
        if(appDelegate.reachable.isReachable){
            Alamofire.request(baseURLWithAuth + kAutoLogin, method: .get, parameters: parameters).validate().responseJSON { responseData in
                switch responseData.result {
                case .success:
                        let json = responseData.result.value as! [String: Any]
                        if("\(json["status"]!)" == "1"){
                            completion(json,nil)
                        }else{
                            completion(nil, NSError.init(domain: "Error", code: Int.init("\(json["status"]!)")!, userInfo: nil))
                    }
                case .failure(let error):
                    completion(nil, NSError.init(domain: "Error", code: 0, userInfo: nil))
                    print (error)
                }
                
            }
        }
    }
    
    func MyActivity(parameters : [String : String], completion:@escaping (_ dict: Any?,_ error: NSError?) -> ()){
        if(appDelegate.reachable.isReachable){
            Alamofire.request(baseURLWithAuth + kMyActivity, method: .get, parameters: parameters).validate().responseJSON { responseData in
                switch responseData.result {
                case .success:
                    let json = responseData.result.value as! [String: Any]
                    if("\(json["status"]!)" == "1"){
                        completion(json,nil)
                    }else{
                        completion(nil, NSError.init(domain: "Error", code: Int.init("\(json["status"]!)")!, userInfo: nil))
                    }
                case .failure(let error):
                    completion(nil, NSError.init(domain: "Error", code: 0, userInfo: nil))
                    print (error)
                }
                
            }
        }
    }
    
    
    func productPurchas(parameters : [String : String], completion:@escaping (_ dict: Any?,_ error: NSError?) -> ()){
        if(appDelegate.reachable.isReachable){
            Alamofire.request(baseURLWithAuth + kUpdateMembership, method: .post, parameters: parameters).validate().responseJSON {
                response in
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if("\(json["status"]!)" == "1"){
                            completion(json,nil)
                            return
                        }else{
                            completion(nil, NSError.init(domain: "Error", code: Int.init("\(json["status"]!)")!, userInfo: nil))
                            return
                        }
                    }else {
                        completion(nil, NSError.init(domain: "Error", code: 0, userInfo: nil))
                        return
                    }
                case .failure(let error):
                    print (error)
                }
            }
        }
    }

    //MARK:- AutoLogin -
    func autoLoginForSave(){
        var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as? String
        if authKey == nil {
            authKey = ""
        }
        let param = ["auth_key": authKey, "device_id" : "\(UIDevice.current.identifierForVendor!)", "device_name" : "\(UIDevice().modelName)", "app_version" : "\(Bundle.main.infoDictionary!["CFBundleShortVersionString"]!)"]
        
        APIManager().AutoLogin(parameters: param as! [String : String]) { (response, error) in
            if(error == nil){
                let responseResult = response! as! [String : AnyObject]
                if(Int("\(responseResult["status"]!)")! == 1){
                    appDelegate.TotalMeditationTime = "\(responseResult["total_meditation_time"]!)"
                    numberOfDaysRemaining = Int("\(responseResult["remaining_days"]!)")!
                    let todayDate = Date().trialDateFormate()
                    appDelegate.membership_plan_id = "\(responseResult["membership_plan_id"]!)"
                    expiryDate = "\(responseResult["trial_expiry_date"]!)"
                    if(Int("\(responseResult["membership_plan_id"]!)")! == 0){
                        if(defaults.value(forKey: SubscriptionStartDate) != nil){
                            let storedDate = "\(defaults.value(forKey: SubscriptionStartDate)!)"
                            if(storedDate != todayDate){
                                defaults.set("\(todayDate)", forKey: SubscriptionStartDate)
                                NotificationCenter.default.post(name: NSNotification.Name.init("showSubscriptionView"), object: nil)
                            }
                        }else{
                            defaults.set("\(todayDate)", forKey: SubscriptionStartDate)
                            NotificationCenter.default.post(name: NSNotification.Name.init("showSubscriptionView"), object: nil)
                        }
                    }
                }
                
            }else{
                print(error!)
            }
        }
    }
    
    func backGroundTrackAPI(parameters : [String : String], completion:@escaping (_ dict: Any?,_ error: NSError?) -> ()){
        if(appDelegate.reachable.isReachable){
            Alamofire.request(baseURLWithAuth + kGetBackgroundTracks, method: .get, parameters: parameters).validate().responseJSON { responseData in
                switch responseData.result {
                case .success:
                    let json = responseData.result.value as! [String: Any]
                    if("\(json["status"]!)" == "1"){
                        completion(json,nil)
                    }else{
                        completion(nil, NSError.init(domain: "Error", code: Int.init("\(json["status"]!)")!, userInfo: nil))
                    }
                case .failure(let error):
                    completion(nil, NSError.init(domain: "Error", code: 0, userInfo: nil))
                    print (error)
                }
                
            }
        }
    }

    func cancelMembershipAPI(parameters : [String : String], completion:@escaping (_ dict: Any?,_ error: NSError?) -> ()){
        if(appDelegate.reachable.isReachable){
            Alamofire.request(baseURLWithAuth + kcancelmembership, method: .post, parameters: parameters).validate().responseJSON { responseData in
                switch responseData.result {
                case .success:
                    let json = responseData.result.value as! [String: Any]
                    if("\(json["status"]!)" == "1"){
                        completion(json,nil)
                    }else{
                        completion(nil, NSError.init(domain: "Error", code: Int.init("\(json["status"]!)")!, userInfo: nil))
                    }
                case .failure(let error):
                    completion(nil, NSError.init(domain: "Error", code: 0, userInfo: nil))
                    print (error)
                }
                
            }
        }
    }

    
    
}

