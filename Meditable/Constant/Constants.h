//
//  Constants.h
//  PicBuk
//
//  Created by Vivek Shah on 1/20/16.
//  Copyright © 2016 Vivek Shah. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define isTESTING             1

#define enableTouch(enable)   [[UIApplication sharedApplication].keyWindow setUserInteractionEnabled:enable];

#define AlertTitle            @"Meditable"
#define NET_MSG               @"No Internet Connection"
#define AppDelegate           ((MTAppDelegate *)[UIApplication sharedApplication].delegate)
#define isPad                 (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define imageName(imageName)  [UIImage imageNamed:imageName]

#define UserDefaults1           [NSUserDefaults standardUserDefaults]

//#define NotificationCenter    [NSNotificationCenter defaultCenter]

#define enableTouch(enable)   [[UIApplication sharedApplication].keyWindow setUserInteractionEnabled:enable];

#define RGBCOLOR(r,g,b)             [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define RGBCOLORWITHALPHA(r,g,b,a)  [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]
#define MenuTextColor               [UIColor colorWithRed:244/255.0 green:141/255.0 blue:25/255.0 alpha:1]
#define Color(colorName)            [UIColor colorName]
#define	HEXCOLOR(c) [UIColor colorWithRed:((c>>24)&0xFF)/255.0 green:((c>>16)&0xFF)/255.0 blue:((c>>8)&0xFF)/255.0 alpha:((c)&0xFF)/255.0]

#define kFontRoboto_Regular  @"TitilliumWeb-Regular"
#define kFontRoboto_Medium   @"Moon-Bold"
#define kFontRoboto_Bold     @"TitilliumWeb-Bold"

#define kAppId  @"932890590114182"
#define kAppFID @"fb932890590114182"

#define INSTAGRAM_APP_ID     @"1845e81cc8364eaabed3390c5cb26381"
#define InstaClientSECRET    @"ecebd17b271c457d9c4f3a3ccd9a7272"
#define InstaRedirectUrl     @"ig1845e81cc8364eaabed3390c5cb26381://authorize"

//PayPal Details
#define kPayPalEnvironment PayPalEnvironmentNoNetwork
#define kPayPalEnvironmentSandbox    PayPalEnvironmentSandbox
#define kPayPalEnvironmentProduction PayPalEnvironmentProduction

#define kStroryBoardIphone    @"Main"

#define APP_NAME              @"Primer City App"
#define APP_VERSION           @"1.0"

#define  kDeviceName          [[UIDevice currentDevice] name];
#define  kLocalizedModel      [[UIDevice currentDevice] localizedModel];
#define  kSystemVersion       [[UIDevice currentDevice] systemVersion];
#define  kSystemName          [[UIDevice currentDevice] systemName];
#define  kModel               [[UIDevice currentDevice] model];

#define kErrorHeader          @"Error"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define IsBlank(str) (([str isEqualToString:@""] || str==nil || [str isEqualToString:@"(null)"] || [str isEqual:[NSNull null]]) ? YES : NO)

//Messages...
#define kMsgAlertLoading      @"Please Wait..."
#define kMsgErrorMessage      @"Error: Please try later"

#define setAnimationForImage(img,duration,alpha) {[UIView animateWithDuration:duration animations:^{[img setAlpha:alpha];}];}

#define goBack {[self.navigationController popViewControllerAnimated:YES];}
#define pushViewController(controller){[self.navigationController pushViewController:controller animated:YES];}
#define setImage(image_Obj,imageName){[image_Obj setImage:[UIImage imageNamed:imageName]];}
#define setLabelFont(lbl,fontName,fontSize){[lbl setFont:[UIFont fontWithName:fontName size:fontSize]];}

#define setButtonFont(button,fontName,fontSize){[button.titleLabel setFont:[UIFont fontWithName:fontName size:fontSize]];}

#define setTextField_Font_Color(textField,placeholderText,color,fontName,fontSize){textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholderText attributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName : [UIFont fontWithName:fontName size:fontSize]}];}

#define setTextField_Font(textField,fontName,fontSize){[textField setFont:[UIFont fontWithName:fontName size:fontSize]];}

#define AlertViewNetwork {UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"No Internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];[alertView show];}

#define DATE_COMPONENTS  (NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekdayOrdinal | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekOfMonth)

//TODO: Live Services//////////

//
#define WS_BASE_URL                 @"http://www.locum.us/locumus/api/web/v1/api/" 
//@"http://149.56.108.65/locumus/api/web/v1/api/"
//@"http://briskbrain.com/projects/locumus/api/web/v1/api/"
#define WS_GETLetLong_URL           @"http://maps.googleapis.com/maps/api/geocode/json?"
#define WS_GraphData_URL            @"http://alpha-trends.com/corrtrend/mobilecsv.php"
#define WS_Correlation_URL          @"http://alpha-trends.com/corrtrend/correlation.php"
#define SecretKey                   @"DeleGraph2015"
#define PrivateKey                  @"$1y$12$mJBRcVyuUoAQ5vXKFvMRde26mD3LdhRVSKupddNEvNEJHkdGOA5SP"
#define WEB_URL(v)                  [NSString stringWithFormat:@"%@%@", WS_BASE_URL, v]
#define ABCGoogleApiKey             @"AIzaSyCfwGrtPmrGnhafCZBj_RiVFs6_cjUysUE";
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define ShareApp_URL                @"https://itunes.apple.com/us/app/locumus/id1136147374?ls=1&mt=8"
#define ShareApp_URL_Google         @"https://play.google.com/store/apps/details?id=com.locumusapp"

#define RateApp_URL                 @"https://itunes.apple.com/us/app/locumus/id1136147374?ls=1&mt=8"
#define Website_URL                 @"http://locum.us"
#define FAQs_URL                    @"http://locum.us/faq.html"
#define PrivacyPolicy_URL           @"http://locum.us/privacy-policy.html"
#define TermsConditions_URL         @"http://locum.us/terms.html"

#define strtoint(str) [str intValue]
#define strtofloat(str) [str floatValue]
#define inttostr(intval) [NSString stringWithFormat:@"%d", intval]
#define floattostr(floatval) [NSString stringWithFormat:@"%0.2f", floatval]
#define floattostrformat(floatval, format) [NSString stringWithFormat:format, floatval]

#define trim(str) [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]
#define isempty(str) trim(str).length == 0
#define isinvalidamount(str) trim(str).intValue <= 0

// UI Helpers:

#define initview(viewclass, var) \
NSString *nibname = NSStringFromClass([viewclass class]); \
viewclass *var = [[viewclass alloc] initWithNibName:nibname bundle:nil]

#define IS_IPAD      (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE_4s ([[UIScreen mainScreen] bounds].size.height == 480.0f)
#define IS_IPHONE_5  ([[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_6  ([[UIScreen mainScreen] bounds].size.height == 667.0f)
#define IS_IPHONE_6P ([[UIScreen mainScreen] bounds].size.height == 736.0f)
#define IS_ScreenWith ([[UIScreen mainScreen] bounds].size.width)

#define    IsPortrait               UIInterfaceOrientationIsPortrait([UIDevice currentDevice].orientation)
#define    IsLandscape              UIInterfaceOrientationIsLandscape([UIDevice currentDevice].orientation)

#define    WINDOW_WIDTH             [UIScreen mainScreen].applicationFrame.size.width    ///< i.e. for iPhone it is 320 for portrait and 300 for landscape orientation
#define    WINDOW_HEIGHT            [UIScreen mainScreen].applicationFrame.size.height   ///< i.e. for iPhone it is 460 for portrait and 480 for landscape orientation
#define    WINDOW_WIDTH_ORIENTED    (IsPortrait ? WINDOW_WIDTH : WINDOW_HEIGHT)          ///< i.e. 320 for portrait and 480 for landscape orientation
#define    WINDOW_HEIGHT_ORIENTED   (IsPortrait ? WINDOW_HEIGHT : WINDOW_WIDTH)          ///< i.e. 460 for portrait and 320 for landscape orientation


#define    SCREEN_WIDTH             [UIScreen mainScreen].bounds.size.width
#define    SCREEN_HEIGHT            [UIScreen mainScreen].bounds.size.height
#define    SCREEN_WIDTH_ORIENTED    (IsPortrait ? SCREEN_WIDTH : SCREEN_HEIGHT)
#define    SCREEN_HEIGHT_ORIENTED   (IsPortrait ? SCREEN_HEIGHT : SCREEN_WIDTH)
#define    NAVBAR_HEIGHT            44.
#define    TOOLBAR_HEIGHT           44.
#define    TABBAR_HEIGHT            49.

#define    BtnVideoCenter_i5_x      160
#define    BtnVideoCenter_i5_y      506
#define    BtnVideoCenter_i4_x      160
#define    BtnVideoCenter_i4_y      431


#define     STATUSBAR_FRAME         [[UIApplication sharedApplication] statusBarFrame]


#define    KEYBOARD_SIZE_PORTRAIT   (IsIPhone ? CGSizeMake(320, 216) : CGSizeMake(768, 264))
#define    KEYBOARD_SIZE_LANDSCAPE  (IsIPhone ? CGSizeMake(480, 162) : CGSizeMake(1024, 352))
#define    KEYBOARD_SIZE            (IsPortrait ? KEYBOARD_SIZE_PORTRAIT : KEYBOARD_SIZE_LANDSCAPE)

#define    AUTORESIZE_CENTER        UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin
#define    AUTORESIZE_STRETCH       UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight
////// NSLOG
#define   SHOW_LOGS             YES
#define   SHOW_TEXTURES_LOGS    NO
#define   Log(format, ...)      if (SHOW_LOGS) NSLog(@"%s: %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:format, ## __VA_ARGS__]);
#define   TexLog(format, ...)   if (SHOW_LOGS && SHOW_TEXTURES_LOGS) NSLog(@"%s: %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:format, ## __VA_ARGS__]);
#define   Error(format, ...)    if (SHOW_LOGS) NSLog(@"ERROR: %@", [NSString stringWithFormat:format, ## __VA_ARGS__]);
#define   Mark                  if (SHOW_LOGS) NSLog(@"Start %s", __PRETTY_FUNCTION__);
#define   MarkEnd                  if (SHOW_LOGS) NSLog(@"End %s", __PRETTY_FUNCTION__);

// user defaults
#define XDEL_OBJECT(k) [[NSUserDefaults standardUserDefaults] removeObjectForKey:k];

//Object
#define XGET_OBJECT(v) [[NSUserDefaults standardUserDefaults] objectForKey:v]
#define XSET_OBJECT(k,v) [[NSUserDefaults standardUserDefaults] setObject:v forKey:k];

//Value
#define XGET_VALUE(v) [[NSUserDefaults standardUserDefaults] valueForKey:v]
#define XSET_VALUE(k,v) [[NSUserDefaults standardUserDefaults] setValue:v forKey:k];

//String
#define XGET_STRING(v) [[NSUserDefaults standardUserDefaults] stringForKey:v]
#define XSET_STRING(k,v) [[NSUserDefaults standardUserDefaults] setObject:v forKey:k];

//Float
#define XGET_FLOAT(v) [[NSUserDefaults standardUserDefaults] floatForKey:v]
#define XSET_FLOAT(k,v) [[NSUserDefaults standardUserDefaults] setFloat:v forKey:k];

//Bool
#define XGET_BOOL(v) [[NSUserDefaults standardUserDefaults] boolForKey:v]
#define XSET_BOOL(k,v) [[NSUserDefaults standardUserDefaults] setBool:v forKey:k];

//Integer
#define XGET_INT(v) [[NSUserDefaults standardUserDefaults] integerForKey:v]
#define XSET_INT(k,v) [[NSUserDefaults standardUserDefaults] setInteger:v forKey:k];
#define XSYNC [[NSUserDefaults standardUserDefaults] synchronize];

#define XREMOVE(k) [[NSUserDefaults standardUserDefaults] removeObjectForKey:k];
#define  SystemVersion [[[UIDevice currentDevice] systemVersion] floatValue]
//#define dataPath = [DocumentsPath stringByAppendingPathComponent:Media_Folder];

// notification center
#define XNC [NSNotificationCenter defaultCenter]
#define XNCADD(n,sel) [[NSNotificationCenter defaultCenter] addObserver:self selector:sel name:n object:nil];
#define XNCREMOVE [[NSNotificationCenter defaultCenter] removeObserver:self];
#define XNCPOST(name) [[NSNotificationCenter defaultCenter] postNotificationName:name object:self];
#define XNSPOSTWITHPARM(name,dic) [[NSNotificationCenter defaultCenter] postNotificationName:name object:self userInfo:dic];
// collection shortcuts
#define XDEFAULT(_value, _default) ([[NSNull null] isEqual:(_value)] ? (_default) : (_value))
#define XFMT(...) [NSString stringWithFormat: __VA_ARGS__]
#define XARRAY(...) [NSArray arrayWithObjects: __VA_ARGS__, nil]
#define XDICT(...) [NSDictionary dictionaryWithObjectsAndKeys: __VA_ARGS__, nil]
#define XMARRAY(...) [NSMutableArray arrayWithObjects: __VA_ARGS__, nil]
#define XMDICT(...) [NSMutableDictionary dictionaryWithObjectsAndKeys: __VA_ARGS__, nil]

#define XMCOPY(_obj) [_obj mutableCopy]
#define xcopy(_obj) [_obj copy]

//UIAlertView methods
//alert with Delegate
#define signUpAlert(msg) { UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]; alertView.tag=100; [alertView show]; }
//alert with only message
#define DisplayAlert(msg) { UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]; [alertView show]; }
//alert with message and title
#define DisplayAlertWithTitle(msg,title) {UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]; [alertView show];}

//alert with only localized message
#define DisplayLocalizedAlert(msg){UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(msg,@"") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]; [alertView show]; [alertView autorelease];}
//alert with localized message and title
#define DisplayLocalizedAlertWithTitle(msg,title){UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(title,@"") message:NSLocalizedString(msg,@"") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]; [alertView show]; [alertView autorelease];}

#define ALERT_VIEW(title,msg)\
{\
UIAlertView *av = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];\
[av show];\
}
// Paths:

#define   BundlePath                    [[NSBundle mainBundle] resourcePath]
#define   PathToResource(resourceName)  [BundlePath stringByAppendingPathComponent:resourceName]

#define   DocumentsPath                 [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define Media_Folder                    @"media_folder"

#define   documentDirectory             [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectoryinDomains:NSUserDomainMask] lastObject]

#define   LibraryPath                   [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define   SharedDataPath                DocumentsPath

#define   CachedDataPath                [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define   TemporaryDataPath             [NSHomeDirectory() stringByAppendingPathComponent:@"tmp"]


//-------------------------------------------------------------------------------------------------------------------------------------------------
#define		FIREBASE_STORAGE					@"gs://mytravella-bed2c.appspot.com"
//-------------------------------------------------------------------------------------------------------------------------------------------------
#define		ONESIGNAL_APPID						@"6b8b03e5-2f31-40de-8bd3-3bba874648bf"
//-------------------------------------------------------------------------------------------------------------------------------------------------
#define		SINCH_HOST							@"sandbox.sinch.com"
#define		SINCH_KEY							@"b515eb8b-dcaf-473d-982a-81c5a97a3a1e"
#define		SINCH_SECRET						@"mgnwHKZLIkahFoj90UsbCg=="
//-------------------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------------------
#define		VERSION_PREMIUM
//---------------------------------------------------------------------------------
#define		DEFAULT_TAB							0
#define		VIDEO_LENGTH						5
#define		AUDIO_LENGTH						5
#define		INSERT_MESSAGES						10
#define		DOWNLOAD_TIMEOUT					300
//---------------------------------------------------------------------------------
#define		STATUS_LOADING						1
#define		STATUS_SUCCEED						2
#define		STATUS_MANUAL						3
//---------------------------------------------------------------------------------
#define		MEDIA_IMAGE							1
#define		MEDIA_VIDEO							2
#define		MEDIA_AUDIO							3
//---------------------------------------------------------------------------------
#define		NETWORK_MANUAL						1
#define		NETWORK_WIFI						2
#define		NETWORK_ALL							3
//---------------------------------------------------------------------------------
#define		KEEPMEDIA_WEEK						1
#define		KEEPMEDIA_MONTH						2
#define		KEEPMEDIA_FOREVER					3
//---------------------------------------------------------------------------------
#define		DEL_ACCOUNT_NONE					1
#define		DEL_ACCOUNT_ONE						2
#define		DEL_ACCOUNT_ALL						3
//---------------------------------------------------------------------------------
#define		CHAT_PRIVATE						@"private"
#define		CHAT_MULTIPLE						@"multiple"
#define		CHAT_GROUP							@"group"
//---------------------------------------------------------------------------------
#define		CALLHISTORY_AUDIO					@"audio"
#define		CALLHISTORY_VIDEO					@"video"
//---------------------------------------------------------------------------------
#define		MESSAGE_TEXT						@"text"
#define		MESSAGE_EMOJI						@"emoji"
#define		MESSAGE_PICTURE						@"picture"
#define		MESSAGE_VIDEO						@"video"
#define		MESSAGE_AUDIO						@"audio"
#define		MESSAGE_LOCATION					@"location"
//---------------------------------------------------------------------------------
#define		LOGIN_EMAIL							@"Email"
#define		LOGIN_FACEBOOK						@"Facebook"
#define		LOGIN_GOOGLE						@"Google"
//---------------------------------------------------------------------------------
#define		COLOR_OUTGOING						HEXCOLOR(0x007AFFFF)
#define		COLOR_INCOMING						HEXCOLOR(0xE6E5EAFF)
//---------------------------------------------------------------------------------
#define		TEXT_QUEUED							@"Queued"
#define		TEXT_SENT							@"Sent"
#define		TEXT_READ							@"Read"
//---------------------------------------------------------------------------------
#define		PHOTOS_ALBUM_TITLE					@"Chat"
//---------------------------------------------------------------------------------
#define		LINK_PREMIUM						@"http://www.relatedcode.com/premium"
//---------------------------------------------------------------------------------
#define		SCREEN_WIDTH						[UIScreen mainScreen].bounds.size.width
#define		SCREEN_HEIGHT						[UIScreen mainScreen].bounds.size.height
//---------------------------------------------------------------------------------
#define		TEXT_INVITE_SMS						@"Check out PremiumChat for your smartphone. Download it today."
//-------------------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------------------
#define		FUSER_PATH							@"User"					//	Path name
#define		FUSER_OBJECTID						@"objectId"				//	String

#define		FUSER_EMAIL							@"email"				//	String
#define		FUSER_PHONE							@"phone"				//	String

#define		FUSER_FIRSTNAME						@"firstname"			//	String
#define		FUSER_LASTNAME						@"lastname"				//	String
#define		FUSER_FULLNAME						@"fullname"				//	String
#define		FUSER_COUNTRY						@"country"				//	String
#define		FUSER_LOCATION						@"location"				//	String
#define		FUSER_STATUS						@"status"				//	String

#define		FUSER_PICTURE						@"picture"				//	String
#define		FUSER_THUMBNAIL						@"thumbnail"			//	String

#define		FUSER_KEEPMEDIA						@"keepMedia"			//	Number
#define		FUSER_NETWORKIMAGE					@"networkImage"			//	Number
#define		FUSER_NETWORKVIDEO					@"networkVideo"			//	Number
#define		FUSER_NETWORKAUDIO					@"networkAudio"			//	Number
#define		FUSER_AUTOSAVEMEDIA					@"autoSaveMedia"		//	Boolean
#define		FUSER_WALLPAPER						@"wallpaper"			//	String

#define		FUSER_LOGINMETHOD					@"loginMethod"			//	String
#define		FUSER_ONESIGNALID					@"oneSignalId"			//	String
#define     FUSER_QBID                          @"qbUserId"             //  String

#define		FUSER_LASTACTIVE					@"lastActive"			//	Timestamp
#define		FUSER_LASTTERMINATE					@"lastTerminate"		//	Timestamp

#define		FUSER_CREATEDAT						@"createdAt"			//	Timestamp
#define		FUSER_UPDATEDAT						@"updatedAt"			//	Timestamp
//---------------------------------------------------------------------------------
#define		FCALLHISTORY_PATH					@"CallHistory"			//	Path name
#define		FCALLHISTORY_OBJECTID				@"objectId"				//	String

#define		FCALLHISTORY_INITIATORID			@"initiatorId"			//	String
#define		FCALLHISTORY_RECIPIENTID			@"recipientId"			//	String
#define		FCALLHISTORY_PHONENUMBER			@"phoneNumber"			//	String

#define		FCALLHISTORY_TYPE					@"type"					//	String
#define		FCALLHISTORY_TEXT					@"text"					//	String

#define		FCALLHISTORY_STATUS					@"status"				//	String
#define		FCALLHISTORY_DURATION				@"duration"				//	Number

#define		FCALLHISTORY_STARTEDAT				@"startedAt"			//	Timestamp
#define		FCALLHISTORY_ESTABLISHEDAT			@"establishedAt"		//	Timestamp
#define		FCALLHISTORY_ENDEDAT				@"endedAt"				//	Timestamp

#define		FCALLHISTORY_ISDELETED				@"isDeleted"			//	Boolean

#define		FCALLHISTORY_CREATEDAT				@"createdAt"			//	Timestamp
#define		FCALLHISTORY_UPDATEDAT				@"updatedAt"			//	Timestamp
//---------------------------------------------------------------------------------
#define		FGROUP_PATH							@"Group"				//	Path name
#define		FGROUP_OBJECTID						@"objectId"				//	String

#define		FGROUP_USERID						@"userId"				//	String
#define		FGROUP_NAME							@"name"					//	String
#define		FGROUP_PICTURE						@"picture"				//	String
#define		FGROUP_MEMBERS						@"members"				//	Array

#define		FGROUP_ISDELETED					@"isDeleted"			//	Boolean

#define		FGROUP_CREATEDAT					@"createdAt"			//	Timestamp
#define		FGROUP_UPDATEDAT					@"updatedAt"			//	Timestamp
//---------------------------------------------------------------------------------
#define		FMESSAGE_PATH						@"Message"				//	Path name
#define		FMESSAGE_OBJECTID					@"objectId"				//	String

#define		FMESSAGE_GROUPID					@"groupId"				//	String
#define		FMESSAGE_SENDERID					@"senderId"				//	String
#define		FMESSAGE_SENDERNAME					@"senderName"			//	String
#define		FMESSAGE_SENDERINITIALS				@"senderInitials"		//	String

#define		FMESSAGE_TYPE						@"type"					//	String
#define		FMESSAGE_TEXT						@"text"					//	String

#define		FMESSAGE_PICTURE					@"picture"				//	String
#define		FMESSAGE_PICTURE_WIDTH				@"picture_width"		//	Number
#define		FMESSAGE_PICTURE_HEIGHT				@"picture_height"		//	Number
#define		FMESSAGE_PICTURE_MD5				@"picture_md5"			//	String

#define		FMESSAGE_VIDEO						@"video"				//	String
#define		FMESSAGE_VIDEO_DURATION				@"video_duration"		//	Number
#define		FMESSAGE_VIDEO_MD5					@"video_md5"			//	String

#define		FMESSAGE_AUDIO						@"audio"				//	String
#define		FMESSAGE_AUDIO_DURATION				@"audio_duration"		//	Number
#define		FMESSAGE_AUDIO_MD5					@"audio_md5"			//	String

#define		FMESSAGE_LATITUDE					@"latitude"				//	Number
#define		FMESSAGE_LONGITUDE					@"longitude"			//	Number

#define		FMESSAGE_STATUS						@"status"				//	String
#define		FMESSAGE_ISDELETED					@"isDeleted"			//	Boolean

#define		FMESSAGE_CREATEDAT					@"createdAt"			//	Timestamp
#define		FMESSAGE_UPDATEDAT					@"updatedAt"			//	Timestamp
//---------------------------------------------------------------------------------
#define		FRECENT_PATH						@"Recent"				//	Path name
#define		FRECENT_OBJECTID					@"objectId"				//	String

#define		FRECENT_USERID						@"userId"				//	String
#define		FRECENT_GROUPID						@"groupId"				//	String

#define		FRECENT_INITIALS					@"initials"				//	String
#define		FRECENT_PICTURE						@"picture"				//	String
#define		FRECENT_DESCRIPTION					@"description"			//	String
#define		FRECENT_MEMBERS						@"members"				//	Array
#define		FRECENT_TYPE						@"type"					//	String
#define     FRECENT_QBIDS                       @"qbUserIds"            //  Array

#define		FRECENT_COUNTER						@"counter"				//	Number
#define		FRECENT_LASTMESSAGE					@"lastMessage"			//	String
#define		FRECENT_LASTMESSAGEDATE				@"lastMessageDate"		//	Timestamp

#define		FRECENT_ISARCHIVED					@"isArchived"			//	Boolean
#define		FRECENT_ISDELETED					@"isDeleted"			//	Boolean

#define		FRECENT_CREATEDAT					@"createdAt"			//	Timestamp
#define		FRECENT_UPDATEDAT					@"updatedAt"			//	Timestamp
//---------------------------------------------------------------------------------
#define		FTYPING_PATH						@"Typing"				//	Path name
//---------------------------------------------------------------------------------
#define		FUSERSTATUS_PATH					@"UserStatus"			//	Path name
#define		FUSERSTATUS_OBJECTID				@"objectId"				//	String

#define		FUSERSTATUS_NAME					@"name"					//	String

#define		FUSERSTATUS_CREATEDAT				@"createdAt"			//	Timestamp
#define		FUSERSTATUS_UPDATEDAT				@"updatedAt"			//	Timestamp
//-------------------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------------------
#define		ONESIGNALID							@"OneSignalId"
#define     QBID                                @"QBId"
#define		USER_ACCOUNTS						@"UserAccounts"
#define		REACHABILITY_CHANGED				@"ReachabilityChanged"
//---------------------------------------------------------------------------------
#define		NOTIFICATION_APP_STARTED			@"NotificationAppStarted"
#define		NOTIFICATION_USER_LOGGED_IN			@"NotificationUserLoggedIn"
#define		NOTIFICATION_USER_LOGGED_OUT		@"NotificationUserLoggedOut"
//---------------------------------------------------------------------------------
#define		NOTIFICATION_REFRESH_CALLHISTORIES	@"NotificationRefreshCallHistories"
#define		NOTIFICATION_REFRESH_CONTACTS		@"NotificationRefreshContacts"
#define		NOTIFICATION_REFRESH_GROUPS			@"NotificationRefreshGroups"
#define		NOTIFICATION_REFRESH_MESSAGES1		@"NotificationRefreshMessages1"
#define		NOTIFICATION_REFRESH_MESSAGES2		@"NotificationRefreshMessages2"
#define		NOTIFICATION_REFRESH_RECENTS		@"NotificationRefreshRecents"
#define		NOTIFICATION_REFRESH_USERS			@"NotificationRefreshUsers"
//---------------------------------------------------------------------------------
#define		NOTIFICATION_CLEANUP_CHATVIEW		@"NotificationCleanupChatView"
//-------------------------------------------------------------------------------------------------------------------------------------------------

#define Core [QBCore instance]


#endif /* Constants_h */
