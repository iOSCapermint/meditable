//
//  CustomMeditationPlaylistViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 07/06/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON
import TPKeyboardAvoiding
import AVFoundation
import AudioToolbox

class CustomMeditationPlaylistViewController: UIViewController, NVActivityIndicatorViewable, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate, AVAudioPlayerDelegate {
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblMeditations: UILabel!
    
    @IBOutlet weak var lblCreateMeditations: UILabel!
    
    @IBOutlet weak var txtMeditationsName: UITextField!
    
    @IBOutlet weak var vwMeditationsName: UIView!
    
    @IBOutlet weak var tblTracks: UITableView!
    
    @IBOutlet weak var btnPlayMeditation: UIButton!
    
    @IBOutlet weak var tblTracksHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var saveTrackNameView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var trackDetailsView: UIView!
    
    @IBOutlet weak var lblSaveCustomMeditation: UILabel!
    
    @IBOutlet weak var txtTrackName: CustomTextfield!
    
    @IBOutlet weak var btnSaveTrackName: UIButton!
    
    @IBOutlet weak var btnClose1: UIButton!
    
    @IBOutlet weak var btnClose1HeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var trackOptionsView: UIView!
    
    @IBOutlet weak var tblTrackOptions: UITableView!
    
    @IBOutlet weak var btnClose2: UIButton!
    
    @IBOutlet weak var introTrackOptionsView: UIView!
    
    @IBOutlet weak var tblIntroTrackOptions: UITableView!
    
    @IBOutlet weak var btnClose3: UIButton!
    
    @IBOutlet weak var endTracksOptionsView: UIView!
    
    @IBOutlet weak var tblEndTrackOptions: UITableView!
    
    @IBOutlet weak var btnClose4: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!

    @IBOutlet weak var lblTotalTrackTime: UILabel!
    
    @IBOutlet weak var addtoAnotherMeditationView: UIView!
    @IBOutlet weak var innerAddtoAnotherMeditationView: UIView!
    @IBOutlet weak var btnCloseAddToAnotherView: UIButton!
    @IBOutlet weak var btnAddToNewMeditation: UIButton!
    @IBOutlet weak var btnAddToExistingMeditation: UIButton!

    
    var trackOptionsList = ["Add Silence 30 Sec", "Move Up", "Move Down", "Add to Another Meditation", "Delete"]
    var introOptionsList = ["Change Intro", "Add Silence 30 Sec"]
    var endOptionsList = ["Change Ending"]
    var responseArray = [[String: AnyObject]]()
    var editIndex = Int()
    var urlString = ""
    var trackList = [[String : AnyObject]]()
    var index : Int = 0
    var backgroundList = [String : AnyObject]()
    var silenceList = [String : AnyObject]()
    var player = AVPlayer()
    var btnindex = Int()
    var meditationType = ""
    var isOriginal = ""
    var isEdited = "N"
    var fromSavedMeditation : Bool = false
    var meditationID = ""
    var meditationDescription = ""
    var totalTime = ""
    var backgroundSeconds : Int = 0
    var totalSeconds : Int = 0
    var currentPlayIndex = 500
    var fromActivityScreen = false
    var isDownloaded = false
    var isFromMeditationListViewController = false
    var downloadedOriginal = false
    
//    lazy var session : URLSession = {
//        let config = URLSessionConfiguration.ephemeral
//        config.allowsCellularAccess = true
//        let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
//        return session
//    }()
    var task : URLSessionTask!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(isDownloaded == true){
            lblMeditations.text = "Downloaded Meditation"
            if(isOriginal == "Y"){
                downloadedOriginal = true
            }
        }else if(isOriginal == "Y"){
            lblMeditations.text = "\(MadeByMeditable)"
        }else{
            lblMeditations.text = "\(MadeByYou)"
        }
        

        
        addGoogleAnalytics(screenName: "Custom Meditation Play Screen")

//        if(isDownloaded == true){
//            lblMeditations.text = "Saved Meditation"
//        }else{
//            lblMeditations.text = "Custom Meditation"
//        }
        
        if self.responseArray[0]["is_original"] != nil {
            if("\(self.responseArray[0]["is_original"]!)" == "Y"){
                isOriginal = "Y"
            }else{
                isOriginal = "N"
            }
        }
        lblCreateMeditations.text = "Title of meditation"
        btnSave.isHidden = true

        
        NotificationCenter.default.addObserver(self, selector: #selector(CustomMeditationPlaylistViewController.addNewTrackFromList(notification:)), name: Notification.Name("SelectedTrackToAddInCustomMeditaionList"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(CustomMeditationPlaylistViewController.musicChange(sender: )), name: NSNotification.Name.init("changeMusicCustomMeditaion"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(CustomMeditationPlaylistViewController.refreshList(notification:)), name: Notification.Name("refreshCustomMeditationPlayList"), object: nil)

        
        //For background and Silence Audio
        trackList = self.responseArray[0]["track"] as! [[String : AnyObject]]
        txtMeditationsName.text = "\(self.responseArray[0]["title"]!)"
        
        for i in 0...trackList.count - 1 {
            if("\(trackList[i]["track_type"]!)" == "B"){
                backgroundList = trackList[i]
                trackList.remove(at: i)
                break
            }
        }
        
        for i in 0...trackList.count - 1 {
            if("\(trackList[i]["track_type"]!)" == "S"){
                silenceList = trackList[i]
                if(isFromMeditationListViewController == true){
                   trackList.remove(at: i)
                }
                break
            }
        }
        if(silenceList.count == 0){
            self.getSilenceTracks()
        }

        self.vwMeditationsName.layer.cornerRadius = 5.0
        self.vwMeditationsName.layer.borderColor = UIColor.white.cgColor
        self.vwMeditationsName.layer.borderWidth = 1
        
        btnSave.layer.cornerRadius = btnSave.frame.size.height / 2
        btnPlayMeditation.layer.cornerRadius = btnPlayMeditation.frame.size.height / 2
        btnPlayMeditation.layer.borderColor = UIColor().yellowBorderColor.cgColor
        
        btnAddToNewMeditation.layer.cornerRadius = btnPlayMeditation.frame.size.height / 2
        btnAddToExistingMeditation.layer.cornerRadius = btnPlayMeditation.frame.size.height / 2
        innerAddtoAnotherMeditationView.layer.cornerRadius = 5.0

        btnPlayMeditation.layer.borderWidth = 1
        
        txtTrackName.layer.borderColor = UIColor.black.cgColor
        txtTrackName.layer.borderWidth = 1.0
        txtTrackName.layer.cornerRadius = 5.0
        txtTrackName.layer.masksToBounds = true
        
        btnSaveTrackName.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnSaveTrackName.layer.borderWidth = 1.0
        btnSaveTrackName.layer.cornerRadius = btnSaveTrackName.frame.size.height / 2
        btnSaveTrackName.layer.masksToBounds = true
        
        trackDetailsView.layer.cornerRadius = 5.0
        saveTrackNameView.layer.masksToBounds = true
        
        tblTrackOptions.layer.cornerRadius = 5.0
        tblTrackOptions.layer.masksToBounds = true
        
        tblIntroTrackOptions.layer.cornerRadius = 5.0
        tblIntroTrackOptions.layer.masksToBounds = true
        
        tblEndTrackOptions.layer.cornerRadius = 5.0
        tblEndTrackOptions.layer.masksToBounds = true
        
        tblTracks.rowHeight = UITableViewAutomaticDimension
        tblTracks.estimatedRowHeight = 90
        
        if IS_IPHONE4s || IS_IPHONE5 {
            btnClose1HeightConstraint.constant = 94
        }
        else if IS_IPHONE6 {
            btnClose1HeightConstraint.constant = 144
        }
        else {
            btnClose1HeightConstraint.constant = 178
        }
        tblTracksHeightConstraint.constant = CGFloat((self.trackList.count + 1) * 80)
        
        lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: trackList).time)"

        NotificationCenter.default.addObserver(self, selector: #selector(CustomMeditationPlaylistViewController.itemDidFinishPlaying(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        addGoogleAnalytics(screenName: "Custom Meditation Play")
        self.btnindex = -1
        tblTracks.reloadData()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super .viewDidDisappear(true)
        player.pause()
        player.rate = 0
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        self.txtMeditationsName.resignFirstResponder()
        if(btnSave.isHidden == false){
            let alertController = UIAlertController(title: AppName, message: LikeToSaveEditedMeditation, preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
            { action -> Void in
                self.txtMeditationsName.resignFirstResponder()
                self.backgroundView.isHidden = false
                self.saveTrackNameView.isHidden = false
                var originalCustome = "N"
                if self.responseArray[0]["is_original"] != nil {
                    if("\(self.responseArray[0]["is_original"]!)" == "Y"){
                        originalCustome = "Y"
                    }else{
                        originalCustome = "N"
                    }
                }
                
                if(originalCustome == "Y" && self.isOriginal == "N"){
                    self.txtTrackName.text = "My \(self.txtMeditationsName.text!)"
                }else{
                    self.txtTrackName.text = "\(self.txtMeditationsName.text!)"
                }

//                self.saveCustomMeditatioAPI(title: "\(self.txtMeditationsName.text!)")
            })
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
            { action -> Void in
                self.popToController()
            })
            self.present(alertController, animated: true, completion: nil)
        }else{
            self.popToController()
        }
        //_ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPlayMeditation_Clicked(_ sender: Any) {

        if(isDownloaded == true && isEdited == "N"){
            let playMeditationViewController = self.storyboard?.instantiateViewController(withIdentifier: "PlayOfflineViewController") as! PlayOfflineViewController
            playMeditationViewController.trackList = trackList
            playMeditationViewController.backgroundSound = backgroundList
            playMeditationViewController.silenceSound = silenceList
            playMeditationViewController.totalTrackTime = "\(getTotalTrackTime(track: trackList).time)"
            playMeditationViewController.backgroundSeconds = backgroundSeconds
            playMeditationViewController.totalSeconds = getTotalTrackTime(track: trackList).totalSeconds
            playMeditationViewController.meditationTitle = "\(self.responseArray[0]["title"]!)"
            playMeditationViewController.meditationID = meditationID
            playMeditationViewController.meditationDescription = meditationDescription
            if(isOriginal == "Y"){
                playMeditationViewController.meditaionBy = "\(MadeByMeditable)"
            }else{
                playMeditationViewController.meditaionBy = "\(MadeByYou)"
            }
            if(isFromMeditationListViewController == true){
                playMeditationViewController.fromActivity = true
            }
            self.navigationController?.pushViewController(playMeditationViewController, animated: true)

        }else{
            let playMeditationViewController = self.storyboard?.instantiateViewController(withIdentifier: "PlayMeditationViewController") as! PlayMeditationViewController
            playMeditationViewController.trackList = trackList
            playMeditationViewController.backgroundSound = backgroundList
            playMeditationViewController.silenceSound = silenceList
            playMeditationViewController.totalTrackTime = "\(getTotalTrackTime(track: trackList).time)"
            //"Total Meditation Time: \(self.responseArray[0]["total_meditation_time"]!)"
            playMeditationViewController.meditationTitle = "\(self.responseArray[0]["title"]!)"
            playMeditationViewController.fromSavedMeditation = fromSavedMeditation
            playMeditationViewController.meditationID = meditationID
            if(isOriginal == "Y"){
                playMeditationViewController.meditaionBy = "\(MadeByMeditable)"
            }else{
                playMeditationViewController.meditaionBy = "\(MadeByYou)"
            }
            if(isFromMeditationListViewController == true){
                playMeditationViewController.fromActivity = true
            }
            playMeditationViewController.backgroundSeconds = backgroundSeconds
            playMeditationViewController.totalSeconds = getTotalTrackTime(track: trackList).totalSeconds
            self.txtMeditationsName.resignFirstResponder()
            self.navigationController?.pushViewController(playMeditationViewController, animated: true)
        }
    }
    
    @IBAction func btnSave_Clicked(_ sender: Any) {
        if(isDownloaded == true && isFromMeditationListViewController == false){
            let alertController = UIAlertController(title: AppName, message: updateMeditation, preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
            { action -> Void in
                self.txtMeditationsName.resignFirstResponder()
                self.backgroundView.isHidden = false
                self.saveTrackNameView.isHidden = false
                var originalCustome = "N"
                if self.responseArray[0]["is_original"] != nil {
                    if("\(self.responseArray[0]["is_original"]!)" == "Y"){
                        originalCustome = "Y"
                    }else{
                        originalCustome = "N"
                    }
                }else{
                    originalCustome = "Y"
                }

                
                if(originalCustome == "Y" && self.isOriginal == "N"){
                    self.txtTrackName.text = "My \(self.txtMeditationsName.text!)"
                }else{
                    self.txtTrackName.text = "\(self.txtMeditationsName.text!)"
                }
            })
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
            { action -> Void in

            })
            self.present(alertController, animated: true, completion: nil)
        }else{
            self.txtMeditationsName.resignFirstResponder()
            backgroundView.isHidden = false
            saveTrackNameView.isHidden = false
            var originalCustome = "N"
            if self.responseArray[0]["is_original"] != nil {
                if("\(self.responseArray[0]["is_original"]!)" == "Y"){
                    originalCustome = "Y"
                }else{
                    originalCustome = "N"
                }
            }else{
                originalCustome = "Y"
            }
            if(originalCustome == "Y" && isOriginal == "N"){
                txtTrackName.text = "My \(txtMeditationsName.text!)"
            }else{
                txtTrackName.text = "\(txtMeditationsName.text!)"
            }
        }
    }
    
    @IBAction func btnSaveTrackName_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        saveTrackNameView.isHidden = true
        txtTrackName.resignFirstResponder()
        if(txtTrackName.text!.isEmptyField){
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Meditaion name should not be empty.")
        }else{
            self.saveCustomMeditatioAPI(title: "\(txtTrackName.text!)")
        }
    }
    func saveCustomMeditatioAPI(title : String){
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            var params = Parameters()
            
            var trackID = ""
            var trackType = ""
            
            for index in 0...trackList.count - 1{
                if(index == 0){
                    trackID.append("\(trackList[index]["track_id"]!)")
                    trackType.append("\(trackList[index]["track_type"]!)")
                }else{
                    trackID.append(",\(trackList[index]["track_id"]!)")
                    trackType.append(",\(trackList[index]["track_type"]!)")
                }
            }
            if(backgroundList.count != 0){
                trackID.append(",\(backgroundList["track_id"]!)")
                trackType.append(",\(backgroundList["track_type"]!)")
            }
            var meditation_id = ""
            if(self.responseArray[0]["custom_meditation_id"] == nil){
                meditation_id = "\(self.responseArray[0]["meditation_id"]!)"
            }else{
                meditation_id = "\(self.responseArray[0]["custom_meditation_id"]!)"
            }
            if(isFromMeditationListViewController == true){
                params = ["auth_key": authKey, "track_ids": trackID, "track_types" : trackType, "title" : "\(title)", "is_original" : "N"]
            }else{
                params = ["auth_key": authKey, "custom_meditation_id" : "\(meditation_id)", "track_ids": trackID, "track_types" : trackType, "title" : "\(title)", "is_original" : "\(isOriginal)"]
            }
            print(params)
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            if(isFromMeditationListViewController == true){
                Alamofire.request(baseURLWithAuth + kSaveCustomList, method: .post, parameters: params).validate().responseJSON { response in
                    self.stopAnimating()
                    
                    switch response.result {
                    case .success:
                        
                        if let json: NSDictionary = response.result.value as? NSDictionary{
                            if json.value(forKey: "status") as? Int == 1{
                                let responseResult = response.result.value as! [String : Any]
                                //                    if("\(responseResult["result"]!)" == "1"){
                                
                                UIAlertController().alertViewWithTitleAndMessage(self, message: "\(responseResult["message"]!)")
                                self.btnSave.isHidden = true

//                                let alertSuccess: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: "\(responseResult["message"]!)", preferredStyle: .alert)
//                                let btnOk: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
//                                    _ in
//                                    let transition = CATransition()
//                                    transition.duration = 0.3
//                                    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//                                    transition.type = kCATransitionPush
//                                    self.navigationController?.view.layer.add(transition, forKey: nil)
//                                    _ = self.navigationController?.popViewController(animated: false)
//                                })
//                                
//                                alertSuccess.addAction(btnOk)
//                                self.present(alertSuccess, animated: true, completion: nil)
                                
                                NotificationCenter.default.post(name: NSNotification.Name.init("SetCustomeMeditationAdded"), object: nil)
                            }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                                
                                let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                                alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                                { action -> Void in
                                    let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                    
                                    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                    defaults.set(deviceToken,forKey: "DeviceToken")
                                    defaults.synchronize()
                                    
                                    let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                    self.navigationController?.pushViewController(welcomeViewController, animated: true)
                                })
                                self.present(alertController, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    case .failure(let error):
                        UIAlertController().alertViewWithErrorMessage(self)
                    }
                }
            }else{
                Alamofire.request(baseURLWithAuth + kEditCustomList, method: .post, parameters: params).validate().responseJSON { response in
                    self.stopAnimating()
                    
                    switch response.result {
                    case .success:
                        if let json: NSDictionary = response.result.value as? NSDictionary{
                            if json.value(forKey: "status") as? Int == 1{
                                NotificationCenter.default.post(name: NSNotification.Name.init("refreshMeditaionList"), object: nil)
                                if(self.fromActivityScreen == false){
                                    NotificationCenter.default.post(name: NSNotification.Name.init("CustomeMeditationAdded"), object: nil)
                                }
                                let responseResult = response.result.value as! [String : Any]
                                
                                if(appDelegate.downloadedMeditations.count > 0){
                                    for i in 0...appDelegate.downloadedMeditations.count - 1{
                                        if("\(appDelegate.downloadedMeditations[i]["meditation_id"]!)" == self.meditationID){
                                            self.removeFromBundle(index: i)
                                            appDelegate.downloadedMeditations.remove(at: i)
                                            if(self.fromActivityScreen == false){
                                                downloadAgain = true
                                            }
                                            break
                                        }
                                    }
                                    let archiveData = NSKeyedArchiver.archivedData(withRootObject: appDelegate.downloadedMeditations)
                                    defaults.set(archiveData, forKey: kdownloadedMeditations)
                                    defaults.synchronize()
                                }
                                
                                UIAlertController().alertViewWithTitleAndMessage(self, message: "\(responseResult["message"]!)")
                                self.btnSave.isHidden = true

//                                let alertSuccess: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: "\(responseResult["message"]!)", preferredStyle: .alert)
//                                let btnOk: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
//                                    _ in
//                                    let transition = CATransition()
//                                    transition.duration = 0.3
//                                    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//                                    transition.type = kCATransitionPush
//                                    self.navigationController?.view.layer.add(transition, forKey: nil)
//                                    _ = self.navigationController?.popViewController(animated: false)
//                                })
                                
//                                alertSuccess.addAction(btnOk)
//                                self.present(alertSuccess, animated: true, completion: nil)
                            }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                                
                                let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                                alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                                { action -> Void in
                                    let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                    
                                    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                    defaults.set(deviceToken,forKey: "DeviceToken")
                                    defaults.synchronize()
                                    
                                    let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                    self.navigationController?.pushViewController(welcomeViewController, animated: true)
                                })
                                self.present(alertController, animated: true, completion: nil)
                                
                            }
                        }
                    case .failure(let error):
                        UIAlertController().alertViewWithErrorMessage(self)
                    }
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func removeFromBundle(index : Int){
        let trackArray : [[String : AnyObject]] = appDelegate.downloadedMeditations[index]["track"] as! [[String : AnyObject]]
        for i in 0...trackArray.count - 1{
            let trackTitle = "\(trackArray[i]["title"]!)-\(appDelegate.downloadedMeditations[index]["meditation_id"]!)\(trackArray[i]["track_type"]!)\(trackArray[i]["track_id"]!).mp3"
            self.removeMeditation(trackTitle: trackTitle)
        }
    }
    func removeMeditation(trackTitle : String) {
        let fileManager = FileManager.default
        if !FileManager.default.fileExists(atPath: appDelegate.myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: appDelegate.myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        let path =  appDelegate.myDownloadPath + "/" + "\(trackTitle)"
        do {
            try fileManager.removeItem(atPath: path)
        } catch let error as NSError {
            print(error.debugDescription)
        }
    }
    
    
    
    @IBAction func btnClose1_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        saveTrackNameView.isHidden = true
    }
    
    @IBAction func btnClose2_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        trackOptionsView.isHidden = true
    }
    
    @IBAction func btnClose3_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        introTrackOptionsView.isHidden = true
    }
    
    @IBAction func btnClose4_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        endTracksOptionsView.isHidden = true
    }
    
    //Edit Track
    func editTrackDetails(sender: UIButton) {
        self.txtMeditationsName.resignFirstResponder()
        let buttonPosition: CGPoint = sender.convert(.zero, to: tblTracks)
        let indexPath: NSIndexPath = tblTracks.indexPathForRow(at: buttonPosition) as NSIndexPath!
        
        editIndex = indexPath.row
        
        if(btnindex != -1){
            DispatchQueue.main.async {
                self.player.pause()
                self.tblTracks.reloadData()
                self.btnindex = -1
            }
        }
        
        if(indexPath.row == 0 || indexPath.row == self.trackList.count){
            if indexPath.row == 0 {
                backgroundView.isHidden = false
                introTrackOptionsView.isHidden = false
                self.tblIntroTrackOptions.frame.size.height = CGFloat(self.introOptionsList.count * 60)
            }
            else if indexPath.row == self.trackList.count{
                backgroundView.isHidden = false
                endTracksOptionsView.isHidden = false
                self.tblEndTrackOptions.frame.size.height = CGFloat(self.endOptionsList.count * 60)
            }
        }else{
            if(indexPath.row == 1){
                trackOptionsList = ["Add Silence 30 Sec", "Move Down", "Add to Another Meditation", "Delete"]
            }else if(indexPath.row == trackList.count - 2){
                trackOptionsList = ["Add Silence 30 Sec", "Move Up", "Add to Another Meditation", "Delete"]
            }else if(indexPath.row != trackList.count - 1){
                trackOptionsList = ["Add Silence 30 Sec", "Move Up", "Move Down", "Add to Another Meditation", "Delete"]
            }
            if(trackList.count <= 3){
                trackOptionsList = ["Add Silence 30 Sec", "Add to Another Meditation", "Delete"]
            }
            self.tblTrackOptions.frame.size.height = CGFloat(self.trackOptionsList.count * 60)
            tblTrackOptions.reloadData()
            backgroundView.isHidden = false
            trackOptionsView.isHidden = false
        }
    }
    
    //Add Track to Meditation
    func addTrackName(sender: UIButton) {
        let addTracksViewController: AddTracksViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddTracksViewController") as! AddTracksViewController
        self.txtMeditationsName.resignFirstResponder()
        self.navigationController?.pushViewController(addTracksViewController, animated: true)
    }
    
    //Check Track Info
    func checkTrackInfo(sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(.zero, to: tblTracks)
        let indexPath: NSIndexPath = tblTracks.indexPathForRow(at: buttonPosition) as NSIndexPath!
        
        let trackInfoViewController: TrackInfoViewController = self.storyboard?.instantiateViewController(withIdentifier: "TrackInfoViewController") as! TrackInfoViewController
        if(indexPath.row == trackList.count){
            trackInfoViewController.trackList =  trackList[indexPath.row - 1]
        }else{
            trackInfoViewController.trackList =  trackList[indexPath.row]
        }
        self.txtMeditationsName.resignFirstResponder()
        self.navigationController?.pushViewController(trackInfoViewController, animated: true)
    }
    @IBAction func btnCloseAddToAnotherMeditation(_ sender: UIButton) {
        backgroundView.isHidden = true
        addtoAnotherMeditationView.isHidden = true
    }
    @IBAction func btnAddToNewMeditationView(_ sender: UIButton) {
        backgroundView.isHidden = true
        addtoAnotherMeditationView.isHidden = true
        let customMeditationViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationViewController") as! CustomMeditationViewController
        customMeditationViewController.trackToAdd = self.trackList[editIndex]
        customMeditationViewController.isEdited = true
        self.navigationController?.pushViewController(customMeditationViewController, animated: true)
        
    }
    @IBAction func btnAddToExistingMeditationView(_ sender: UIButton) {
        backgroundView.isHidden = true
        addtoAnotherMeditationView.isHidden = true
        let allMeditationListViewController: AllMeditationListViewController = self.storyboard?.instantiateViewController(withIdentifier: "AllMeditationListViewController") as! AllMeditationListViewController
        allMeditationListViewController.track_id = self.trackList[editIndex]["track_id"] as! Int
        self.navigationController?.pushViewController(allMeditationListViewController, animated: true)
    }

    /*
    //MARK: - Download Meditation - 
    func downloadMeditation(at_index : Int){
        var trackData = trackList
        trackData.append(backgroundList)
        var finishedMeditationCount = trackData.count
        var foundedInx = 0
        
        for i in 0...trackData.count - 1{
            if let audioUrl = URL(string: trackData[i]["audio_url"] as! String) {
                // then lets create your document folder url
                print(trackData[i])
                let destinationUrl = appDelegate.myDownloadPath
                let pathURL = NSURL(fileURLWithPath: destinationUrl)
                let trackTitle = "\(trackData[i]["title"]!)-\(meditationID)\(trackData[i]["track_type"]!)\(trackData[i]["track_id"]!).mp3"
                print(trackTitle)
                let filePath = pathURL.appendingPathComponent("\(trackTitle)")?.path
                print(filePath)
                if !FileManager.default.fileExists(atPath: appDelegate.myDownloadPath) {
                    try! FileManager.default.createDirectory(atPath: appDelegate.myDownloadPath, withIntermediateDirectories: true, attributes: nil)
                }
                // to check if it exists before downloading it
                if FileManager.default.fileExists(atPath: filePath!) {
                    print("The file already exists at path")
                    finishedMeditationCount = finishedMeditationCount - 1
                } else {
                    // you can use NSURLSession.sharedSession to download the data asynchronously
                    URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                        guard let location = location, error == nil else {
                            return
                        }
                        do {
                            // after downloading your file you need to move it to your destination url
                            try FileManager.default.moveItem(at: location, to: URL.init(fileURLWithPath: filePath!))
                            print("File moved to documents folder")
                            finishedMeditationCount = finishedMeditationCount - 1
                            if(finishedMeditationCount == 0){
                                self.saveDownloaded(trackData: trackData, at_index: at_index)
                            }
                            
                        } catch let error as NSError {
                            print(error.localizedDescription)
                        }
                    }).resume()
                }
            }
        }
    }
    
    func saveDownloaded(trackData : [[String : AnyObject]], at_index : Int){
        var meditationData = [String : AnyObject]()
        meditationData["title"] = responseArray[0]["title"] as AnyObject
        meditationData["meditation_id"] = responseArray[0]["custom_meditation_id"] as AnyObject
        meditationData["total_meditation_time"] = responseArray[0]["total_meditation_time"] as AnyObject
        meditationData["description"] = "" as AnyObject
        meditationData["track"] = trackData as AnyObject
        if("\(responseArray[0]["is_original"]!)" == "N"){
            meditationData["is_original"] = "Made by Me" as AnyObject
        }else{
            meditationData["is_original"] = "Made by Meditable" as AnyObject
        }

        appDelegate.downloadedMeditations.insert(meditationData, at: at_index)
        let archiveData = NSKeyedArchiver.archivedData(withRootObject: appDelegate.downloadedMeditations)
        defaults.set(archiveData, forKey: kdownloadedMeditations)
        defaults.synchronize()
        print(appDelegate.downloadedMeditations)
    }
    */
    //MARK:- Refresh List -
    func refreshList(notification: Notification){
        if let backgroundSound = notification.userInfo! as? [String : AnyObject] {
            backgroundList = backgroundSound
        }
    }
    
    //MARK:- SilenceTrack if isn't found
    
    func getSilenceTracks(){
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetSilenceTracks, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            let backgroundTrackArray = json.value(forKey: "data") as! [[String : AnyObject]]
                            self.silenceList = backgroundTrackArray[0]
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                            
                        }

                    }else {
                        
                    }
                case .failure(let error):
                    print (error)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    //MARK:- Change Into Edit Track
    
    func musicChange (sender: Notification) {
        isOriginal = "N"
        isEdited = "Y"
        if let senderArray = sender.object as? [String : AnyObject] {
            if "\(senderArray["track_type"]!)" == "I"{
                trackList.remove(at: 0)
                trackList.insert(senderArray, at: 0)
            }
            if "\(senderArray["track_type"]!)" == "E"{
                trackList.removeLast()
                trackList.append(senderArray)
            }
        }
        tblTracks.reloadData()
        lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: trackList).time)"
    }

    
    //Add new track from tracklist
    func addNewTrackFromList(notification: Notification){
        btnSave.isHidden = false
        currentPlayIndex = 500
        isOriginal = "N"
        isEdited = "Y"
        if let notificationArray = notification.object as? [String : AnyObject]  {
            trackList.insert(notificationArray , at: trackList.count - 1)
            tblTracks.reloadData()
            lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: trackList).time)"
        }
        tblTracksHeightConstraint.constant = CGFloat((self.trackList.count + 1) * 80)
    }
    
    func addTrackLog(index : Int){
        var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
        if authKey == nil {
            authKey = ""
        }
        var meditationType = ""
        if(isOriginal == "Y"){
            meditationType = "O"
        }else{
            meditationType = "C"
        }
        let trackType = "\(self.trackList[index]["track_type"]!)"
        let param = ["auth_key": authKey, "track_id" : "\(self.trackList[index]["track_id"]!)", "track_type" : trackType, "meditation_type" : meditationType]
        APIManager().AddTrackLogs(parameters: param as! [String : String]) { (response, error) in
            if(error == nil){
//                print(response!)
            }else{
                if(error?.code == 5 || error?.code == 3){
                    let alertController = UIAlertController(title: AppName, message: "Unauthorized", preferredStyle:UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                    { action -> Void in
                        let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        defaults.set(deviceToken,forKey: "DeviceToken")
                        defaults.synchronize()
                        let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                        self.navigationController?.pushViewController(welcomeViewController, animated: true)
                    })
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    //Play Track
    func playTrack(sender: UIButton) {
        let indexPath: NSIndexPath = self.tblTracks.indexPath(for: (sender.superview?.superview as! UITableViewCell))! as NSIndexPath
        editIndex = indexPath.row

        if btnindex == indexPath.row{
            btnindex = -1
        }else{
            btnindex = indexPath.row
        }
        
        if(indexPath.row == trackList.count){
            urlString = (trackList[indexPath.row - 1]["audio_url"] as? String)!
        }else{
            urlString = (trackList[indexPath.row]["audio_url"] as? String)!
        }
        
        if(isDownloaded == false){
            if urlString != ""{
                urlString = urlString.replacingOccurrences(of: " ", with: "%20")
                let url = URL.init(string: urlString)
                do {
                    let avPlayerItem: AVPlayerItem = AVPlayerItem.init(url: url!)
                    if(currentPlayIndex != indexPath.row){
                        currentPlayIndex = indexPath.row
                        player = AVPlayer.init(playerItem: avPlayerItem)
                    }
                    if #available(iOS 10.0, *) {
                        player.automaticallyWaitsToMinimizeStalling = false
                    } else {
                        // Fallback on earlier versions
                    }
                    if(sender.isSelected == false){
                        let size = CGSize(width: 75, height: 75)
                        let color : UIColor = .white
                        startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
                        self.player.addObserver(self, forKeyPath: "status", options: .initial, context: nil)
                        if(indexPath.row == trackList.count){
                            self.addTrackLog(index: indexPath.row - 1)
                        }else{
                            self.addTrackLog(index: indexPath.row)
                        }
                        player.play()
                    }else{
                        player.pause()
                        player.rate = 0
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }else{
            if urlString != ""{
                var backgroundPath = ""
                var indexInt = 500
                if(indexPath.row == trackList.count){
                    indexInt = indexPath.row - 1
                }else{
                    indexInt = indexPath.row
                }

                let trackName = "\(trackList[indexInt]["title"]!)-\(meditationID)\(trackList[indexInt]["track_type"]!)\(trackList[indexInt]["track_id"]!).mp3"
                let bgpath = appDelegate.myDownloadPath
                let url = NSURL(fileURLWithPath: bgpath)
                let filePath = url.appendingPathComponent("\(trackName)")?.path
                let fileManager = FileManager.default
                if fileManager.fileExists(atPath: filePath!) {
                    backgroundPath = filePath!
                } else {
                    urlString = urlString.replacingOccurrences(of: " ", with: "%20")
                    let url = URL.init(string: urlString)
                    do {
                        let avPlayerItem: AVPlayerItem = AVPlayerItem.init(url: url!)
                        if(currentPlayIndex != indexPath.row){
                            currentPlayIndex = indexPath.row
                            player = AVPlayer.init(playerItem: avPlayerItem)
                        }
                        if #available(iOS 10.0, *) {
                            player.automaticallyWaitsToMinimizeStalling = false
                        } else {
                            // Fallback on earlier versions
                        }
                            let size = CGSize(width: 75, height: 75)
                            let color : UIColor = .white
                            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
                            self.player.addObserver(self, forKeyPath: "status", options: .initial, context: nil)
                    }
                }
                if(sender.isSelected == false){
                    do {
                        if(currentPlayIndex != indexPath.row){
                            currentPlayIndex = indexPath.row
                            if(backgroundPath != ""){
                                player = try AVPlayer(url: URL.init(fileURLWithPath: backgroundPath))
                            }
                        }
                        player.play()
                    }
                    catch{
                        print(error)
                    }
                }else{
                    player.pause()
                    player.rate = 0
                }
            }
        }

        
        self.tblTracks.reloadData()
    }
    
    func itemDidFinishPlaying(notification:NSNotification) {
        player.pause()
        player.rate = 0
        self.btnindex = -1
        let indexPath = IndexPath(item: editIndex, section: 0)
        tblTracks.reloadRows(at: [indexPath], with: .none)
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if (keyPath == "status") {
            if (player.status == .readyToPlay) {
                self.player.removeObserver(self, forKeyPath: "status", context: nil)
                self.stopAnimating()
            }
        }
    }
    
    //TableView Delegate & Data source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblTracks {
            return self.trackList.count + 1
        }
        else if tableView == tblIntroTrackOptions {
            return introOptionsList.count
        }
        else if tableView == tblEndTrackOptions {
            return endOptionsList.count
        }
        else {
            return trackOptionsList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        if tableView == tblTracks {
            let cell: TimeLineTableViewCell! = tblTracks.dequeueReusableCell(withIdentifier: cellIdentifier) as! TimeLineTableViewCell
            if(indexPath.row == 0){
                cell.topTitleConstraint.constant = 5
                cell.lblTrackName.text = trackList[indexPath.row]["title"] as? String
                cell.lblTrackName.font = UIFont(name: "FuturaBT-Medium", size: 16.0)
                if let trackTime = trackList[indexPath.row]["time"] as? String {
                    cell.lblTrackTime.text = "\(trackTime)"
                }
                
                cell.lblTrackName.textColor = UIColor().yellowBorderColor
                
                cell.btnEditTrackName.removeTarget(self, action: #selector(CustomMeditationPlaylistViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.removeTarget(self, action: #selector(CustomMeditationPlaylistViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.removeTarget(self, action: #selector(CustomMeditationPlaylistViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnEditTrackName.addTarget(self, action: #selector(CustomMeditationPlaylistViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.addTarget(self, action: #selector(CustomMeditationPlaylistViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.addTarget(self, action: #selector(CustomMeditationPlaylistViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnInfo.isHidden = false
                cell.btnPlay.isHidden = false
                cell.btnEditTrackName.isHidden = false

            }else if(indexPath.row == trackList.count - 1){
                cell.topTitleConstraint.constant = 20
                cell.lblTrackName.text = "Add Track"
                cell.lblTrackTime.text = ""
                cell.lblTrackName.font = UIFont(name: "FuturaBT-MediumItalic", size: 16.0)
                cell.lblTrackName.textColor = UIColor.white
                cell.lblTrackName.sizeToFit()
                cell.btnEditTrackName.removeTarget(self, action: #selector(CustomMeditationViewController.addNewTrack(sender: )), for: .touchUpInside)
                cell.btnEditTrackName.addTarget(self, action: #selector(CustomMeditationViewController.addNewTrack(sender: )), for: .touchUpInside)
                cell.btnInfo.isHidden = true
                cell.btnPlay.isHidden = true
                cell.btnEditTrackName.isHidden = true
            }else if(indexPath.row == self.trackList.count){
                cell.topTitleConstraint.constant = 5
                cell.lblTrackName.text = trackList[indexPath.row - 1]["title"] as? String
                cell.lblTrackName.font = UIFont(name: "FuturaBT-Medium", size: 16.0)
                if let trackTime = trackList[indexPath.row - 1]["time"] as? String {
                    cell.lblTrackTime.text = "\(trackTime)"
                }
                cell.lblTrackName.textColor = UIColor().yellowBorderColor
                cell.lblTrackName.sizeToFit()
                
                cell.btnEditTrackName.removeTarget(self, action: #selector(CustomMeditationPlaylistViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.removeTarget(self, action: #selector(CustomMeditationPlaylistViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.removeTarget(self, action: #selector(CustomMeditationPlaylistViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnEditTrackName.addTarget(self, action: #selector(CustomMeditationPlaylistViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.addTarget(self, action: #selector(CustomMeditationPlaylistViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.addTarget(self, action: #selector(CustomMeditationPlaylistViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnInfo.isHidden = false
                cell.btnPlay.isHidden = false
                cell.btnEditTrackName.isHidden = false
            }else{
                cell.topTitleConstraint.constant = 5
                cell.lblTrackName.text = self.trackList[indexPath.row]["title"] as? String
                cell.lblTrackName.font = UIFont(name: "FuturaBT-Medium", size: 16.0)
                if let trackTime = self.trackList[indexPath.row]["time"] as? String {
                    cell.lblTrackTime.text = "\(trackTime)"
                }
                cell.lblTrackName.textColor = UIColor().yellowBorderColor
                
                cell.btnEditTrackName.removeTarget(self, action: #selector(CustomMeditationViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.removeTarget(self, action: #selector(CustomMeditationViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.removeTarget(self, action: #selector(CustomMeditationViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnEditTrackName.addTarget(self, action: #selector(CustomMeditationViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.addTarget(self, action: #selector(CustomMeditationViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.addTarget(self, action: #selector(CustomMeditationViewController.playTrack(sender: )), for: .touchUpInside)
                
                let strTrackType = self.trackList[indexPath.row]["track_type"] as! String
                if strTrackType == "S" {
                    cell.btnPlay.isHidden = true
                } else {
                    cell.btnPlay.isHidden = false
                }
                
                cell.btnInfo.isHidden = false
                cell.btnEditTrackName.isHidden = false
            }
            
            cell.allRows = self.trackList.count + 1
            cell.currentIndexPath = indexPath
            cell.setNeedsDisplay()
            
            cell.isUserInteractionEnabled = true
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            if btnindex == indexPath.row{
                cell.btnPlay.isSelected = true
                cell.btnPlay.setImage(#imageLiteral(resourceName: "Stop"), for: UIControlState.selected)
            }else{
                cell.btnPlay.isSelected = false
                cell.btnPlay.setImage(#imageLiteral(resourceName: "SmallPlay"), for: UIControlState.normal)
            }

            
            return cell
        }
        else if tableView == tblIntroTrackOptions {
            var cell: UITableViewCell! = tblIntroTrackOptions.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
            
            if cell == nil {
                cell = UITableViewCell.init(style: .default, reuseIdentifier: cellIdentifier)
                cell.isUserInteractionEnabled = true
                cell.accessoryType = .none
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
            }
            
            let lblOptionName: UILabel = cell.viewWithTag(10) as! UILabel
            lblOptionName.text = introOptionsList[indexPath.row]
            
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            return cell
        }
        else if tableView == tblEndTrackOptions {
            var cell: UITableViewCell! = tblEndTrackOptions.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
            
            if cell == nil {
                cell = UITableViewCell.init(style: .default, reuseIdentifier: cellIdentifier)
                cell.isUserInteractionEnabled = true
                cell.accessoryType = .none
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
            }
            
            let lblOptionName: UILabel = cell.viewWithTag(10) as! UILabel
            lblOptionName.text = endOptionsList[indexPath.row]
            
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            return cell
        }
        else {
            var cell: UITableViewCell! = tblTrackOptions.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
            
            if cell == nil {
                cell = UITableViewCell.init(style: .default, reuseIdentifier: cellIdentifier)
                cell.isUserInteractionEnabled = true
                cell.accessoryType = .none
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
            }
            
            let lblOptionName: UILabel = cell.viewWithTag(10) as! UILabel
            lblOptionName.text = trackOptionsList[indexPath.row]
            
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblTrackOptions {
            if(trackOptionsList[indexPath.row] == "Move Up"){
                isOriginal = "N"
                isEdited = "Y"
                currentPlayIndex = 500
                btnSave.isHidden = false
                self.trackList.rearrange(from: editIndex, to: editIndex - 1 )
                backgroundView.isHidden = true
            }else if(trackOptionsList[indexPath.row] == "Move Down"){
                isOriginal = "N"
                isEdited = "Y"
                currentPlayIndex = 500
                btnSave.isHidden = false
                self.trackList.rearrange(from: editIndex, to: editIndex + 1)
                backgroundView.isHidden = true
            }else if(trackOptionsList[indexPath.row] == "Delete"){
                isOriginal = "N"
                isEdited = "Y"
                btnSave.isHidden = false
                currentPlayIndex = 500
                self.trackList.remove(at: editIndex)
                lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: trackList).time)"
                backgroundView.isHidden = true
            }else if(trackOptionsList[indexPath.row] == "Add Silence 30 Sec"){
                isOriginal = "N"
                isEdited = "Y"
                currentPlayIndex = 500
                btnSave.isHidden = false
                addSilenceTrack(index: editIndex + 1)
                backgroundView.isHidden = true
            }else if(trackOptionsList[indexPath.row] == "Add to Another Meditation"){
                backgroundView.isHidden = false
                addtoAnotherMeditationView.isHidden = false
            }
            trackOptionsView.isHidden = true
            tblTracks.reloadData()
        }
        else if tableView == tblTracks {
            if (indexPath.row == trackList.count - 1) {
                if(btnindex != -1){
                    DispatchQueue.main.async {
                        self.player.pause()
                        self.tblTracks.reloadData()
                        self.btnindex = -1
                    }
                }
                let addTracksViewController: AddTracksViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddTracksViewController") as! AddTracksViewController
                self.txtMeditationsName.resignFirstResponder()
                self.navigationController?.pushViewController(addTracksViewController, animated: true)
            }
        }
        else if tableView == tblIntroTrackOptions {
            btnSave.isHidden = false
            isOriginal = "N"
            isEdited = "Y"
            if introOptionsList[indexPath.row] == "Add Silence 30 Sec" {
                addSilenceTrack(index: editIndex + 1)
            }
            else if introOptionsList[indexPath.row] == "Change Intro" {
                let backgroundMusicViewController: BackgroundMusicViewController = self.storyboard?.instantiateViewController(withIdentifier: "BackgroundMusicViewController") as! BackgroundMusicViewController
                backgroundMusicViewController.strController = "intro"
                self.txtMeditationsName.resignFirstResponder()
                self.navigationController?.pushViewController(backgroundMusicViewController, animated: true)
            }
            backgroundView.isHidden = true
            introTrackOptionsView.isHidden = true
        }
        else if tableView == tblEndTrackOptions {
            btnSave.isHidden = false
            isOriginal = "N"
            isEdited = "Y"
            if endOptionsList[indexPath.row] == "Change Ending" {
                let backgroundMusicViewController: BackgroundMusicViewController = self.storyboard?.instantiateViewController(withIdentifier: "BackgroundMusicViewController") as! BackgroundMusicViewController
                backgroundMusicViewController.strController = "ending"
                self.txtMeditationsName.resignFirstResponder()
                self.navigationController?.pushViewController(backgroundMusicViewController, animated: true)
            }
            backgroundView.isHidden = true
            endTracksOptionsView.isHidden = true
        }
        tblTracksHeightConstraint.constant = CGFloat((self.trackList.count + 1) * 80)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    
    func addSilenceTrack(index : Int){
        var silenceDictionary = [String : AnyObject]()
        silenceDictionary["reminder"] = silenceList["reminder"] as AnyObject
        silenceDictionary["track_type"] = "S" as AnyObject
        silenceDictionary["audio_url"] = silenceList["audio_url"] as AnyObject
        silenceDictionary["description"] = silenceList["description"] as AnyObject
        silenceDictionary["title"] = silenceList["title"] as AnyObject
        silenceDictionary["track_id"] = silenceList["track_id"] as AnyObject
        silenceDictionary["time"] = silenceList["time"] as AnyObject
        silenceDictionary["display_order"] = silenceList["display_order"] as AnyObject
        trackList.insert(silenceDictionary, at: index)
        tblTracks.reloadData()
        lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: trackList).time)"
    }
    
    //UITextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtMeditationsName.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        isOriginal = "N"
        isEdited = "Y"
        btnSave.isHidden = false
        return true
    }
    
    func popToController(){
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
