//
//  CustomMeditationListViewController.swift
//  Meditable
//
//  Created by Apple on 20/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON
import MZDownloadManager
import UICircularProgressRing

let alertControllerViewTag: Int = 500

class CustomMeditationListViewController: UIViewController,NVActivityIndicatorViewable, UITableViewDelegate, UITableViewDataSource, MZDownloadManagerDelegate {
    
    @IBOutlet weak var tblMeditationList: UITableView!
    @IBOutlet weak var btnAddNewMeditation: UIButton!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var btnDownloadedMeditations: UIButton!
    @IBOutlet weak var tblDownloaded: UITableView!

    var meditationList = [[String: AnyObject]]()
    var refresher: UIRefreshControl!
    
    var meditationData = [String : AnyObject]()
    var soundArrayList = [String]()
    var trackNameArray = [String]()
    var availableDownloadsArray: [String] = []
    var selectedIndexPath : IndexPath!
    var totalProgressArray = [Float]()
    var meditationID = ""
    var downloadIndexpath = IndexPath()
    var isPause = ""
    var onlineLoad = true
    lazy var downloadManager: MZDownloadManager = {
        [unowned self] in
        let sessionIdentifer: String = self.generateRandomStringWithLength(length: 10)
        var completion = appDelegate.backgroundSessionCompletionHandler
        let downloadmanager = MZDownloadManager(session: sessionIdentifer, delegate: self, completion: completion)
        return downloadmanager
        }()   
    
    //MARK: - View LifeCycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        lblNoDataFound.isHidden = true
        
        getCustomMeditationList()
        
        btnDownloadedMeditations.setTitle("Create New Meditation", for: .normal)
        
        //RefreshControl
        refresher = UIRefreshControl()
        tblMeditationList!.alwaysBounceVertical = true
        refresher.tintColor = .white
        refresher.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tblMeditationList!.addSubview(refresher)
        
        tblMeditationList.estimatedRowHeight = 120
        tblMeditationList.rowHeight = UITableViewAutomaticDimension

        tblDownloaded.estimatedRowHeight = 120
        tblDownloaded.rowHeight = UITableViewAutomaticDimension

        NotificationCenter.default.addObserver(self, selector: #selector(CustomMeditationListViewController.refreshMeditaionList(sender: )), name: NSNotification.Name.init("refreshMeditaionList"), object: nil)
        

        
//        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(CustomMeditationListViewController.panButton(pan:)))
//        btnAddNewMeditation.addGestureRecognizer(panGesture)
        btnAddNewMeditation.isHidden = true
        
        btnDownloadedMeditations.layer.borderWidth = 1
        btnDownloadedMeditations.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnDownloadedMeditations.layer.cornerRadius = btnDownloadedMeditations.frame.size.height / 2
        btnDownloadedMeditations.layer.masksToBounds = true
    }
    

    func panButton(pan: UIPanGestureRecognizer) {
        if pan.state == .began {
            
        } else if pan.state == .ended || pan.state == .failed || pan.state == .cancelled {
            let location = pan.location(in: view) // get pan location
            if(location.y > self.view.bounds.size.height - 110){
                btnAddNewMeditation.center.y = self.view.bounds.size.height - 110
            }else if(location.y < 80){
                btnAddNewMeditation.center.y = 80
            }
        } else {
            let location = pan.location(in: view) // get pan location
            btnAddNewMeditation.center.y = location.y // set button to where finger is
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        addGoogleAnalytics(screenName: "My Meditation")
        if(appDelegate.reachable.isReachable){
            onlineLoad = true
            if(self.meditationList.count == 0){
                getCustomMeditationList()
            }else{
                tblMeditationList.reloadData()
            }
            tblDownloaded.isHidden = true
            tblMeditationList.isHidden = false
        }else{
            onlineLoad = false
            tblDownloaded.reloadData()
            tblDownloaded.isHidden = false
            tblMeditationList.isHidden = true
        }
        if(downloadAgain == true){
            isPause = "Play"
            downloadIndexpath = downloadRefIndex
            print(downloadIndexpath)

            for indexPath in self.tblMeditationList.indexPathsForVisibleRows! {
                if (indexPath.section == self.downloadIndexpath.section) {
                    if let cell : customMeditationCell = self.tblMeditationList.cellForRow(at: self.downloadIndexpath) as? customMeditationCell{
                        DispatchQueue.global().async {
                            cell.progressView.setProgress(value: 0.0, animationDuration: 0)
                            cell.progressView.isHidden = false
                            cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_control_stop"), for: UIControlState.normal)
                        }
                    }
                }
            }
            self.prepareForDownload(indexPath: downloadRefIndex)
            meditationID = "\(self.meditationList[downloadRefIndex.section]["custom_meditation_id"]!)"
            UserDefaults.standard.set(true, forKey: "startDownloading")
            UserDefaults.standard.synchronize()
            self.setDownloader()
        }
        downloadAgain = false
        downloadRefIndex = IndexPath()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:-  Button Events -

    @IBAction func btnDownloadedMeditations(_ sender: UIButton) {
        if(appDelegate.reachable.isReachable){
            let customMeditationViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationViewController") as! CustomMeditationViewController
            self.navigationController?.pushViewController(customMeditationViewController, animated: true)
        }else{
            let alertController = UIAlertController(title: AppName, message: NoInternetMessage, preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            { action -> Void in
                
            })
            self.present(alertController, animated: true, completion: nil)
        }
//        let savedMeditationViewController = self.storyboard?.instantiateViewController(withIdentifier: "OfflineSavedMeditationViewController") as! OfflineSavedMeditationViewController
//        self.navigationController?.pushViewController(savedMeditationViewController, animated: true)
    }
    
    //MARK:-  Refresh -

    func refreshList() {
        var downloading : Bool = false
        if(defaults.value(forKey: "startDownloading") != nil){
            downloading = defaults.value(forKey: "startDownloading") as! Bool
        }
        if(downloading == true){
            for i in 0...self.downloadManager.downloadingArray.count - 1{
                self.downloadManager.cancelTaskAtIndex(i)
            }
            self.removeFromBundle(index: self.downloadIndexpath.section)
            defaults.set(false, forKey: "startDownloading")
        }
        isPause = ""
        self.refresher.endRefreshing()
        getCustomMeditationList()
    }
    
    func refreshMeditaionList (sender: Notification) {
        if(self.meditationList.count != 0){
            self.lblNoDataFound.isHidden = false
        }else{
            self.lblNoDataFound.isHidden = true
        }
        self.refreshList()
    }
    func removeFromBundle(index : Int){
        var trackArray = [[String : AnyObject]]()
        if(onlineLoad == true){
            trackArray  = meditationList[index]["track"] as! [[String : AnyObject]]
            for i in 0...trackArray.count - 1{
                let trackTitle = "\(trackArray[i]["title"]!)-\(meditationList[index]["custom_meditation_id"]!)\(trackArray[i]["track_type"]!)\(trackArray[i]["track_id"]!).mp3"
                self.removeMeditation(trackTitle: trackTitle)
            }
        }else{
            trackArray = appDelegate.downloadedMeditations[index]["track"] as! [[String : AnyObject]]
            for i in 0...trackArray.count - 1{
                let trackTitle = "\(trackArray[i]["title"]!)-\(appDelegate.downloadedMeditations[index]["meditation_id"]!)\(trackArray[i]["track_type"]!)\(trackArray[i]["track_id"]!).mp3"
                self.removeMeditation(trackTitle: trackTitle)
            }
        }
    }
    func removeMeditation(trackTitle : String) {
        let fileManager = FileManager.default
        if !FileManager.default.fileExists(atPath: appDelegate.myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: appDelegate.myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        let path =  appDelegate.myDownloadPath + "/" + "\(trackTitle)"
        do {
            try fileManager.removeItem(atPath: path)
        } catch let error as NSError {
            print(error.debugDescription)
        }
    }
    
    //MARK: - UITableView Delegate & Data source Methods -
    func numberOfSections(in tableView: UITableView) -> Int {
        if(tableView == tblMeditationList){
            return self.meditationList.count
        }else{
            if(appDelegate.downloadedMeditations.count == 0 && appDelegate.reachable.isReachable == false){
                lblNoDataFound.isHidden = false
            }else{
                lblNoDataFound.isHidden = true
            }
            return appDelegate.downloadedMeditations.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = UIView()
        headerView.backgroundColor = .clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cellCustomMeditation"
        let cell: customMeditationCell = tblMeditationList.dequeueReusableCell(withIdentifier: cellIdentifier) as! customMeditationCell
        
        cell.btnMeditationType.layer.cornerRadius = 12.0

        if(tableView == tblMeditationList){
            var downloaded = false
            cell.lblMeditationTitle.text = self.meditationList[indexPath.section]["title"] as? String
            let trackArray : [[String : AnyObject]] = meditationList[indexPath.section]["track"] as! [[String : AnyObject]]
            var bgTime = "00:00"
            for i in 0...trackArray.count - 1 {
                if("\(trackArray[i]["track_type"]!)" == "B"){
                    bgTime = "\(trackArray[i]["time"]!)"
                    break
                }
            }
            let time = self.getTotalTime(totalTime: "\(self.meditationList[indexPath.section]["total_meditation_time"]!)", backgroundMusicTime: bgTime)
            cell.lblTime.text = "\(time.time)"
            
            if("\(self.meditationList[indexPath.section]["is_original"]!)" == "Y"){
                cell.btnMeditationType.setTitle("Made by Meditable", for: .normal)
                cell.btnMeditationType.backgroundColor = UIColor().yellowBorderColor
            }else{
                cell.btnMeditationType.setTitle("Made by Me", for: .normal)
                cell.btnMeditationType.backgroundColor = UIColor().whiteBorderColor
            }
            
            cell.progressView.isHidden = true
            cell.progressView.shouldShowValueText = false
            cell.btnDownload.isHidden = false
            
            
            cell.btnDelete.addTarget(self, action: #selector(CustomMeditationListViewController.btnDelete(sender:)), for: .touchUpInside)
            cell.btnDownload.addTarget(self, action: #selector(CustomMeditationListViewController.btnDownload(sender:)), for: .touchUpInside)
            
            if(appDelegate.downloadedMeditations.count != 0){
                for i in 0...appDelegate.downloadedMeditations.count - 1{
                    if("\(appDelegate.downloadedMeditations[i]["meditation_id"]!)" == "\(self.meditationList[indexPath.section]["custom_meditation_id"]!)"){
                        cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadedCloud"), for: UIControlState.normal)
                        downloaded = true
                        break
                    }else{
                        cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadCloud"), for: UIControlState.normal)
                    }
                }
            }else{
                cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadCloud"), for: UIControlState.normal)
            }
            
            var downloading : Bool = false
            if(UserDefaults.standard.value(forKey: "startDownloading") != nil){
                downloading = UserDefaults.standard.value(forKey: "startDownloading") as! Bool
            }
            if downloading == true{
                if(indexPath == downloadIndexpath){
                    cell.progressView.isHidden = false
                    cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_control_stop"), for: UIControlState.normal)
                }
            }
            
            if("\(self.meditationList[indexPath.section]["is_locked"]!)" == "Y" && downloaded == false){
                cell.leftConstraintTitle.constant = 25
                cell.lblMeditationTitle.alpha = 0.5
                cell.lblTime.alpha = 0.5
                cell.btnLock.alpha = 0.5
                cell.btnDownload.alpha = 0.5
                cell.btnDelete.alpha = 0.5
                cell.btnMeditationType.alpha = 0.5
                cell.btnLock.isHidden = false
                cell.btnDelete.isEnabled = false
                cell.btnDownload.isEnabled = false
                cell.btnMeditationType.isEnabled = false
            }else{
                cell.leftConstraintTitle.constant = 0
                cell.lblMeditationTitle.alpha = 1.0
                cell.lblTime.alpha = 1.0
                cell.btnLock.alpha = 1.0
                cell.btnDownload.alpha = 1.0
                cell.btnDelete.alpha = 1.0
                cell.btnMeditationType.alpha = 1.0
                cell.btnLock.isHidden = true
                cell.btnDelete.isEnabled = true
                cell.btnDownload.isEnabled = true
                cell.btnMeditationType.isEnabled = true
            }
            
        }else{
            cell.lblMeditationTitle.text = "\(appDelegate.downloadedMeditations[indexPath.section]["title"]!)"
            // For Total Time
            let trackList = appDelegate.downloadedMeditations[indexPath.section]["track"] as! [[String : AnyObject]]
            var bgTime = "00:00"
            for i in 0...trackList.count - 1 {
                if("\(trackList[i]["track_type"]!)" == "B"){
                    bgTime = "\(trackList[i]["time"]!)"
                    break
                }
            }
            let time = self.getTotalTime(totalTime: "\(appDelegate.downloadedMeditations[indexPath.section]["total_meditation_time"]!)", backgroundMusicTime: bgTime)
            cell.lblTime.text = "\(time.time)"
            
            let isOrig = "\(appDelegate.downloadedMeditations[indexPath.section]["is_original"]!)"
                cell.btnMeditationType.setTitle("\(isOrig)", for: .normal)
            if(isOrig == "Made by Meditable"){
                cell.btnMeditationType.backgroundColor = UIColor().yellowBorderColor
            }else{
                cell.btnMeditationType.backgroundColor = UIColor().whiteBorderColor
            }
            
            cell.progressView.isHidden = true
            cell.progressView.shouldShowValueText = false
            cell.btnDownload.isHidden = false
            
            
            cell.btnDelete.addTarget(self, action: #selector(CustomMeditationListViewController.btnDelete(sender:)), for: .touchUpInside)
            cell.btnDownload.addTarget(self, action: #selector(CustomMeditationListViewController.btnDownload(sender:)), for: .touchUpInside)
            
            cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadedCloud"), for: UIControlState.normal)
            
            cell.leftConstraintTitle.constant = 0
            cell.btnLock.isHidden = true

        }

        
        cell.btnDelete.tag = indexPath.section
        cell.btnDownload.tag = indexPath.section
        cell.btnMeditationType.tag = indexPath.section
        
        cell.layer.borderColor = UIColor.white.cgColor
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1
        cell.layer.masksToBounds = true
        
        cell.backgroundColor = .clear
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        appDelegate.playFromLibrary = false
        downloadRefIndex = indexPath
        if(tableView == tblMeditationList){

            var downloaded : Bool = false
            if(appDelegate.downloadedMeditations.count != 0){
                for i in 0...appDelegate.downloadedMeditations.count - 1{
                    if("\(appDelegate.downloadedMeditations[i]["meditation_id"]!)" == "\(self.meditationList[indexPath.section]["custom_meditation_id"]!)"){
                        downloaded = true
                        break
                    }
                }
            }
            
            if(downloaded == false){
                if("\(self.meditationList[indexPath.section]["is_locked"]!)" == "Y"){
                    let alertController = UIAlertController(title: AppName, message: LockMeditationMessage, preferredStyle:UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Subscribe", style: UIAlertActionStyle.default)
                    { action -> Void in
                        let subscriptionPlanvc: SubscriptionPlanViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionPlanViewController") as! SubscriptionPlanViewController
                        self.navigationController?.pushViewController(subscriptionPlanvc, animated: true)
                    })
                    alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
                    { action -> Void in
                        
                    })
                    self.present(alertController, animated: true, completion: nil)
                }else{
                    let customMeditationPlaylistViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationPlaylistViewController") as! CustomMeditationPlaylistViewController
                    customMeditationPlaylistViewController.responseArray = [self.meditationList[indexPath.section]]
                    customMeditationPlaylistViewController.index = indexPath.section
                    customMeditationPlaylistViewController.isOriginal = "\(self.meditationList[indexPath.section]["is_original"]!)"
                    customMeditationPlaylistViewController.meditationID = "\(self.meditationList[indexPath.section]["custom_meditation_id"]!)"
                    
                    let trackList = self.meditationList[indexPath.section]["track"] as! [[String : AnyObject]]
                    var bgTime = "00:00"
                    for i in 0...trackList.count - 1 {
                        if("\(trackList[i]["track_type"]!)" == "B"){
                            bgTime = "\(trackList[i]["time"]!)"
                            break
                        }
                    }
                    let time = self.getTotalTime(totalTime: "\(self.meditationList[indexPath.section]["total_meditation_time"]!)", backgroundMusicTime: bgTime)
                    customMeditationPlaylistViewController.totalTime = "\(time.time)"
                    customMeditationPlaylistViewController.backgroundSeconds = time.backgroundSeconds
                    customMeditationPlaylistViewController.totalSeconds = time.totalSeconds
                    
                    self.navigationController?.pushViewController(customMeditationPlaylistViewController, animated: true)
                }
                
            }else{
//                let customMeditationPlaylistViewController = self.storyboard?.instantiateViewController(withIdentifier: "SavedTrackListViewController") as! SavedTrackListViewController
                let customMeditationPlaylistViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationPlaylistViewController") as! CustomMeditationPlaylistViewController
                customMeditationPlaylistViewController.responseArray = [self.meditationList[indexPath.section]]
                customMeditationPlaylistViewController.index = indexPath.section
                
                if(self.meditationList[indexPath.section]["meditation_id"] == nil){
                    customMeditationPlaylistViewController.meditationID = "\(self.meditationList[indexPath.section]["custom_meditation_id"]!)"
                }else{
                    customMeditationPlaylistViewController.meditationID = "\(self.meditationList[indexPath.section]["meditation_id"]!)"
                }
                let trackList = self.meditationList[indexPath.section]["track"] as! [[String : AnyObject]]
                
                var bgTime = "00:00"
                for i in 0...trackList.count - 1 {
                    if("\(trackList[i]["track_type"]!)" == "B"){
                        bgTime = "\(trackList[i]["time"]!)"
                        break
                    }
                }
                let time = self.getTotalTime(totalTime: "\(self.meditationList[indexPath.section]["total_meditation_time"]!)", backgroundMusicTime: bgTime)
                
                customMeditationPlaylistViewController.isOriginal = "\(self.meditationList[indexPath.section]["is_original"]!)"
                customMeditationPlaylistViewController.totalTime = "\(time.time)"
                customMeditationPlaylistViewController.backgroundSeconds = time.backgroundSeconds
                customMeditationPlaylistViewController.totalSeconds = time.totalSeconds
                customMeditationPlaylistViewController.isDownloaded = true

                self.navigationController?.pushViewController(customMeditationPlaylistViewController, animated: true)
            }
        }else{
            let customMeditationPlaylistViewController = self.storyboard?.instantiateViewController(withIdentifier: "SavedTrackListViewController") as! SavedTrackListViewController
            customMeditationPlaylistViewController.responseArray = [appDelegate.downloadedMeditations[indexPath.section]]
            customMeditationPlaylistViewController.index = indexPath.section
            customMeditationPlaylistViewController.meditationID = "\(appDelegate.downloadedMeditations[indexPath.section]["meditation_id"]!)"
            let trackList = appDelegate.downloadedMeditations[indexPath.section]["track"] as! [[String : AnyObject]]
            
            var bgTime = "00:00"
            for i in 0...trackList.count - 1 {
                if("\(trackList[i]["track_type"]!)" == "B"){
                    bgTime = "\(trackList[i]["time"]!)"
                    break
                }
            }
            let time = self.getTotalTime(totalTime: "\(appDelegate.downloadedMeditations[indexPath.section]["total_meditation_time"]!)", backgroundMusicTime: bgTime)
            
            customMeditationPlaylistViewController.isOriginal = "\(appDelegate.downloadedMeditations[indexPath.section]["is_original"]!)"
            customMeditationPlaylistViewController.totalTime = "\(time.time)"
            customMeditationPlaylistViewController.backgroundSeconds = time.backgroundSeconds
            customMeditationPlaylistViewController.totalSeconds = time.totalSeconds
            self.navigationController?.pushViewController(customMeditationPlaylistViewController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }

    //MARK: - Button Actions -
    @IBAction func btnAddNewMeditation_Clicked(_ sender: Any) {
        if(appDelegate.reachable.isReachable){
            let customMeditationViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationViewController") as! CustomMeditationViewController
            self.navigationController?.pushViewController(customMeditationViewController, animated: true)
        }else{
            let alertController = UIAlertController(title: AppName, message: NoInternetMessage, preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            { action -> Void in

            })
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    
    func prepareForDownload(indexPath : IndexPath){
        soundArrayList.removeAll()
        trackNameArray.removeAll()
        totalProgressArray.removeAll()

        meditationData["title"] = self.meditationList[indexPath.section]["title"] as AnyObject
        meditationData["meditation_id"] = self.meditationList[indexPath.section]["custom_meditation_id"] as AnyObject
        meditationData["total_meditation_time"] = self.meditationList[indexPath.section]["total_meditation_time"] as AnyObject
        meditationData["description"] = "Need to set from backend" as AnyObject
        meditationData["track"] = self.meditationList[indexPath.section]["track"] as AnyObject
        if("\(self.meditationList[indexPath.section]["is_original"]!)" == "Y"){
            meditationData["is_original"] = "\(MadeByMeditable)" as AnyObject
        }else{
            meditationData["is_original"] = "\(MadeByYou)" as AnyObject
        }
        let trackArray : [[String : AnyObject]] = self.meditationData["track"] as! [[String : AnyObject]]
        for i in 0...trackArray.count - 1{
            soundArrayList.append("\(trackArray[i]["audio_url"]!)")
            let trackTitle = "\(trackArray[i]["title"]!)-\(meditationData["meditation_id"]!)\(trackArray[i]["track_type"]!)\(trackArray[i]["track_id"]!).mp3"
            trackNameArray.append("\(trackTitle)")
        }
    }

    
    @IBAction func btnDownload(sender: UIButton) {
        if(onlineLoad == true){
            let cell : customMeditationCell = sender.superview?.superview as? UITableViewCell as! customMeditationCell
            let indexPath = tblMeditationList.indexPath(for: cell)
            var downloading : Bool = false
            if(UserDefaults.standard.value(forKey: "startDownloading") != nil){
                downloading = UserDefaults.standard.value(forKey: "startDownloading") as! Bool
            }
            var found : Bool = false
            if(appDelegate.downloadedMeditations.count != 0){
                for i in 0...appDelegate.downloadedMeditations.count - 1{
                    if("\(appDelegate.downloadedMeditations[i]["meditation_id"]!)" == "\(self.meditationList[sender.tag]["custom_meditation_id"]!)"){
                        found = true
                        break
                    }else{
                        found = false
                    }
                }
            }else{
                cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadCloud"), for: UIControlState.normal)
            }
            if(found == false){
                if(isPause == "Pause" && downloadIndexpath == indexPath){
                    for i in 0...self.downloadManager.downloadingArray.count - 1{
                        self.downloadManager.resumeDownloadTaskAtIndex(i)
                    }
                    cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_control_stop"), for: UIControlState.normal)
                    isPause = "Play"
                }else if(isPause == "Play" && downloadIndexpath == indexPath){
                    for i in 0...self.downloadManager.downloadingArray.count - 1{
                        self.downloadManager.pauseDownloadTaskAtIndex(i)
                    }
                    cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_play"), for: UIControlState.normal)
                    isPause = "Pause"
                }else{
                    if(downloading == false){
                        isPause = "Play"
                        downloadIndexpath = indexPath!
                        cell.progressView.isHidden = false
                        cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_control_stop"), for: UIControlState.normal)
                        self.prepareForDownload(indexPath: indexPath!)
                        meditationID = "\(self.meditationList[indexPath!.section]["custom_meditation_id"]!)"
                        UserDefaults.standard.set(true, forKey: "startDownloading")
                        UserDefaults.standard.synchronize()
                        self.setDownloader()
                    }else{
                        UIAlertController().alertViewWithTitleAndMessage(self, message: "Please wait, Previous meditation download is in progress")
                    }
                }
            }else{
                
                let alertController = UIAlertController(title: AppName, message: DeleteDownloadedMeditaionMessage, preferredStyle:UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
                { action -> Void in
                    self.removeFromBundle(index: sender.tag)
                    if(appDelegate.downloadedMeditations.count != 0){
                        for i in 0...appDelegate.downloadedMeditations.count - 1{
                            if("\(appDelegate.downloadedMeditations[i]["meditation_id"]!)" == "\(self.meditationList[sender.tag]["custom_meditation_id"]!)"){
                                appDelegate.downloadedMeditations.remove(at: i)
                                break
                            }
                        }
                    }
                    let archiveData = NSKeyedArchiver.archivedData(withRootObject: appDelegate.downloadedMeditations)
                    defaults.set(archiveData, forKey: kdownloadedMeditations)
                    defaults.synchronize()
                    self.refreshList()
                })
                alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
                { action -> Void in
                    
                })
                self.present(alertController, animated: true, completion: nil)
//                UIAlertController().alertViewWithTitleAndMessage(self, message: "This meditation is already downloaded")
            }
        }
    }
    
    @IBAction func btnDelete(sender: UIButton) {
        var downloading : Bool = false
        if(defaults.value(forKey: "startDownloading") != nil){
            downloading = defaults.value(forKey: "startDownloading") as! Bool
        }
        if(downloading == true){
            for i in 0...self.downloadManager.downloadingArray.count - 1{
                self.downloadManager.cancelTaskAtIndex(i)
            }
            self.removeFromBundle(index: self.downloadIndexpath.section)
            defaults.set(false, forKey: "startDownloading")
        }
        isPause = ""
        
        let cell = sender.superview?.superview as? UITableViewCell
        if onlineLoad == true{
            let indexPath = tblMeditationList.indexPath(for: cell!)
            var found : Bool = false
            var deleteIndex = 0
            if(appDelegate.downloadedMeditations.count != 0){
                for i in 0...appDelegate.downloadedMeditations.count - 1{
                    if("\(appDelegate.downloadedMeditations[i]["meditation_id"]!)" == "\(self.meditationList[sender.tag]["custom_meditation_id"]!)"){
                        deleteIndex = i
                        found = true
                        break
                    }else{
                        found = false
                    }
                }
            }
            if(found == true){
                let alertController = UIAlertController(title: AppName, message: DeleteSavedAndListMeditaionMessage, preferredStyle:UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
                { action -> Void in
                    self.removeFromBundle(index: indexPath!.section)
                    appDelegate.downloadedMeditations.remove(at: deleteIndex)
                    let archiveData = NSKeyedArchiver.archivedData(withRootObject: appDelegate.downloadedMeditations)
                    defaults.set(archiveData, forKey: kdownloadedMeditations)
                    defaults.synchronize()
                    self.deleteMeditation(id: "\(self.meditationList[indexPath!.section]["custom_meditation_id"]!)", index: indexPath!.section)
//                    self.tblDownloaded.reloadData()
//                    self.refreshList()
                })
                alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
                { action -> Void in
                    
                })
                self.present(alertController, animated: true, completion: nil)
            }else{
                let alertController = UIAlertController(title: AppName, message: DeleteMeditaionMessage, preferredStyle:UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
                { action -> Void in
                    self.deleteMeditation(id: "\(self.meditationList[indexPath!.section]["custom_meditation_id"]!)", index: indexPath!.section)
                })
                alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
                { action -> Void in
                    
                })
                self.present(alertController, animated: true, completion: nil)
            }
        }else{
            let indexPath = tblDownloaded.indexPath(for: cell!)
            let alertController = UIAlertController(title: AppName, message: DeleteDownloadedMeditaionMessage, preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
            { action -> Void in
                self.removeFromBundle(index: indexPath!.section)
                appDelegate.downloadedMeditations.remove(at: indexPath!.section)
                let archiveData = NSKeyedArchiver.archivedData(withRootObject: appDelegate.downloadedMeditations)
                defaults.set(archiveData, forKey: kdownloadedMeditations)
                defaults.synchronize()
                self.tblDownloaded.reloadData()
                NotificationCenter.default.post(name: NSNotification.Name.init("refreshMeditaionList"), object: nil)
                if(appDelegate.downloadedMeditations.count == 0){
                    self.lblNoDataFound.isHidden = false
                    self.tblDownloaded.isHidden = true
                }else{
                    self.lblNoDataFound.isHidden = true
                    self.tblDownloaded.isHidden = false
                }
            })
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
            { action -> Void in
                
            })
            self.present(alertController, animated: true, completion: nil)
            }
    }
    
    func deleteMeditation(id : String, index : Int){
        if appDelegate.reachable.isReachable{
            
                var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
                if authKey == nil {
                    authKey = ""
                }
                
                var params = Parameters()
                
                params = ["auth_key": authKey, "custom_meditation_id" : "\(id)"]
                
                
                let size = CGSize(width: 75, height: 75)
                let color : UIColor = .white
                startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
                
                Alamofire.request(baseURLWithAuth + kDeleteMeditation, method: .post, parameters: params).validate().responseJSON {
                    response in
                    self.stopAnimating()
                    switch response.result {
                    case .success:
                        if let json: NSDictionary = response.result.value as? NSDictionary{
                            UIAlertController().alertViewWithTitleAndMessage(self, message: "\(json["message"]!)")
                            if json.value(forKey: "status") as? Int == 1{
                                self.meditationList.remove(at: index)
                                if(self.meditationList.count == 0){
                                    self.lblNoDataFound.isHidden = false
                                    NotificationCenter.default.post(name: NSNotification.Name.init("CreateMeditationShow"), object: nil)
                                }else{
                                    self.lblNoDataFound.isHidden = true
                                }
                                self.tblMeditationList.reloadData()
                            }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                                
                                let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                                alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                                { action -> Void in
                                    let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                    
                                    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                    defaults.set(deviceToken,forKey: "DeviceToken")
                                    defaults.synchronize()
                                    
                                    let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                    self.navigationController?.pushViewController(welcomeViewController, animated: true)
                                })
                                self.present(alertController, animated: true, completion: nil)
                                
                            }

                        }else {

                        }
                    case .failure(let error):
                        UIAlertController().alertViewWithErrorMessage(self)

                    }
                }
            }
            else {
                UIAlertController().alertViewWithNoInternet(self)
            }
    }
    
    func getCustomMeditationList() {
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetCustomMeditationList, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            self.meditationList = json.value(forKey: "data") as! [[String : AnyObject]]
                            print(json)
                            if self.meditationList.count > 0{
                                self.lblNoDataFound.isHidden = true
                                self.tblMeditationList.reloadData()
                            }else{
                                NotificationCenter.default.post(name: NSNotification.Name.init("CreateMeditationShow"), object: nil)
                                self.lblNoDataFound.isHidden = false
                            }
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                            
                        }

                        
                        
                    }else {

                    }
                case .failure(let error):
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
//            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    //MARK:- Downloader Manager - 
    func setDownloader(){
        
        for i in 0...soundArrayList.count - 1{
            let fileURL  : NSString = soundArrayList[i] as NSString
            var fileName : NSString = fileURL.lastPathComponent as NSString
            fileName = trackNameArray[i] as NSString //MZUtility.getUniqueFileNameWithPath((myDownloadPath as NSString).appendingPathComponent(fileName as String) as NSString)
            self.downloadManager.addDownloadTask(fileName as String, fileURL: fileURL as String, destinationPath: appDelegate.myDownloadPath)
            totalProgressArray.append(0.0)
        }
    }
    
    func downloadProgress(indexPath : IndexPath, downloadModel: MZDownloadModel){
        DispatchQueue.global().async {
            
            if(downloadModel.progress != 1.0 && self.totalProgressArray[indexPath.row] != 1.0){
                self.totalProgressArray[indexPath.row] = downloadModel.progress
            }else{
                self.totalProgressArray.remove(at: indexPath.row)
                self.totalProgressArray.append(downloadModel.progress)
            }
            let totalSum = self.totalProgressArray.reduce(0,+)
            let totalProgress = (totalSum * 100) / Float(self.totalProgressArray.count)
            print("************************************Total Progress - \(totalProgress)")
            
            for indexPath in self.tblMeditationList.indexPathsForVisibleRows! {
                if (indexPath.section == self.downloadIndexpath.section) {
                    if let cell : customMeditationCell = self.tblMeditationList.cellForRow(at: self.downloadIndexpath) as? customMeditationCell{
                        DispatchQueue.global().async {
                            cell.progressView.setProgress(value: CGFloat(totalProgress), animationDuration: 0)
                        }
                    }
                }
            }
        }
    }
    
    func saveToLocalData(){
        if(appDelegate.downloadedMeditations.count > 0){
            for i in 0...appDelegate.downloadedMeditations.count - 1{
                if("\(appDelegate.downloadedMeditations[i]["meditation_id"]!)" == meditationID){
                    appDelegate.downloadedMeditations.remove(at: i)
                    break
                }
            }
        }
        appDelegate.downloadedMeditations.insert(meditationData, at: 0)
        let archiveData = NSKeyedArchiver.archivedData(withRootObject: appDelegate.downloadedMeditations)
        defaults.set(archiveData, forKey: kdownloadedMeditations)
        defaults.synchronize()
        meditationID = ""
        
        tblMeditationList.reloadData()
        
//        let cell : customMeditationCell = tblMeditationList.cellForRow(at: downloadIndexpath) as! customMeditationCell
//        cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadedCloud"), for: UIControlState.normal)
//        cell.progressView.isHidden = true
//        cell.btnDownload.isHidden = false
    }
    func refreshCellForIndex(_ downloadModel: MZDownloadModel, index: Int) {
        let indexPath = IndexPath.init(row: index, section: 0)
        self.downloadProgress(indexPath: indexPath, downloadModel: downloadModel)
    }
    // MARK: UIAlertController Handler Extension
    
    func showAppropriateActionController(_ requestStatus: String) {
        
        if requestStatus == TaskStatus.downloading.description() {
            self.showAlertControllerForPause()
        } else if requestStatus == TaskStatus.failed.description() {
            self.showAlertControllerForRetry()
        } else if requestStatus == TaskStatus.paused.description() {
            self.showAlertControllerForStart()
        }
    }
    
    func showAlertControllerForPause() {
        
        let pauseAction = UIAlertAction(title: "Pause", style: .default) { (alertAction: UIAlertAction) in
            self.downloadManager.pauseDownloadTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let removeAction = UIAlertAction(title: "Remove", style: .destructive) { (alertAction: UIAlertAction) in
            self.downloadManager.cancelTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.view.tag = alertControllerViewTag
        alertController.addAction(pauseAction)
        alertController.addAction(removeAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertControllerForRetry() {
        
        let retryAction = UIAlertAction(title: "Retry", style: .default) { (alertAction: UIAlertAction) in
            self.downloadManager.retryDownloadTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let removeAction = UIAlertAction(title: "Remove", style: .destructive) { (alertAction: UIAlertAction) in
            self.downloadManager.cancelTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.view.tag = alertControllerViewTag
        alertController.addAction(retryAction)
        alertController.addAction(removeAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertControllerForStart() {
        
        let startAction = UIAlertAction(title: "Start", style: .default) { (alertAction: UIAlertAction) in
            self.downloadManager.resumeDownloadTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let removeAction = UIAlertAction(title: "Remove", style: .destructive) { (alertAction: UIAlertAction) in
            self.downloadManager.cancelTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.view.tag = alertControllerViewTag
        alertController.addAction(startAction)
        alertController.addAction(removeAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func safelyDismissAlertController() {
        /***** Dismiss alert controller if and only if it exists and it belongs to MZDownloadManager *****/
        /***** E.g App will eventually crash if download is completed and user tap remove *****/
        /***** As it was already removed from the array *****/
        if let controller = self.presentedViewController {
            guard controller is UIAlertController && controller.view.tag == alertControllerViewTag else {
                return
            }
            controller.dismiss(animated: true, completion: nil)
        }
    }
    
    func downloadRequestStarted(_ downloadModel: MZDownloadModel, index: Int) {

    }
    
    func downloadRequestDidPopulatedInterruptedTasks(_ downloadModels: [MZDownloadModel]) {

    }
    
    func downloadRequestDidUpdateProgress(_ downloadModel: MZDownloadModel, index: Int) {
        self.refreshCellForIndex(downloadModel, index: index)
    }
    
    func downloadRequestDidPaused(_ downloadModel: MZDownloadModel, index: Int) {
        self.refreshCellForIndex(downloadModel, index: index)
    }
    
    func downloadRequestDidResumed(_ downloadModel: MZDownloadModel, index: Int) {
        self.refreshCellForIndex(downloadModel, index: index)
    }
    
    func downloadRequestCanceled(_ downloadModel: MZDownloadModel, index: Int) {
        self.safelyDismissAlertController()
    }
    
    func downloadRequestFinished(_ downloadModel: MZDownloadModel, index: Int) {
        if(downloadManager.downloadingArray.count == 0){
            print("All track Downloaded")
            defaults.set(false, forKey: "startDownloading")
            defaults.synchronize()
            saveToLocalData()
            
            for indexPath in self.tblMeditationList.indexPathsForVisibleRows! {
                if (indexPath.section == self.downloadIndexpath.section) {
                    if let cell : customMeditationCell = self.tblMeditationList.cellForRow(at: self.downloadIndexpath) as? customMeditationCell{
                        DispatchQueue.global().async {
                            cell.progressView.setProgress(value: CGFloat(100.0), animationDuration: 0)
                        }
                    }
                }
            }
        }
        self.safelyDismissAlertController()
        
//        downloadManager.presentNotificationForDownload("Ok", notifBody: "Download did completed")
        
        let indexPath = IndexPath.init(row: index, section: 0)
//        tblView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
        
        let docDirectoryPath : NSString = (MZUtility.baseFilePath as NSString).appendingPathComponent(downloadModel.fileName) as NSString
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: MZUtility.DownloadCompletedNotif as String), object: docDirectoryPath)
    }
    
    func downloadRequestDidFailedWithError(_ error: NSError, downloadModel: MZDownloadModel, index: Int) {
        self.safelyDismissAlertController()
        self.refreshCellForIndex(downloadModel, index: index)
        
        debugPrint("Error while downloading file: \(downloadModel.fileName)  Error: \(error)")
    }
    
    //Oppotunity to handle destination does not exists error
    //This delegate will be called on the session queue so handle it appropriately
    func downloadRequestDestinationDoestNotExists(_ downloadModel: MZDownloadModel, index: Int, location: URL) {
        let myDownloadPath = MZUtility.baseFilePath + "/Default folder"
        if !FileManager.default.fileExists(atPath: myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        let fileName = MZUtility.getUniqueFileNameWithPath((myDownloadPath as NSString).appendingPathComponent(downloadModel.fileName as String) as NSString)
        let path =  myDownloadPath + "/" + (fileName as String)
        try! FileManager.default.moveItem(at: location, to: URL(fileURLWithPath: path))
        debugPrint("Default folder path: \(myDownloadPath)")
    }
    
    
    func generateRandomStringWithLength(length:Int) -> String {
        
        let randomString:NSMutableString = NSMutableString(capacity: length)
        
        let letters:NSMutableString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        var i: Int = 0
        
        while i < length {
            
            let randomIndex:Int = Int(arc4random_uniform(UInt32(letters.length)))
            randomString.append("\(Character( UnicodeScalar( letters.character(at: randomIndex))!))")
            i += 1
        }
        
        return String(randomString)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class customMeditationCell : UITableViewCell{
    @IBOutlet weak var btnLock: UIButton!
    @IBOutlet weak var lblMeditationTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnMeditationType: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var progressView: UICircularProgressRingView!
    @IBOutlet weak var leftConstraintTitle: NSLayoutConstraint!
}
