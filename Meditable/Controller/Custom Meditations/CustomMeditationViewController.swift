//
//  CustomMeditationViewController.swift
//  Meditable
//
//  Created by Apple on 19/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON
import TPKeyboardAvoiding
import AVFoundation
import AudioToolbox


class CustomMeditationViewController: UIViewController, NVActivityIndicatorViewable, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var lblMeditations: UILabel!
    
    @IBOutlet weak var lblCreateMeditations: UILabel!
    
    @IBOutlet weak var txtMeditationsName: UITextField!
    
    @IBOutlet weak var vwMeditationsName: UIView!
    
    @IBOutlet weak var tblTracks: UITableView!
    
    @IBOutlet weak var btnPlayMeditation: UIButton!
    
    @IBOutlet weak var tblTracksHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var saveTrackNameView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var trackDetailsView: UIView!
    
    @IBOutlet weak var lblSaveCustomMeditation: UILabel!
    
    @IBOutlet weak var txtTrackName: CustomTextfield!
    
    @IBOutlet weak var btnSaveTrackName: UIButton!
    
    @IBOutlet weak var btnClose1: UIButton!
    
    @IBOutlet weak var btnClose1HeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var trackOptionsView: UIView!
    
    @IBOutlet weak var tblTrackOptions: UITableView!
    
    @IBOutlet weak var btnClose2: UIButton!
    
    @IBOutlet weak var introTrackOptionsView: UIView!
    
    @IBOutlet weak var tblIntroTrackOptions: UITableView!
    
    @IBOutlet weak var btnClose3: UIButton!
    
    @IBOutlet weak var endTracksOptionsView: UIView!
    
    @IBOutlet weak var tblEndTrackOptions: UITableView!
    
    @IBOutlet weak var btnClose4: UIButton!
    
    @IBOutlet weak var lblTotalTrackTime: UILabel!

    var trackOptionsList = ["Add Silence 30 Sec", "Move Up", "Move Down", "Add to Another Meditation", "Delete"]
    
    var introOptionsList = ["Change Intro", "Add Silence 30 Sec"]
    
    var endOptionsList = ["Change Ending"]
    
    var defaultMeditationList = [[String: AnyObject]]()
    var backgroundList = [String : AnyObject]()
    var silenceList = [String : AnyObject]()
    var btnindex = Int()
    var player = AVPlayer()
    var urlString = ""
    var editIndex = Int()
    var totalTime = ""
    var backgroundSeconds : Int = 0
    var totalSeconds : Int = 0
    var currentPlayIndex = 500
    var isEdited = false
    var trackToAdd = [String : AnyObject]()
    
    //MARK: - View LifeCycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getDefaultMeditationList()

        self.btnindex = -1

        self.vwMeditationsName.layer.cornerRadius = 5.0
        self.vwMeditationsName.layer.borderColor = UIColor.white.cgColor
        self.vwMeditationsName.layer.borderWidth = 1
        
        btnSave.layer.cornerRadius = btnSave.frame.size.height / 2
        btnPlayMeditation.layer.cornerRadius = btnPlayMeditation.frame.size.height / 2
        btnPlayMeditation.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnPlayMeditation.layer.borderWidth = 1
        
        txtTrackName.layer.borderColor = UIColor.black.cgColor
        txtTrackName.layer.borderWidth = 1.0
        txtTrackName.layer.cornerRadius = 5.0
        txtTrackName.layer.masksToBounds = true
        
        btnSaveTrackName.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnSaveTrackName.layer.borderWidth = 1.0
        btnSaveTrackName.layer.cornerRadius = btnSaveTrackName.frame.size.height / 2
        btnSaveTrackName.layer.masksToBounds = true
        
        trackDetailsView.layer.cornerRadius = 5.0
        saveTrackNameView.layer.masksToBounds = true
        
        tblTrackOptions.layer.cornerRadius = 5.0
        tblTrackOptions.layer.masksToBounds = true
        
        tblIntroTrackOptions.layer.cornerRadius = 5.0
        tblIntroTrackOptions.layer.masksToBounds = true
        
        tblEndTrackOptions.layer.cornerRadius = 5.0
        tblEndTrackOptions.layer.masksToBounds = true
        
        tblTracks.rowHeight = UITableViewAutomaticDimension
        tblTracks.estimatedRowHeight = 90
        
        if IS_IPHONE4s || IS_IPHONE5 {
            btnClose1HeightConstraint.constant = 94
//            tblTracksHeightConstraint.constant = (90 * 4)  //(row height 80 * row count 4)
        }
        else if IS_IPHONE6 {
            btnClose1HeightConstraint.constant = 144
//            tblTracksHeightConstraint.constant = (85 * 4) + 60 //(row height 80 * row count 4)
        }
        else {
            btnClose1HeightConstraint.constant = 178
//            tblTracksHeightConstraint.constant = (95 * 4) + 70  //(row height 80 * row count 4)
        }
        
        

        
        NotificationCenter.default.addObserver(self, selector: #selector(CustomMeditationViewController.addNewTrackFromList(notification:)), name: Notification.Name("SelectedTrackToAddInCreaeList"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CustomMeditationViewController.musicChange(sender: )), name: NSNotification.Name.init("changeMusicCreateMeditaion"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(CustomMeditationViewController.itemDidFinishPlaying(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        addGoogleAnalytics(screenName: "Create Custom Meditation")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        if(isEdited == true){
            let alertController = UIAlertController(title: AppName, message: LikeToSaveEditedMeditation, preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
            { action -> Void in
                self.btnSave_Clicked(self.btnSave)
            })
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
            { action -> Void in
                let transition = CATransition()
                transition.duration = 0.3
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transition.type = kCATransitionPush
                self.navigationController?.view.layer.add(transition, forKey: nil)
                _ = self.navigationController?.popViewController(animated: false)
            })
            self.present(alertController, animated: true, completion: nil)
        }else{
            let transition = CATransition()
            transition.duration = 0.3
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionPush
            self.navigationController?.view.layer.add(transition, forKey: nil)
            _ = self.navigationController?.popViewController(animated: false)
        }
        //_ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSave_Clicked(_ sender: Any) {
        self.txtMeditationsName.resignFirstResponder()
        backgroundView.isHidden = false
        saveTrackNameView.isHidden = false
        txtTrackName.text = "\(txtMeditationsName.text!)"
    }
    
    //MARK:- Play meditation event
    
    func addTrackLog(index : Int){
        var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
        if authKey == nil {
            authKey = ""
        }
        let trackType = "\(self.defaultMeditationList[index]["track_type"]!)"
        let param = ["auth_key": authKey, "track_id" : "\(self.defaultMeditationList[index]["track_id"]!)", "track_type" : trackType, "meditation_type" : "C"]
        APIManager().AddTrackLogs(parameters: param as! [String : String]) { (response, error) in
            if(error == nil){
//                print(response!)
            }else{
                if(error?.code == 5 || error?.code == 3){
                    let alertController = UIAlertController(title: AppName, message: "Unauthorized", preferredStyle:UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                    { action -> Void in
                        let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        defaults.set(deviceToken,forKey: "DeviceToken")
                        defaults.synchronize()
                        let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                        self.navigationController?.pushViewController(welcomeViewController, animated: true)
                    })
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }

    
    
    @IBAction func btnPlayMeditation_Clicked(_ sender: UIButton) {
        let playMeditationViewController = self.storyboard?.instantiateViewController(withIdentifier: "PlayMeditationViewController") as! PlayMeditationViewController
        playMeditationViewController.trackList = self.defaultMeditationList
        playMeditationViewController.backgroundSound = backgroundList
        playMeditationViewController.silenceSound = silenceList
        playMeditationViewController.meditationTitle = "\(txtMeditationsName.text!)"
        playMeditationViewController.totalTrackTime = "\(getTotalTrackTime(track: defaultMeditationList).time)"
        playMeditationViewController.meditaionBy = "\(MadeByYou)"

        
        
        self.txtMeditationsName.resignFirstResponder()
        self.navigationController?.pushViewController(playMeditationViewController, animated: true)
    }
    
    @IBAction func btnSaveTrackName_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        saveTrackNameView.isHidden = true
        self.txtMeditationsName.resignFirstResponder()
        txtTrackName.resignFirstResponder()
        
        if(txtTrackName.text!.isEmptyField){
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Meditaion name should not be empty.")
        }else{
            if appDelegate.reachable.isReachable{
                
                var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
                if authKey == nil {
                    authKey = ""
                }
                var params = Parameters()
                
                var trackID = ""
                var trackType = ""
                
                for index in 0...defaultMeditationList.count - 1{
                    if(index == 0){
                        trackID.append("\(defaultMeditationList[index]["track_id"]!)")
                        trackType.append("\(defaultMeditationList[index]["track_type"]!)")
                    }else{
                        trackID.append(",\(defaultMeditationList[index]["track_id"]!)")
                        trackType.append(",\(defaultMeditationList[index]["track_type"]!)")
                    }
                }
                if(backgroundList.count != 0){
                    trackID.append(",\(backgroundList["track_id"]!)")
                    trackType.append(",\(backgroundList["track_type"]!)")
                }
                
                params = ["auth_key": authKey, "track_ids": trackID, "track_types" : trackType, "title" : txtTrackName.text!, "is_original" : "N"]
                
                
                let size = CGSize(width: 75, height: 75)
                let color : UIColor = .white
                startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
                
                Alamofire.request(baseURLWithAuth + kSaveCustomList, method: .post, parameters: params).validate().responseJSON { response in
                    self.stopAnimating()
                    
                    switch response.result {
                    case .success:
                        if let json: NSDictionary = response.result.value as? NSDictionary{
                            if json.value(forKey: "status") as? Int == 1{
                                let responseResult = response.result.value as! [String : Any]
                                
                                UIAlertController().alertViewWithTitleAndMessage(self, message: "\(responseResult["message"]!)")
                                self.btnSave.isHidden = true
                                NotificationCenter.default.post(name: NSNotification.Name.init("refreshMeditaionList"), object: nil)
                                NotificationCenter.default.post(name: NSNotification.Name.init("SetCustomeMeditationAdded"), object: nil)

                                
//                                let alertSuccess: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: "\(responseResult["message"]!)", preferredStyle: .alert)
//                                let btnOk: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
//                                    _ in
//                                    let transition = CATransition()
//                                    transition.duration = 0.3
//                                    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//                                    transition.type = kCATransitionPush
//                                    self.navigationController?.view.layer.add(transition, forKey: nil)
//                                    _ = self.navigationController?.popViewController(animated: false)
//                                })
//                                alertSuccess.addAction(btnOk)
//                                self.present(alertSuccess, animated: true, completion: nil)
                                
                            }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                                
                                let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                                alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                                { action -> Void in
                                    let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                    
                                    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                    defaults.set(deviceToken,forKey: "DeviceToken")
                                    defaults.synchronize()
                                    
                                    let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                    self.navigationController?.pushViewController(welcomeViewController, animated: true)
                                })
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                       
                        
                    case .failure(let error):
                        UIAlertController().alertViewWithErrorMessage(self)
                    }
                }
            }
            else {
                UIAlertController().alertViewWithNoInternet(self)
            }
        }

        
    }


    @IBAction func btnClose1_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        saveTrackNameView.isHidden = true
    }
    
    @IBAction func btnClose2_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        trackOptionsView.isHidden = true
    }
    
    @IBAction func btnClose3_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        introTrackOptionsView.isHidden = true
    }
    
    @IBAction func btnClose4_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        endTracksOptionsView.isHidden = true
    }
    
    //Edit Track
    func editTrackDetails(sender: UIButton) {
        self.txtMeditationsName.resignFirstResponder()
        let buttonPosition: CGPoint = sender.convert(.zero, to: tblTracks)
        let indexPath: NSIndexPath = tblTracks.indexPathForRow(at: buttonPosition) as NSIndexPath!
        
        editIndex = indexPath.row
        
        if(btnindex != -1){
            DispatchQueue.main.async {
                self.player.pause()
                self.tblTracks.reloadData()
                self.btnindex = -1
            }
        }
        
        if(indexPath.row == 0 || indexPath.row == self.defaultMeditationList.count){
            if indexPath.row == 0 {
                backgroundView.isHidden = false
                introTrackOptionsView.isHidden = false
                self.tblIntroTrackOptions.frame.size.height = CGFloat(self.introOptionsList.count * 60)
                
            }
            else if indexPath.row == self.defaultMeditationList.count{
                backgroundView.isHidden = false
                endTracksOptionsView.isHidden = false
                self.tblEndTrackOptions.frame.size.height = CGFloat(self.endOptionsList.count * 60)
                
            }
        }else{
            if(indexPath.row == 1){
                trackOptionsList = ["Add Silence 30 Sec", "Move Down", "Add to Another Meditation", "Delete"]
            }else if(indexPath.row == defaultMeditationList.count - 2){
                trackOptionsList = ["Add Silence 30 Sec", "Move Up", "Add to Another Meditation", "Delete"]
            }else if(indexPath.row != defaultMeditationList.count - 1){
                trackOptionsList = ["Add Silence 30 Sec", "Move Up", "Move Down", "Add to Another Meditation", "Delete"]
            }
            if(defaultMeditationList.count <= 3){
                trackOptionsList = ["Add Silence 30 Sec", "Add to Another Meditation", "Delete"]
            }
            self.tblTrackOptions.frame.size.height = CGFloat(self.trackOptionsList.count * 60)
            tblTrackOptions.reloadData()
            backgroundView.isHidden = false
            trackOptionsView.isHidden = false
        }
    }
    
    //Add new track from tracklist
    func addNewTrackFromList(notification: Notification){
        //Take Action on Notification
        if let notificationArray = notification.object as? [String : AnyObject]  {
            currentPlayIndex = 500
            self.defaultMeditationList.insert(notificationArray , at: self.defaultMeditationList.count - 1)
            tblTracks.reloadData()
            lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: defaultMeditationList).time)"
        }
        isEdited = true
        tblTracksHeightConstraint.constant = CGFloat((self.defaultMeditationList.count + 1) * 80)
    }
    
    //MARK:- Change Into Edit Track
    
    func musicChange (sender: Notification) {
        if let senderArray = sender.object as? [String : AnyObject] {
            if "\(senderArray["track_type"]!)" == "I"{
                self.defaultMeditationList.remove(at: 0)
                self.defaultMeditationList.insert(senderArray, at: 0)
            }
            if "\(senderArray["track_type"]!)" == "E"{
                self.defaultMeditationList.removeLast()
                self.defaultMeditationList.append(senderArray)
            }
        }
        isEdited = true
        tblTracks.reloadData()
        lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: defaultMeditationList).time)"
    }
    
    //MARK:- Add New Track
    func addNewTrack(sender: UIButton) {
        let addTracksViewController: AddTracksViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddTracksViewController") as! AddTracksViewController
        self.txtMeditationsName.resignFirstResponder()
        self.navigationController?.pushViewController(addTracksViewController, animated: true)
    }
    
    //MARK:- Check Track Info
    func checkTrackInfo(sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(.zero, to: tblTracks)
        let indexPath: NSIndexPath = tblTracks.indexPathForRow(at: buttonPosition) as NSIndexPath!
        
        let trackInfoViewController: TrackInfoViewController = self.storyboard?.instantiateViewController(withIdentifier: "TrackInfoViewController") as! TrackInfoViewController
        if(indexPath.row == defaultMeditationList.count){
            trackInfoViewController.trackList =  defaultMeditationList[indexPath.row - 1]
        }else{
            trackInfoViewController.trackList =  defaultMeditationList[indexPath.row]
        }
        self.txtMeditationsName.resignFirstResponder()
        self.navigationController?.pushViewController(trackInfoViewController, animated: true)
    }
    
    //Play Track
    func playTrack(sender: UIButton) {
        self.txtMeditationsName.resignFirstResponder()
        let indexPath: NSIndexPath = self.tblTracks.indexPath(for: (sender.superview?.superview as! UITableViewCell))! as NSIndexPath
        editIndex = indexPath.row

        if btnindex == indexPath.row{
            btnindex = -1
        }else{
            btnindex = indexPath.row
        }
        if(indexPath.row == defaultMeditationList.count){
            urlString = (defaultMeditationList[indexPath.row - 1]["audio_url"] as? String)!
        }else{
            urlString = (defaultMeditationList[indexPath.row]["audio_url"] as? String)!
        }
        if urlString != ""{
            urlString = urlString.replacingOccurrences(of: " ", with: "%20")
            let url = URL.init(string: urlString)
            do {
                let avPlayerItem: AVPlayerItem = AVPlayerItem.init(url: url!)
                if(currentPlayIndex != indexPath.row){
                    currentPlayIndex = indexPath.row
                    player = AVPlayer.init(playerItem: avPlayerItem)
                }
                if #available(iOS 10.0, *) {
                    player.automaticallyWaitsToMinimizeStalling = false
                } else {
                    // Fallback on earlier versions
                }
                if(sender.isSelected == false){
                    let size = CGSize(width: 75, height: 75)
                    let color : UIColor = .white
                    startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
                    self.player.addObserver(self, forKeyPath: "status", options: .initial, context: nil)
                    if(indexPath.row == defaultMeditationList.count){
                        self.addTrackLog(index: indexPath.row - 1)
                    }else{
                        self.addTrackLog(index: indexPath.row)
                    }
                    player.play()
                }else{
                    player.pause()
                    player.rate = 0
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
        self.tblTracks.reloadData()
    }
    
    
    func itemDidFinishPlaying(notification:NSNotification) {
        player.pause()
        player.rate = 0
        self.btnindex = -1
        let indexPath = IndexPath(item: editIndex, section: 0)
        tblTracks.reloadRows(at: [indexPath], with: .none)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if (keyPath == "status") {
            if (player.status == .readyToPlay) {
                self.player.removeObserver(self, forKeyPath: "status", context: nil)
                self.stopAnimating()
            }
        }
    }
    
    //MARK: - GetMeditationList API
    func getDefaultMeditationList() {
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetDefaultmeditationtrack, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            self.defaultMeditationList = json.value(forKey: "track") as! [[String : AnyObject]]
//                            self.defaultMeditationList = responseResult[0]["track"] as! [[String : AnyObject]]
                            
                            if self.defaultMeditationList.count > 0{
                                
                                //For background and Silence Audio
                                for i in 0...self.defaultMeditationList.count - 1 {
                                    if("\(self.defaultMeditationList[i]["track_type"]!)" == "B"){
                                        self.backgroundList = self.defaultMeditationList[i]
                                        self.defaultMeditationList.remove(at: i)
                                        break
                                    }
                                }
                                for i in 0...self.defaultMeditationList.count - 1 {
                                    if("\(self.defaultMeditationList[i]["track_type"]!)" == "S"){
                                        self.silenceList = self.defaultMeditationList[i]
                                        self.defaultMeditationList.remove(at: i)
                                        break
                                    }
                                }
                                if(self.trackToAdd.count != 0){
                                    for i in 0...self.defaultMeditationList.count - 1 {
                                        if("\(self.defaultMeditationList[i]["track_type"]!)" == "I"){
                                            self.defaultMeditationList.insert(self.trackToAdd, at: i+1)
                                        }
                                    }
                                }
                                
                                self.tblTracksHeightConstraint.constant = CGFloat((self.defaultMeditationList.count + 1) * 80)
                                self.tblTracks.reloadData()
                                self.setTotalTime()
                            }else{
                                let alertSuccess: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: "No default list available.", preferredStyle: .alert)
                                
                                let btnOk: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
                                    _ in
                                    let transition = CATransition()
                                    transition.duration = 0.3
                                    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                                    transition.type = kCATransitionPush
                                    self.navigationController?.view.layer.add(transition, forKey: nil)
                                    _ = self.navigationController?.popViewController(animated: true)
                                })
                                
                                alertSuccess.addAction(btnOk)
                                self.present(alertSuccess, animated: true, completion: nil)
                            }
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                    }else {
                        
                    }
                case .failure(let error):
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func setTotalTime(){
        self.lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: self.defaultMeditationList).time)"
    }
    
    //TableView Delegate & Data source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblTracks {
            if(self.defaultMeditationList.count != 0){
                return self.defaultMeditationList.count + 1
            }else{
                return self.defaultMeditationList.count
            }
        }
        else if tableView == tblIntroTrackOptions {
            return introOptionsList.count
        }
        else if tableView == tblEndTrackOptions {
            return endOptionsList.count
        }
        else {
            return trackOptionsList.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        if tableView == tblTracks {
            let cell: TimeLineTableViewCell! = tblTracks.dequeueReusableCell(withIdentifier: cellIdentifier) as! TimeLineTableViewCell
            if(indexPath.row == 0){
                cell.topTitleConstraint.constant = 5
                cell.lblTrackName.text = self.defaultMeditationList[indexPath.row]["title"] as? String
                cell.lblTrackName.font = UIFont(name: "FuturaBT-Medium", size: 16.0)
                if let trackTime = self.defaultMeditationList[indexPath.row]["time"] as? String {
                    cell.lblTrackTime.text = "\(trackTime)"
                }
                
                cell.lblTrackName.textColor = UIColor().yellowBorderColor
                
                cell.btnEditTrackName.removeTarget(self, action: #selector(CustomMeditationViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.removeTarget(self, action: #selector(CustomMeditationViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.removeTarget(self, action: #selector(CustomMeditationViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnEditTrackName.addTarget(self, action: #selector(CustomMeditationViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.addTarget(self, action: #selector(CustomMeditationViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.addTarget(self, action: #selector(CustomMeditationViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnInfo.isHidden = false
                cell.btnPlay.isHidden = false
                cell.btnEditTrackName.isHidden = false
                
            }else if(indexPath.row == self.defaultMeditationList.count - 1){
                cell.topTitleConstraint.constant = 20
                cell.lblTrackName.text = "Add Track"
                cell.lblTrackTime.text = ""
                cell.lblTrackName.font = UIFont(name: "FuturaBT-MediumItalic", size: 16.0)
                cell.lblTrackName.textColor = UIColor.white
                cell.lblTrackName.sizeToFit()
                cell.btnEditTrackName.removeTarget(self, action: #selector(CustomMeditationViewController.addNewTrack(sender: )), for: .touchUpInside)
                cell.btnEditTrackName.addTarget(self, action: #selector(CustomMeditationViewController.addNewTrack(sender: )), for: .touchUpInside)
                cell.btnInfo.isHidden = true
                cell.btnPlay.isHidden = true
                cell.btnEditTrackName.isHidden = true
                
            }else if(indexPath.row == self.defaultMeditationList.count){
                cell.topTitleConstraint.constant = 5
                cell.lblTrackName.text = self.defaultMeditationList[indexPath.row - 1]["title"] as? String
                cell.lblTrackName.font = UIFont(name: "FuturaBT-Medium", size: 16.0)
                if let trackTime = self.defaultMeditationList[indexPath.row - 1]["time"] as? String {
                    cell.lblTrackTime.text = "\(trackTime)"
                }
                cell.lblTrackName.textColor = UIColor().yellowBorderColor
                cell.lblTrackName.sizeToFit()
                
                
                
                cell.btnEditTrackName.removeTarget(self, action: #selector(CustomMeditationViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.removeTarget(self, action: #selector(CustomMeditationViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.removeTarget(self, action: #selector(CustomMeditationViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnEditTrackName.addTarget(self, action: #selector(CustomMeditationViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.addTarget(self, action: #selector(CustomMeditationViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.addTarget(self, action: #selector(CustomMeditationViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnInfo.isHidden = false
                cell.btnPlay.isHidden = false
                cell.btnEditTrackName.isHidden = false
                
            }else{
                cell.topTitleConstraint.constant = 5
                cell.lblTrackName.text = self.defaultMeditationList[indexPath.row]["title"] as? String
                cell.lblTrackName.font = UIFont(name: "FuturaBT-Medium", size: 16.0)
                if let trackTime = self.defaultMeditationList[indexPath.row]["time"] as? String {
                    cell.lblTrackTime.text = "\(trackTime)"
                }
                cell.lblTrackName.textColor = UIColor().yellowBorderColor
                
                
                cell.btnEditTrackName.removeTarget(self, action: #selector(CustomMeditationViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.removeTarget(self, action: #selector(CustomMeditationViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.removeTarget(self, action: #selector(CustomMeditationViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnEditTrackName.addTarget(self, action: #selector(CustomMeditationViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.addTarget(self, action: #selector(CustomMeditationViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.addTarget(self, action: #selector(CustomMeditationViewController.playTrack(sender: )), for: .touchUpInside)
                
                let strTrackType = self.defaultMeditationList[indexPath.row]["track_type"] as! String
                if strTrackType == "S" {
                    cell.btnPlay.isHidden = true
                } else {
                    cell.btnPlay.isHidden = false
                }
                cell.btnInfo.isHidden = false
                cell.btnEditTrackName.isHidden = false
            }
            
            cell.allRows = self.defaultMeditationList.count + 1
            cell.currentIndexPath = indexPath
            cell.setNeedsDisplay()
            
            cell.isUserInteractionEnabled = true
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            if btnindex == indexPath.row{
                cell.btnPlay.isSelected = true
                cell.btnPlay.setImage(#imageLiteral(resourceName: "Stop"), for: UIControlState.selected)
            }else{
                cell.btnPlay.isSelected = false
                cell.btnPlay.setImage(#imageLiteral(resourceName: "SmallPlay"), for: UIControlState.normal)
            }
            
            return cell
        }
        else if tableView == tblIntroTrackOptions {
            var cell: UITableViewCell! = tblIntroTrackOptions.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
            
            if cell == nil {
                cell = UITableViewCell.init(style: .default, reuseIdentifier: cellIdentifier)
                cell.isUserInteractionEnabled = true
                cell.accessoryType = .none
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
            }
            
            let lblOptionName: UILabel = cell.viewWithTag(10) as! UILabel
            lblOptionName.text = introOptionsList[indexPath.row]
            
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            return cell
        }
        else if tableView == tblEndTrackOptions {
            var cell: UITableViewCell! = tblEndTrackOptions.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
            
            if cell == nil {
                cell = UITableViewCell.init(style: .default, reuseIdentifier: cellIdentifier)
                cell.isUserInteractionEnabled = true
                cell.accessoryType = .none
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
            }
            
            let lblOptionName: UILabel = cell.viewWithTag(10) as! UILabel
            lblOptionName.text = endOptionsList[indexPath.row]
            
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            return cell
        }
        else {
            var cell: UITableViewCell! = tblTrackOptions.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
            
            if cell == nil {
                cell = UITableViewCell.init(style: .default, reuseIdentifier: cellIdentifier)
                cell.isUserInteractionEnabled = true
                cell.accessoryType = .none
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
            }
            
            let lblOptionName: UILabel = cell.viewWithTag(10) as! UILabel
            lblOptionName.text = trackOptionsList[indexPath.row]
            
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblTrackOptions {
            if(trackOptionsList[indexPath.row] == "Move Up"){
                currentPlayIndex = 500
                self.defaultMeditationList.rearrange(from: editIndex, to: editIndex - 1 )
            }else if(trackOptionsList[indexPath.row] == "Move Down"){
                currentPlayIndex = 500
                self.defaultMeditationList.rearrange(from: editIndex, to: editIndex + 1)
            }else if(trackOptionsList[indexPath.row] == "Delete"){
                currentPlayIndex = 500
                self.defaultMeditationList.remove(at: editIndex)
                lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: defaultMeditationList).time)"
            }else if(trackOptionsList[indexPath.row] == "Add Silence 30 Sec"){
                currentPlayIndex = 500
                addSilenceTrack(index: editIndex + 1)
            }else if(trackOptionsList[indexPath.row] == "Add to Another Meditation"){
                let allMeditationListViewController: AllMeditationListViewController = self.storyboard?.instantiateViewController(withIdentifier: "AllMeditationListViewController") as! AllMeditationListViewController
                allMeditationListViewController.track_id = self.defaultMeditationList[editIndex]["track_id"] as! Int
                self.txtMeditationsName.resignFirstResponder()
                self.navigationController?.pushViewController(allMeditationListViewController, animated: true)
            }
            isEdited = true
            backgroundView.isHidden = true
            trackOptionsView.isHidden = true
            tblTracks.reloadData()
        }
        else if tableView == tblTracks {
            if (indexPath.row == self.defaultMeditationList.count - 1) {
                if(btnindex != -1){
                    DispatchQueue.main.async {
                        self.player.pause()
                        self.tblTracks.reloadData()
                        self.btnindex = -1
                    }
                }
                let addTracksViewController: AddTracksViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddTracksViewController") as! AddTracksViewController
                self.txtMeditationsName.resignFirstResponder()
                self.navigationController?.pushViewController(addTracksViewController, animated: true)
            }
        }
        else if tableView == tblIntroTrackOptions {
            if introOptionsList[indexPath.row] == "Add Silence 30 Sec" {
                addSilenceTrack(index: editIndex + 1)
            }
            
            else if introOptionsList[indexPath.row] == "Change Intro" {
                let backgroundMusicViewController: BackgroundMusicViewController = self.storyboard?.instantiateViewController(withIdentifier: "BackgroundMusicViewController") as! BackgroundMusicViewController
                backgroundMusicViewController.strController = "intro"
                self.txtMeditationsName.resignFirstResponder()
                self.navigationController?.pushViewController(backgroundMusicViewController, animated: true)
            }
            backgroundView.isHidden = true
            introTrackOptionsView.isHidden = true
        }
        else if tableView == tblEndTrackOptions {
            if endOptionsList[indexPath.row] == "Change Ending" {
                let backgroundMusicViewController: BackgroundMusicViewController = self.storyboard?.instantiateViewController(withIdentifier: "BackgroundMusicViewController") as! BackgroundMusicViewController
                backgroundMusicViewController.strController = "ending"
                self.txtMeditationsName.resignFirstResponder()
                self.navigationController?.pushViewController(backgroundMusicViewController, animated: true)
            }
            backgroundView.isHidden = true
            endTracksOptionsView.isHidden = true
        }
        tblTracksHeightConstraint.constant = CGFloat((self.defaultMeditationList.count + 1) * 80)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    
    func addSilenceTrack(index : Int){
        var silenceDictionary = [String : AnyObject]()
        silenceDictionary["reminder"] = silenceList["reminder"] as AnyObject
        silenceDictionary["track_type"] = "S" as AnyObject
        silenceDictionary["audio_url"] = silenceList["audio_url"] as AnyObject
        silenceDictionary["description"] = silenceList["description"] as AnyObject
        silenceDictionary["title"] = silenceList["title"] as AnyObject
        silenceDictionary["track_id"] = silenceList["track_id"] as AnyObject
        silenceDictionary["time"] = silenceList["time"] as AnyObject
        silenceDictionary["display_order"] = silenceList["display_order"] as AnyObject
        self.defaultMeditationList.insert(silenceDictionary, at: index)
        tblTracks.reloadData()
        lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: defaultMeditationList).time)"
    }

    //UITextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtMeditationsName.resignFirstResponder()
        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
