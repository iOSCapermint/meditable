//
//  OfflineSavedMeditationViewController.swift
//  Meditable
//
//  Created by capermintmini6 on 29/06/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit

class OfflineSavedMeditationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tblView.estimatedRowHeight = 90
        tblView.rowHeight = UITableViewAutomaticDimension
        tblView.tableFooterView = UIView(frame: .zero)
        if(appDelegate.downloadedMeditations.count > 0){
            lblNoDataFound.isHidden = true
            tblView.isHidden = false
        }else{
            lblNoDataFound.isHidden = false
            tblView.isHidden = true
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }

    //MARK: - UITableView Delegate & Data source Methods -
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appDelegate.downloadedMeditations.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "offlineTableCell"
        
        let cell: offlineTableCell! = tblView.dequeueReusableCell(withIdentifier: cellIdentifier) as! offlineTableCell!
        
        cell.lblTitle.text = "\(appDelegate.downloadedMeditations[indexPath.row]["title"]!)"
        
        // For Total Time
        let trackList = appDelegate.downloadedMeditations[indexPath.row]["track"] as! [[String : AnyObject]]
        var bgTime = "00:00"
        for i in 0...trackList.count - 1 {
            if("\(trackList[i]["track_type"]!)" == "B"){
                bgTime = "\(trackList[i]["time"]!)"
                break
            }
        }
        let time = self.getTotalTime(totalTime: "\(appDelegate.downloadedMeditations[indexPath.row]["total_meditation_time"]!)", backgroundMusicTime: bgTime)
        cell.lblTotalTime.text = "\(time.time)"

        cell.btnDelete.addTarget(self, action: #selector(OfflineSavedMeditationViewController.btnDelete(_:)), for: .touchUpInside)
        cell.btnDelete.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let customMeditationPlaylistViewController = self.storyboard?.instantiateViewController(withIdentifier: "SavedTrackListViewController") as! SavedTrackListViewController
        customMeditationPlaylistViewController.responseArray = [appDelegate.downloadedMeditations[indexPath.row]]
        customMeditationPlaylistViewController.index = indexPath.row
        customMeditationPlaylistViewController.meditationID = "\(appDelegate.downloadedMeditations[indexPath.row]["meditation_id"]!)"
        
        let trackList = appDelegate.downloadedMeditations[indexPath.row]["track"] as! [[String : AnyObject]]
        
        var bgTime = "00:00"
        for i in 0...trackList.count - 1 {
            if("\(trackList[i]["track_type"]!)" == "B"){
                bgTime = "\(trackList[i]["time"]!)"
                break
            }
        }
        let time = self.getTotalTime(totalTime: "\(appDelegate.downloadedMeditations[indexPath.row]["total_meditation_time"]!)", backgroundMusicTime: bgTime)
        customMeditationPlaylistViewController.totalTime = "\(time.time)"
        customMeditationPlaylistViewController.backgroundSeconds = time.backgroundSeconds
        customMeditationPlaylistViewController.totalSeconds = time.totalSeconds

        self.navigationController?.pushViewController(customMeditationPlaylistViewController, animated: true)
    }

    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
  
    
    @IBAction func btnDelete(_ sender: UIButton) {
        let alertController = UIAlertController(title: AppName, message: DeleteMeditaionMessage, preferredStyle:UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
        { action -> Void in
            self.removeFromBundle(index: sender.tag)
            appDelegate.downloadedMeditations.remove(at: sender.tag)
            let archiveData = NSKeyedArchiver.archivedData(withRootObject: appDelegate.downloadedMeditations)
            defaults.set(archiveData, forKey: kdownloadedMeditations)
            defaults.synchronize()
            self.tblView.reloadData()
            NotificationCenter.default.post(name: NSNotification.Name.init("refreshMeditaionList"), object: nil)
            if(appDelegate.downloadedMeditations.count == 0){
                self.lblNoDataFound.isHidden = false
                self.tblView.isHidden = true
            }else{
                self.lblNoDataFound.isHidden = true
                self.tblView.isHidden = false
            }
        })
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
        { action -> Void in
            
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    func removeFromBundle(index : Int){
        let trackArray : [[String : AnyObject]] = appDelegate.downloadedMeditations[index]["track"] as! [[String : AnyObject]]
        for i in 0...trackArray.count - 1{
            let trackTitle = "\(trackArray[i]["title"]!)-\(appDelegate.downloadedMeditations[index]["meditation_id"]!)\(trackArray[i]["track_type"]!)\(trackArray[i]["track_id"]!).mp3"
            self.removeMeditation(trackTitle: trackTitle)
        }
    }
    func removeMeditation(trackTitle : String) {
        let fileManager = FileManager.default
        if !FileManager.default.fileExists(atPath: appDelegate.myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: appDelegate.myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        let path =  appDelegate.myDownloadPath + "/" + "\(trackTitle)"
        do {
            try fileManager.removeItem(atPath: path)
        } catch let error as NSError {
            print(error.debugDescription)
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class offlineTableCell : UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
//    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTotalTime: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
}
