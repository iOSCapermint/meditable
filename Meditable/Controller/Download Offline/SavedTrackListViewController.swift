//
//  SavedTrackListViewController.swift
//  Meditable
//
//  Created by capermintmini6 on 05/07/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import AVFoundation
import AudioToolbox

class SavedTrackListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate, AVAudioPlayerDelegate {

    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblMeditations: UILabel!
    
    @IBOutlet weak var lblCreateMeditations: UILabel!
    
    @IBOutlet weak var txtMeditationsName: UITextField!
    
    @IBOutlet weak var vwMeditationsName: UIView!
    
    @IBOutlet weak var tblTracks: UITableView!
    
    @IBOutlet weak var btnPlayMeditation: UIButton!
    
    @IBOutlet weak var tblTracksHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTotalTrackTime: UILabel!

    var responseArray = [[String: AnyObject]]()
    var editIndex = Int()
    var urlString = ""
    var trackList = [[String : AnyObject]]()
    var index : Int = 0
    var backgroundList = [String : AnyObject]()
    var silenceList = [String : AnyObject]()
    var player = AVAudioPlayer()
    var btnindex = Int()
    var meditationType = ""
    var isOriginal = ""
    var meditationID = ""
    var meditationDescription = ""
    var isplayingCurrently = false
    //For total time Displayed
    var totalTime = ""
    var backgroundSeconds : Int = 0
    var totalSeconds : Int = 0
    var currentPlayIndex = 500
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        if(isOriginal == "Y"){
            lblMeditations.text = "\(MadeByMeditable)"
        }else{
            lblMeditations.text = "\(MadeByYou)"
        }
//        if self.responseArray[0]["is_original"] != nil {
//                isOriginal = "\(self.responseArray[0]["is_original"]!)"
//        }else{
//            isOriginal = "\(MadeByYou)"
//        }
        //For background and Silence Audio
        trackList = self.responseArray[0]["track"] as! [[String : AnyObject]]
        
        for i in 0...trackList.count - 1 {
            if("\(trackList[i]["track_type"]!)" == "S"){
                silenceList = trackList[i]
                trackList.remove(at: i)
                break
            }
        }
        
        txtMeditationsName.text = "\(self.responseArray[0]["title"]!)"
        
        for i in 0...trackList.count - 1 {
            if("\(trackList[i]["track_type"]!)" == "B"){
                backgroundList = trackList[i]
                trackList.remove(at: i)
                break
            }
        }
        
        
        
        self.vwMeditationsName.layer.cornerRadius = 5.0
        self.vwMeditationsName.layer.borderColor = UIColor.white.cgColor
        self.vwMeditationsName.layer.borderWidth = 1
        
        btnPlayMeditation.layer.cornerRadius = btnPlayMeditation.frame.size.height / 2
        btnPlayMeditation.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnPlayMeditation.layer.borderWidth = 1
        
        tblTracks.rowHeight = UITableViewAutomaticDimension
        tblTracks.estimatedRowHeight = 90
        
        lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: trackList).time)"
//        lblTotalTrackTime.text = "Total: \(totalTime)"
        tblTracksHeightConstraint.constant = CGFloat((self.trackList.count + 1) * 80)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        addGoogleAnalytics(screenName: "Custom Meditation Play")
        NotificationCenter.default.addObserver(self, selector: #selector(SavedTrackListViewController.itemDidFinishPlaying(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
        self.btnindex = -1
        tblTracks.reloadData()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        //_ = self.navigationController?.popViewController(animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnPlayMeditation_Clicked(_ sender: Any) {
        if isplayingCurrently == true {
            isplayingCurrently = false
            player.stop()
        }
        let playMeditationViewController = self.storyboard?.instantiateViewController(withIdentifier: "PlayOfflineViewController") as! PlayOfflineViewController
        playMeditationViewController.trackList = trackList
        playMeditationViewController.backgroundSound = backgroundList
        playMeditationViewController.silenceSound = silenceList
        playMeditationViewController.totalTrackTime = "\(getTotalTrackTime(track: trackList).time)"
        playMeditationViewController.backgroundSeconds = backgroundSeconds
        playMeditationViewController.totalSeconds = getTotalTrackTime(track: trackList).totalSeconds
        playMeditationViewController.meditationTitle = "\(self.responseArray[0]["title"]!)"
        playMeditationViewController.meditationID = meditationID
        playMeditationViewController.meditationDescription = meditationDescription
        playMeditationViewController.meditaionBy = "\(isOriginal)"
        self.navigationController?.pushViewController(playMeditationViewController, animated: true)
        
    }
    //Play Track
    func playTrack(sender: UIButton) {
        let indexPath: NSIndexPath = self.tblTracks.indexPath(for: (sender.superview?.superview as! UITableViewCell))! as NSIndexPath
        editIndex = indexPath.row
        
        if btnindex == indexPath.row{
            btnindex = -1
        }else{
            btnindex = indexPath.row
        }
        urlString = (trackList[indexPath.row]["audio_url"] as? String)!
        if urlString != ""{
            var backgroundPath = ""
//            var trackName = ""
//            if(responseArray[0]["meditation_id"] != nil){
//                trackName = "\(trackList[indexPath.row]["title"]!)-\(responseArray[0]["meditation_id"]!)\(trackList[indexPath.row]["track_type"]!)\(trackList[indexPath.row]["track_id"]!).mp3"
//            }else{
//                trackName = "\(trackList[indexPath.row]["title"]!)-\(responseArray[0]["custom_meditation_id"]!)\(trackList[indexPath.row]["track_type"]!)\(trackList[indexPath.row]["track_id"]!).mp3"
//            }
            
           let trackName = "\(trackList[indexPath.row]["title"]!)-\(meditationID)\(trackList[indexPath.row]["track_type"]!)\(trackList[indexPath.row]["track_id"]!).mp3"

            
            let bgpath = appDelegate.myDownloadPath
            let url = NSURL(fileURLWithPath: bgpath)
            let filePath = url.appendingPathComponent("\(trackName)")?.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath!) {
                backgroundPath = filePath!
            } else {
            }
            if(sender.isSelected == false){
                do {
                    if(currentPlayIndex != indexPath.row){
                        currentPlayIndex = indexPath.row
                        player = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: backgroundPath ))
                        player.delegate = self
                    }
                    player.play()
                    isplayingCurrently = true
                }
                catch{
                    print(error)
                }
            }else{
                player.stop()
                isplayingCurrently = false
            }
        }
        self.tblTracks.reloadData()
    }
    
    func itemDidFinishPlaying(notification:NSNotification) {
        player.pause()
        player.rate = 0
        self.btnindex = -1
        let indexPath = IndexPath(item: editIndex, section: 0)
        tblTracks.reloadRows(at: [indexPath], with: .none)
    }
    
    //TableView Delegate & Data source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.trackList.count
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
            let cell: TimeLineTableViewCell! = tblTracks.dequeueReusableCell(withIdentifier: cellIdentifier) as! TimeLineTableViewCell
            if(indexPath.row == 0){
                cell.lblTrackName.text = trackList[indexPath.row]["title"] as? String
                cell.lblTrackName.font = UIFont(name: "FuturaBT-Medium", size: 16.0)
                if let trackTime = trackList[indexPath.row]["time"] as? String {
                    cell.lblTrackTime.text = "\(trackTime)"
                }
                
                cell.lblTrackName.textColor = UIColor().yellowBorderColor
                
                cell.btnInfo.removeTarget(self, action: #selector(SavedTrackListViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.removeTarget(self, action: #selector(SavedTrackListViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnInfo.addTarget(self, action: #selector(SavedTrackListViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.addTarget(self, action: #selector(SavedTrackListViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnInfo.isHidden = false
                cell.btnPlay.isHidden = false                
            }else if(indexPath.row == self.trackList.count){
                cell.lblTrackName.text = trackList[indexPath.row - 1]["title"] as? String
                cell.lblTrackName.font = UIFont(name: "FuturaBT-Medium", size: 16.0)
                if let trackTime = trackList[indexPath.row - 1]["time"] as? String {
                    cell.lblTrackTime.text = "\(trackTime)"
                }
                cell.lblTrackName.textColor = UIColor().yellowBorderColor
                cell.lblTrackName.sizeToFit()
                
                cell.btnInfo.removeTarget(self, action: #selector(SavedTrackListViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.removeTarget(self, action: #selector(SavedTrackListViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnInfo.addTarget(self, action: #selector(SavedTrackListViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.addTarget(self, action: #selector(SavedTrackListViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnInfo.isHidden = false
                cell.btnPlay.isHidden = false
            }else{
                cell.lblTrackName.text = self.trackList[indexPath.row]["title"] as? String
                cell.lblTrackName.font = UIFont(name: "FuturaBT-Medium", size: 16.0)
                if let trackTime = self.trackList[indexPath.row]["time"] as? String {
                    cell.lblTrackTime.text = "\(trackTime)"
                }
                cell.lblTrackName.textColor = UIColor().yellowBorderColor
                
                cell.btnInfo.removeTarget(self, action: #selector(CustomMeditationViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.removeTarget(self, action: #selector(CustomMeditationViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnInfo.addTarget(self, action: #selector(CustomMeditationViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.addTarget(self, action: #selector(CustomMeditationViewController.playTrack(sender: )), for: .touchUpInside)
                
                let strTrackType = self.trackList[indexPath.row]["track_type"] as! String
                if strTrackType == "S" {
                    cell.btnPlay.isHidden = true
                } else {
                    cell.btnPlay.isHidden = false
                }
                
                cell.btnInfo.isHidden = false
            }
            
            cell.allRows = self.trackList.count
            cell.currentIndexPath = indexPath
            cell.setNeedsDisplay()
            
            cell.isUserInteractionEnabled = true
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            if btnindex == indexPath.row{
                cell.btnPlay.isSelected = true
                cell.btnPlay.setImage(#imageLiteral(resourceName: "Stop"), for: UIControlState.selected)
            }else{
                cell.btnPlay.isSelected = false
                cell.btnPlay.setImage(#imageLiteral(resourceName: "SmallPlay"), for: UIControlState.normal)
            }
        
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    
    //UITextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtMeditationsName.resignFirstResponder()
        
        return true
    }
    
    func checkTrackInfo(sender: UIButton) {
        if isplayingCurrently == true {
            isplayingCurrently = false
            player.stop()
        }
        let buttonPosition: CGPoint = sender.convert(.zero, to: tblTracks)
        let indexPath: NSIndexPath = tblTracks.indexPathForRow(at: buttonPosition) as NSIndexPath!
        
        let trackInfoViewController: TrackInfoViewController = self.storyboard?.instantiateViewController(withIdentifier: "TrackInfoViewController") as! TrackInfoViewController
        trackInfoViewController.trackList =  trackList[indexPath.row]
        self.navigationController?.pushViewController(trackInfoViewController, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AVAudioPlayer {
    var isPlayinga: Bool {
        return rate != 0
    }
}
