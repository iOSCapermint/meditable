  //
  //  SubscriptionPlanViewController.swift
  //  Meditable
  //
  //  Created by Capermint's MacBook PRO on 30/05/17.
  //  Copyright © 2017 capermint. All rights reserved.
  //
  
  import UIKit
  import NVActivityIndicatorView
  import Alamofire
  import SwiftyJSON
  import StoreKit
  
  class SubscriptionPlanViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,NVActivityIndicatorViewable, SKProductsRequestDelegate, SKPaymentTransactionObserver, UITextFieldDelegate {
    
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var lblDescrition: UILabel!
    
    @IBOutlet weak var informationView: UIView!
    
    @IBOutlet weak var tblSubscriptions: UITableView!
    
    @IBOutlet weak var lblDiscount: UILabel!
    
    @IBOutlet weak var txtCoupon: CustomTextfield!
    
    @IBOutlet weak var btnSubscribeNow: UIButton!
    
    @IBOutlet weak var btnRestorePurchase: UIButton!
    
    @IBOutlet weak var lblSubscriptionText: UILabel!
    
    @IBOutlet weak var btnTerms: UIButton!
    
    @IBOutlet weak var btnPrivacy: UIButton!
    
    @IBOutlet weak var btnApplyCode: UIButton!

    @IBOutlet var couponCodeView: UIView!
    @IBOutlet weak var innerCouponView: UIView!
    
    @IBOutlet weak var topSubscriptionButton: NSLayoutConstraint!
    @IBOutlet weak var lblPurchasedPlan: UILabel!
 
    //    var planList = ["MONTHLY", "YEARLY", "MONTHLY"]
    //
    //    var offerList = ["50% off for the first 1000 users. Future price:      9,99 USD.", "Payment of $55.9 USD every 12 months.", "Payment of $55.9 USD every 12 months."]
    
    var planList = ["MONTHLY", "YEARLY"]
    var offerList = ["Payment of $4.99 USD every month.", "Payment of $49.99 USD every year."]
    var planDetail = [[String : AnyObject]]()
    var subscription = [MonthlySubscriptionIdentifier, YearlySubscriptionIdentifier]
    var subscriptionFor = ""
    var btnindex = Int()
    
    var list = [SKProduct]()
    var p = SKProduct()
    var transactionID = ""
    var memberShipID = ""
    var promoCodeData = [[String : AnyObject]]()
    var amount = ""
    var load = false
    var currentPlanID = ""
    var currentProductID = ""

    @IBOutlet weak var tickPoint1: UIImageView!
    @IBOutlet weak var tickPoint2: UIImageView!
    @IBOutlet weak var tickPoint3: UIImageView!
    @IBOutlet weak var lblPoint1: UILabel!
    @IBOutlet weak var lblPoint2: UILabel!
    @IBOutlet weak var lblPoint3: UILabel!
    
    
    var resCnt = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblSubscriptions.tableFooterView = UIView(frame: .zero)

        self.btnindex = -1
        
        txtCoupon.layer.cornerRadius = txtCoupon.frame.size.height / 2
        txtCoupon.delegate = self
        informationView.layer.cornerRadius = 5.0
        informationView.layer.borderWidth = 1.0
        informationView.layer.borderColor = UIColor().yellowBorderColor.cgColor
        
        btnSubscribeNow.layer.cornerRadius = btnSubscribeNow.frame.size.height / 2
        btnSubscribeNow.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnSubscribeNow.layer.borderWidth = 1.0
        
        btnApplyCode.layer.cornerRadius = btnSubscribeNow.frame.size.height / 2
        btnApplyCode.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnApplyCode.layer.borderWidth = 1.0
        
        informationView.isHidden = false
        lblPurchasedPlan.isHidden = true
        lblDiscount.isHidden = false

        if appDelegate.membership_plan_id == "4"{
            if(defaults.value(forKey: "applyCouponCode") != nil){
                lblDiscount.attributedText = NSAttributedString.init(string: "\(defaults.value(forKey: "applyCouponCode")!)", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17), NSForegroundColorAttributeName:UIColor().yellowBorderColor])
                
            }else{
                lblDiscount.attributedText = NSAttributedString.init(string: "You had applied a coupon code", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17), NSForegroundColorAttributeName:UIColor().yellowBorderColor])
            }
        }else{
            if(appDelegate.membership_plan_id == "2" || appDelegate.membership_plan_id == "5"){
                lblHeader.text = "You have an active monthly subscription"
                informationView.isHidden = true
                lblPurchasedPlan.isHidden = false
                lblDiscount.isHidden = true
                topSubscriptionButton.constant = 0
            }else if(appDelegate.membership_plan_id == "3" || appDelegate.membership_plan_id == "6"){
                lblHeader.text = "You have an active yearly subscription"
                informationView.isHidden = true
                lblPurchasedPlan.isHidden = false
                lblDiscount.isHidden = true
                topSubscriptionButton.constant = 0
            }else{
                informationView.isHidden = false
                lblPurchasedPlan.isHidden = true
                lblDiscount.isHidden = false
                topSubscriptionButton.constant = 25
                if(appDelegate.membership_plan_id == "1"){
                    lblHeader.text = upgradeMsg
                }else{
                    lblHeader.text = "Meditable is free for the first 14 days."
                }
                let tap = UITapGestureRecognizer(target: self, action: #selector(SubscriptionPlanViewController.tapGotDiscount(sender:)))
                lblDiscount.addGestureRecognizer(tap)
                lblDiscount.attributedText = NSAttributedString.init(string: "Got Discount Coupon?", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17), NSForegroundColorAttributeName:UIColor().yellowBorderColor, NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
            }
        }
        let tapCouponView = UITapGestureRecognizer(target: self, action: #selector(SubscriptionPlanViewController.tapCouponCodeView(sender:)))
        couponCodeView.addGestureRecognizer(tapCouponView)
        couponCodeView.isUserInteractionEnabled = true
        
        couponCodeView.frame = self.view.frame
        self.view.addSubview(couponCodeView)
        couponCodeView.isHidden = true
        couponCodeView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        innerCouponView.layer.cornerRadius = 5.0
        innerCouponView.clipsToBounds = true
        
        self.getPlanDetail()
        
        if(SKPaymentQueue.canMakePayments()) {
            print("IAP is enabled, loading")
            let productID: NSSet = NSSet(objects: "com.meditable.monthlysubscription", "com.meditable.limitedmonthlysubscription", "com.meditable.yearlysubscription", "com.meditable.limitedyearlysubscription")
            let request: SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>)
            request.delegate = self
            request.start()
        } else {
            print("please enable IAPS")
        }
        
        let center = NotificationCenter.default
        center.addObserver(self,
                           selector: #selector(keyboardWillShow(notification:)),
                           name: .UIKeyboardWillShow,
                           object: nil)
        
        center.addObserver(self,
                           selector: #selector(keyboardWillHide(notification:)),
                           name: .UIKeyboardWillHide,
                           object: nil)
    }
    
    func viewWillAppear(){
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Subscription Plan Detail")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        SKPaymentQueue.default().remove(self)
    }
    
    //MARK:- TableView Delegate & Data source Methods -
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.planDetail.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = UIView()
        headerView.backgroundColor = .clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        
        let cell: UITableViewCell! = tblSubscriptions.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
        
        let btnCheck: UIButton = cell.viewWithTag(10) as! UIButton
        
        cell.layer.borderColor = UIColor.clear.cgColor
        if btnindex == indexPath.section{
            btnCheck.isSelected = true
            btnCheck.setImage(#imageLiteral(resourceName: "ic_selected_mark"), for: UIControlState.selected)
            cell.layer.borderColor = UIColor().yellowBorderColor.cgColor
        }else{
            btnCheck.isSelected = false
            btnCheck.setImage(#imageLiteral(resourceName: "ic_unselected_mark"), for: UIControlState.normal)
            cell.layer.borderColor = UIColor().lightAlphaYellowColor.cgColor
        }
        
        btnCheck.addTarget(self, action: #selector(SubscriptionPlanViewController.onBtnCheckClick(sender: )),for: .touchUpInside)
        
        let lblPlan: UILabel = cell.viewWithTag(11) as! UILabel
        if(indexPath.section == 0){
            lblPlan.text = "MONTHLY"
        }else if(indexPath.section == 1){
            lblPlan.text = "YEARLY"
        }

        let lblOffer: UILabel = cell.viewWithTag(12) as! UILabel
        if(indexPath.section == 0){
            lblOffer.text = "$9.99 /month"
        }else if(indexPath.section == 1){
            lblOffer.text = "$6.49/ month"
        }
        let lblRecuring: UILabel = cell.viewWithTag(13) as! UILabel
        if(indexPath.section == 0){
            lblRecuring.text = "Recurring billing, paid monthly, cancel anytime."
        }else if(indexPath.section == 1){
            lblRecuring.text = "Payment of $\(self.planDetail[indexPath.section]["price"]!) every 12 months.Recurring billing, cancel anytime."
        }

            if("\((self.planDetail[indexPath.section]["membership_plan_id"]!))" == "\(appDelegate.membership_plan_id)" && load == false){
                load = true
                btnCheck.isSelected = true
                btnCheck.setImage(#imageLiteral(resourceName: "ic_selected_mark"), for: UIControlState.selected)
                currentPlanID = "\(self.planDetail[indexPath.section]["membership_plan_id"]!)"
                currentProductID = "\(self.planDetail[indexPath.section]["ios_app_store_plane_id"]!)"
                subscriptionFor = "\(currentProductID)"
                cell.layer.borderColor = UIColor().yellowBorderColor.cgColor
            }
        
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1.5
        cell.layer.masksToBounds = true
        
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if btnindex == indexPath.section{
            btnindex = -1
            subscriptionFor = ""
            amount = ""
        }else{
            btnindex = indexPath.section
            subscriptionFor = "\((self.planDetail[indexPath.section]["ios_app_store_plane_id"]!))"
            memberShipID = "\((self.planDetail[indexPath.section]["membership_plan_id"]!))"
            amount = "$\((self.planDetail[indexPath.section]["price"]!))"
        }
        self.tblSubscriptions.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if(indexPath.section == 0){
//            return 110
//        }else{
            return 120
//        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    
    //MARK:- Tap subscription detail
    
    func tapGotDiscount(sender:UITapGestureRecognizer) {
        txtCoupon.text = ""
        couponCodeView.isHidden = false
    }
    
    func tapCouponCodeView(sender:UITapGestureRecognizer) {
        txtCoupon.resignFirstResponder()
    }
    
    //MARK:- Payment -
    
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("product request")
        let myProduct = response.products
        for product in myProduct {
            list.append(product)
        }
        if(self.resCnt == 1){
            self.stopAnimating()
        }else{
            self.resCnt += 1
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        UIAlertController().alertViewWithTitleAndMessage(self, message: "Transaction restored")
        self.stopAnimating()
        for transaction in queue.transactions {
            let t: SKPaymentTransaction = transaction
            let prodID = t.payment.productIdentifier as String
            
            switch prodID {
            case "com.meditable.monthlysubscription":
                print("com.meditable.monthlysubscription")
            case "com.meditable.yearlysubscription":
                print("com.meditable.yearlysubscription")
            case "com.meditable.limitedmonthlysubscription":
                print("com.meditable.limitedmonthlysubscription")
            case "com.meditable.limitedyearlysubscription":
                print("com.meditable.limitedyearlysubscription")
            default:
                print("IAP not found")
            }
        }
    }
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction: AnyObject in transactions {
            let trans = transaction as! SKPaymentTransaction
            switch trans.transactionState {
            case .purchased:
                transactionID = trans.transactionIdentifier!
                defaults.set("\(p.productIdentifier)", forKey: "purchased")
                purchasedProduct()
                tblSubscriptions.reloadData()
                let prodID = p.productIdentifier
                switch prodID {
                case "com.meditable.monthlysubscription":
                    print("com.meditable.monthlysubscription")
                case "com.meditable.yearlysubscription":
                    print("com.meditable.yearlysubscription")
                case "com.meditable.limitedmonthlysubscription":
                    print("com.meditable.limitedmonthlysubscription")
                case "com.meditable.limitedyearlysubscription":
                    print("com.meditable.limitedyearlysubscription")
                default:
                    print("IAP not found")
                }
                queue.finishTransaction(trans)
            case .failed:
                print("buy error")
                queue.finishTransaction(trans)
                break
            default:
                print("Default")
                break
            }
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        self.stopAnimating()
    }

    //MARK:- API -
    func purchasedProduct(){
        if(appDelegate.reachable.isReachable){
            var authKey: String! = "\(defaults.value(forKey: "AuthKey")!)"
            if authKey == nil {
                authKey = ""
            }
            var params = Parameters()
            params = ["auth_key": authKey, "transaction_id" : "\(transactionID)", "membership_plan_id" : "\(memberShipID)"]
            APIManager().productPurchas(parameters: params as! [String : String]) { (response, error) in
                if (error == nil){
//                    print(response!)
                    APIManager().autoLoginForSave()
                }else{
                    if(error?.code == 5 || error?.code == 3){
                        let alertController = UIAlertController(title: AppName, message: "Unauthorized", preferredStyle:UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                        { action -> Void in
                            let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                            defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                            defaults.set(deviceToken,forKey: "DeviceToken")
                            defaults.synchronize()
                            let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                            self.navigationController?.pushViewController(welcomeViewController, animated: true)
                        })
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                
                NotificationCenter.default.post(name: NSNotification.Name.init("DisplayCategoryView"), object: nil)
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                for aViewController:UIViewController in viewControllers {
                    if aViewController.isKind(of: TabbarViewController.self) {
                        _ = self.navigationController?.popToViewController(aViewController, animated: true)
                    }
                }
            }
        }
    }
    
    func applyPromoCode(code: String) {
        if appDelegate.reachable.isReachable{
            
            if txtCoupon.text?.characters.count == 0 {
                UIAlertController().alertViewWithTitleAndMessage(self, message: "Please Enter Coupon Code")
            }
            if appDelegate.reachable.isReachable{
                
                var authKey = ""
                if defaults.value(forKey: "AuthKey") as? String != nil {
                    authKey = defaults.value(forKey: "AuthKey") as! String
                }
                
                let params = ["auth_key": authKey, "code": code]
                
                let size = CGSize(width: 50, height: 50)
                let color : UIColor = .white
                startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
                
                Alamofire.request(baseURLWithAuth + kPromoCode , method: .post, parameters: params).validate().responseJSON { response in
                    self.stopAnimating()
                    switch response.result {
                    case .success:
                        if let json: NSDictionary = response.result.value as! NSDictionary? {
                            
                            if("\(json.value(forKey: "status")!)" == "1"){
                                self.lblDiscount.attributedText = NSAttributedString.init(string: "\(json.value(forKey: "message")!)", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17), NSForegroundColorAttributeName:UIColor().yellowBorderColor])//"You had applied a coupon code"
                                APIManager().autoLoginForSave()
                                defaults.set("\(json.value(forKey: "message")!)", forKey: "applyCouponCode")
                                let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                                { action -> Void in
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name.init("DisplayCategoryView"), object: nil)
                                    
                                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                                    for aViewController:UIViewController in viewControllers {
                                        if aViewController.isKind(of: TabbarViewController.self) {
                                            _ = self.navigationController?.popToViewController(aViewController, animated: true)
                                        }
                                    }
                                })
                                self.present(alertController, animated: true, completion: nil)
                            }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                                
                                let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                                alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                                { action -> Void in
                                    let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                    
                                    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                    defaults.set(deviceToken,forKey: "DeviceToken")
                                    defaults.synchronize()
                                    
                                    let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                    self.navigationController?.pushViewController(welcomeViewController, animated: true)
                                })
                                self.present(alertController, animated: true, completion: nil)
                            }else{
                                UIAlertController().alertViewWithTitleAndMessage(self, message: json.value(forKey: "message") as! String)
                            }
                        }
                    case .failure(let error):
                        UIAlertController().alertViewWithErrorMessage(self)
                    }
                }
            }
            else {
                UIAlertController().alertViewWithNoInternet(self)
            }
        }
    }
    
    //MARK:- Button Events -
    func onBtnCheckClick(sender: UIButton) {
        
        let cell = sender.superview?.superview as? UITableViewCell
        let indexPath = tblSubscriptions.indexPath(for: cell!)
        
        if btnindex == indexPath!.section{
            btnindex = -1
            subscriptionFor = ""
            amount = ""
        }else{
            btnindex = indexPath!.section
            subscriptionFor = "\((self.planDetail[indexPath!.section]["ios_app_store_plane_id"]!))"
            memberShipID = "\((self.planDetail[indexPath!.section]["membership_plan_id"]!))"
            amount = "$\((self.planDetail[indexPath!.section]["price"]!))"
        }
        self.tblSubscriptions.reloadData()
    }
    
    @IBAction func onBtnCloseClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func onBtnSubscribeNowClick(_ sender: Any) {
        if(subscriptionFor != ""){
            var message = ""
            if(appDelegate.membership_plan_id == "2" || appDelegate.membership_plan_id == "5"){
                if(subscriptionFor == MonthlyLimitedSubscriptionIdentifier){
                    UIAlertController().alertViewWithTitleAndMessage(self, message: "You’ve already subscribed for the monthly plan.")
                }else{
                    message = upgradePlanToYearly
                }
            }else if(appDelegate.membership_plan_id == "3" || appDelegate.membership_plan_id == "6"){
                if(subscriptionFor == YearlyLimitedSubscriptionIdentifier){
                    UIAlertController().alertViewWithTitleAndMessage(self, message: "You’ve already subscribed for the yearly plan.")
                }else{
                    message = upgradePlanToMonthly
                }
            }else if(subscriptionFor == MonthlySubscriptionIdentifier){
                message = MonthlyIAPMessage
            }else if (subscriptionFor == YearlySubscriptionIdentifier){
                message = YearlyIAPMessage
            }else if (subscriptionFor == MonthlyLimitedSubscriptionIdentifier){
                message = MonthlyLimitedIAPMessage
            }else if (subscriptionFor == YearlyLimitedSubscriptionIdentifier){
                message = YearlyLimitedIAPMessage
            }else{
                UIAlertController().alertViewWithTitleAndMessage(self, message: "Product ID is not valid")
            }
            
            let alertController = UIAlertController(title: AppName, message: message, preferredStyle: UIAlertControllerStyle.alert)
            let CancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) {
                (result : UIAlertAction) -> Void in
            }
            let OkAction = UIAlertAction(title: "Buy", style: UIAlertActionStyle.default) {
                (result : UIAlertAction) -> Void in

                for product in self.list {
                    let prodID = product.productIdentifier
                    if(prodID == self.subscriptionFor) {
                        self.p = product
                    }
                }
                
                let pay = SKPayment(product: self.p)
                SKPaymentQueue.default().add(self)
                SKPaymentQueue.default().add(pay as SKPayment)
            }
            alertController.addAction(CancelAction)
            alertController.addAction(OkAction)
            self.present(alertController, animated: true, completion: nil)
        }else{
            UIAlertController().alertViewWithTitleAndMessage(self, message: SelectPlan)
        }
    }
    
    @IBAction func onBtnCancelSubscription(_ sender: UIButton) {
        UIApplication.shared.openURL(URL.init(string: cancelSubscriptionURL)!)
    }
    @IBAction func onBtnRestorePurchaseClick(_ sender: Any) {
        let size = CGSize(width: 50, height: 50)
        let color : UIColor = .white
        startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)

        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    @IBAction func onBtnTermsClick(_ sender: Any) {
        let termsAndConditionsViewController: TermsAndConditionsViewController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as! TermsAndConditionsViewController
        self.navigationController?.pushViewController(termsAndConditionsViewController, animated: true)
    }
    
    @IBAction func onBtnPrivacyClick(_ sender: Any) {
        let privacyPolicyViewController: PrivacyPolicyViewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
        self.navigationController?.pushViewController(privacyPolicyViewController, animated: true)
    }
    @IBAction func onBtnApplyCode(_ sender: Any) {
        couponCodeView.isHidden = true
        txtCoupon.resignFirstResponder()
        applyPromoCode(code: txtCoupon.text!)
    }
    @IBAction func btnCloseCouponCodeView(_ sender: UIButton) {
        couponCodeView.isHidden = true
    }
    
    
    //MARK:- API -
    func getPlanDetail() {
        if appDelegate.reachable.isReachable{
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithoutAuth + kGetPlanlist, method: .post, parameters: nil).validate().responseJSON {
                response in
                if(self.resCnt == 1){
                    self.stopAnimating()
                }else{
                    self.resCnt += 1
                }
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            self.planDetail = json.value(forKey: "data") as! [[String : AnyObject]]
//                            self.lblPoint5.text = "\(json.value(forKey: "message")!)"
                            self.tblSubscriptions.reloadData()
                        }
                    }else {
                        
                    }
                case .failure(let error):
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- textDelegate -
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtCoupon.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

    }
   
    func textFieldDidEndEditing(_ textField: UITextField) {
            
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if(innerCouponView.frame.origin.y > (self.view.frame.height - (225.0 + keyboardSize.height))){
                innerCouponView.frame.origin.y -= keyboardSize.height/3.0
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            innerCouponView.frame.origin.y += keyboardSize.height/3.0
        }
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
  }
  
