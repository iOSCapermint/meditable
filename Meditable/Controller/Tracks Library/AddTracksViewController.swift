//
//  AddTracksViewController.swift
//  Meditable
//
//  Created by Apple on 19/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import YSLContainerViewController
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON

class AddTracksViewController: UIViewController , YSLContainerViewControllerDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblAddTracks: UILabel!
    
    var subCategoryList = [[String : AnyObject]]()
    
    var selectedTabIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSubCategoryData()
        
        let performanceTrackListViewController: PerformanceTrackListViewController = self.storyboard!.instantiateViewController(withIdentifier: "PerformanceTrackListViewController") as! PerformanceTrackListViewController
        performanceTrackListViewController.title = "Performance"
        performanceTrackListViewController.view.backgroundColor = UIColor.clear
        
        let healthTrackListViewController: HealthTrackListViewController = self.storyboard!.instantiateViewController(withIdentifier: "HealthTrackListViewController") as! HealthTrackListViewController
        healthTrackListViewController.title = "Health"
        healthTrackListViewController.view.backgroundColor = UIColor.clear
        
        let innerBalanceTrackListViewController: InnerBalanceTrackListViewController = self.storyboard!.instantiateViewController(withIdentifier: "InnerBalanceTrackListViewController") as! InnerBalanceTrackListViewController
        innerBalanceTrackListViewController.title = "Inner Balance"
        innerBalanceTrackListViewController.view.backgroundColor = UIColor.clear
        
        let spiritualityTrackListViewController: SpiritualityTrackListViewController = self.storyboard!.instantiateViewController(withIdentifier: "SpiritualityTrackListViewController") as! SpiritualityTrackListViewController
        spiritualityTrackListViewController.title = "Spirituality"
        spiritualityTrackListViewController.view.backgroundColor = UIColor.clear
        
        let yslContainerViewController: YSLContainerViewController = YSLContainerViewController.init(controllers: [performanceTrackListViewController, healthTrackListViewController, innerBalanceTrackListViewController, spiritualityTrackListViewController], topBarHeight: 0, parentViewController: self, selectedIndex: selectedTabIndex)
        yslContainerViewController.view.backgroundColor = UIColor.init(patternImage: UIImage.init(named: "CategoryBackground")!)
        yslContainerViewController.view.frame = CGRect(x: 0, y: 64, width: SCREEN_WIDTH, height: SCREEN_HEIGHT - 64)
        yslContainerViewController.delegate = self
        
        self.view.addSubview(yslContainerViewController.view)
        self.view.bringSubview(toFront: btnBack)
        self.view.bringSubview(toFront: lblAddTracks)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Add Track Category list")
    }

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBack_Clicked(_ sender: AnyObject) {
        self.navigationController!.popViewController(animated: true)
    }
    
    func containerViewItemIndex(_ index: Int, currentController controller: UIViewController!) {

    }
    
    //MARK: - About track list API
    func getSubCategoryData() {
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetSubCategoryListWithTrackCount, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            self.subCategoryList = json.value(forKey: "data") as! [[String : AnyObject]]
                            for index in 0..<self.subCategoryList.count{
                                let catID = self.subCategoryList[index]["category_id"] as! Int
                                if catID == 1 {
                                    appDelegate.subCatHealthArray = self.subCategoryList[index]["sub_category"] as! [[String : AnyObject]]
                                    NotificationCenter.default.post(name: NSNotification.Name.init("SubCatHealthNotification"), object: nil, userInfo: nil)
                                } else if catID == 4 {
                                    appDelegate.subCatPerformanceArray = self.subCategoryList[index]["sub_category"] as! [[String : AnyObject]]
                                    NotificationCenter.default.post(name: NSNotification.Name.init("SubCatPerformanceNotification"), object: nil, userInfo: nil)
                                } else if catID == 3 {
                                    appDelegate.subCatSpiritualArray = self.subCategoryList[index]["sub_category"] as! [[String : AnyObject]]
                                    NotificationCenter.default.post(name: NSNotification.Name.init("SubCatSpiritualNotification"), object: nil, userInfo: nil)
                                } else {
                                    appDelegate.subCatInnerArray = self.subCategoryList[index]["sub_category"] as! [[String : AnyObject]]
                                    NotificationCenter.default.post(name: NSNotification.Name.init("SubCatInnerBalanceNotification"), object: nil, userInfo: nil)
                                }
                            }
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                            
                        }

                        
                    }else {
//                        if let message = json["message"].string {
//                            UIAlertController().alertViewWithTitleAndMessage(self, message: message)
//                        }
                    }
                case .failure(let error):
                    print (error)
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
