//
//  HealthTrackListViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 03/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit

class HealthTrackListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var tblHealth: UITableView!
    
    var refreshcontrol = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblHealth.estimatedRowHeight = 100
        tblHealth.rowHeight = UITableViewAutomaticDimension
        
        refreshcontrol = UIRefreshControl()
        refreshcontrol.tintColor = .white
        refreshcontrol.addTarget(self, action: #selector(HealthListViewController.refreshTableView(sender:)), for: .valueChanged)
        self.tblHealth.addSubview(refreshcontrol)
        
        NotificationCenter.default.addObserver(self, selector: #selector(HealthTrackListViewController.getHealthData(sender: )), name: NSNotification.Name.init("SubCatHealthNotification"), object: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func getHealthData(sender: Notification) {
        self.tblHealth.reloadData()
    }
    
    //MARK:- UIRefreshControl method
    func refreshTableView(sender:AnyObject) {
        self.tblHealth.reloadData()
        refreshcontrol.endRefreshing()
    }
    
    //TableView Delegate & Data source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return appDelegate.subCatHealthArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = UIView()
        headerView.backgroundColor = .clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        
        let cell: UITableViewCell! = tblHealth.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
        
        let imgMeditationType: UIImageView = cell.viewWithTag(10) as! UIImageView
        if let userImageUrl = appDelegate.subCatHealthArray[indexPath.section]["icon"] as? String {
            let userProfilePicture = MLDownloadImageView()
            userProfilePicture.downloadImage(with: NSURL.init(string: userImageUrl) as URL!, placeHolder: nil, imageError: nil, roundedCorners: false, block: { (image) in
                if image != nil {
                    imgMeditationType.image = image
                }
            })
            userProfilePicture.layer.masksToBounds = true
            userProfilePicture.cacheEnable = true
            userProfilePicture.startDownload()
        }

        
        let lblTrackName: UILabel = cell.viewWithTag(11) as! UILabel
        lblTrackName.text = appDelegate.subCatHealthArray[indexPath.section]["name"] as? String
        
        let lblNoOfTracks: UILabel = cell.viewWithTag(12) as! UILabel
        lblNoOfTracks.text = (appDelegate.subCatHealthArray[indexPath.section]["total_track"] as? String)! + " Tracks"
        
        cell.layer.borderColor = UIColor.white.cgColor
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1
        cell.layer.masksToBounds = true
        
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectTracksViewController: SelectTracksViewController = self.storyboard!.instantiateViewController(withIdentifier: "SelectTracksViewController") as! SelectTracksViewController
        selectTracksViewController.subCatID = appDelegate.subCatHealthArray[indexPath.section]["sub_category_id"] as! Int
//        meditationListViewController.strSubCategoryName = (appDelegate.subCatHealthArray[indexPath.section]["name"] as? String)!
//        meditationListViewController.strImgURL = (appDelegate.subCatHealthArray[indexPath.section]["icon"] as? String)!
        self.navigationController?.pushViewController(selectTracksViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
