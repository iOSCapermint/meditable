//
//  TabbarViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 26/04/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON


class TabbarViewController: XXXTabbarController, UINavigationControllerDelegate, NVActivityIndicatorViewable {
    
//    var isNewCustomMeditationAdded = false
    var isNewCustomMeditationAdded = true
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if(appDelegate.reachable.isReachable){
            autoLogin()
        }
        
        if(defaults.value(forKey: "bgVolume") == nil){
            defaults.set(0.5, forKey: "bgVolume")
        }

        self.reloadData([makeItem("LIBRARY", UIImage.init(named: "Library")!), makeItem("MY MEDITATIONS", UIImage.init(named: "Meditation")!), makeItem("PROFILE", UIImage.init(named: "Profile")!)])
        
        NotificationCenter.default.addObserver(self, selector: #selector(TabbarViewController.meditationAdded(sender: )), name: NSNotification.Name.init("FirstCustomMeditationAdded"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabbarViewController.displayMainCategories(sender: )), name: NSNotification.Name.init("DisplayMainCategory"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabbarViewController.displayCategoryView(sender: )), name: NSNotification.Name.init("DisplayCategoryView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabbarViewController.customMeditationAdded(sender: )), name: NSNotification.Name.init("CustomeMeditationAdded"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabbarViewController.setCustomMeditationAdded(sender: )), name: NSNotification.Name.init("SetCustomeMeditationAdded"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabbarViewController.setCreateMeditationView(sender: )), name: NSNotification.Name.init("CreateMeditationShow"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabbarViewController.setCusomMeditationView(sender: )), name: NSNotification.Name.init("CustomMeditationShow"), object: nil)

        
        //archive And Unarchive offline data
        if(defaults.value(forKey: kdownloadedMeditations) == nil){
            let archiveData = NSKeyedArchiver.archivedData(withRootObject: appDelegate.downloadedMeditations)
            defaults.set(archiveData, forKey: kdownloadedMeditations)
        }else {
            let unarchiveData = defaults.value(forKey: kdownloadedMeditations)
            appDelegate.downloadedMeditations = NSKeyedUnarchiver.unarchiveObject(with: unarchiveData as! Data) as! [[String : AnyObject]]
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Home")
    }
    
    //MARK:- AutoLogin -
    func autoLogin(){
        var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
        if authKey == nil {
            authKey = ""
        }
        let param = ["auth_key": authKey, "device_id" : "\(UIDevice.current.identifierForVendor!)", "device_name" : "\(UIDevice().modelName)", "app_version" : "\(Bundle.main.infoDictionary!["CFBundleShortVersionString"]!)"]
        
        APIManager().AutoLogin(parameters: param as! [String : String]) { (response, error) in
            if(error == nil){
                let responseResult = response! as! [String : AnyObject]
                if(Int("\(responseResult["status"]!)")! == 1){
                    
                    appDelegate.TotalMeditationTime = "\(responseResult["total_meditation_time"]!)"
                    if(Int("\(responseResult["count"]!)")! > 0){
                        self.subDictionary.setValue(self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationListViewController"), forKey: "1")
                    }else{
                        self.subDictionary.setValue(self.storyboard?.instantiateViewController(withIdentifier: "MyMeditationsViewController"), forKey: "1")
                    }
                    
                    numberOfDaysRemaining = Int("\(responseResult["remaining_days"]!)")!
                    let todayDate = Date().trialDateFormate()
                    appDelegate.membership_plan_id = "\(responseResult["membership_plan_id"]!)"
                    if(appDelegate.membership_plan_id == "2" || appDelegate.membership_plan_id == "5" || appDelegate.membership_plan_id == "3" || appDelegate.membership_plan_id == "6"){
                        self.get_receipt_info()
                    }


                    expiryDate = "\(responseResult["trial_expiry_date"]!)"
                    if(Int("\(responseResult["membership_plan_id"]!)")! == 0){
                        if(defaults.value(forKey: SubscriptionStartDate) != nil){
                            let storedDate = "\(defaults.value(forKey: SubscriptionStartDate)!)"
                            if(storedDate != todayDate){
                                defaults.set("\(todayDate)", forKey: SubscriptionStartDate)
                                NotificationCenter.default.post(name: NSNotification.Name.init("showSubscriptionView"), object: nil)

//                                NotificationCenter.default.post(name: NSNotification.Name.init("DisplayCategoryView"), object: nil)
                            }
                        }else{
                            defaults.set("\(todayDate)", forKey: SubscriptionStartDate)
                            NotificationCenter.default.post(name: NSNotification.Name.init("showSubscriptionView"), object: nil)
                        }
                    }
                }else{
                    self.subDictionary.setValue(self.storyboard?.instantiateViewController(withIdentifier: "MyMeditationsViewController"), forKey: "1")
                    
                    UIAlertController().alertViewWithTitleAndMessage(self, message: "\(responseResult["message"]!)")
                }
                
            }else{
                if(error?.code == 5 || error?.code == 3){
                    let alertController = UIAlertController(title: AppName, message: "Unauthorized", preferredStyle:UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                    { action -> Void in
                        let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        defaults.set(deviceToken,forKey: "DeviceToken")
                        defaults.synchronize()
                        let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                        self.navigationController?.pushViewController(welcomeViewController, animated: true)
                    })
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK: - GetMeditationList API
    func getCustomMeditationCount() {
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = [String : String]()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetcustommeditationcount, method: .post, parameters: params).validate().responseJSON { response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    
                    let responseResult = response.result.value as! [String : Any]
                    if(Int("\(responseResult["status"]!)")! == 1){
                        if(Int("\(responseResult["count"]!)")! > 0){
                            self.subDictionary.setValue(self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationListViewController"), forKey: "1")
                        }else{
                            self.subDictionary.setValue(self.storyboard?.instantiateViewController(withIdentifier: "MyMeditationsViewController"), forKey: "1")
                        }

                        numberOfDaysRemaining = Int("\(responseResult["remaining_days"]!)")!
                        
                        let todayDate = Date().trialDateFormate()
                        appDelegate.membership_plan_id = "\(responseResult["membership_plan_id"]!)"
                        if(Int("\(responseResult["membership_plan_id"]!)")! == 0){
                            if(defaults.value(forKey: SubscriptionStartDate) != nil){
                                let storedDate = "\(defaults.value(forKey: SubscriptionStartDate)!)"
                                if(storedDate != todayDate){
                                    defaults.set("\(todayDate)", forKey: SubscriptionStartDate)
                                    NotificationCenter.default.post(name: NSNotification.Name.init("showSubscriptionView"), object: nil)
                                }
                            }else{
                                defaults.set("\(todayDate)", forKey: SubscriptionStartDate)
                                NotificationCenter.default.post(name: NSNotification.Name.init("showSubscriptionView"), object: nil)
                            }
                            
                        }
                    }else if(Int("\(responseResult["status"]!)")! == 5 || Int("\(responseResult["status"]!)")! == 3){
                        
                        let alertController = UIAlertController(title: AppName, message: "\(responseResult["message"]!)", preferredStyle:UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                        { action -> Void in
                            let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                            
                            defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                            defaults.set(deviceToken,forKey: "DeviceToken")
                            defaults.synchronize()
                            
                            let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                            self.navigationController?.pushViewController(welcomeViewController, animated: true)
                        })
                        self.present(alertController, animated: true, completion: nil)
                    }else{
                        self.subDictionary.setValue(self.storyboard?.instantiateViewController(withIdentifier: "MyMeditationsViewController"), forKey: "1")

                            UIAlertController().alertViewWithTitleAndMessage(self, message: "\(responseResult["message"]!)")
                    }
                    
                case .failure(let error):
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    //MARK:- Check Cancel Reciept - 
    func get_receipt_info(){
        let receiptURL = Bundle.main.appStoreReceiptURL
        let receipt = NSData(contentsOf: receiptURL!)
        if(receipt != nil){
            let requestContents: [String: Any] = [
                "receipt-data": receipt!.base64EncodedString(options: []),
                "password": "be1d22c5683b4f0bad617d49f6602bb1"
            ]
            
            let appleServer = receiptURL?.lastPathComponent == "sandboxReceipt" ? "sandbox" : "buy"
            
            let stringURL = "https://\(appleServer).itunes.apple.com/verifyReceipt"
            
            Alamofire.request(stringURL, method: .post, parameters: requestContents, encoding: JSONEncoding.default)
                .responseJSON { response in
                    if let value = response.result.value as? NSDictionary {
                        let dd = value["latest_receipt_info"] as! [[String : AnyObject]]
                        if(dd.last?["cancellation_date"] != nil){
                            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
                            if authKey == nil {
                                authKey = ""
                            }
                            APIManager().cancelMembershipAPI(parameters: ["auth_key" : authKey], completion: { (response, error) in
                                if(error == nil){
                                    let responseResult = response! as! [String : AnyObject]
                                    if(Int("\(responseResult["status"]!)")! == 1){
                                        APIManager().autoLoginForSave()
                                    }
                                }
                            })
                            print("************************canceled*********************")
                        }else{
                            print("************************continue*********************")
                        }
                    } else {
                        print("Receiving receipt from App Store failed: \(response.result)")
                    }
            }
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //Notification Method
    func meditationAdded(sender: Notification) {
        isNewCustomMeditationAdded = true

        for var viewController: UIViewController in (self.navigationController?.viewControllers)! {
            if viewController.isKind(of: TabbarViewController.self) {
                self.subDictionary.removeObject(forKey: "1")
                self.subDictionary.setValue(self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationListViewController"), forKey: "1")
                self.loadViewController(at: 1, animate: false)
                break
            }
        }
    }
    
    func customMeditationAdded(sender: Notification) {
        isNewCustomMeditationAdded = true
        self.subDictionary.setValue(self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationListViewController"), forKey: "1")
        self.loadViewController(at: 1, animate: true)
    }
    
    func setCustomMeditationAdded(sender: Notification) {
        isNewCustomMeditationAdded = true
        self.subDictionary.setValue(self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationListViewController"), forKey: "1")
    }

    
    func displayCategoryView(sender: Notification) {
        self.subDictionary.removeObject(forKey: "0")
        self.subDictionary.setValue(self.storyboard?.instantiateViewController(withIdentifier: "LibraryViewController"), forKey: "0")
        self.tabbar.selectTabIndex(0, animate: true)
        self.loadViewController(at: 0, animate: true)
    }
    
    func displayMainCategories(sender: Notification) {
        self.subDictionary.removeObject(forKey: "0")
        self.subDictionary.setValue(self.storyboard?.instantiateViewController(withIdentifier: "CategoriesViewController"), forKey: "0")
        if let selectedCategory = sender.userInfo?["SelectedCategory"] as? Int {
            appDelegate.selectedTabIndex = selectedCategory
            if(selectedCategory == 0){
                addGoogleAnalytics(screenName: "Performance List Category Screen")
            }else if(selectedCategory == 1){
                addGoogleAnalytics(screenName: "Health List Category Screen")
            }else if(selectedCategory == 2){
                addGoogleAnalytics(screenName: "Inner Balance Category Screen")
            }else if(selectedCategory == 3){
                addGoogleAnalytics(screenName: "Spirituality Category Screen")
            }
        }
        self.loadViewController(at: 0, animate: true)
    }
    
    
    
    func setCreateMeditationView(sender: Notification){
        self.subDictionary.setValue(self.storyboard?.instantiateViewController(withIdentifier: "MyMeditationsViewController"), forKey: "1")
        self.loadViewController(at: 1, animate: true)
    }
    
    func setCusomMeditationView(sender: Notification){
        self.subDictionary.setValue(self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationListViewController"), forKey: "1")
    }
    
    
    //Tabbar Delegate Method
    override func viewController(at idx: UInt) -> UIViewController! {
        switch (idx) {
        case 0:
            return self.storyboard?.instantiateViewController(withIdentifier: "LibraryViewController")
            
        case 1:
            if isNewCustomMeditationAdded {
                return self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationListViewController")
            }
            else {
                return self.storyboard?.instantiateViewController(withIdentifier: "MyMeditationsViewController")
            }
        case 2:
            return self.storyboard?.instantiateViewController(withIdentifier: "MyActivityViewController")
            
        default:
            break
        }
        return nil
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //NotificationCenter.default.removeObserver(self, name: Notification.Name.init("FirstCustomMeditationAdded"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
