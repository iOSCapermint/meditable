//
//  MyMeditationsViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 27/04/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit

class MyMeditationsViewController: UIViewController {
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var lblStartMessage: UILabel!
    
    @IBOutlet weak var lblMessage: UILabel!

    @IBOutlet weak var btnCreateMeditation: UIButton!
    
    @IBOutlet weak var lblStartMessageTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        if IS_IPHONE4s || IS_IPHONE5 {
            lblStartMessageTopConstraint.constant = 230
        }
        else if IS_IPHONE6P {
            lblStartMessageTopConstraint.constant = 320
        }
        
        btnCreateMeditation.layer.cornerRadius = btnCreateMeditation.frame.size.height / 2
        btnCreateMeditation.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnCreateMeditation.layer.borderWidth = 1.0
        btnCreateMeditation.layer.masksToBounds = true
   }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Create New Meditation")
    }

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func btnCreateMeditation_Clicked(_ sender: Any) {
        if(appDelegate.reachable.isReachable){
            let customMeditationViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationViewController") as! CustomMeditationViewController
            self.navigationController?.pushViewController(customMeditationViewController, animated: true)
        }else{
            let alertController = UIAlertController(title: AppName, message: NoInternetMessage, preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            { action -> Void in
                self.navigationController?.popViewController(animated: true)
            })
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
