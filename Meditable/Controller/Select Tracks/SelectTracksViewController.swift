//
//  SelectTracksViewController.swift
//  Meditable
//
//  Created by Apple on 19/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON

class SelectTracksViewController: UIViewController,NVActivityIndicatorViewable,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblSelectTracks: UILabel!
    
    @IBOutlet weak var tblSelectTracks: UITableView!
    
    var subCatID = Int()
    
    var tracksArray = [[String: AnyObject]]()

    //MARK: - View LifeCycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        getTrackList()
        
        tblSelectTracks.estimatedRowHeight = 100
        tblSelectTracks.rowHeight = UITableViewAutomaticDimension
    }

    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        addGoogleAnalytics(screenName: "Add new track list")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - Button Actions -
    @IBAction func btnBack_Clicked(_ sender: Any) {
        //_ = self.navigationController?.popViewController(animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    //Check Track Info
    func checkTrackInfo(sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(.zero, to: tblSelectTracks)
        let indexPath: NSIndexPath = tblSelectTracks.indexPathForRow(at: buttonPosition) as NSIndexPath!
        let trackInfoViewController: TrackInfoViewController = self.storyboard?.instantiateViewController(withIdentifier: "TrackInfoViewController") as! TrackInfoViewController
        trackInfoViewController.trackList = tracksArray[indexPath.section]
        self.navigationController?.pushViewController(trackInfoViewController, animated: true)
    }
    
    //TableView Delegate & Data source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tracksArray.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = UIView()
        headerView.backgroundColor = .clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cellSelectTracks"
        
        let cell: UITableViewCell! = tblSelectTracks.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
        
        let lblTrackName: UILabel = cell.viewWithTag(11) as! UILabel
        lblTrackName.text = self.tracksArray[indexPath.section]["title"] as? String
        
        let lblTrackTime: UILabel = cell.viewWithTag(12) as! UILabel
        lblTrackTime.text = self.tracksArray[indexPath.section]["time"] as? String
        
        let btnTrackInfo: UIButton = cell.viewWithTag(13) as! UIButton
        btnTrackInfo.addTarget(self, action: #selector(SelectTracksViewController.checkTrackInfo(sender: )), for: .touchUpInside)
        
        cell.layer.borderColor = UIColor.white.cgColor
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1
        cell.layer.masksToBounds = true
        
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for var viewController: UIViewController in (self.navigationController?.viewControllers)! {
            if viewController.isKind(of: MeditationTrackListViewController.self) {
                NotificationCenter.default.post(name: Notification.Name("SelectedTrackToAdd"), object: tracksArray[indexPath.section])
                self.navigationController?.popToViewController(viewController, animated: true)
                break
            }
            if viewController.isKind(of: CustomMeditationPlaylistViewController.self) {
                NotificationCenter.default.post(name: Notification.Name("SelectedTrackToAddInCustomMeditaionList"), object: tracksArray[indexPath.section])
                self.navigationController?.popToViewController(viewController, animated: true)
                break
            }
            if viewController.isKind(of: CustomMeditationViewController.self) {
                NotificationCenter.default.post(name: Notification.Name("SelectedTrackToAddInCreaeList"), object: tracksArray[indexPath.section])
                self.navigationController?.popToViewController(viewController, animated: true)
                break
            }
        }
    }
    
    //MARK: - get track list API
    func getTrackList() {
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey, "sub_category_id": self.subCatID]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetTracksList, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                         if json.value(forKey: "status") as? Int == 1{
                            self.tracksArray = json.value(forKey: "data") as! [[String : AnyObject]]
                            
                            if self.tracksArray.count > 0{
                                self.tblSelectTracks.reloadData()
                            }else{
                                let alertSuccess: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: "No Tracks available", preferredStyle: .alert)
                                
                                let btnOk: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
                                    _ in
                                    let transition = CATransition()
                                    transition.duration = 0.3
                                    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                                    transition.type = kCATransitionPush
                                    self.navigationController?.view.layer.add(transition, forKey: nil)
                                    _ = self.navigationController?.popViewController(animated: true)
                                })
                                
                                alertSuccess.addAction(btnOk)
                                self.present(alertSuccess, animated: true, completion: nil)
                            }
                         }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                        
                    }else {
                        //                        if let message = json["status"].string {
                        //                            UIAlertController().alertViewWithTitleAndMessage(self, message: message)
                        //                        }
                    }
                case .failure(let error):
                    print (error)
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
