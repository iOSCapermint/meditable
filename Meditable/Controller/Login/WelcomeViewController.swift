//
//  WelcomeViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 15/04/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var lblLetsStart: UILabel!
    
    @IBOutlet weak var lblStartMessage: UILabel!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var btnSignup: UIButton!
    
    @IBOutlet weak var lblLetsStartTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var loginTopConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        if IS_IPHONE5 {
//            lblLetsStartTopConstraint.constant = 250
//        }
//        else if IS_IPHONE6P {
//            lblLetsStartTopConstraint.constant = 350
//        }

        btnLogin.layer.cornerRadius = btnLogin.frame.size.height / 2
        
        btnSignup.layer.cornerRadius = btnSignup.frame.size.height / 2
        btnSignup.layer.borderColor = UIColor().whiteBorderColor.cgColor
        btnSignup.layer.borderWidth = 1.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Welcome")
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func btnLogin_Clicked(_ sender: Any) {
        let loginViewController: LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(loginViewController, animated: true)
    }
    
    @IBAction func btnSignup_Clicked(_ sender: Any) {
        let signupViewController: SignupViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(signupViewController, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
