//
//  SignupViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 15/04/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON

class SignupViewController: UIViewController, NVActivityIndicatorViewable, UITextFieldDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var btnEditProfileImage: UIButton!
    
    @IBOutlet weak var lblSignup: UILabel!
    
    @IBOutlet weak var txtName: CustomTextfield!
    
    @IBOutlet weak var txtEmail: CustomTextfield!
    
    @IBOutlet weak var txtPassword: CustomTextfield!
    
    @IBOutlet weak var btnShowPassword: UIButton!
    
    @IBOutlet weak var txtConfirmPassword: CustomTextfield!
    
    @IBOutlet weak var btnShowConfirmPassword: UIButton!
    
    @IBOutlet weak var btnSignup: UIButton!
    
    @IBOutlet weak var btnEditProfileImageTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnSignupTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnTermsAndConditions: UIButton!
    
    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtName.layer.cornerRadius = txtName.frame.size.height / 2
        txtEmail.layer.cornerRadius = txtEmail.frame.size.height / 2
        
        txtPassword.layer.cornerRadius = txtPassword.frame.size.height / 2
        btnShowPassword.layer.cornerRadius = btnShowPassword.frame.size.height / 2
        
        txtConfirmPassword.layer.cornerRadius = txtConfirmPassword.frame.size.height / 2
        btnShowConfirmPassword.layer.cornerRadius = btnShowConfirmPassword.frame.size.height / 2
        
        btnSignup.layer.cornerRadius = btnSignup.frame.size.height / 2
        btnSignup.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnSignup.layer.borderWidth = 1.0
        
        imgUser.layer.cornerRadius = imgUser.frame.size.height / 2
        imgUser.layer.masksToBounds = true
        
        if IS_IPHONE5 {
            btnEditProfileImageTrailingConstraint.constant = 95
        }
        else if IS_IPHONE6P {
            btnEditProfileImageTrailingConstraint.constant = 144
            btnSignupTopConstraint.constant = 116
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        addGoogleAnalytics(screenName: "Signup")
    }

    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        //_ = self.navigationController?.popViewController(animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnEditProfileImage_Clicked(_ sender: Any) {
        let actionSheet: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: "Upload profile image", preferredStyle: .actionSheet)
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheet.addAction(cancelAction)
        
        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Using Camera", style: .default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(takePictureAction)
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose Existing Photo", style: .default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
                imagePicker.allowsEditing = false
                imagePicker.mediaTypes = [kUTTypeImage as String]
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(choosePictureAction)
        
        //Present the AlertController
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func btnShowPassword_Clicked(_ sender: Any) {
        txtPassword.isSecureTextEntry = !txtPassword.isSecureTextEntry
    }
    
    @IBAction func btnShowConfirmPassword_Clicked(_ sender: Any) {
        txtConfirmPassword.isSecureTextEntry = !txtConfirmPassword.isSecureTextEntry
    }
    
    @IBAction func btnSignup_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        
        if txtName.text?.characters.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please Enter Name")
        }
        else if txtEmail.text?.characters.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please Enter Email")
        }
        else if txtPassword.text?.characters.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please Enter Password")
        }
        else if txtConfirmPassword.text?.characters.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please Enter Confirm Password")
        }
        else if !(txtEmail.text?.isEmpty)! && !HelperMethod.isValidEmail(txtEmail.text, strict: false) {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please Enter Valid Email")
        }
        else if !(txtPassword.text?.isEmpty)! && !(txtConfirmPassword.text?.isEmpty)! && txtPassword.text != txtConfirmPassword.text {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Password and Confirm Password doesn't match")
        }
        else {
            //            let tabbarViewController: TabbarViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
            //            tabbarViewController.isNewCustomMeditationAdded = false
            //            self.navigationController?.pushViewController(tabbarViewController, animated: true)
            
            self .postSignupData()
            
        }
    }
    
    @IBAction func btnTermsCondition_Clicked(_ sender: Any) {
        let termsAndConditionsViewController: TermsAndConditionsViewController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as! TermsAndConditionsViewController
        self.navigationController?.pushViewController(termsAndConditionsViewController, animated: true)
    }
    
    @IBAction func onBtnPrivacyPolicyClick(_ sender: Any) {
        let privacyPolicyViewController: PrivacyPolicyViewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
        self.navigationController?.pushViewController(privacyPolicyViewController, animated: true)
    }
    
    //MARK: - API for Signup
    func postSignupData() {
        if appDelegate.reachable.isReachable{
            var deviceToken = ""
            if defaults.value(forKey: "DeviceToken") as? String != nil {
                deviceToken = defaults.value(forKey: "DeviceToken") as! String
            }
            else {
                deviceToken = ""
            }
            
            var isUpload = "N"
            if self.imgUser.image != nil {
                isUpload = "Y"
            }else{
                isUpload = "N"
            }
            
            let params: Parameters = ["User[user_name]": txtName.text!, "User[email]": txtEmail.text!, "User[password]": txtPassword.text!, "User[device_token]": deviceToken, "User[app_version]": appVersion, "User[device_name]": "\(UIDevice.current.identifierForVendor!)", "User[is_upload]" : isUpload]
            
            let size = CGSize(width: 50, height: 50)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.upload(multipartFormData: {
                multipartFormData in
                //append image
                if let imageData = UIImageJPEGRepresentation(self.imgUser.image!, 0.2) {
                    multipartFormData.append(imageData, withName: "file", fileName: "user.png", mimeType: "image/png")
                }
                
                //append parameters
                for (key, value) in params {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, usingThreshold: 1, to: baseURLWithoutAuth + kUserSignup, method: .post, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON(completionHandler: {
                        response in
                        self.stopAnimating()
                        
                        switch response.result {
                        case .success:
                            let json = JSON(response.result.value!)
                            if json["status"].int == 1 {
                                print(json)
                                //
//                                let userId = json["data"]["user_id"].int!
                                
//                                defaults.setValue(String(userId), forKey: "user_id")
//                                defaults.setValue(json["data"]["auth_key"].string, forKey: "AuthKey")
//                                defaults.setValue(json["data"]["user_name"].string, forKey: "Name")
//                                defaults.setValue(json["data"]["email"].string, forKey: "Email")
//                                defaults.setValue(json["data"]["image_url"].string, forKey: "image_url")
//                                defaults.setValue(json["data"]["notificationstatus"].string, forKey: "PushNotification")
//                                defaults.synchronize()

                                let alertController = UIAlertController(title: AppName, message: signUpSuccessfull, preferredStyle:UIAlertControllerStyle.alert)
                                alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                                { action -> Void in
                                    self.navigationController?.popViewController(animated: true)
                                })
                                self.present(alertController, animated: true, completion: nil)

                                
                                
//                                let tabbarViewController: TabbarViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
//                                self.navigationController?.pushViewController(tabbarViewController, animated: true)
                            }
                            else {
                                if let message = json["message"].string {
                                    UIAlertController().alertViewWithTitleAndMessage(self, message: message)
                                }
                            }
                        case .failure(let error):
                            print (error)
                            UIAlertController().alertViewWithErrorMessage(self)
                        }
                    })
                case .failure(let encodingError):
                    self.stopAnimating()
                    UIAlertController().alertViewWithTitleAndMessage(self, message: encodingError.localizedDescription)
                }
            })
        }else{
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    //Text Field Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtName.isFirstResponder {
            txtEmail.becomeFirstResponder()
        }
        else if txtEmail.isFirstResponder {
            txtPassword.becomeFirstResponder()
        }
        else if txtPassword.isFirstResponder {
            txtConfirmPassword.becomeFirstResponder()
        }
        else if txtConfirmPassword.isFirstResponder {
            txtConfirmPassword.resignFirstResponder()
        }
        
        return true
    }
    
    //MARK: ImagePickerController Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if var image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.dismiss(animated: true, completion: nil)
            image = image.scale(toNewSize: self.imgUser.frame.size)
            self.imgUser.image = image
            self.imgUser.contentMode = .scaleAspectFill
            self.imgUser.layer.masksToBounds = true
            //self.updateProfileImage()
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
