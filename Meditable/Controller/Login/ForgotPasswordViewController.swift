//
//  ForgotPasswordViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 15/04/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class ForgotPasswordViewController: UIViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblForgotPassword: UILabel!
    
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var txtEmail: CustomTextfield!
    
    @IBOutlet weak var btnReset: UIButton!
    
    @IBOutlet weak var lblForgotPasswordTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnResetTopConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if IS_IPHONE5 {
            lblForgotPasswordTopConstraint.constant = 70
            btnResetTopConstraint.constant = 148
        }
        else if IS_IPHONE6P {
            lblForgotPasswordTopConstraint.constant = 110
            btnResetTopConstraint.constant = 286
        }
        
        txtEmail.layer.cornerRadius = txtEmail.frame.size.height / 2
        
        btnReset.layer.cornerRadius = btnReset.frame.size.height / 2
        btnReset.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnReset.layer.borderWidth = 1.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        addGoogleAnalytics(screenName: "Forgot Password")
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    @IBAction func btnBack_Clicked(_ sender: Any) {
        //_ = self.navigationController?.popViewController(animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnReset_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        
        if txtEmail.text?.characters.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please Enter Email")
        }
        else if !(txtEmail.text?.isEmpty)! && !HelperMethod.isValidEmail(txtEmail.text, strict: false) {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please Enter Valid Email")
        }
        else {
            if appDelegate.reachable.isReachable{
                
                let params: Parameters = ["email": txtEmail.text!]
                let size = CGSize(width: 50, height: 50)
                let color : UIColor = .white
                startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
                
                Alamofire.request(baseURLWithoutAuth + kResetPwd , method: .post, parameters: params).validate().responseJSON { response in
                    self.stopAnimating()
                    switch response.result {
                    case .success:
                        if let json: NSDictionary = response.result.value as! NSDictionary? {
                            if json.value(forKey: "status") as? Int == 1 {
                                let alertController: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: json.value(forKey: "message") as? String, preferredStyle: .alert)
                                
                                let btnOk: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
                                    _ in
                                    _ = self.navigationController?.popViewController(animated: true)
                                })
                                
                                alertController.addAction(btnOk)
                                self.present(alertController, animated: true, completion: nil)
                            }
                            else {
                                UIAlertController().alertViewWithTitleAndMessage(self, message: json.value(forKey: "message") as! String)
                            }
                        }
                    case .failure(let error):
                        print (error)
                        UIAlertController().alertViewWithErrorMessage(self)
                    }
                }
            }
            else {
                UIAlertController().alertViewWithNoInternet(self)
            }
        }
    }
    
    //Text Field Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtEmail.isFirstResponder {
            txtEmail.resignFirstResponder()
        }
        
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
