//
//  TutorialViewController.swift
//  Meditable
//
//  Created by capermintmini6 on 03/08/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var textView: UITextView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var backgroundImageFirstScreen: UIImageView!
    @IBOutlet var backgroundImageSecondScreen: UIImageView!
    @IBOutlet var backgroundImageThirdScreen: UIImageView!
    @IBOutlet var backgroundImageForthScreen: UIImageView!
    @IBOutlet var backgroundImageFifthScreen: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    
    var lblFirstScreenSubtitle : UILabel!
    var lblSecondScreenSubtitle : UILabel!
    var lblThirdScreenSubtitle : UILabel!
    var lblFourthScreenSubtitle : UILabel!
    var lblFifthScreenSubtitle : UILabel!

    var lblFirstScreenTitle : UILabel!
    var lblSecondScreenTitle : UILabel!
    var lblThirdScreenTitle : UILabel!
    var lblForthScreenTitle : UILabel!
    var lblFifthScreenTitle : UILabel!

    var fontName : String = "themeFuturaFontName"
    var yPositionForTitleLabel : CGFloat! = 340
    var yPositionForSubtitleLabel  : CGFloat! = 368
    var heightForTitleLabel : CGFloat! = 60
    var heightForSubtitleLabel : CGFloat! = 140
    var TitltFontSize : CGFloat! = 18
    var SubTitleFontSize : CGFloat! = 15
    var currentPage = 0
    //    var backgroundImageFirstScreen: UIImageView!
    //    var backgroundImageSecondScreen: UIImageView!
    //    var backgroundImageThirdScreen: UIImageView!
    //    var backgroundImageForthScreen: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if(IS_IPHONE5 || IS_IPHONE4s){
            yPositionForTitleLabel = self.view.frame.height - 235
            yPositionForSubtitleLabel  = self.view.frame.height - 210
            heightForSubtitleLabel = 120
        }else{
            yPositionForTitleLabel = self.view.frame.height - 245
            yPositionForSubtitleLabel  = self.view.frame.height - 235
        }
        
        //Set scroll view
        self.scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        //Set Title
        self.setTitleForAllScreens()
        //Set Subtitle
        self.setSubtitleForAllScreens()
        //Set content size of Scroll view
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width * 5, height: self.scrollView.frame.height)
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
    }

    func setScrollViewAnimation(_ scrollViewObj:UIScrollView, width:CGFloat, firstTitle: UILabel, secondTitle: UILabel, firstSubtitle: UILabel, secondSubtitle: UILabel, firstImage:UIImageView, secondImage:UIImageView) {
        
        let scrollViewWidth = scrollViewObj.frame.size.width
        let scrollContentOffset = scrollView.contentOffset.x - width
        
        let percentForLabelTransform = (scrollContentOffset / scrollViewWidth)
        let ReversePercentForLabelTransform = 1 - percentForLabelTransform
        
        print("percentForLabelTransform = \(percentForLabelTransform)")
        print("ReversePercentForLabelTransform = \(ReversePercentForLabelTransform)")
        
        firstImage.alpha = ReversePercentForLabelTransform
        secondImage.alpha = percentForLabelTransform
    }
    
    
    //MARK: UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        if(scrollView.contentOffset.x > 0 && scrollView.contentOffset.x <= scrollViewWidth) {
            self.setScrollViewAnimation(self.scrollView, width: 0, firstTitle: lblFirstScreenTitle, secondTitle: lblSecondScreenTitle, firstSubtitle: lblFirstScreenSubtitle, secondSubtitle: lblSecondScreenSubtitle,firstImage: backgroundImageFirstScreen,secondImage: backgroundImageSecondScreen)
            currentPage = 1
            
        } else if(scrollView.contentOffset.x > scrollViewWidth && scrollView.contentOffset.x <= scrollViewWidth*2){
            self.setScrollViewAnimation(self.scrollView, width: scrollViewWidth, firstTitle: lblSecondScreenTitle, secondTitle: lblThirdScreenTitle, firstSubtitle: lblSecondScreenSubtitle, secondSubtitle: lblThirdScreenSubtitle,firstImage: backgroundImageSecondScreen,secondImage: backgroundImageThirdScreen)
            currentPage = 2
            
        } else if(scrollView.contentOffset.x > scrollViewWidth*2 && scrollView.contentOffset.x <= scrollViewWidth*3){
            self.setScrollViewAnimation(self.scrollView, width: scrollViewWidth*2, firstTitle: lblThirdScreenTitle, secondTitle: lblForthScreenTitle, firstSubtitle: lblThirdScreenSubtitle, secondSubtitle: lblFourthScreenSubtitle,firstImage: backgroundImageThirdScreen,secondImage: backgroundImageForthScreen)
            currentPage = 3
        } else if(scrollView.contentOffset.x > scrollViewWidth*3 && scrollView.contentOffset.x <= scrollViewWidth*4){
            self.setScrollViewAnimation(self.scrollView, width: scrollViewWidth*3, firstTitle: lblForthScreenTitle, secondTitle: lblFifthScreenTitle, firstSubtitle: lblFourthScreenSubtitle, secondSubtitle: lblFifthScreenSubtitle,firstImage: backgroundImageForthScreen,secondImage: backgroundImageFifthScreen)
            currentPage = 4
        }else{
            currentPage = 0
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        // Set current screen/page index
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        self.pageControl.currentPage = Int(currentPage);
    }
    
    func scrollToPage(page: Int, animated: Bool) {
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(page);
        frame.origin.y = 0;
        self.scrollView.scrollRectToVisible(frame, animated: animated)
        self.pageControl.currentPage = page
    }

    
    @IBAction func btnNext(_ sender: UIButton) {
        if(currentPage != 4){
            self.scrollToPage(page: currentPage + 1, animated: true)
        }else{
            let welcomeViewController: WelcomeViewController = self.storyboard!.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
            self.navigationController?.pushViewController(welcomeViewController, animated: true)
        }
    }
    
    @IBAction func btnSkip(_ sender: UIButton) {
        let welcomeViewController: WelcomeViewController = self.storyboard!.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
        self.navigationController?.pushViewController(welcomeViewController, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTitleForAllScreens()
    {
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        
        lblFirstScreenTitle = UILabel()
        lblFirstScreenTitle.frame = CGRect(x: 60, y: yPositionForTitleLabel+5, width: scrollViewWidth-120, height: heightForTitleLabel)
        lblFirstScreenTitle.text = "Choose Your Goals"
        lblFirstScreenTitle.textAlignment = .center
        lblFirstScreenTitle.font = UIFont.init(name: String().themeFuturaFontName, size: TitltFontSize)
        lblFirstScreenTitle.textColor = UIColor().yellowBorderColor
        lblFirstScreenTitle.numberOfLines = 1
        
        lblSecondScreenTitle = UILabel()
        if(IS_IPHONE4s || IS_IPHONE5){
            lblSecondScreenTitle.frame = CGRect(x: scrollViewWidth+60, y: yPositionForTitleLabel+10, width: scrollViewWidth-120, height: heightForTitleLabel)
        }else{
            lblSecondScreenTitle.frame = CGRect(x: scrollViewWidth+60, y: yPositionForTitleLabel+5, width: scrollViewWidth-120, height: heightForTitleLabel)
        }
        lblSecondScreenTitle.text = "Add Tracks"
        lblSecondScreenTitle.textAlignment = .center
        lblSecondScreenTitle.font = UIFont.init(name: String().themeFuturaFontName, size: TitltFontSize)
        lblSecondScreenTitle.textColor = UIColor().yellowBorderColor
        lblSecondScreenTitle.numberOfLines = 1
        
        lblThirdScreenTitle = UILabel()
        lblThirdScreenTitle.frame = CGRect(x: (scrollViewWidth*2)+60, y: yPositionForTitleLabel, width: scrollViewWidth-120, height: heightForTitleLabel)
        lblThirdScreenTitle.text = "Edit Intro & Ending"
        lblThirdScreenTitle.textAlignment = .center
        lblThirdScreenTitle.font = UIFont.init(name: String().themeFuturaFontName, size: TitltFontSize)
        lblThirdScreenTitle.textColor = UIColor().yellowBorderColor
        lblThirdScreenTitle.numberOfLines = 1
        
        lblForthScreenTitle = UILabel()
        lblForthScreenTitle.frame = CGRect(x: (scrollViewWidth*3)+60, y: yPositionForTitleLabel, width: scrollViewWidth-120, height: heightForTitleLabel)
        lblForthScreenTitle.text = "Add Silence"
        lblForthScreenTitle.textAlignment = .center
        lblForthScreenTitle.font = UIFont.init(name: String().themeFuturaFontName, size: TitltFontSize)
        lblForthScreenTitle.textColor = UIColor().yellowBorderColor
        lblForthScreenTitle.numberOfLines = 1
        
        lblFifthScreenTitle = UILabel()
        lblFifthScreenTitle.frame = CGRect(x: (scrollViewWidth*4)+60, y: yPositionForTitleLabel, width: scrollViewWidth-120, height: heightForTitleLabel)
        lblFifthScreenTitle.text = "Choose Music"
        lblFifthScreenTitle.textAlignment = .center
        lblFifthScreenTitle.font = UIFont.init(name: String().themeFuturaFontName, size: TitltFontSize)
        lblFifthScreenTitle.textColor = UIColor().yellowBorderColor
        lblFifthScreenTitle.numberOfLines = 1
        
        lblFirstScreenTitle.lineBreakMode = .byClipping
        lblFirstScreenTitle.adjustsFontSizeToFitWidth = true
        lblFirstScreenTitle.minimumScaleFactor = 0.5
        lblFirstScreenTitle.font = UIFont.init(name: String().themeFuturaFontName, size: TitltFontSize)
        lblFirstScreenTitle.font = UIFont.init(name: String().themeFuturaFontName, size: TitltFontSize)

        lblSecondScreenTitle.lineBreakMode = .byClipping
        lblSecondScreenTitle.adjustsFontSizeToFitWidth = true
        lblSecondScreenTitle.minimumScaleFactor = 0.5
        lblSecondScreenTitle.font = UIFont.init(name: String().themeFuturaFontName, size: TitltFontSize)

        lblThirdScreenTitle.lineBreakMode = .byClipping
        lblThirdScreenTitle.adjustsFontSizeToFitWidth = true
        lblThirdScreenTitle.minimumScaleFactor = 0.5
        lblThirdScreenTitle.font = UIFont.init(name: String().themeFuturaFontName, size: TitltFontSize)

        lblForthScreenTitle.lineBreakMode = .byClipping
        lblForthScreenTitle.adjustsFontSizeToFitWidth = true
        lblForthScreenTitle.minimumScaleFactor = 0.5
        lblForthScreenTitle.font = UIFont.init(name: String().themeFuturaFontName, size: TitltFontSize)

        lblFifthScreenTitle.lineBreakMode = .byClipping
        lblFifthScreenTitle.adjustsFontSizeToFitWidth = true
        lblFifthScreenTitle.minimumScaleFactor = 0.5
        lblFifthScreenTitle.font = UIFont.init(name: String().themeFuturaFontName, size: TitltFontSize)
        lblFifthScreenTitle.font = UIFont.init(name: String().themeFuturaFontName, size: TitltFontSize)
        
        self.scrollView.addSubview(lblFirstScreenTitle)
        self.scrollView.addSubview(lblSecondScreenTitle)
        self.scrollView.addSubview(lblThirdScreenTitle)
        self.scrollView.addSubview(lblForthScreenTitle)
        self.scrollView.addSubview(lblFifthScreenTitle)
    }
    
    //Set subtitle text + position for all screen as provided in Sketch
    func setSubtitleForAllScreens()
    {
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        
        lblFirstScreenSubtitle = UILabel()
        lblFirstScreenSubtitle.frame = CGRect(x: 40, y: yPositionForSubtitleLabel+5, width: scrollViewWidth-80, height: heightForSubtitleLabel)
        lblFirstScreenSubtitle.text = "Choose from various life areas you'd like to improve and mix different topics within a meditation session."
        lblFirstScreenSubtitle.textAlignment = .center
        lblFirstScreenSubtitle.font = UIFont.systemFont(ofSize: 15)
        lblFirstScreenSubtitle.textColor = UIColor.white
        lblFirstScreenSubtitle.numberOfLines = 0
        
        lblSecondScreenSubtitle = UILabel()
        if(IS_IPHONE4s || IS_IPHONE5){
            lblSecondScreenSubtitle.frame = CGRect(x: scrollViewWidth+40, y: yPositionForSubtitleLabel+10, width: scrollViewWidth-80, height: heightForSubtitleLabel)
        }else{
            lblSecondScreenSubtitle.frame = CGRect(x: scrollViewWidth+40, y: yPositionForSubtitleLabel+8, width: scrollViewWidth-80, height: heightForSubtitleLabel)
        }
        lblSecondScreenSubtitle.text = "Add 3-8-minute meditation tracks to your session.Add as many as you want. This determines the length & focus of your meditation."
        lblSecondScreenSubtitle.textAlignment = .center
        lblSecondScreenSubtitle.font = UIFont.systemFont(ofSize: 15)
        lblSecondScreenSubtitle.textColor = UIColor.white
        lblSecondScreenSubtitle.numberOfLines = 0
        
        lblThirdScreenSubtitle = UILabel()
        if(IS_IPHONE6){
            lblThirdScreenSubtitle.frame = CGRect(x: (scrollViewWidth*2)+40, y: yPositionForSubtitleLabel+25, width: scrollViewWidth-80, height: 100)
        }else{
            lblThirdScreenSubtitle.frame = CGRect(x: (scrollViewWidth*2)+40, y: yPositionForSubtitleLabel+20, width: scrollViewWidth-80, height: 100)
        }
        lblThirdScreenSubtitle.text = "Choose the beginning and the ending of your meditation. Try various breathing, counting, visualization and relaxation techniques."
        lblThirdScreenSubtitle.textAlignment = .center
        lblThirdScreenSubtitle.font = UIFont.systemFont(ofSize: 15)
        lblThirdScreenSubtitle.textColor = UIColor.white
        lblThirdScreenSubtitle.numberOfLines = 0
        
        lblFourthScreenSubtitle = UILabel()
        if(IS_IPHONE6){
            lblFourthScreenSubtitle.frame = CGRect(x: (scrollViewWidth*3)+40, y: yPositionForSubtitleLabel+25, width: scrollViewWidth-80, height: 100)
        }else{
            lblFourthScreenSubtitle.frame = CGRect(x: (scrollViewWidth*3)+40, y: yPositionForSubtitleLabel+15, width: scrollViewWidth-80, height: 100)
        }
        lblFourthScreenSubtitle.text = "Place some unguided time in your session. This helps you to spend as much time with a positive thought or mental image as you want to."
        lblFourthScreenSubtitle.textAlignment = .center
        lblFourthScreenSubtitle.font = UIFont.systemFont(ofSize: 15)
        lblFourthScreenSubtitle.textColor = UIColor.white
        lblFourthScreenSubtitle.numberOfLines = 0
        
        lblFifthScreenSubtitle = UILabel()
        if(IS_IPHONE6){
            lblFifthScreenSubtitle.frame = CGRect(x: (scrollViewWidth*4)+40, y: yPositionForSubtitleLabel+25, width: scrollViewWidth-80, height: 100)
        }else{
            lblFifthScreenSubtitle.frame = CGRect(x: (scrollViewWidth*4)+40, y: yPositionForSubtitleLabel+15, width: scrollViewWidth-80, height: 100)
        }
        lblFifthScreenSubtitle.text = "Choose a background music that suits your taste. Adjust its volume or leave it silent and concentrate only on the voice guidance."
        lblFifthScreenSubtitle.textAlignment = .center
        lblFifthScreenSubtitle.font = UIFont.systemFont(ofSize: 15)
        lblFifthScreenSubtitle.textColor = UIColor.white
        lblFifthScreenSubtitle.numberOfLines = 0
        
        
        lblFirstScreenSubtitle.lineBreakMode = .byClipping
        lblFirstScreenSubtitle.adjustsFontSizeToFitWidth = true
        lblFirstScreenSubtitle.minimumScaleFactor = 0.5
        lblFirstScreenSubtitle.font = UIFont.init(name: String().themeFuturaFontName, size: SubTitleFontSize)
        
        lblSecondScreenSubtitle.lineBreakMode = .byClipping
        lblSecondScreenSubtitle.adjustsFontSizeToFitWidth = true
        lblSecondScreenSubtitle.minimumScaleFactor = 0.5
        lblSecondScreenSubtitle.font = UIFont.init(name: String().themeFuturaFontName, size: SubTitleFontSize)
        
        lblThirdScreenSubtitle.lineBreakMode = .byClipping
        lblThirdScreenSubtitle.adjustsFontSizeToFitWidth = true
        lblThirdScreenSubtitle.minimumScaleFactor = 0.5
        lblThirdScreenSubtitle.font = UIFont.init(name: String().themeFuturaFontName, size: SubTitleFontSize)
        
        lblFourthScreenSubtitle.lineBreakMode = .byClipping
        lblFourthScreenSubtitle.adjustsFontSizeToFitWidth = true
        lblFourthScreenSubtitle.minimumScaleFactor = 0.5
        lblFourthScreenSubtitle.font = UIFont.init(name: String().themeFuturaFontName, size: SubTitleFontSize)

        lblFifthScreenSubtitle.lineBreakMode = .byClipping
        lblFifthScreenSubtitle.adjustsFontSizeToFitWidth = true
        lblFifthScreenSubtitle.minimumScaleFactor = 0.5
        lblFifthScreenSubtitle.font = UIFont.init(name: String().themeFuturaFontName, size: SubTitleFontSize)

        self.scrollView.addSubview(lblFirstScreenSubtitle)
        self.scrollView.addSubview(lblSecondScreenSubtitle)
        self.scrollView.addSubview(lblThirdScreenSubtitle)
        self.scrollView.addSubview(lblFourthScreenSubtitle)
        self.scrollView.addSubview(lblFifthScreenSubtitle)

    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
