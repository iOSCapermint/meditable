//
//  LoginViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 15/04/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import FacebookCore
import FacebookLogin

class LoginViewController: UIViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var txtUserName: CustomTextfield!
    
    @IBOutlet weak var txtPassword: CustomTextfield!
    
    @IBOutlet weak var btnShowPassword: UIButton!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var seperatorView: UIView!
    
    @IBOutlet weak var btnFacebook: UIButton!
    
    @IBOutlet weak var btnForgotPassword: UIButton!
    
    @IBOutlet weak var btnTermsAndConditions: UIButton!
    
    @IBOutlet weak var txtUserNameTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    
    var socialId: String = ""
    var socialEmail: String = ""
    var socialProfileName: String = ""
    var socialFirstName: String = ""
    var socialLastName: String = ""
    var profilePictureURL: String = ""
    var socialMobile: String = ""
    
    var userInfoList = [String: String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtUserName.layer.cornerRadius = txtUserName.frame.size.height / 2
        txtPassword.layer.cornerRadius = txtPassword.frame.size.height / 2
        btnShowPassword.layer.cornerRadius = btnShowPassword.frame.size.height / 2
        
        btnLogin.layer.cornerRadius = btnLogin.frame.size.height / 2
        btnLogin.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnLogin.layer.borderWidth = 1.0
        
        btnFacebook.layer.cornerRadius = btnFacebook.frame.size.height / 2
        
        if IS_IPHONE5 {
            btnFacebook.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        }
        else if IS_IPHONE6P {
            btnFacebook.imageEdgeInsets = UIEdgeInsetsMake(0, -80, 0, 0)
            txtUserNameTopConstraint.constant = 380
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        addGoogleAnalytics(screenName: "Login")
    }

    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        //_ = self.navigationController?.popViewController(animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnShowPassword_Clicked(_ sender: Any) {
        txtPassword.isSecureTextEntry = !txtPassword.isSecureTextEntry
    }
    
    @IBAction func btnLogin_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        
        if txtUserName.text?.characters.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please Enter Email")
        }
        else if txtPassword.text?.characters.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please Enter Password")
        }
        else if !(txtUserName.text?.isEmpty)! && !HelperMethod.isValidEmail(txtUserName.text, strict: false) {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please Enter Valid Email")
        }
        else {
            if appDelegate.reachable.isReachable{
                
                var deviceToken = ""
                if defaults.value(forKey: "DeviceToken") as? String != nil {
                    deviceToken = defaults.value(forKey: "DeviceToken") as! String!
                }else {
                    deviceToken = ""
                }
                
                let params: Parameters = ["email": txtUserName.text!, "password": txtPassword.text!, "device_name": "\(UIDevice.current.identifierForVendor!)", "device_token": deviceToken, "app_version": appVersion]
                
                let size = CGSize(width: 50, height: 50)
                let color : UIColor = .white
                startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
                
                Alamofire.request(baseURLWithoutAuth + kUserLogin , method: .post, parameters: params).validate().responseJSON { response in
                    self.stopAnimating()
                    switch response.result {
                    case .success:
                        
                        if let json: NSDictionary = response.result.value as! NSDictionary? {
                            if json.value(forKey: "status") as? Int == 1 {
                                print(json)
                                let userData: NSDictionary = json.value(forKey: "data") as! NSDictionary
                                if("\(userData.value(forKey:"is_verified")!)" == "Y"){
                                    var userID = ""
                                    if userData.value(forKey:"user_id") as? Int != nil {
                                        userID = String(userData.value(forKey: "user_id") as! Int)
                                    }
                                    var userContactNumber = ""
                                    if userData.value(forKey:"mobile") as? String != nil {
                                        userContactNumber = userData.value(forKey: "mobile") as! String
                                    }
                                    defaults.set(userData.value(forKey: "auth_key") as! String, forKey: "AuthKey")
                                    defaults.set(userData.value(forKey: "email") as! String, forKey: "Email")
                                    defaults.set(self.txtPassword.text!, forKey: "Password")
                                    defaults.set(userID, forKey: "UserID")
                                    defaults.set(userData.value(forKey: "user_name") as! String, forKey: "Name")
                                    defaults.set(userData.value(forKey: "image_url") as! String, forKey: "image_url")
                                    defaults.set(userData.value(forKey: "user_type") as! String, forKey: "UserType")
                                    defaults.set(userData.value(forKey: "notificationstatus"), forKey: "isNotificationEnable")
                                    defaults.synchronize()
                                    
                                    let tabbarViewController: TabbarViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                                    self.navigationController?.pushViewController(tabbarViewController, animated: true)
                                }else{
                                    UIAlertController().alertViewWithTitleAndMessage(self, message: "Please verify your email address.")
                                }
                            }
                            else {
                                UIAlertController().alertViewWithTitleAndMessage(self, message: json.value(forKey: "message") as! String)
                            }
                        }
                    case .failure(let error):
                        UIAlertController().alertViewWithErrorMessage(self)
                    }
                }
            }
            else {
                UIAlertController().alertViewWithNoInternet(self)
            }
            //            let tabbarViewController: TabbarViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
            //            tabbarViewController.isNewCustomMeditationAdded = false
            //            self.navigationController?.pushViewController(tabbarViewController, animated: true)
        }
    }
    
    @IBAction func btnFacebook_Clicked(_ sender: Any) {
        loginWithFacebook()
        //        let tabbarViewController: TabbarViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        //        self.navigationController?.pushViewController(tabbarViewController, animated: true)
    }
    
    @IBAction func btnForgotPassword_Clicked(_ sender: Any) {
        let forgotPasswordViewController: ForgotPasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotPasswordViewController, animated: true)
    }
    
    @IBAction func btnTermsCondition_Clicked(_ sender: Any) {
        let termsAndConditionsViewController: TermsAndConditionsViewController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as! TermsAndConditionsViewController
        self.navigationController?.pushViewController(termsAndConditionsViewController, animated: true)
    }
    
    @IBAction func onBtnPrivacyPolicyClick(_ sender: Any) {
        let privacyPolicyViewController: PrivacyPolicyViewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
        self.navigationController?.pushViewController(privacyPolicyViewController, animated: true)
        
    }
    
    //Text Field Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtUserName.isFirstResponder {
            txtPassword.becomeFirstResponder()
        }
        else if txtPassword.isFirstResponder {
            txtPassword.resignFirstResponder()
        }
        
        return true
    }
    //MARK: - Facebook login
    func loginWithFacebook() {
        if AccessToken.current == nil {
            let loginManager = LoginManager()
            
            loginManager.logIn([.publicProfile, .email, .userFriends], viewController: self) { loginResult in
                switch loginResult {
                    
                case .failed(let error):
                    print(error.localizedDescription)
                case .cancelled:
                    print("User cancelled login.")
                case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                    if grantedPermissions.contains("email") || grantedPermissions.contains("publicProfile") || grantedPermissions.contains("userFriends") {
                        self.getFBUserData()
                    }
                }
            }
        }
        else {
            print("Current Access Token is:\(String(describing: AccessToken.current))")
            getFBUserData()
        }
    }
    
    func getFBUserData() {
        let params = ["fields":"email, id, name, first_name, last_name, gender"]
        
        let request = GraphRequest(graphPath: "me", parameters: params, accessToken: AccessToken.current, httpMethod: .GET, apiVersion: FacebookCore.GraphAPIVersion.defaultVersion)
        request.start { (response, result) in
            switch result {
            case .success(let value):
                var parsedData = value.dictionaryValue as Dictionary<String, AnyObject>?
                
                if let firstName = parsedData?["first_name"] {
                    self.socialFirstName = firstName as! String
                }
                else {
                    self.socialFirstName = ""
                }
                
                if let email = parsedData?["email"] {
                    self.socialEmail = email as! String
                }
                else {
                    self.socialEmail = ""
                }
                
                if let id = parsedData?["id"] {
                    self.socialId = id as! String
                    let pictureURL = "https://graph.facebook.com/" + "\(self.socialId)/" + "picture?type=large&return_ssl_resources=1"
                    self.profilePictureURL = pictureURL
                }
                else {
                    self.socialId = ""
                    self.profilePictureURL = ""
                }
                
                if let last_name = parsedData?["last_name"] {
                    self.socialLastName = last_name  as! String
                }
                else {
                    self.socialLastName = ""
                }
                
                if let gender = parsedData?["gender"] {
                }
                
                if let name = parsedData?["name"] {
                    self.socialProfileName = name as! String
                }
                else {
                    self.socialProfileName = ""
                }
                
                self.userSocialLogin(socialLoginType: "Facebook")
                
            case .failed(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func userSocialLogin(socialLoginType: String) {
        if appDelegate.reachable.isReachable{
            
            var deviceToken = ""
            if defaults.value(forKey: "DeviceToken") as? String != nil {
                deviceToken = defaults.value(forKey: "DeviceToken") as! String
            }
            
            var params = Parameters()
            if socialLoginType == "Facebook" {
                
                params = ["User[social_id]": socialId, "User[email]": socialEmail, "User[user_name]": socialFirstName, "User[app_version]": appVersion, "User[device_name]": "iphone7", "User[device_token]": deviceToken, "User[image_url]": profilePictureURL]
                
                //                userInfoList["social_id"] = socialId
                //                userInfoList["email"] = socialEmail
                //                userInfoList["user_name"] = socialFirstName
                //                userInfoList["user_lname"] = socialLastName
                //                userInfoList["image_url"] = profilePictureURL
                //                userInfoList["user_type"] = "F"
            }
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithoutAuth + kSocialLogin, method: .post, parameters: params).validate().responseJSON { response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    if json["status"] == 1 {
                            let userId = json["data"]["user_id"].int!
                            defaults.setValue(String(userId), forKey: "UserID")
                            defaults.setValue(json["data"]["user_name"].string, forKey: "Name")
                            defaults.setValue(json["data"]["user_lname"].string, forKey: "user_lname")
                            defaults.setValue(json["data"]["email"].string, forKey: "Email")
                            defaults.setValue(json["data"]["contact_no"].string, forKey: "contact_no")
                            defaults.setValue(json["data"]["dob"].string, forKey: "dob")
                            defaults.setValue(json["data"]["unit_no"].string, forKey: "unit_no")
                            defaults.setValue(json["data"]["address"].string, forKey: "address")
                            defaults.setValue(json["data"]["city"].string, forKey: "city")
                            defaults.setValue(json["data"]["postal_code"].string, forKey: "postal_code")
                            defaults.setValue(json["data"]["image_url"].string, forKey: "image_url")
                            defaults.setValue(json["data"]["user_type"].string, forKey: "UserType")
                            defaults.setValue(json["data"]["auth_key"].string, forKey: "AuthKey")
                            defaults.setValue(json["data"]["is_active"].string, forKey: "is_active")
                            defaults.setValue(json["data"]["is_notification_enable"].string, forKey: "isNotificationEnable")
                            defaults.setValue(json["data"]["is_alert_enable"].string, forKey: "is_alert_enable")
                            
                            defaults.synchronize()
                            
                            let tabbarViewController: TabbarViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                            self.navigationController?.pushViewController(tabbarViewController, animated: true)
                            //                            self.navigatTOView()
//                        }
                    }
                    else {
                        if let message = json["message"].string {
                            UIAlertController().alertViewWithTitleAndMessage(self, message: message)
                        }
                    }
                case .failure(let error):
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
