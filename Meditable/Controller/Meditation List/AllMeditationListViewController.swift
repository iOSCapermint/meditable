//
//  AllMeditationListViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 28/06/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON

class AllMeditationListViewController: UIViewController,NVActivityIndicatorViewable, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblAllMeditation: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnAdd: UIButton!
    
    var btnindex = Int()
    
    var track_id = Int()
    
    var meditationList = [[String: AnyObject]]()

    var custom_meditation_id = Int()
    var index = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblAllMeditation.tableFooterView = UIView(frame: .zero)

        btnindex = -1
        
        btnAdd.layer.cornerRadius = btnAdd.frame.size.height / 2
        
        self.lblNoDataFound.isHidden = true
        self.btnAdd.isHidden = true
        
        getCustomMeditationList()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Add to existing meditation list")
    }
    
    
    @IBAction func onBtnBackClick(_ sender: Any) {
        let alertController = UIAlertController(title: AppName, message: "Are you sure you want to leave without saving?", preferredStyle:UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
        { action -> Void in
            let transition = CATransition()
            transition.duration = 0.3
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionPush
            self.navigationController?.view.layer.add(transition, forKey: nil)
            _ = self.navigationController?.popViewController(animated: false)
        })
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
        { action -> Void in
            
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func onBtnAddClick(_ sender: UIButton) {
        addToAnotherMeditation(custom_id: custom_meditation_id, track_id: self.track_id)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.meditationList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "AllMeditationListCell"
        
        let cell: AllMeditationListCell! = tblAllMeditation.dequeueReusableCell(withIdentifier: cellIdentifier) as! AllMeditationListCell!
        
        cell.lblName.text = self.meditationList[indexPath.row]["title"] as? String
        
        
        let trackList = self.meditationList[indexPath.row]["track"] as! [[String : AnyObject]]
        var bgTime = "00:00"
        for i in 0...trackList.count - 1 {
            if("\(trackList[i]["track_type"]!)" == "B"){
                bgTime = "\(trackList[i]["time"]!)"
                break
            }
        }
        let time = self.getTotalTime(totalTime: "\(self.meditationList[indexPath.row]["total_meditation_time"]!)", backgroundMusicTime: bgTime)
        cell.lblTime.text = "\(time.time)"

        
        
//        cell.lblTime.text = self.meditationList[indexPath.row]["total_meditation_time"] as? String
        
        if btnindex == indexPath.row{
            cell.btnCheck.isSelected = true
            cell.btnCheck.setImage(#imageLiteral(resourceName: "ic_selected_mark"), for: UIControlState.selected)
        }else{
            cell.btnCheck.isSelected = false
            cell.btnCheck.setImage(#imageLiteral(resourceName: "ic_unselected_mark"), for: UIControlState.normal)
        }
        
        cell.btnCheck.addTarget(self, action: #selector(SubscriptionPlanViewController.onBtnCheckClick(sender: )),for: .touchUpInside)
        cell.btnCheck.tag = indexPath.row
//        cell.layer.borderColor = UIColor.white.cgColor
//        cell.layer.cornerRadius = 5
//        cell.layer.borderWidth = 1
//        cell.layer.masksToBounds = true
        
        cell.backgroundColor = .clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func onBtnCheckClick(sender: UIButton) {
        
        let cell = sender.superview?.superview as? AllMeditationListCell
        let indexPath = tblAllMeditation.indexPath(for: cell!)
        
        if btnindex == indexPath!.row{
            btnindex = -1
            self.btnAdd.isHidden = true
        }else{
            btnindex = indexPath!.row
            self.btnAdd.isHidden = false
            self.index = indexPath!.row
            print(self.index)
        }
        
        custom_meditation_id = self.meditationList[sender.tag]["custom_meditation_id"] as! Int
        
        self.tblAllMeditation.reloadData()
    }
    
    func getCustomMeditationList() {
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetCustomMeditationList, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            self.meditationList = json.value(forKey: "data") as! [[String : AnyObject]]
                            
                            if self.meditationList.count > 0{
                                self.tblAllMeditation.reloadData()
                            }else{
                                self.lblNoDataFound.isHidden = false
                            }
                            
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                        
                    }else {
                        //                        if let message = json["status"].string {
                        //                            UIAlertController().alertViewWithTitleAndMessage(self, message: message)
                        //                        }
                    }
                case .failure(let error):
                    print (error)
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func addToAnotherMeditation(custom_id: Int, track_id: Int) {
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            var params = Parameters()
            
            params = ["auth_key": authKey, "custom_meditation_id": custom_id, "track_id": track_id]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kAddToAnother, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            NotificationCenter.default.post(name: NSNotification.Name.init("refreshMeditaionList"), object: nil)

                            let alertSuccess: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: "\(json["message"]!)", preferredStyle: .alert)
                            
                            let btnOk: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
                                _ in
                               
//                                downloadAgain = true
//                                downloadRefIndex = IndexPath.init(row: 0, section: self.index)
//                                print(downloadRefIndex)
                                let transition = CATransition()
                                transition.duration = 0.3
                                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                                transition.type = kCATransitionPush
                                self.navigationController?.view.layer.add(transition, forKey: nil)
                                _ = self.navigationController?.popViewController(animated: false)
                            })
                            
                            alertSuccess.addAction(btnOk)
                            self.present(alertSuccess, animated: true, completion: nil)
                            
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                            
                        }

                        
                        
                    }else {
                        //                        if let message = json["status"].string {
                        //                            UIAlertController().alertViewWithTitleAndMessage(self, message: message)
                        //                        }
                    }
                case .failure(let error):
                    print (error)
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
