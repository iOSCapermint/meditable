//
//  BackgroundMusicViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 20/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON
import TPKeyboardAvoiding

class BackgroundMusicViewController: UIViewController, NVActivityIndicatorViewable, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblSelectMusic: UILabel!
    
    @IBOutlet weak var imgActivityBackground: UIImageView!
    
    @IBOutlet weak var tblBackgroundMusic: UITableView!
    
    
    var backgroundTrackArray = [[String: AnyObject]]()
    var refresher: UIRefreshControl!
    
    var strController = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.strController == "background" {
            self.lblSelectMusic.text = "Background Tracks"
            getBackgroundTracks()
        } else if self.strController == "intro" {
            self.lblSelectMusic.text = "Intro Tracks"
            getIntroTracks()
        } else if self.strController == "ending"{
            self.lblSelectMusic.text = "Ending Tracks"
            getEndingTracks()
        } else {
            self.lblSelectMusic.text = "Silence Tracks"
            getSilenceTracks()
        }
        
        addGoogleAnalytics(screenName: "Change \(self.lblSelectMusic.text!) Track Screen")
        
        
        
        //RefreshControl
        refresher = UIRefreshControl()
        tblBackgroundMusic!.alwaysBounceVertical = true
        refresher.tintColor = .white
        refresher.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tblBackgroundMusic!.addSubview(refresher)
        
        tblBackgroundMusic.rowHeight = UITableViewAutomaticDimension
        tblBackgroundMusic.estimatedRowHeight = 100
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Change \(self.lblSelectMusic.text!) Track")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:-  Refresh -
    func refreshList() {
        self.refresher.endRefreshing()
        getBackgroundTracks()
    }
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        //_ = self.navigationController?.popViewController(animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    //TableView Delegate & Data source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.backgroundTrackArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = UIView()
        headerView.backgroundColor = .clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        
        var cell: backgroundMusicCell! = tblBackgroundMusic.dequeueReusableCell(withIdentifier: cellIdentifier) as! backgroundMusicCell
        
        cell.isUserInteractionEnabled = true
        cell.accessoryType = .none
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        
        cell.lblOptionName.text = self.backgroundTrackArray[indexPath.section]["title"] as? String
        
        cell.lblTime.text = self.backgroundTrackArray[indexPath.section]["time"] as? String
        
        let strType = self.backgroundTrackArray[indexPath.section]["track_type"] as? String
        if strType == "B"{
            cell.btnInfoHConstraint.constant = 0.0
            cell.btnAddTopConstrain.constant = 10.0
        } else {
            cell.btnInfoHConstraint.constant = 40.0
            cell.btnAddTopConstrain.constant = 1.0
        }
        cell.btnInfo.addTarget(self, action: #selector(BackgroundMusicViewController.onBtnInfoClick(sender:)), for: .touchUpInside)
        
        cell.layer.borderColor = UIColor().whiteBorderColor.cgColor
        cell.layer.borderWidth = 1.0
        cell.layer.cornerRadius = 5.0
        cell.layer.masksToBounds = true
        
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.strController == "background" {
            NotificationCenter.default.post(name: NSNotification.Name.init("changeBackgroundMusic"), object: backgroundTrackArray[indexPath.section])
            NotificationCenter.default.post(name: NSNotification.Name.init("changeBackgroundMusicOffline"), object: backgroundTrackArray[indexPath.section])
            self.navigationController?.popViewController(animated: true)
            return
        } else{
            for var viewController: UIViewController in (self.navigationController?.viewControllers)! {
                if viewController.isKind(of: MeditationTrackListViewController.self) {
                    NotificationCenter.default.post(name: NSNotification.Name.init("changeMusic"), object: backgroundTrackArray[indexPath.section])
                    break
                }
                if viewController.isKind(of: CustomMeditationPlaylistViewController.self) {
                    NotificationCenter.default.post(name: NSNotification.Name.init("changeMusicCustomMeditaion"), object: backgroundTrackArray[indexPath.section])
                    break
                }
                if viewController.isKind(of: CustomMeditationViewController.self) {
                    NotificationCenter.default.post(name: NSNotification.Name.init("changeMusicCreateMeditaion"), object: backgroundTrackArray[indexPath.section])
                    break
                }
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    
    //MARK:- info button action method
    
    func onBtnInfoClick(sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(.zero, to: tblBackgroundMusic)
        let indexPath: NSIndexPath = tblBackgroundMusic.indexPathForRow(at: buttonPosition) as NSIndexPath!
        
        let trackInfoViewController: TrackInfoViewController = self.storyboard?.instantiateViewController(withIdentifier: "TrackInfoViewController") as! TrackInfoViewController
        if(indexPath.section == backgroundTrackArray.count){
            trackInfoViewController.trackList =  backgroundTrackArray[indexPath.section - 1]
        }else{
            trackInfoViewController.trackList =  backgroundTrackArray[indexPath.section]
        }
        self.navigationController?.pushViewController(trackInfoViewController, animated: true)
        
    }
    
    func getBackgroundTracks(){
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetBackgroundTracks, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            self.backgroundTrackArray = json.value(forKey: "data") as! [[String : AnyObject]]
                            
                            if self.backgroundTrackArray.count > 0{
                                self.tblBackgroundMusic.reloadData()
                            }
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                            
                        }

                        
                    }else {
                        //                        if let message = json["message"].string {
                        //                            UIAlertController().alertViewWithTitleAndMessage(self, message: message)
                        //                        }
                    }
                case .failure(let error):
                    print (error)
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getIntroTracks(){
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetIntroTracks, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            self.backgroundTrackArray = json.value(forKey: "data") as! [[String : AnyObject]]
                            
                            if self.backgroundTrackArray.count > 0{
                                self.tblBackgroundMusic.reloadData()
                            }
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                            
                        }

                        
                    }else {
                        //                        if let message = json["message"].string {
                        //                            UIAlertController().alertViewWithTitleAndMessage(self, message: message)
                        //                        }
                    }
                case .failure(let error):
                    print (error)
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getEndingTracks(){
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetEndingTracks, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            self.backgroundTrackArray = json.value(forKey: "data") as! [[String : AnyObject]]
                            
                            if self.backgroundTrackArray.count > 0{
                                self.tblBackgroundMusic.reloadData()
                            }
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                            
                        }

                        
                    }else {
                        //                        if let message = json["message"].string {
                        //                            UIAlertController().alertViewWithTitleAndMessage(self, message: message)
                        //                        }
                    }
                case .failure(let error):
                    print (error)
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    func getSilenceTracks(){
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetSilenceTracks, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            self.backgroundTrackArray = json.value(forKey: "data") as! [[String : AnyObject]]
                            
                            if self.backgroundTrackArray.count > 0{
                                self.tblBackgroundMusic.reloadData()
                            }
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                            
                        }

                        
                    }else {
                        //                        if let message = json["message"].string {
                        //                            UIAlertController().alertViewWithTitleAndMessage(self, message: message)
                        //                        }
                    }
                case .failure(let error):
                    print (error)
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

class backgroundMusicCell : UITableViewCell{
    
    @IBOutlet weak var lblOptionName: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var btnInfo: UIButton!
    
    @IBOutlet weak var btnAdd: UIButton!
    
    @IBOutlet weak var btnInfoHConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnAddTopConstrain: NSLayoutConstraint!
}
