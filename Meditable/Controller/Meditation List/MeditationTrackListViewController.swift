//
//  MeditationTrackListViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 17/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON
import TPKeyboardAvoiding
import AVFoundation
import AudioToolbox

class MeditationTrackListViewController: UIViewController, NVActivityIndicatorViewable, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UITextFieldDelegate, AVAudioPlayerDelegate {
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblMeditations: UILabel!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var lblMeditationName: UILabel!
    
    @IBOutlet weak var seperatorView: UIView!
    
    @IBOutlet weak var lblMeditationDescription: UILabel!
    
    @IBOutlet weak var tblTracks: UITableView!
    
    @IBOutlet weak var btnPlayMeditation: UIButton!
    
    @IBOutlet weak var tblTracksHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var saveTrackNameView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var trackDetailsView: UIView!
    
    @IBOutlet weak var lblSaveCustomMeditation: UILabel!
    
    @IBOutlet weak var txtTrackName: CustomTextfield!
    
    @IBOutlet weak var btnSaveTrackName: UIButton!
    
    @IBOutlet weak var btnClose1: UIButton!
    
    @IBOutlet weak var btnClose1HeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var trackOptionsView: UIView!
    
    @IBOutlet weak var tblTrackOptions: UITableView!
    
    @IBOutlet weak var btnClose2: UIButton!
    
    @IBOutlet weak var introTrackOptionsView: UIView!
    
    @IBOutlet weak var tblIntroTrackOptions: UITableView!
    
    @IBOutlet weak var btnClose3: UIButton!
    
    @IBOutlet weak var endTracksOptionsView: UIView!
    
    @IBOutlet weak var tblEndTrackOptions: UITableView!
    
    @IBOutlet weak var btnClose4: UIButton!
    
    @IBOutlet weak var lblTotalTrackTime: UILabel!
    
    @IBOutlet weak var addtoAnotherMeditationView: UIView!
    @IBOutlet weak var innerAddtoAnotherMeditationView: UIView!
    @IBOutlet weak var btnCloseAddToAnotherView: UIButton!
    @IBOutlet weak var btnAddToNewMeditation: UIButton!
    @IBOutlet weak var btnAddToExistingMeditation: UIButton!
    
    var trackOptionsList = ["Add Silence 30 Sec", "Move Up", "Move Down", "Add to Another Meditation", "Delete"]
    
    var introOptionsList = ["Change Intro", "Add Silence 30 Sec"]
    
    var endOptionsList = ["Change Ending"]
    
    var strCatID = Int()
    
    var trackArray = [[String: AnyObject]]()
    
    var btnindex = Int()
    
    var player = AVPlayer()
    
    var urlString = ""
    
    var editIndex = Int()
    
    var currentPlayIndex = 500
    
    var strSilenceTrackURL = ""
    
    var trackListArray = [[String: AnyObject]]() //new

    
    var responseArray = [[String : AnyObject]]()
    var trackList = [[String : AnyObject]]()
    var index : Int = 0
    var backgroundList = [String : AnyObject]()
    var silenceList = [String : AnyObject]()
    var isOrignal : Bool = true
    
    var totalTime = ""
    var backgroundSeconds : Int = 0
    var totalSeconds : Int = 0
    var savedMeditation = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblMeditations.text = "\(MadeByMeditable)"

        NotificationCenter.default.addObserver(self, selector: #selector(MeditationTrackListViewController.addNewTrackFromList(notification:)), name: Notification.Name("SelectedTrackToAdd"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(MeditationTrackListViewController.musicChange(sender: )), name: NSNotification.Name.init("changeMusic"), object: nil)
        
        //For background and Silence Audio
        trackList = self.responseArray[0]["track"] as! [[String : AnyObject]]
        for i in 0...trackList.count - 1 {
            if("\(trackList[i]["track_type"]!)" == "B"){
                backgroundList = trackList[i]
                trackList.remove(at: i)
                break
            }
        }
        for i in 0...trackList.count - 1 {
            if("\(trackList[i]["track_type"]!)" == "S"){
                silenceList = trackList[i]
                trackList.remove(at: i)
                break
            }
        }
        if(silenceList.count == 0){
            self.getSilenceTracks()
        }
        
        self.btnindex = -1
        
        btnSave.layer.cornerRadius = btnSave.frame.size.height / 2
        btnPlayMeditation.layer.cornerRadius = btnPlayMeditation.frame.size.height / 2
        btnPlayMeditation.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnPlayMeditation.layer.borderWidth = 1
        
        btnAddToNewMeditation.layer.cornerRadius = btnPlayMeditation.frame.size.height / 2
        btnAddToExistingMeditation.layer.cornerRadius = btnPlayMeditation.frame.size.height / 2
        innerAddtoAnotherMeditationView.layer.cornerRadius = 5.0

        txtTrackName.layer.borderColor = UIColor.black.cgColor
        txtTrackName.layer.borderWidth = 1.0
        txtTrackName.layer.cornerRadius = 5.0
        txtTrackName.layer.masksToBounds = true

        btnSaveTrackName.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnSaveTrackName.layer.borderWidth = 1.0
        btnSaveTrackName.layer.cornerRadius = btnSaveTrackName.frame.size.height / 2
        btnSaveTrackName.layer.masksToBounds = true
        
        trackDetailsView.layer.cornerRadius = 5.0
        saveTrackNameView.layer.masksToBounds = true
        
        tblTrackOptions.layer.cornerRadius = 5.0
        tblTrackOptions.layer.masksToBounds = true
        
        tblIntroTrackOptions.layer.cornerRadius = 5.0
        tblIntroTrackOptions.layer.masksToBounds = true
        
        tblEndTrackOptions.layer.cornerRadius = 5.0
        tblEndTrackOptions.layer.masksToBounds = true
        
        tblTracks.rowHeight = UITableViewAutomaticDimension
        tblTracks.estimatedRowHeight = 90
        
        if IS_IPHONE4s || IS_IPHONE5 {
            btnClose1HeightConstraint.constant = 94
        }
        else if IS_IPHONE6 {
            btnClose1HeightConstraint.constant = 144
        }
        else {
            btnClose1HeightConstraint.constant = 178
        }
        
        self.lblMeditationName.text = responseArray[0]["title"] as? String
        self.lblMeditationDescription.text = responseArray[0]["description"] as? String
        
        tblTracksHeightConstraint.constant = CGFloat((self.trackList.count + 1) * 80)
        self.tblTracks.reloadData()
        
        lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: trackList).time)"
        
        NotificationCenter.default.addObserver(self, selector: #selector(MeditationTrackListViewController.itemDidFinishPlaying(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MeditationTrackListViewController.refreshList(notification:)), name: Notification.Name("refreshLibraryList"), object: nil)
        
        addtoAnotherMeditationView.isHidden = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        addGoogleAnalytics(screenName: "Meditation Track List")
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        if(isOrignal == false && savedMeditation == false){
            let alertController = UIAlertController(title: AppName, message: LikeToSaveEditedMeditation, preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
            { action -> Void in
                self.backgroundView.isHidden = false
                self.saveTrackNameView.isHidden = false
                if(self.isOrignal == true){
                    self.txtTrackName.text = "\(self.responseArray[0]["title"]!)"
                }else{
                    self.txtTrackName.text = "My \(self.responseArray[0]["title"]!)"
                }
//                self.saveCustomMeditationAPI(title: "My \(self.responseArray[0]["title"]!)")
            })
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
            { action -> Void in
                self.popToController()
            })
            self.present(alertController, animated: true, completion: nil)
        }else{
            self.popToController()
        }
        
        //_ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSave_Clicked(_ sender: Any) {
        backgroundView.isHidden = false
        saveTrackNameView.isHidden = false
        if(isOrignal == true){
            txtTrackName.text = "\(responseArray[0]["title"]!)"
        }else{
            txtTrackName.text = "My \(responseArray[0]["title"]!)"
        }
    }
    //MARK:- Refresh List -
    func refreshList(notification: Notification){
        if let background = notification.userInfo! as? [String : AnyObject] {
            backgroundList = background
        }
    }
    
    //MARK:- Play meditation event
    @IBAction func btnPlayMeditation_Clicked(_ sender: UIButton) {
        let playMeditationViewController = self.storyboard?.instantiateViewController(withIdentifier: "PlayMeditationViewController") as! PlayMeditationViewController
        playMeditationViewController.trackList = trackList
        playMeditationViewController.backgroundSound = backgroundList
        playMeditationViewController.silenceSound = silenceList
        playMeditationViewController.meditationTitle = "\(self.responseArray[0]["title"]!)"
        playMeditationViewController.meditationID = "\(self.responseArray[0]["meditation_id"]!)"
        playMeditationViewController.meditationDescription = "\(self.responseArray[0]["description"]!)"
        playMeditationViewController.totalTrackTime = "\(getTotalTrackTime(track: trackList).time)"//"Total Meditation Time: \(self.responseArray[0]["total_meditation_time"]!)"
        if(isOrignal == true){
            playMeditationViewController.meditaionBy = "\(MadeByMeditable)"
        }else{
            playMeditationViewController.meditaionBy = "\(MadeByYou)"
        }
        playMeditationViewController.backgroundSeconds = backgroundSeconds
        playMeditationViewController.totalSeconds = getTotalTrackTime(track: trackList).totalSeconds
        self.navigationController?.pushViewController(playMeditationViewController, animated: true)
    }
    
    @IBAction func btnSaveTrackName_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        saveTrackNameView.isHidden = true
        txtTrackName.resignFirstResponder()

        if(txtTrackName.text!.isEmptyField){
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Meditaion name should not be empty.")
        }else{
            self.saveCustomMeditationAPI(title: "\(txtTrackName.text!)")
        }
    }
    
    func saveCustomMeditationAPI(title : String){
        if appDelegate.reachable.isReachable{
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            var params = Parameters()
            
            var trackID = ""
            var trackType = ""
            
            for index in 0...trackList.count - 1{
                if(index == 0){
                    trackID.append("\(trackList[index]["track_id"]!)")
                    trackType.append("\(trackList[index]["track_type"]!)")
                }else{
                    trackID.append(",\(trackList[index]["track_id"]!)")
                    trackType.append(",\(trackList[index]["track_type"]!)")
                }
            }
            if(backgroundList.count != 0){
                trackID.append(",\(backgroundList["track_id"]!)")
                trackType.append(",\(backgroundList["track_type"]!)")
            }
            
            if(isOrignal == true){
                params = ["auth_key": authKey, "track_ids": trackID, "track_types" : trackType, "title" : "\(title)", "is_original" : "Y"]
            }else{
                params = ["auth_key": authKey, "track_ids": trackID, "track_types" : trackType, "title" : "\(title)", "is_original" : "N"]
            }
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kSaveCustomList, method: .post, parameters: params).validate().responseJSON { response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            let responseResult = response.result.value as! [String : Any]
                            //                    if("\(responseResult["result"]!)" == "1"){
                            UIAlertController().alertViewWithTitleAndMessage(self, message: "\(responseResult["message"]!)")
                            self.savedMeditation = true
                            
//                            let alertSuccess: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: "\(responseResult["message"]!)", preferredStyle: .alert)
//                            let btnOk: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
//                                _ in
//                                let transition = CATransition()
//                                transition.duration = 0.3
//                                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//                                transition.type = kCATransitionPush
//                                self.navigationController?.view.layer.add(transition, forKey: nil)
//                                _ = self.navigationController?.popViewController(animated: false)
//                            })
//                            
//                            alertSuccess.addAction(btnOk)
//                            self.present(alertSuccess, animated: true, completion: nil)
                            
                            NotificationCenter.default.post(name: NSNotification.Name.init("SetCustomeMeditationAdded"), object: nil)
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                        }

                    }
                  
                    
                case .failure(let error):
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    @IBAction func btnClose1_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        saveTrackNameView.isHidden = true
    }
    
    @IBAction func btnClose2_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        trackOptionsView.isHidden = true
    }
    
    @IBAction func btnClose3_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        introTrackOptionsView.isHidden = true
    }
    
    @IBAction func btnClose4_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        endTracksOptionsView.isHidden = true
    }
    
    @IBAction func btnCloseAddToAnotherMeditation(_ sender: UIButton) {
        backgroundView.isHidden = true
        addtoAnotherMeditationView.isHidden = true
    }
    @IBAction func btnAddToNewMeditationView(_ sender: UIButton) {
        backgroundView.isHidden = true
        addtoAnotherMeditationView.isHidden = true
        let customMeditationViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationViewController") as! CustomMeditationViewController
        customMeditationViewController.trackToAdd = self.trackList[editIndex]
        self.navigationController?.pushViewController(customMeditationViewController, animated: true)

    }
    @IBAction func btnAddToExistingMeditationView(_ sender: UIButton) {
        backgroundView.isHidden = true
        addtoAnotherMeditationView.isHidden = true
        let allMeditationListViewController: AllMeditationListViewController = self.storyboard?.instantiateViewController(withIdentifier: "AllMeditationListViewController") as! AllMeditationListViewController
        allMeditationListViewController.track_id = self.trackList[editIndex]["track_id"] as! Int
        self.navigationController?.pushViewController(allMeditationListViewController, animated: true)
    }

    //Add new track from tracklist
    func addNewTrackFromList(notification: Notification){
        //Take Action on Notification
        isOrignal = false
        currentPlayIndex = 500
        if let notificationArray = notification.object as? [String : AnyObject]  {
            trackList.insert(notificationArray , at: trackList.count - 1)
            tblTracks.reloadData()
            lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: trackList).time)"
        }
        tblTracksHeightConstraint.constant = CGFloat((self.trackList.count + 1) * 80)
    }
    //MARK:- SilenceTrack if isn't found
    
    func getSilenceTracks(){
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetSilenceTracks, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            let backgroundTrackArray = json.value(forKey: "data") as! [[String : AnyObject]]
                            self.silenceList = backgroundTrackArray[0]
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                        }

                    }else {
                        
                    }
                case .failure(let error):
                    print (error)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    
    //MARK:- Change Into Edit Track

    func musicChange (sender: Notification) {
        isOrignal = false
        if let senderArray = sender.object as? [String : AnyObject] {
            if "\(senderArray["track_type"]!)" == "I"{
                trackList.remove(at: 0)
                trackList.insert(senderArray, at: 0)
            }
            if "\(senderArray["track_type"]!)" == "E"{
                trackList.removeLast()
                trackList.append(senderArray)
            }
        }
        tblTracks.reloadData()
        lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: trackList).time)"
    }
    
    //MARK:- Edit Track
    func editTrackDetails(sender: UIButton) {
        
        let buttonPosition: CGPoint = sender.convert(.zero, to: tblTracks)
        let indexPath: NSIndexPath = tblTracks.indexPathForRow(at: buttonPosition) as NSIndexPath!
        
        editIndex = indexPath.row
        if(btnindex != -1){
            DispatchQueue.main.async {
                self.player.pause()
                self.tblTracks.reloadData()
                self.btnindex = -1
            }
        }
        
        if(indexPath.row == 0 || indexPath.row == self.trackList.count){
            if indexPath.row == 0 {
                backgroundView.isHidden = false
                introTrackOptionsView.isHidden = false
                self.tblIntroTrackOptions.frame.size.height = CGFloat(self.introOptionsList.count * 60)
                
            }
            else if indexPath.row == self.trackList.count{
                backgroundView.isHidden = false
                endTracksOptionsView.isHidden = false
                self.tblEndTrackOptions.frame.size.height = CGFloat(self.endOptionsList.count * 60)
                
            }
        }else{
            if(indexPath.row == 1){
                trackOptionsList = ["Add Silence 30 Sec", "Move Down", "Add to Another Meditation", "Delete"]
            }else if(indexPath.row == trackList.count - 2){
                trackOptionsList = ["Add Silence 30 Sec", "Move Up", "Add to Another Meditation", "Delete"]
            }else if(indexPath.row != trackList.count - 1){
                trackOptionsList = ["Add Silence 30 Sec", "Move Up", "Move Down", "Add to Another Meditation", "Delete"]
            }
            if(trackList.count <= 3){
                trackOptionsList = ["Add Silence 30 Sec", "Add to Another Meditation", "Delete"]
            }
            self.tblTrackOptions.frame.size.height = CGFloat(self.trackOptionsList.count * 60)
            tblTrackOptions.reloadData()
            backgroundView.isHidden = false
            trackOptionsView.isHidden = false
        }
    }
    
    //MARK:- Add New Track
    func addNewTrack(sender: UIButton) {
        let addTracksViewController: AddTracksViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddTracksViewController") as! AddTracksViewController
        self.navigationController?.pushViewController(addTracksViewController, animated: true)
    }
    
    //MARK:- Check Track Info
    func checkTrackInfo(sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(.zero, to: tblTracks)
        let indexPath: NSIndexPath = tblTracks.indexPathForRow(at: buttonPosition) as NSIndexPath!
        
        let trackInfoViewController: TrackInfoViewController = self.storyboard?.instantiateViewController(withIdentifier: "TrackInfoViewController") as! TrackInfoViewController
        if(indexPath.row == trackList.count){
            trackInfoViewController.trackList =  trackList[indexPath.row - 1]
        }else{
            trackInfoViewController.trackList =  trackList[indexPath.row]
        }
        self.navigationController?.pushViewController(trackInfoViewController, animated: true)
    }
    
    //MARK:- Play Track
    func playTrack(sender: UIButton) {
//        player.rate = 0
        let indexPath: NSIndexPath = self.tblTracks.indexPath(for: (sender.superview?.superview as! UITableViewCell))! as NSIndexPath
        editIndex = indexPath.row

        if btnindex == indexPath.row{
            btnindex = -1
        }else{
            btnindex = indexPath.row
        }
        if(indexPath.row == trackList.count){
            urlString = (trackList[indexPath.row - 1]["audio_url"] as? String)!
        }else{
            urlString = (trackList[indexPath.row]["audio_url"] as? String)!
        }
        if urlString != ""{
            urlString = urlString.replacingOccurrences(of: " ", with: "%20")
            let url = URL.init(string: urlString)
            do {
                let avPlayerItem: AVPlayerItem = AVPlayerItem.init(url: url!)
                if(currentPlayIndex != indexPath.row){
                    currentPlayIndex = indexPath.row
                    player = AVPlayer.init(playerItem: avPlayerItem)
                }
                
                if #available(iOS 10.0, *) {
                    player.automaticallyWaitsToMinimizeStalling = false
                }
                if(sender.isSelected == false){
                    let size = CGSize(width: 75, height: 75)
                    let color : UIColor = .white
                    startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
                    self.player.addObserver(self, forKeyPath: "status", options: .initial, context: nil)
                    player.play()

                }else{
                    player.pause()
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
        self.tblTracks.reloadData()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if (keyPath == "status") {
            if (player.status == .readyToPlay) {
                self.player.removeObserver(self, forKeyPath: "status", context: nil)
                self.stopAnimating()
            }
        }
    }
    
    func itemDidFinishPlaying(notification:NSNotification) {
            player.pause()
            player.rate = 0
        self.btnindex = -1
        let indexPath = IndexPath(item: editIndex, section: 0)
        tblTracks.reloadRows(at: [indexPath], with: .none)
    }
    
    //MARK:- TableView Delegate & Data source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblTracks {
            return self.trackList.count + 1

//            return self.trackArray.count + 3
        }
        else if tableView == tblIntroTrackOptions {
            return introOptionsList.count
        }
        else if tableView == tblEndTrackOptions {
            return endOptionsList.count
        }
        else {
            return trackOptionsList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        if tableView == tblTracks {
            let cell: TimeLineTableViewCell! = tblTracks.dequeueReusableCell(withIdentifier: cellIdentifier) as! TimeLineTableViewCell
            if(indexPath.row == 0){
                cell.topTitleConstraint.constant = 5
                cell.lblTrackName.text = trackList[indexPath.row]["title"] as? String
                cell.lblTrackName.font = UIFont(name: "FuturaBT-Medium", size: 16.0)
                if let trackTime = trackList[indexPath.row]["time"] as? String {
                    cell.lblTrackTime.text = "\(trackTime)"
                }
                
                cell.lblTrackName.textColor = UIColor().yellowBorderColor
                
                cell.btnEditTrackName.removeTarget(self, action: #selector(MeditationTrackListViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.removeTarget(self, action: #selector(MeditationTrackListViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.removeTarget(self, action: #selector(MeditationTrackListViewController.playTrack(sender: )), for: .touchUpInside)

                cell.btnEditTrackName.addTarget(self, action: #selector(MeditationTrackListViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.addTarget(self, action: #selector(MeditationTrackListViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.addTarget(self, action: #selector(MeditationTrackListViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnInfo.isHidden = false
                cell.btnPlay.isHidden = false
                cell.btnEditTrackName.isHidden = false
                
            }else if(indexPath.row == trackList.count - 1){
                cell.topTitleConstraint.constant = 20
                cell.lblTrackName.text = "Add Track"
                cell.lblTrackTime.text = ""
                cell.lblTrackName.font = UIFont(name: "FuturaBT-MediumItalic", size: 16.0)
                cell.lblTrackName.textColor = UIColor.white
                cell.lblTrackName.sizeToFit()
                cell.btnEditTrackName.removeTarget(self, action: #selector(CustomMeditationViewController.addNewTrack(sender: )), for: .touchUpInside)
                cell.btnEditTrackName.addTarget(self, action: #selector(CustomMeditationViewController.addNewTrack(sender: )), for: .touchUpInside)
                cell.btnInfo.isHidden = true
                cell.btnPlay.isHidden = true
                cell.btnEditTrackName.isHidden = true
            }else if(indexPath.row == self.trackList.count){
                cell.topTitleConstraint.constant = 5
                cell.lblTrackName.text = trackList[indexPath.row - 1]["title"] as? String
                cell.lblTrackName.font = UIFont(name: "FuturaBT-Medium", size: 16.0)
                if let trackTime = trackList[indexPath.row - 1]["time"] as? String {
                    cell.lblTrackTime.text = "\(trackTime)"
                }
                cell.lblTrackName.textColor = UIColor().yellowBorderColor
                cell.lblTrackName.sizeToFit()
                
                
                
                cell.btnEditTrackName.removeTarget(self, action: #selector(MeditationTrackListViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.removeTarget(self, action: #selector(MeditationTrackListViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.removeTarget(self, action: #selector(MeditationTrackListViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnEditTrackName.addTarget(self, action: #selector(MeditationTrackListViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.addTarget(self, action: #selector(MeditationTrackListViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.addTarget(self, action: #selector(MeditationTrackListViewController.playTrack(sender: )), for: .touchUpInside)
                cell.btnInfo.isHidden = false
                cell.btnPlay.isHidden = false
                cell.btnEditTrackName.isHidden = false
                
            }else{
                cell.topTitleConstraint.constant = 5
                cell.lblTrackName.text = self.trackList[indexPath.row]["title"] as? String
                cell.lblTrackName.font = UIFont(name: "FuturaBT-Medium", size: 16.0)
                if let trackTime = self.trackList[indexPath.row]["time"] as? String {
                    cell.lblTrackTime.text = "\(trackTime)"
                }
                cell.lblTrackName.textColor = UIColor().yellowBorderColor
                
                
                cell.btnEditTrackName.removeTarget(self, action: #selector(CustomMeditationViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.removeTarget(self, action: #selector(CustomMeditationViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.removeTarget(self, action: #selector(CustomMeditationViewController.playTrack(sender: )), for: .touchUpInside)
                
                cell.btnEditTrackName.addTarget(self, action: #selector(CustomMeditationViewController.editTrackDetails(sender: )), for: .touchUpInside)
                cell.btnInfo.addTarget(self, action: #selector(CustomMeditationViewController.checkTrackInfo(sender: )), for: .touchUpInside)
                cell.btnPlay.addTarget(self, action: #selector(CustomMeditationViewController.playTrack(sender: )), for: .touchUpInside)
                
                let strTrackType = self.trackList[indexPath.row]["track_type"] as! String
                if strTrackType == "S" {
                    cell.btnPlay.isHidden = true
                } else {
                    cell.btnPlay.isHidden = false
                }
                cell.btnInfo.isHidden = false
                cell.btnEditTrackName.isHidden = false
            }
            
            cell.allRows = self.trackList.count + 1
            cell.currentIndexPath = indexPath
            cell.setNeedsDisplay()

            cell.isUserInteractionEnabled = true
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            if btnindex == indexPath.row{
                cell.btnPlay.isSelected = true
                cell.btnPlay.setImage(#imageLiteral(resourceName: "Stop"), for: UIControlState.selected)
            }else{
                cell.btnPlay.isSelected = false
                cell.btnPlay.setImage(#imageLiteral(resourceName: "SmallPlay"), for: UIControlState.normal)
            }
            
            return cell
        }
        else if tableView == tblIntroTrackOptions {
            var cell: UITableViewCell! = tblIntroTrackOptions.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
            
            if cell == nil {
                cell = UITableViewCell.init(style: .default, reuseIdentifier: cellIdentifier)
                cell.isUserInteractionEnabled = true
                cell.accessoryType = .none
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
            }
            
            let lblOptionName: UILabel = cell.viewWithTag(10) as! UILabel
            lblOptionName.text = introOptionsList[indexPath.row]
            
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            return cell
        }
        else if tableView == tblEndTrackOptions {
            var cell: UITableViewCell! = tblEndTrackOptions.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
            
            if cell == nil {
                cell = UITableViewCell.init(style: .default, reuseIdentifier: cellIdentifier)
                cell.isUserInteractionEnabled = true
                cell.accessoryType = .none
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
            }
            
            let lblOptionName: UILabel = cell.viewWithTag(10) as! UILabel
            lblOptionName.text = endOptionsList[indexPath.row]
            
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            return cell
        }
        else {
            var cell: UITableViewCell! = tblTrackOptions.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
            
            if cell == nil {
                cell = UITableViewCell.init(style: .default, reuseIdentifier: cellIdentifier)
                cell.isUserInteractionEnabled = true
                cell.accessoryType = .none
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
            }
            
            let lblOptionName: UILabel = cell.viewWithTag(10) as! UILabel
            lblOptionName.text = trackOptionsList[indexPath.row]
            
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblTrackOptions {
            if(trackOptionsList[indexPath.row] == "Move Up"){
                currentPlayIndex = 500
                self.trackList.rearrange(from: editIndex, to: editIndex - 1 )
                backgroundView.isHidden = true
                isOrignal = false
            }else if(trackOptionsList[indexPath.row] == "Move Down"){
                currentPlayIndex = 500
                self.trackList.rearrange(from: editIndex, to: editIndex + 1)
                backgroundView.isHidden = true
                isOrignal = false
            }else if(trackOptionsList[indexPath.row] == "Delete"){
                currentPlayIndex = 500
                self.trackList.remove(at: editIndex)
                lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: trackList).time)"
                backgroundView.isHidden = true
                isOrignal = false
            }else if(trackOptionsList[indexPath.row] == "Add Silence 30 Sec"){
                currentPlayIndex = 500
                addSilenceTrack(index: editIndex + 1)
                backgroundView.isHidden = true
                isOrignal = false
            }else if(trackOptionsList[indexPath.row] == "Add to Another Meditation"){
                backgroundView.isHidden = false
                addtoAnotherMeditationView.isHidden = false
            }
            trackOptionsView.isHidden = true
            tblTracks.reloadData()
        }
        else if tableView == tblTracks {
            if (indexPath.row == trackList.count - 1) {
                if(btnindex != -1){
                    DispatchQueue.main.async {
                        self.player.pause()
                        self.tblTracks.reloadData()
                        self.btnindex = -1
                    }
                }
                let addTracksViewController: AddTracksViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddTracksViewController") as! AddTracksViewController
                self.navigationController?.pushViewController(addTracksViewController, animated: true)
            }
        }
        else if tableView == tblIntroTrackOptions {
            if introOptionsList[indexPath.row] == "Add Silence 30 Sec" {
                addSilenceTrack(index: editIndex + 1)
            }
            else if introOptionsList[indexPath.row] == "Change Intro" {
                let backgroundMusicViewController: BackgroundMusicViewController = self.storyboard?.instantiateViewController(withIdentifier: "BackgroundMusicViewController") as! BackgroundMusicViewController
                backgroundMusicViewController.strController = "intro"
                self.navigationController?.pushViewController(backgroundMusicViewController, animated: true)
            }
            backgroundView.isHidden = true
            introTrackOptionsView.isHidden = true
        }
        else if tableView == tblEndTrackOptions {
            if endOptionsList[indexPath.row] == "Change Ending" {
                let backgroundMusicViewController: BackgroundMusicViewController = self.storyboard?.instantiateViewController(withIdentifier: "BackgroundMusicViewController") as! BackgroundMusicViewController
                backgroundMusicViewController.strController = "ending"
                self.navigationController?.pushViewController(backgroundMusicViewController, animated: true)
            }
            backgroundView.isHidden = true
            endTracksOptionsView.isHidden = true
        }
        tblTracksHeightConstraint.constant = CGFloat((self.trackList.count + 1) * 80)
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    
    
    func addSilenceTrack(index : Int){
        var silenceDictionary = [String : AnyObject]()
        silenceDictionary["reminder"] = silenceList["reminder"] as AnyObject
        silenceDictionary["track_type"] = "S" as AnyObject
        silenceDictionary["audio_url"] = silenceList["audio_url"] as AnyObject
        silenceDictionary["description"] = silenceList["description"] as AnyObject
        silenceDictionary["title"] = silenceList["title"] as AnyObject
        silenceDictionary["track_id"] = silenceList["track_id"] as AnyObject
        silenceDictionary["time"] = silenceList["time"] as AnyObject
        silenceDictionary["display_order"] = silenceList["display_order"] as AnyObject
        trackList.insert(silenceDictionary, at: index)
        tblTracks.reloadData()
        lblTotalTrackTime.text = "Total: \(getTotalTrackTime(track: trackList).time)"
    }
    
    //UITextField Delegate Methods
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtTrackName.resignFirstResponder()
        return true
    }
    func popToController(){
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

