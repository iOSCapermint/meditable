//
//  TrackInfoViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 20/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

class TrackInfoViewController: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblInfo: UILabel!
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var lblTrackTitle: UILabel!
    
    @IBOutlet weak var firstSeperator: UIView!
    
    @IBOutlet weak var lblType: UILabel!
    
    @IBOutlet weak var lblTrackTypeName: UILabel!
    
    @IBOutlet weak var secondSeperator: UIView!
    
    @IBOutlet weak var lblTrackInfo: UILabel!
    
    @IBOutlet weak var lblReminder: UILabel!
    
    @IBOutlet weak var lblReminderThought: UILabel!

//    var trackInfoArray = [[String: AnyObject]]()
    
    var trackList = [String : AnyObject]()

    var strType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()


        lblType.layer.cornerRadius = lblType.frame.size.height / 2
        lblType.layer.masksToBounds = true
        
        lblReminder.layer.cornerRadius = lblReminder.frame.size.height / 2
        lblReminder.layer.masksToBounds = true
        
            self.lblTrackTitle.text = trackList["title"] as? String
            self.lblTrackTypeName.text = trackList["type"] as? String
            self.lblTrackInfo.text = trackList["description"] as? String
            self.lblReminderThought.text = trackList["reminder"] as? String
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Track information")
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        //_ = self.navigationController?.popViewController(animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
