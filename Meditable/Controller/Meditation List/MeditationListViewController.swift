//
//  MeditationListViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 19/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON
import UICircularProgressRing
import MZDownloadManager


class MeditationListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable, MZDownloadManagerDelegate {
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var imgSubCategory: UIImageView!
    
    @IBOutlet weak var lblSubCategoryName: UILabel!
    
    @IBOutlet weak var tblTracks: UITableView!
    
    @IBOutlet weak var imgActivityBackground: UIImageView!

    var meditationArray = [[String: AnyObject]]()
    
    var subCatID = Int()
    var strSubCategoryName = ""
    var strImgURL = ""
    var refresher: UIRefreshControl!
    var downloadFromThis : Bool = false
    var mID = ""
    
    var meditationData = [String : AnyObject]()
    var soundArrayList = [String]()
    var trackNameArray = [String]()
    var availableDownloadsArray: [String] = []
    var selectedIndexPath : IndexPath!
    var totalProgressArray = [Float]()
    var meditationID = ""
    var meditationName = ""
    var downloadIndexpath = IndexPath()
    var isPause = ""

    lazy var downloadManager: MZDownloadManager = {
        [unowned self] in
        let sessionIdentifer: String = self.generateRandomStringWithLength(length: 10)
        var completion = appDelegate.backgroundSessionCompletionHandler
        let downloadmanager = MZDownloadManager(session: sessionIdentifer, delegate: self, completion: completion)
        return downloadmanager
        }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        //RefreshControl
        refresher = UIRefreshControl()
        tblTracks!.alwaysBounceVertical = true
        refresher.tintColor = .white
        refresher.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tblTracks!.addSubview(refresher)
        
        getMeditationList()

        lblSubCategoryName.text = self.strSubCategoryName
        
        if let userImageUrl = self.strImgURL as? String {
            let userProfilePicture = MLDownloadImageView()
            userProfilePicture.downloadImage(with: NSURL.init(string: userImageUrl) as URL!, placeHolder: nil, imageError: nil, roundedCorners: false, block: { (image) in
                if image != nil {
                    self.imgSubCategory.image = image
                }
            })
            userProfilePicture.layer.masksToBounds = true
            userProfilePicture.cacheEnable = true
            userProfilePicture.startDownload()
        }
        
        tblTracks.rowHeight = UITableViewAutomaticDimension
        tblTracks.estimatedRowHeight = 90
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        addGoogleAnalytics(screenName: "Meditation List")
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        //_ = self.navigationController?.popViewController(animated: true)
        
        var downloading : Bool = false
        if(defaults.value(forKey: "startDownloading") != nil && self.downloadFromThis == true){
            downloading = defaults.value(forKey: "startDownloading") as! Bool
        }
        if(downloading == true){
            let alertController = UIAlertController(title: AppName, message: QuitMeditaionMessage, preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
            { action -> Void in
                
                for i in 0...self.downloadManager.downloadingArray.count - 1{
                    self.downloadManager.cancelTaskAtIndex(i)
                }
                self.removeFromBundle(index: self.downloadIndexpath.row)
                defaults.set(false, forKey: "startDownloading")
                self.downloadFromThis = false
                let transition = CATransition()
                transition.duration = 0.3
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transition.type = kCATransitionPush
                self.navigationController?.view.layer.add(transition, forKey: nil)
                _ = self.navigationController?.popViewController(animated: false)
                
            })
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
            { action -> Void in
                
            })
            self.present(alertController, animated: true, completion: nil)
        }else{
            let transition = CATransition()
            transition.duration = 0.3
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionPush
            self.navigationController?.view.layer.add(transition, forKey: nil)
            _ = self.navigationController?.popViewController(animated: false)
        }
    }
    
    func removeFromBundle(index : Int){
        let trackArray : [[String : AnyObject]] = meditationArray[index]["track"] as! [[String : AnyObject]]
        for i in 0...trackArray.count - 1{
            let trackTitle = "\(trackArray[i]["title"]!)-\(mID)\(trackArray[i]["track_type"]!)\(trackArray[i]["track_id"]!).mp3"
            self.removeMeditation(trackTitle: trackTitle)
        }
    }
    func removeMeditation(trackTitle : String) {
        let fileManager = FileManager.default
        if !FileManager.default.fileExists(atPath: appDelegate.myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: appDelegate.myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        let path =  appDelegate.myDownloadPath + "/" + "\(trackTitle)"
        do {
            try fileManager.removeItem(atPath: path)
        } catch let error as NSError {
            print(error.debugDescription)
        }
    }
    
    
    //MARK:-  Refresh -
    func refreshList() {
        
        var downloading : Bool = false
        if(defaults.value(forKey: "startDownloading") != nil && self.downloadFromThis == true){
            downloading = defaults.value(forKey: "startDownloading") as! Bool
        }
        if(downloading == true){
                for i in 0...self.downloadManager.downloadingArray.count - 1{
                    self.downloadManager.cancelTaskAtIndex(i)
                }
                self.removeFromBundle(index: self.downloadIndexpath.row)
                defaults.set(false, forKey: "startDownloading")
            self.downloadFromThis = false
        }
        isPause = ""
        self.refresher.endRefreshing()
        getMeditationList()
    }
    
     //MARK:-  TableView Delegate & Data source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.meditationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "meditationListCell"
        var downloaded = false
        let cell: meditationListCell = tblTracks.dequeueReusableCell(withIdentifier: cellIdentifier) as! meditationListCell!
        
            cell.accessoryType = .none
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            cell.isUserInteractionEnabled = true
       
        cell.lblMeditationTitle.text = (self.meditationArray[indexPath.row]["title"] as! String)
        cell.lblDescription.text = (self.meditationArray[indexPath.row]["description"] as! String)
        
        let trackArray : [[String : AnyObject]] = meditationArray[indexPath.row]["track"] as! [[String : AnyObject]]
        var bgTime = "00:00"
        for i in 0...trackArray.count - 1 {
            if("\(trackArray[i]["track_type"]!)" == "B"){
                bgTime = "\(trackArray[i]["time"]!)"
                break
            }
        }
        var silenceTime = "00:00"
        for i in 0...trackArray.count - 1 {
            print(trackArray[i])
            if("\(trackArray[i]["track_type"]!)" == "S"){
                silenceTime = "\(trackArray[i]["time"]!)"
                break
            }
        }
        let time = self.getTotalTimeWithoutSilenceBackGround(totalTime: "\(self.meditationArray[indexPath.row]["total_meditation_time"]!)", backgroundMusicTime: bgTime, silenceMusic: silenceTime)

        cell.lblTime.text = "\(time.time)"

        cell.btnLock.addTarget(self, action: #selector(MeditationListViewController.btnLock(sender:)), for: .touchUpInside)
        cell.btnDownload.addTarget(self, action: #selector(MeditationListViewController.btnDownload(sender:)), for: .touchUpInside)

        cell.btnLock.tag = indexPath.row
        cell.btnDownload.tag = indexPath.row
        cell.progressView.isHidden = true
        cell.progressView.shouldShowValueText = false
        
        cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadCloud"), for: UIControlState.normal)
        var trackListMeditation = self.meditationArray[indexPath.row]["track"] as! [[String : AnyObject]]
        if(trackListMeditation.count != 0){
            if(appDelegate.downloadedMeditations.count != 0){
                for i in 0...appDelegate.downloadedMeditations.count - 1{
                    if("\(appDelegate.downloadedMeditations[i]["title"]!)" == "\(self.meditationArray[indexPath.row]["title"]!)"){
                        for i in 0...trackListMeditation.count - 1 {
                            if("\(trackListMeditation[i]["track_type"]!)" == "S"){
                                trackListMeditation.remove(at: i)
                                break
                            }
                        }
                        var trackListAppdelegate = appDelegate.downloadedMeditations[i]["track"] as! [[String : AnyObject]]
                        var same = true
                        if(trackListAppdelegate.count == trackListMeditation.count){
                            for i in 0...trackListMeditation.count - 1 {
                                if("\(trackListMeditation[i]["audio_url"]!)" != "\(trackListAppdelegate[i]["audio_url"]!)"){
                                    same = false
                                }
                            }
                        }else{
                            same = false
                        }
                        if(same == true){
                            cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadedCloud"), for: UIControlState.normal)
                            downloaded = true
                        }else{
                            cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadCloud"), for: UIControlState.normal)
                        }
                        break
                    }else{
                        cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadCloud"), for: UIControlState.normal)
                    }
                }
            }else{
                cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadCloud"), for: UIControlState.normal)
            }
        }
      
        
        var downloading : Bool = false
        if(UserDefaults.standard.value(forKey: "startDownloading") != nil){
            downloading = UserDefaults.standard.value(forKey: "startDownloading") as! Bool
        }
        if downloading == true{
            if(indexPath == downloadIndexpath){
                cell.progressView.isHidden = false
                cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_control_stop"), for: UIControlState.normal)
            }
        }

        if("\(self.meditationArray[indexPath.row]["is_locked"]!)" == "Y" && downloaded == false){
            cell.titleLeftConstraint.constant = 43
            cell.lblMeditationTitle.alpha = 0.5
            cell.lblTime.alpha = 0.5
            cell.lblDescription.alpha = 0.5
            cell.btnLock.alpha = 0.5
            cell.btnDownload.alpha = 0.5
            cell.btnLock.isHidden = false
            cell.progressView.alpha = 0.5
        }else{
            cell.titleLeftConstraint.constant = 20
            cell.lblMeditationTitle.alpha = 1.0
            cell.lblTime.alpha = 1.0
            cell.lblDescription.alpha = 1.0
            cell.btnLock.alpha = 1.0
            cell.btnDownload.alpha = 1.0
            cell.btnLock.isHidden = true
            cell.progressView.alpha = 1.0
        }

        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        appDelegate.playFromLibrary = true

        
        var downloaded : Bool = false
        var trackListMeditation = self.meditationArray[indexPath.row]["track"] as! [[String : AnyObject]]
        if(appDelegate.downloadedMeditations.count != 0){
            for i in 0...appDelegate.downloadedMeditations.count - 1{
                if("\(appDelegate.downloadedMeditations[i]["title"]!)" == "\(self.meditationArray[indexPath.row]["title"]!)"){
                    for i in 0...trackListMeditation.count - 1 {
                        if("\(trackListMeditation[i]["track_type"]!)" == "S"){
                            trackListMeditation.remove(at: i)
                            break
                        }
                    }
                    var trackListAppdelegate = appDelegate.downloadedMeditations[i]["track"] as! [[String : AnyObject]]
                    var same = true
                    if(trackListAppdelegate.count == trackListMeditation.count){
                        for i in 0...trackListMeditation.count - 1 {
                            if("\(trackListMeditation[i]["audio_url"]!)" != "\(trackListAppdelegate[i]["audio_url"]!)"){
                                same = false
                            }
                        }
                    }else{
                        same = false
                    }
                    if(same == true){
                        downloaded = true
                        mID = "\(appDelegate.downloadedMeditations[i]["meditation_id"]!)"
                    }
                    break
                }
            }
        }
//        if(appDelegate.downloadedMeditations.count != 0){
//            for i in 0...appDelegate.downloadedMeditations.count - 1{
//                if("\(appDelegate.downloadedMeditations[i]["meditation_id"]!)" == "\(self.meditationArray[indexPath.row]["meditation_id"]!)"){
//                    downloaded = true
//                    break
//                }
//            }
//        }
        
        
        if(downloaded == false){
            if("\(self.meditationArray[indexPath.row]["is_locked"]!)" == "Y"){
                
                let alertController = UIAlertController(title: AppName, message: LockMeditationMessage, preferredStyle:UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Subscribe", style: UIAlertActionStyle.default)
                { action -> Void in
                    let subscriptionPlanvc: SubscriptionPlanViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionPlanViewController") as! SubscriptionPlanViewController
                    self.navigationController?.pushViewController(subscriptionPlanvc, animated: true)
                })
                alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
                { action -> Void in
                    
                })
                self.present(alertController, animated: true, completion: nil)
            }else{
                let meditationTrackListViewController: MeditationTrackListViewController = self.storyboard?.instantiateViewController(withIdentifier: "MeditationTrackListViewController") as! MeditationTrackListViewController
                appDelegate.healthTracksArray = [self.meditationArray[indexPath.row]]
                meditationTrackListViewController.responseArray = [self.meditationArray[indexPath.row]]
                meditationTrackListViewController.index = indexPath.row
                
                let trackList = self.meditationArray[indexPath.row]["track"] as! [[String : AnyObject]]
                var bgTime = "00:00"
                for i in 0...trackList.count - 1 {
                    if("\(trackList[i]["track_type"]!)" == "B"){
                        bgTime = "\(trackList[i]["time"]!)"
                        break
                    }
                }
                var silenceTime = "00:00"
                for i in 0...trackList.count - 1 {
                    if("\(trackList[i]["track_type"]!)" == "S"){
                        silenceTime = "\(trackList[i]["time"]!)"
                        break
                    }
                }
                
                let time = self.getTotalTimeWithoutSilenceBackGround(totalTime: "\(self.meditationArray[indexPath.row]["total_meditation_time"]!)", backgroundMusicTime: bgTime, silenceMusic: silenceTime)
                meditationTrackListViewController.totalTime = "\(time.time)"
                meditationTrackListViewController.backgroundSeconds = time.backgroundSeconds
                meditationTrackListViewController.totalSeconds = time.totalSeconds
                
                self.navigationController?.pushViewController(meditationTrackListViewController, animated: true)
            }
        }else{
            print(self.meditationArray[indexPath.row])
            let customMeditationPlaylistViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationPlaylistViewController") as! CustomMeditationPlaylistViewController
            customMeditationPlaylistViewController.responseArray = [self.meditationArray[indexPath.row]]
            customMeditationPlaylistViewController.index = indexPath.row
            customMeditationPlaylistViewController.isOriginal = "Y"

            if(self.meditationArray[indexPath.section]["meditation_id"] == nil){
                customMeditationPlaylistViewController.meditationID = "\(self.meditationArray[indexPath.row]["custom_meditation_id"]!)"
            }else{
                customMeditationPlaylistViewController.meditationID = "\(mID)"
            }
            var trackList = self.meditationArray[indexPath.row]["track"] as! [[String : AnyObject]]
            
            var bgTime = "00:00"
            for i in 0...trackList.count - 1 {
                if("\(trackList[i]["track_type"]!)" == "B"){
                    bgTime = "\(trackList[i]["time"]!)"
                    break
                }
            }

            let time = self.getTotalTime(totalTime: "\(self.meditationArray[indexPath.row]["total_meditation_time"]!)", backgroundMusicTime: bgTime)
            
            customMeditationPlaylistViewController.totalTime = "\(time.time)"
            customMeditationPlaylistViewController.backgroundSeconds = time.backgroundSeconds
            customMeditationPlaylistViewController.totalSeconds = time.totalSeconds
            customMeditationPlaylistViewController.isDownloaded = true
            customMeditationPlaylistViewController.isFromMeditationListViewController = true
            self.navigationController?.pushViewController(customMeditationPlaylistViewController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }

    //MARK: - GetMeditationList API -
    func getMeditationList() {
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey, "sub_category_id": self.subCatID]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetMeditationList, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            self.meditationArray = json.value(forKey: "data") as! [[String : AnyObject]]
                            
                            if self.meditationArray.count > 0{
                                self.tblTracks.reloadData()
                            }else{
                                let alertSuccess: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: "No Meditation available", preferredStyle: .alert)
                                
                                let btnOk: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
                                    _ in
                                    let transition = CATransition()
                                    transition.duration = 0.3
                                    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                                    transition.type = kCATransitionPush
                                    self.navigationController?.view.layer.add(transition, forKey: nil)
                                    _ = self.navigationController?.popViewController(animated: true)
                                })
                                
                                alertSuccess.addAction(btnOk)
                                self.present(alertSuccess, animated: true, completion: nil)
                            }
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                        
                    }else {

                    }
                case .failure(let error):
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    @IBAction func btnLock(sender: UIButton) {
    
    }
    @IBAction func btnDownload(sender: UIButton) {
        
        let cell : meditationListCell = sender.superview?.superview as! meditationListCell
        let indexPath = tblTracks.indexPath(for: cell)
        var downloading : Bool = false
        var foundedIndex = 5000
        if("\(self.meditationArray[indexPath!.row]["is_locked"]!)" == "Y"){
            let alertController = UIAlertController(title: AppName, message: LockMeditationMessage, preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Subscribe", style: UIAlertActionStyle.default)
            { action -> Void in
                let subscriptionPlanvc: SubscriptionPlanViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionPlanViewController") as! SubscriptionPlanViewController
                self.navigationController?.pushViewController(subscriptionPlanvc, animated: true)
            })
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
            { action -> Void in
                
            })
            self.present(alertController, animated: true, completion: nil)
        }else{
            if(UserDefaults.standard.value(forKey: "startDownloading") != nil){
                downloading = UserDefaults.standard.value(forKey: "startDownloading") as! Bool
            }
            var found : Bool = false
            if(appDelegate.downloadedMeditations.count != 0){
                var trackListMeditation = self.meditationArray[sender.tag]["track"] as! [[String : AnyObject]]
                for i in 0...appDelegate.downloadedMeditations.count - 1{
                    if("\(appDelegate.downloadedMeditations[i]["title"]!)" == "\(self.meditationArray[sender.tag]["title"]!)"){
                        for i in 0...trackListMeditation.count - 1 {
                            if("\(trackListMeditation[i]["track_type"]!)" == "S"){
                                trackListMeditation.remove(at: i)
                                break
                            }
                        }
                        var trackListAppdelegate = appDelegate.downloadedMeditations[i]["track"] as! [[String : AnyObject]]
                        var same = true
                        if(trackListAppdelegate.count == trackListMeditation.count){
                            for i in 0...trackListMeditation.count - 1 {
                                if("\(trackListMeditation[i]["audio_url"]!)" != "\(trackListAppdelegate[i]["audio_url"]!)"){
                                    same = false
                                }
                            }
                        }else{
                            same = false
                        }
                        if(same == true){
                            found = true
                            foundedIndex = i
                        }
                        break
                    }
                }
//                for i in 0...appDelegate.downloadedMeditations.count - 1{
//                    if("\(appDelegate.downloadedMeditations[i]["meditation_id"]!)" == "\(self.meditationArray[sender.tag]["meditation_id"]!)"){
//                        found = true
//                        break
//                    }else{
//                        found = false
//                    }
//                }
            }else{
                cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadCloud"), for: UIControlState.normal)
            }
            if(found == false){
                if(isPause == "Pause" && downloadIndexpath == indexPath){
                    for i in 0...self.downloadManager.downloadingArray.count - 1{
                        self.downloadManager.resumeDownloadTaskAtIndex(i)
                    }
                    cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_control_stop"), for: UIControlState.normal)
                    isPause = "Play"
                }else if(isPause == "Play" && downloadIndexpath == indexPath){
                    for i in 0...self.downloadManager.downloadingArray.count - 1{
                        self.downloadManager.pauseDownloadTaskAtIndex(i)
                    }
                    cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_play"), for: UIControlState.normal)
                    isPause = "Pause"
                }else{
                    if(downloading == false){
                        isPause = "Play"
                        downloadFromThis = true
                        downloadIndexpath = indexPath!
                        cell.progressView.isHidden = false
                        cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_control_stop"), for: UIControlState.normal)
                        saveForCustomMeditation(indexPath: indexPath!)
//                        self.prepareForDownload(indexPath: indexPath!)
                        meditationID = "\(self.meditationArray[indexPath!.section]["meditation_id"]!)"
                        meditationName = "\(self.meditationArray[indexPath!.section]["title"]!)"
                        UserDefaults.standard.set(true, forKey: "startDownloading")
                        UserDefaults.standard.synchronize()
                        
                    }else{
                        UIAlertController().alertViewWithTitleAndMessage(self, message: "Please wait, Previous meditation download is in progress")
                    }
                }   
            }else{
                
                let alertController = UIAlertController(title: AppName, message: DeleteDownloadedMeditaionMessage, preferredStyle:UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
                { action -> Void in
                    self.removeFromBundle(index: sender.tag)
                    appDelegate.downloadedMeditations.remove(at: foundedIndex)
//                    if(appDelegate.downloadedMeditations.count != 0){
//                        for i in 0...appDelegate.downloadedMeditations.count - 1{
//                            if("\(appDelegate.downloadedMeditations[i]["meditation_id"]!)" == "\(self.meditationArray[sender.tag]["meditation_id"]!)"){
//                                appDelegate.downloadedMeditations.remove(at: i)
//                                break
//                            }
//                        }
//                    }
                    let archiveData = NSKeyedArchiver.archivedData(withRootObject: appDelegate.downloadedMeditations)
                    defaults.set(archiveData, forKey: kdownloadedMeditations)
                    defaults.synchronize()
                    self.refreshList()
                })
                alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
                { action -> Void in
                    
                })
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Download Manager -
    
    func prepareForDownload(indexPath : IndexPath){
        soundArrayList.removeAll()
        trackNameArray.removeAll()
        totalProgressArray.removeAll()
        
        meditationData["title"] = self.meditationArray[indexPath.row]["title"] as AnyObject
        meditationData["meditation_id"] = "\(mID)" as AnyObject// self.meditationArray[indexPath.row]["meditation_id"] as AnyObject
        meditationData["total_meditation_time"] = self.meditationArray[indexPath.row]["total_meditation_time"] as AnyObject
        meditationData["description"] = "\(self.meditationArray[indexPath.row]["description"]!)" as AnyObject
        
        var trackList = self.meditationArray[indexPath.row]["track"] as! [[String : AnyObject]]
        for index in 0...trackList.count - 1{
            if("\(trackList[index]["track_type"]!)" == "S"){
                trackList.remove(at: index)
                break
            }
        }
        meditationData["track"] = trackList as AnyObject
        meditationData["is_original"] = "Made by Meditable" as AnyObject
        
        for i in 0...trackList.count - 1{
            soundArrayList.append("\(trackList[i]["audio_url"]!)")
            let trackTitle = "\(trackList[i]["title"]!)-\(mID)\(trackList[i]["track_type"]!)\(trackList[i]["track_id"]!).mp3"
            trackNameArray.append("\(trackTitle)")
        }
    }
    func setDownloader(){
        
        for i in 0...soundArrayList.count - 1{
            let fileURL  : NSString = soundArrayList[i] as NSString
            var fileName : NSString = fileURL.lastPathComponent as NSString
            fileName = trackNameArray[i] as NSString //MZUtility.getUniqueFileNameWithPath((myDownloadPath as NSString).appendingPathComponent(fileName as String) as NSString)
            self.downloadManager.addDownloadTask(fileName as String, fileURL: fileURL as String, destinationPath: appDelegate.myDownloadPath)
            totalProgressArray.append(0.0)
        }
    }
    
    func downloadProgress(indexPath : IndexPath, downloadModel: MZDownloadModel){
        DispatchQueue.global().async {
            
//            let cell : meditationListCell = self.tblTracks.cellForRow(at: self.downloadIndexpath) as! meditationListCell
            
            if(downloadModel.progress != 1.0 && self.totalProgressArray[indexPath.row] != 1.0){
                self.totalProgressArray[indexPath.row] = downloadModel.progress
            }else{
                self.totalProgressArray.remove(at: indexPath.row)
                self.totalProgressArray.append(downloadModel.progress)
            }
            let totalSum = self.totalProgressArray.reduce(0,+)
            let totalProgress = (totalSum * 100) / Float(self.totalProgressArray.count)
            print("************************************Total Progress - \(totalProgress)")
            
            for indexPath in self.tblTracks.indexPathsForVisibleRows! {
                if (indexPath.section == self.downloadIndexpath.section) {
                    if let cell : meditationListCell = self.tblTracks.cellForRow(at: self.downloadIndexpath) as? meditationListCell{
                        DispatchQueue.global().async {
                            cell.progressView.setProgress(value: CGFloat(totalProgress), animationDuration: 0)
                        }
                    }
                }
            }
        }
    }
    
    func saveToLocalData(mID : String){
        meditationData["meditation_id"] = mID as AnyObject
        appDelegate.downloadedMeditations.insert(meditationData, at: 0)
        let archiveData = NSKeyedArchiver.archivedData(withRootObject: appDelegate.downloadedMeditations)
        defaults.set(archiveData, forKey: kdownloadedMeditations)
        defaults.synchronize()
        meditationID = ""
        meditationName = ""
        let cell : meditationListCell = tblTracks.cellForRow(at: downloadIndexpath) as! meditationListCell
        cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadedCloud"), for: UIControlState.normal)
        cell.progressView.isHidden = true
        cell.btnDownload.isHidden = false
//        self.saveForCustomMeditation()
    }
    
    func saveForCustomMeditation(indexPath : IndexPath){
        if appDelegate.reachable.isReachable{
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            var params = Parameters()
            
            var trackID = ""
            var trackType = ""
            let trackList = self.meditationArray[downloadIndexpath.row]["track"] as! [[String : AnyObject]]

            for index in 0...trackList.count - 1{
                if("\(trackList[index]["track_type"]!)" != "S"){
                    if(index == 0){
                        trackID.append("\(trackList[index]["track_id"]!)")
                        trackType.append("\(trackList[index]["track_type"]!)")
                    }else{
                        trackID.append(",\(trackList[index]["track_id"]!)")
                        trackType.append(",\(trackList[index]["track_type"]!)")
                    }
                }
            }
            params = ["auth_key": authKey, "track_ids": trackID, "track_types" : trackType, "title" : "\(self.meditationArray[downloadIndexpath.row]["title"]!)", "is_original" : "Y"]
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kSaveCustomList, method: .post, parameters: params).validate().responseJSON { response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            let responseResult = response.result.value as! [String : Any]
                            self.mID = "\(responseResult["custom_meditation_id"]!)"
                            self.prepareForDownload(indexPath: indexPath)
                            self.setDownloader()
                            NotificationCenter.default.post(name: NSNotification.Name.init("SetCustomeMeditationAdded"), object: nil)
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }

                    
                case .failure(let error):
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
    }
    
    
    func refreshCellForIndex(_ downloadModel: MZDownloadModel, index: Int) {
        let indexPath = IndexPath.init(row: index, section: 0)
        self.downloadProgress(indexPath: indexPath, downloadModel: downloadModel)
        //        let cell = self.tblView.cellForRow(at: indexPath)
        //        if let cell = cell {
        //            let downloadCell = cell as! MZDownloadingCell
        //            downloadCell.updateCellForRowAtIndexPath(indexPath, downloadModel: downloadModel)
        //        }
    }
    // MARK: UIAlertController Handler Extension
    
    func showAppropriateActionController(_ requestStatus: String) {
        
        if requestStatus == TaskStatus.downloading.description() {
            self.showAlertControllerForPause()
        } else if requestStatus == TaskStatus.failed.description() {
            self.showAlertControllerForRetry()
        } else if requestStatus == TaskStatus.paused.description() {
            self.showAlertControllerForStart()
        }
    }
    
    func showAlertControllerForPause() {
        
        let pauseAction = UIAlertAction(title: "Pause", style: .default) { (alertAction: UIAlertAction) in
            self.downloadManager.pauseDownloadTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let removeAction = UIAlertAction(title: "Remove", style: .destructive) { (alertAction: UIAlertAction) in
            self.downloadManager.cancelTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.view.tag = alertControllerViewTag
        alertController.addAction(pauseAction)
        alertController.addAction(removeAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertControllerForRetry() {
        
        let retryAction = UIAlertAction(title: "Retry", style: .default) { (alertAction: UIAlertAction) in
            self.downloadManager.retryDownloadTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let removeAction = UIAlertAction(title: "Remove", style: .destructive) { (alertAction: UIAlertAction) in
            self.downloadManager.cancelTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.view.tag = alertControllerViewTag
        alertController.addAction(retryAction)
        alertController.addAction(removeAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertControllerForStart() {
        
        let startAction = UIAlertAction(title: "Start", style: .default) { (alertAction: UIAlertAction) in
            self.downloadManager.resumeDownloadTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let removeAction = UIAlertAction(title: "Remove", style: .destructive) { (alertAction: UIAlertAction) in
            self.downloadManager.cancelTaskAtIndex(self.selectedIndexPath.row)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.view.tag = alertControllerViewTag
        alertController.addAction(startAction)
        alertController.addAction(removeAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func safelyDismissAlertController() {
        /***** Dismiss alert controller if and only if it exists and it belongs to MZDownloadManager *****/
        /***** E.g App will eventually crash if download is completed and user tap remove *****/
        /***** As it was already removed from the array *****/
        if let controller = self.presentedViewController {
            guard controller is UIAlertController && controller.view.tag == alertControllerViewTag else {
                return
            }
            controller.dismiss(animated: true, completion: nil)
        }
    }
    
    func downloadRequestStarted(_ downloadModel: MZDownloadModel, index: Int) {
        
    }
    
    func downloadRequestDidPopulatedInterruptedTasks(_ downloadModels: [MZDownloadModel]) {
        
    }
    
    func downloadRequestDidUpdateProgress(_ downloadModel: MZDownloadModel, index: Int) {
        self.refreshCellForIndex(downloadModel, index: index)
    }
    
    func downloadRequestDidPaused(_ downloadModel: MZDownloadModel, index: Int) {
        self.refreshCellForIndex(downloadModel, index: index)
    }
    
    func downloadRequestDidResumed(_ downloadModel: MZDownloadModel, index: Int) {
        self.refreshCellForIndex(downloadModel, index: index)
    }
    
    func downloadRequestCanceled(_ downloadModel: MZDownloadModel, index: Int) {
        self.safelyDismissAlertController()
    }
    
    func downloadRequestFinished(_ downloadModel: MZDownloadModel, index: Int) {
        if(downloadManager.downloadingArray.count == 0){
            print("All track Downloaded")
            downloadFromThis = false
            defaults.set(false, forKey: "startDownloading")
            defaults.synchronize()
            saveToLocalData(mID: self.mID)
            
            for indexPath in self.tblTracks.indexPathsForVisibleRows! {
                if (indexPath.section == self.downloadIndexpath.section) {
                    if let cell : meditationListCell = self.tblTracks.cellForRow(at: self.downloadIndexpath) as? meditationListCell{
                        DispatchQueue.global().async {
                            cell.progressView.setProgress(value: CGFloat(100.0), animationDuration: 0)
                        }
                    }
                }
            }
        }
        self.safelyDismissAlertController()
        
//        downloadManager.presentNotificationForDownload("Ok", notifBody: "Download did completed")
        
        let docDirectoryPath : NSString = (MZUtility.baseFilePath as NSString).appendingPathComponent(downloadModel.fileName) as NSString
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: MZUtility.DownloadCompletedNotif as String), object: docDirectoryPath)
    }
    
    func downloadRequestDidFailedWithError(_ error: NSError, downloadModel: MZDownloadModel, index: Int) {
        self.safelyDismissAlertController()
        self.refreshCellForIndex(downloadModel, index: index)
        
        debugPrint("Error while downloading file: \(downloadModel.fileName)  Error: \(error)")
    }
    
    //Oppotunity to handle destination does not exists error
    //This delegate will be called on the session queue so handle it appropriately
    func downloadRequestDestinationDoestNotExists(_ downloadModel: MZDownloadModel, index: Int, location: URL) {
        let myDownloadPath = MZUtility.baseFilePath + "/Default folder"
        if !FileManager.default.fileExists(atPath: myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        let fileName = MZUtility.getUniqueFileNameWithPath((myDownloadPath as NSString).appendingPathComponent(downloadModel.fileName as String) as NSString)
        let path =  myDownloadPath + "/" + (fileName as String)
        try! FileManager.default.moveItem(at: location, to: URL(fileURLWithPath: path))
        debugPrint("Default folder path: \(myDownloadPath)")
    }
    
    
    func generateRandomStringWithLength(length:Int) -> String {
        
        let randomString:NSMutableString = NSMutableString(capacity: length)
        
        let letters:NSMutableString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        var i: Int = 0
        
        while i < length {
            
            let randomIndex:Int = Int(arc4random_uniform(UInt32(letters.length)))
            randomString.append("\(Character( UnicodeScalar( letters.character(at: randomIndex))!))")
            i += 1
        }
        
        return String(randomString)
    }

    
}
class meditationListCell : UITableViewCell{
    @IBOutlet weak var titleLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblMeditationTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnLock: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var progressView: UICircularProgressRingView!
    
}
