//
//  AllMeditationListCell.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 28/06/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit

class AllMeditationListCell: UITableViewCell {

    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var btnCheck: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
