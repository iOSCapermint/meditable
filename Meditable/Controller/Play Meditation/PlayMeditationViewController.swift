//
//  PlayMeditationViewController.swift
//  Meditable
//
//  Created by Apple on 19/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import AVFoundation
import AudioToolbox
import MediaPlayer
import NVActivityIndicatorView
import Alamofire


class PlayMeditationViewController: UIViewController, AVAudioPlayerDelegate, NVActivityIndicatorViewable {

    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!

    @IBOutlet weak var btnPlayMusic: UIButton!
    
    @IBOutlet weak var btnMute: UIButton!
    
    @IBOutlet weak var btnVolumeUp: UIButton!
    
    @IBOutlet weak var btnChange: UIButton!

    @IBOutlet weak var lblTrackName: UILabel!

    @IBOutlet weak var lblCurrentTimeOfTrack: UILabel!
    
    @IBOutlet weak var lblDurationOfTrack: UILabel!
    
    @IBOutlet weak var lblBackgroundMusicName: UILabel!

    @IBOutlet weak var sliderTrack: UISlider!
    
    @IBOutlet weak var sliderVolume: UISlider!

    @IBOutlet weak var heightConstantSliderTrack: NSLayoutConstraint!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var optionsView: UIView!
    
    @IBOutlet weak var lblTimeRemaining: UILabel!
    
    @IBOutlet weak var btnMembershipOptions: UIButton!
    
    @IBOutlet weak var btnClose1: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblMadeByYou: UILabel!

    @IBOutlet weak var lblTotalPlayListTime: UILabel!
    
    @IBOutlet weak var lblTotalTrack: UILabel!
    
    @IBOutlet weak var complitionView: UIView!
    
    @IBOutlet weak var innerComplitionView: UIView!
    
    @IBOutlet weak var innerImageView: UIImageView!
    
    @IBOutlet weak var innerLblmessage: UILabel!
    
    @IBOutlet weak var btnCloseComplitionView: UIButton!
    
    var trackList = [[String : AnyObject]]()
    var backgroundSound = [String : AnyObject]()
    var silenceSound = [String : AnyObject]()
    var audioPlayer : AVAudioPlayer!
    var trackQueueArray = [String]()
    var meditationTitle = ""
    var meditationID = ""
    var meditationDescription = ""
    var currentlyPlayingTrackCnt = 0
    var addObserver : Bool = false
    var backPress : Bool = false
    var meditaionBy = ""
    var audioFadeOut : Bool = false
    var playerMeditaion = AVQueuePlayer()
    var playerBackground = AVAudioPlayer()
    var meditationData = [String : AnyObject]()
    var downloadStart : Bool = false
    var soundArrayList = [String]()
    var fromSavedMeditation : Bool = false
    var totalMeditationTime = Float()
    var formattedTime = ""
    var isLoading : Bool = false
    var pressPlayWhileLoading : Bool = false
    var seconds : Int = 0
    var total_meditation_time = 0
    var totalTrackTime = ""
    var backgroundSeconds : Int = 0
    var totalSeconds : Int = 0
    var sendMeditationTrackLog = false
    var backgroundPath = ""
    var fromActivity = false
    var bgVolume : Float = defaults.value(forKey: "bgVolume") as! Float
    var timerForProgress = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            self.startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
        }

        timerForProgress = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
        
        self.myTotalMeditationTime()
        self.totalMeditationTime = 00.00
        for index in 0..<self.trackList.count{
            var time = self.trackList[index]["time"] as! String
            time = time.replace(":", replacement: ".")
            self.totalMeditationTime += Float(time)!
        }

        self.formattedTime = String(format: "%.02f", self.totalMeditationTime)
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        lblTitle.text = meditationTitle
        lblTotalPlayListTime.text = totalTrackTime
        lblMadeByYou.text = "\(meditaionBy)"
        self.lblTrackName.text = "\(self.trackList[0]["title"]!)"
        if (backgroundSound["title"] != nil) {
            self.lblBackgroundMusicName.text = "\(backgroundSound["title"]!)"
        }
        for i in 0...trackList.count - 1 {
            trackQueueArray.append("\(trackList[i]["audio_url"]!)")
        }
        self.lblTotalTrack.text = "\(self.currentlyPlayingTrackCnt+1)/\(self.trackList.count)"
        
        btnChange.layer.cornerRadius = btnChange.frame.size.height / 2
        btnChange.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnChange.layer.borderWidth = 1
        
        optionsView.layer.cornerRadius = 5.0
        optionsView.layer.masksToBounds = true
        
        innerComplitionView.layer.cornerRadius = 5.0
        innerComplitionView.layer.masksToBounds = true
        innerComplitionView.layer.borderColor = UIColor().lightAlphaYellowColor.cgColor
        innerComplitionView.layer.borderWidth = 1
        
        btnMembershipOptions.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnMembershipOptions.layer.borderWidth = 1.0
        btnMembershipOptions.layer.cornerRadius = btnMembershipOptions.frame.size.height / 2
        btnMembershipOptions.layer.masksToBounds = true
        self.sliderTrack.minimumValue = 0.0
        
        self.isLoading = true
        self.setPlayers()
        NotificationCenter.default.addObserver(self, selector: #selector(PlayMeditationViewController.itemDidFinishPlaying(notification:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Play Meditation")
    }

    func update() {
        self.stopAnimating()
    }

    override func viewWillDisappear(_ animated: Bool) {
        timerForProgress.invalidate()
    }
    
    //MARK:- Set Players -
    
    func setPlayers(){
        
        complitionView.isHidden = true

        self.lblCurrentTimeOfTrack.text = "00:00"
        self.lblDurationOfTrack.text = "Total: \(self.totalTrackTime)"

        NotificationCenter.default.addObserver(self, selector: #selector(PlayMeditationViewController.backgroundChange(sender: )), name: NSNotification.Name.init("changeBackgroundMusic"), object: nil)
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//        self.isLoading = true
        
//            let size = CGSize(width: 75, height: 75)
//            let color : UIColor = .white
//            self.startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
        
        //Set Meditataion Player
        var avPlayerItem : [AVPlayerItem] = []
        for index in 0..<self.trackQueueArray.count{
            let tmp = self.trackQueueArray[index]
            var url = URL.init(string: tmp)

            if(url == nil){
                url = URL(string:tmp.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)!
            }
            let it = AVPlayerItem.init(url: url!)
            avPlayerItem.append(it)
        }
        playerMeditaion = AVQueuePlayer.init(items: avPlayerItem)
        sliderVolume.value = bgVolume
        defaults.set(bgVolume, forKey: "bgVolume")
        
        
        //For background check
            let audioUrl = URL(string: "\(self.backgroundSound["audio_url"]!)")
            var foundInSavedBGTracks = false
            let path = appDelegate.myDownloadBGPath
            let url = NSURL(fileURLWithPath: path)
            let filePath = url.appendingPathComponent("\(audioUrl!.lastPathComponent)")?.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath!) {
                foundInSavedBGTracks = true
                backgroundPath = filePath!
            } else {
                foundInSavedBGTracks = false
            }
        
        if(foundInSavedBGTracks == true){
            self.isLoading = false
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            do {
                playerBackground = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: backgroundPath))
                playerBackground.delegate = self
                playerBackground.prepareToPlay()
            }
            catch{
                print(error)
            }
        }else{
            //Set Background Player
            DispatchQueue.global().async {
                if let theURL = URL(string: "\(self.backgroundSound["audio_url"]!)") {
                    do{
                        if let data = try? Data(contentsOf: theURL){
                            self.playerBackground = try AVAudioPlayer(data: data)
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            self.isLoading = false
                            if(self.pressPlayWhileLoading == true && self.backPress == false){
                                self.pressPlayWhileLoading = false
                                DispatchQueue.main.async {
                                    self.btnPlayMusic.isSelected = false
                                    self.btnPlayMusic_Clicked(self.btnPlayMusic)
                                }
                            }
                            self.playerBackground.delegate = self
                            if(self.btnMute.isSelected == true){
                                self.playerBackground.volume = 0
                            }
                        }
                    }catch{
                        
                    }
                }
            }
        }
    }
    
   //MARK:- close popup
    @IBAction func btnCloseComplitionView(_ sender: UIButton) {
        complitionView.isHidden = true
        let transition = CATransition()
        transition.duration = 1.0
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    //MARK:- Background Music -
    func backgroundChange (sender: Notification) {
        if let senderArray = sender.object as? [String : AnyObject] {
            backgroundSound = senderArray
            self.lblBackgroundMusicName.text = "\(backgroundSound["title"]!)"
            if self.isLoading == false{
                    playBackgroundMusic()
            }
        }
        if(appDelegate.reachable.isReachable){
            self.changeBackgroundAPI()
        }
    }
    
    func playBackgroundMusic(){
        let audioUrl = URL(string: "\(self.backgroundSound["audio_url"]!)")
        var foundInSavedBGTracks = false
        let path = appDelegate.myDownloadBGPath
        let url = NSURL(fileURLWithPath: path)
        let filePath = url.appendingPathComponent("\(audioUrl!.lastPathComponent)")?.path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath!) {
            foundInSavedBGTracks = true
            backgroundPath = filePath!
        } else {
            foundInSavedBGTracks = false
        }
        
        if(foundInSavedBGTracks == true){
            self.isLoading = false
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            do {
                playerBackground = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: backgroundPath))
                playerBackground.delegate = self
                if self.playerMeditaion.isPlaying{
                    playerBackground.volume = sliderVolume.value
                    self.playerBackground.play()
                }
            }
            catch{
                print(error)
            }
        }else{
            showToast(message: "Music will change soon")
            DispatchQueue.global().async {
                if let theURL = URL(string: "\(self.backgroundSound["audio_url"]!)") {
                    do{
                        if let data = try? Data(contentsOf: theURL){
                            self.playerBackground = try AVAudioPlayer(data: data)
                            self.playerBackground.volume = 1.0
                            self.playerBackground.delegate = self
                            if self.playerMeditaion.isPlaying{
                                self.playerBackground.volume = self.sliderVolume.value
                                self.playerBackground.play()
                            }
                            if(self.btnMute.isSelected == true){
                                self.playerBackground.volume = 0
                            }
                        }
                    }catch{
                        
                    }
                }
            }
        }
        

    }

    func changeBackgroundAPI(){
        if(meditationID != ""){
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            var params = Parameters()
            
            var trackID = ""
            var trackType = ""
            
            for index in 0...trackList.count - 1{
                if(index == 0){
                    trackID.append("\(trackList[index]["track_id"]!)")
                    trackType.append("\(trackList[index]["track_type"]!)")
                }else{
                    trackID.append(",\(trackList[index]["track_id"]!)")
                    trackType.append(",\(trackList[index]["track_type"]!)")
                }
            }
            if(backgroundSound.count != 0){
                trackID.append(",\(backgroundSound["track_id"]!)")
                trackType.append(",\(backgroundSound["track_type"]!)")
            }
            
            var meditationType = ""
            if(meditaionBy == "Made by Meditable"){
                meditationType = "O"
            }else{
                meditationType = "C"
            }
            params = ["auth_key": authKey, "custom_meditation_id" : "\(meditationID)", "track_ids": trackID, "track_types" : trackType, "title" : "\(lblTitle.text!)", "is_original" : "\(meditationType)"]
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kEditCustomList, method: .post, parameters: params).validate().responseJSON { response in
                self.stopAnimating()
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as! NSDictionary? {
                        if json.value(forKey: "status") as? Int == 1 {
                            let responseResult = response.result.value as! [String : Any]
                            
                            if(appDelegate.downloadedMeditations.count > 0){
                                for i in 0...appDelegate.downloadedMeditations.count - 1{
                                    if("\(appDelegate.downloadedMeditations[i]["meditation_id"]!)" == self.meditationID){
                                        self.removeFromBundle(index: i)
                                        appDelegate.downloadedMeditations.remove(at: i)
                                        break
                                    }
                                }
                                let archiveData = NSKeyedArchiver.archivedData(withRootObject: appDelegate.downloadedMeditations)
                                defaults.set(archiveData, forKey: kdownloadedMeditations)
                                defaults.synchronize()
                            }
                            if(self.fromActivity == true){
                                downloadAgain = false
                            }
                            self.lblMadeByYou.text = "\(MadeByYou)"
                            if(appDelegate.playFromLibrary == true && self.fromActivity == false){
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshLibraryList"), object: nil, userInfo: self.backgroundSound)
                            }else{
                                NotificationCenter.default.post(name: NSNotification.Name.init("refreshMeditaionList"), object: nil)
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshCustomMeditationPlayList"), object: nil, userInfo: self.backgroundSound)
                            }

                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                    
                    
                case .failure(let error):
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
    }
    
    func removeFromBundle(index : Int){
        let trackArray : [[String : AnyObject]] = appDelegate.downloadedMeditations[index]["track"] as! [[String : AnyObject]]
        for i in 0...trackArray.count - 1{
            let trackTitle = "\(trackArray[i]["title"]!)-\(appDelegate.downloadedMeditations[index]["meditation_id"]!)\(trackArray[i]["track_type"]!)\(trackArray[i]["track_id"]!).mp3"
            self.removeMeditation(trackTitle: trackTitle)
        }
    }
    func removeMeditation(trackTitle : String) {
        let fileManager = FileManager.default
        if !FileManager.default.fileExists(atPath: appDelegate.myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: appDelegate.myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        let path =  appDelegate.myDownloadPath + "/" + "\(trackTitle)"
        do {
            try fileManager.removeItem(atPath: path)
        } catch let error as NSError {
            print(error.debugDescription)
        }
    }

    
    //MARK:- Play Meditation -

    @IBAction func btnPlayMusic_Clicked(_ sender: UIButton) {
        
        if self.isLoading == true{
            pressPlayWhileLoading = true
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please wait, your meditation will play after finish loading")
        } else {
            playerMeditaion.rate = 0
            playerMeditaion.pause()
            
            if self.btnPlayMusic.isSelected == true{
                self.btnPlayMusic.isSelected = false
            }else{
                self.btnPlayMusic.isSelected = true
            }
            
            do {
                if #available(iOS 10.0, *) {
                    playerMeditaion.automaticallyWaitsToMinimizeStalling = false
                } else {
                    
                }
                if(sender.isSelected == true){
                    //For Play/Pause in Notification Center
                    
                    MPRemoteCommandCenter.shared().playCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
                        self.playerMeditaion.play()
                        self.playerBackground.play()
                        return MPRemoteCommandHandlerStatus.success
                    }
                    MPRemoteCommandCenter.shared().pauseCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
                        self.playerMeditaion.pause()
                        if(self.playerBackground.isPlaying == true || self.playerBackground.rate == 0){
                            self.playerBackground.pause()
                        }
                        return MPRemoteCommandHandlerStatus.success
                    }
                    if(appDelegate.reachable.isReachable){
                        if(backPress == false){
                            self.playerMeditaion.play()
                        }
                        self.addBackgroundTrackLog()
                        
                        if(backgroundSound["audio_url"] != nil){
                            self.addBackgroundTrackLog()
                            playerBackground.volume = sliderVolume.value
                            playerBackground.play()
                        }
                    }
                    
                }else{
                    playerMeditaion.pause()
                }
                //            var cntPlayerItem : Int = 0
                playerMeditaion.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1.0, Int32(NSEC_PER_SEC)), queue: nil) { time in
                    if(self.addObserver == false && self.backPress == false){
                        self.addObserver = true
                        self.playerMeditaion.currentItem!.addObserver(self, forKeyPath: "playbackBufferEmpty", options: .new, context: nil)
                        self.playerMeditaion.currentItem!.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options: .new, context: nil)
                    }
                    
                    if let currentItem = self.playerMeditaion.currentItem {
                        let playhead = currentItem.currentTime().seconds + Double.init(self.seconds)
                        let duration = currentItem.duration.seconds
                        if !(duration.isNaN || duration.isInfinite){
                            
                            self.lblCurrentTimeOfTrack.text = String(self.formatTimeFor(seconds: playhead))
                            
                            self.sliderTrack.maximumValue = Float(duration)
                            self.sliderTrack.value = Float(CMTimeGetSeconds(time))
                            
                            if(playhead > Double.init(self.totalSeconds)/2.0 && self.sendMeditationTrackLog == false){
                                self.sendMeditationTrackLog = true
                                self.addTrackLogForMeditation()
                            }
                            if(self.currentlyPlayingTrackCnt == self.trackList.count - 1 && self.audioFadeOut == false){
                                if(playhead > Double.init(self.totalSeconds) - 15){
                                    print("****** fade out *****")
                                    self.audioFadeOut = true
                                    self.playerBackground.fadeOutNew(vol: 0, sliderVolume: self.playerBackground.volume)
                                }
                            }
                        }else {
                            
                        }
                    }
                }
            }
        }
    }
    
    func MPPlayInformationNow(){
        let currentItem = self.playerMeditaion.currentItem
        let playhead = currentItem!.currentTime().seconds
        let duration = currentItem!.duration.seconds
        var albumInfo = [AnyHashable: Any]()
        albumInfo[MPMediaItemPropertyPlaybackDuration] = duration
        albumInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = playhead
        MPNowPlayingInfoCenter.default().nowPlayingInfo = (albumInfo as! [String : Any])
        MPRemoteCommandCenter.shared().seekBackwardCommand.isEnabled = false
        MPRemoteCommandCenter.shared().seekForwardCommand.isEnabled = false
        MPRemoteCommandCenter.shared().skipForwardCommand.isEnabled = false
        MPRemoteCommandCenter.shared().skipBackwardCommand.isEnabled = false
        MPRemoteCommandCenter.shared().previousTrackCommand.isEnabled = false
        MPRemoteCommandCenter.shared().nextTrackCommand.isEnabled = false
    }
    
    //MARK:- Finish Playing -

    func itemDidFinishPlaying(notification:NSNotification) {
        print(notification.object!)
        print("self.sliderTrack.value - ***************\(self.sliderTrack.value)")
        if(self.sliderTrack.value != 0){
            print("************************Play")
            if(self.currentlyPlayingTrackCnt != self.trackList.count - 1){
                if(addObserver == true){
                    addObserver = false
                    self.playerMeditaion.currentItem?.removeObserver(self, forKeyPath: "playbackBufferEmpty")
                    self.playerMeditaion.currentItem?.removeObserver(self, forKeyPath: "playbackLikelyToKeepUp")
                }
                seconds = getSeconds(totalTime: "\(lblCurrentTimeOfTrack.text!)")
                self.currentlyPlayingTrackCnt = self.currentlyPlayingTrackCnt + 1
                self.lblTrackName.text = "\(self.trackList[self.currentlyPlayingTrackCnt]["title"]!)"
                self.addTrackLog(index: self.currentlyPlayingTrackCnt)
                self.lblTotalTrack.text = "\(self.currentlyPlayingTrackCnt+1)/\(self.trackList.count)"
            }else if(self.currentlyPlayingTrackCnt == self.trackList.count - 1){
                if(addObserver == true){
                    addObserver = false
                    self.playerMeditaion.currentItem!.removeObserver(self, forKeyPath: "playbackBufferEmpty")
                    self.playerMeditaion.currentItem!.removeObserver(self, forKeyPath: "playbackLikelyToKeepUp")
                }
                playerMeditaion.pause()
                playerMeditaion.rate = 0
                if(self.playerBackground.isPlaying == true || self.playerBackground.rate == 0){
                    playerBackground.pause()
                    playerBackground.rate = 0
                }
                backPress = true
                
                if(appDelegate.reachable.isReachable){
                    let currentSeconds = getTotalMeditationTime(trackTime: "\(lblCurrentTimeOfTrack.text!)")
                    self.sendMyTotalMeditaionTime(totalTime: currentSeconds + total_meditation_time)
                }
                
                let transition = CATransition()
                transition.duration = 1.0
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transition.type = kCATransitionFade
                complitionView.layer.add(transition, forKey: nil)
                complitionView.isHidden = false
            }
        }else{
            print("************************Play Skip")
            
            if(self.addObserver == true){
                self.addObserver = false
                self.playerMeditaion.currentItem!.removeObserver(self, forKeyPath: "playbackBufferEmpty")
                self.playerMeditaion.currentItem!.removeObserver(self, forKeyPath: "playbackLikelyToKeepUp")
            }
            self.playerMeditaion.pause()
            self.playerMeditaion.rate = 0
            if self.isLoading == false{
                if self.playerBackground.isPlaying{
                    self.playerBackground.pause()
                    self.playerBackground.rate = 0
                }
            }
            self.backPress = true

            let alertController = UIAlertController(title: AppName, message: errorWhileLoading, preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
            { action -> Void in
                let transition = CATransition()
                transition.duration = 2.0
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transition.type = kCATransitionFade
                self.navigationController?.view.layer.add(transition, forKey: nil)
                _ = self.navigationController?.popViewController(animated: false)
            })
            
//            alertController.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default)
//            { action -> Void in
//                DispatchQueue.main.async {
//                    self.btnPlayMusic.isSelected = true
//                }
//                self.setPlayers()
//            })
            
            self.present(alertController, animated: true, completion: nil)
        }


//            let transition = CATransition()
//            transition.duration = 2.0
//            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//            transition.type = kCATransitionFade
//            self.navigationController?.view.layer.add(transition, forKey: nil)
//            _ = self.navigationController?.popViewController(animated: false)
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        playerBackground.play()
    }
  
    //MARK:- Calculating Slider Value -

    func getHoursMinutesSecondsFrom(seconds: Double) -> (hours: Int, minutes: Int, seconds: Int) {
        let secs = Int(seconds)
        let hours = secs / 3600
        let minutes = (secs % 3600) / 60
        let seconds = (secs % 3600) % 60
        return (hours, minutes, seconds)
    }
    
    func formatTimeFor(seconds: Double) -> String {
        let result = getHoursMinutesSecondsFrom(seconds: seconds)
        let hoursString = "\(result.hours)"
        var minutesString = "\(result.minutes)"
        if minutesString.characters.count == 1 {
            minutesString = "0\(result.minutes)"
        }
        var secondsString = "\(result.seconds)"
        if secondsString.characters.count == 1 {
            secondsString = "0\(result.seconds)"
        }
        var time = "\(hoursString):"
        if result.hours >= 1 {
            time.append("\(minutesString):\(secondsString)")
        }
        else {
            time = "\(minutesString):\(secondsString)"
        }
        return time
    }
    
    
    
    //MARK:- Add Track Logs - 
    func addTrackLog(index : Int){
        var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
        if authKey == nil {
            authKey = ""
        }
        var meditationType = ""
        if(meditaionBy == "Made by Meditable"){
            meditationType = "O"
        }else{
            meditationType = "C"
        }
        let trackType = "\(self.trackList[index]["track_type"]!)"
        let param = ["auth_key": authKey, "track_id" : "\(self.trackList[index]["track_id"]!)", "track_type" : trackType, "meditation_type" : meditationType]
        APIManager().AddTrackLogs(parameters: param as! [String : String]) { (response, error) in
            if(error == nil){
//                print(response!)
            }else{
                if(error?.code == 5 || error?.code == 3){
                    let alertController = UIAlertController(title: AppName, message: "Unauthorized", preferredStyle:UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                    { action -> Void in
                        let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        defaults.set(deviceToken,forKey: "DeviceToken")
                        defaults.synchronize()
                        let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                        self.navigationController?.pushViewController(welcomeViewController, animated: true)
                    })
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }

    func addBackgroundTrackLog(){
        var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
        if authKey == nil {
            authKey = ""
        }
        var meditationType = ""
        if(meditaionBy == "Made by meditable"){
            meditationType = "O"
        }else{
            meditationType = "C"
        }
        let param = ["auth_key": authKey, "track_id" : "\(backgroundSound["track_id"]!)", "track_type" : "B", "meditation_type" : meditationType]
        APIManager().AddTrackLogs(parameters: param as! [String : String]) { (response, error) in
            if(error == nil){
//                print(response!)
            }else{
                if(error?.code == 5 || error?.code == 3){
                    let alertController = UIAlertController(title: AppName, message: "Unauthorized", preferredStyle:UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                    { action -> Void in
                        let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        defaults.set(deviceToken,forKey: "DeviceToken")
                        defaults.synchronize()
                        let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                        self.navigationController?.pushViewController(welcomeViewController, animated: true)
                    })
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func addTrackLogForMeditation(){
        if(meditationID != "" && appDelegate.reachable.isReachable){
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            var meditationType = ""
            var isCustom = ""
            if(meditaionBy == "Made by Meditable"){
                meditationType = "O"
                isCustom = "M"
            }else{
                meditationType = "C"
                isCustom = "CM"
            }
            let param = ["auth_key": authKey, "track_id" : "\(meditationID)", "track_type" : "\(isCustom)", "meditation_type" : "\(meditationType)"]
            APIManager().AddTrackLogs(parameters: param as! [String : String]) { (response, error) in
                if(error == nil){
//                    print(response!)
                }else{
                    if(error?.code == 5 || error?.code == 3){
                        let alertController = UIAlertController(title: AppName, message: "Unauthorized", preferredStyle:UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                        { action -> Void in
                            let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                            defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                            defaults.set(deviceToken,forKey: "DeviceToken")
                            defaults.synchronize()
                            let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                            self.navigationController?.pushViewController(welcomeViewController, animated: true)
                        })
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
        }
    }


    
    //Get TotalTrack Time
    
    func myTotalMeditationTime(){
        total_meditation_time = getTotalMeditationTime(trackTime: "\(appDelegate.TotalMeditationTime)")
    }
    
    func sendMyTotalMeditaionTime(totalTime : Int){
       let time = setTotalMeditationTime(totalseconds: totalTime)
        appDelegate.TotalMeditationTime = time
        var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
        if authKey == nil {
            authKey = ""
        }
        let param = ["auth_key": authKey, "total_meditation_time" : "\(time)"]
        APIManager().AddTrackLogs(parameters: param as! [String : String]) { (response, error) in
            if(error == nil){
//                print(response!)
            }else{
                if(error?.code == 5 || error?.code == 3){
                    let alertController = UIAlertController(title: AppName, message: "Unauthorized", preferredStyle:UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                    { action -> Void in
                        let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        defaults.set(deviceToken,forKey: "DeviceToken")
                        defaults.synchronize()
                        let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                        self.navigationController?.pushViewController(welcomeViewController, animated: true)
                    })
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK:- Slider Value Change -

    @IBAction func sliderVolumerChanged(_ sender: UISlider) {
        bgVolume = sender.value
        defaults.set(bgVolume, forKey: "bgVolume")
        if self.isLoading == false{
            if playerBackground.isPlaying{
                playerBackground.volume = sender.value
            }
        }
        if(btnMute.isSelected == true){
            btnMute.isSelected = false
        }
    }
    
    @IBAction func sliderAudio(_ sender: UISlider) {
        if(appDelegate.reachable.isReachable){
            let timeScale: Int32 = playerMeditaion.currentItem!.asset.duration.timescale
            let time: CMTime = CMTimeMakeWithSeconds(Float64(sender.value), timeScale)
            playerMeditaion.seek(to: time)
        }
    }
    
    //MARK:- Button Events -
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        let currentSeconds = getTotalMeditationTime(trackTime: "\(lblCurrentTimeOfTrack.text!)")
        if(appDelegate.reachable.isReachable){
            print(total_meditation_time)
            self.sendMyTotalMeditaionTime(totalTime: currentSeconds + total_meditation_time)
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        var downloading : Bool = false
//        if(UserDefaults.standard.value(forKey: "startDownloading") != nil){
//            downloading = UserDefaults.standard.value(forKey: "startDownloading") as! Bool
//            if(downloading == false){
//                
//            }else{
//                UIAlertController().alertViewWithTitleAndMessage(self, message: "Meditation will play automatically after finish downloading.")
//                return
//            }
//        }
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        backPress = true
        if(addObserver == true){
            addObserver = false
            self.playerMeditaion.currentItem!.removeObserver(self, forKeyPath: "playbackBufferEmpty")
            self.playerMeditaion.currentItem!.removeObserver(self, forKeyPath: "playbackLikelyToKeepUp")
        }
        playerMeditaion = AVQueuePlayer()
        self.playerMeditaion.pause()
        self.playerMeditaion.rate = 0
        if self.isLoading == false {
            if(self.playerBackground.isPlaying == true || self.playerBackground.rate == 0){
                self.playerBackground.pause()
                self.playerBackground.rate = 0
            }
        }
        self.isLoading = true
        
        self.navigationController!.popViewController(animated: false)
    }
    
    @IBAction func btnChange_Clicked(_ sender: Any) {
        let backgroundMusicViewController: BackgroundMusicViewController = self.storyboard?.instantiateViewController(withIdentifier: "BackgroundMusicViewController") as! BackgroundMusicViewController
        backgroundMusicViewController.strController = "background"
        self.navigationController?.pushViewController(backgroundMusicViewController, animated: true)
    }

    
    @IBAction func btnMute(_ sender: UIButton) {
        if(btnMute.isSelected == false){
            btnMute.isSelected = true
            if playerBackground.isPlaying {
                playerBackground.volume = 0
            }
            sliderVolume.value = 0
            defaults.set(sliderVolume.value, forKey: "bgVolume")
            
        }else{
            btnMute.isSelected = false
            if playerBackground.isPlaying {
                playerBackground.volume = bgVolume
                sliderVolume.value = bgVolume
                defaults.set(bgVolume, forKey: "bgVolume")
            }
        }
    }
    
    @IBAction func btnVolumUp(_ sender: UIButton) {
        if self.isLoading == false{
            if playerBackground.isPlaying {
                sliderVolume.value = sliderVolume.maximumValue
                playerBackground.volume = 1
                bgVolume = 1.0
                defaults.set(bgVolume, forKey: "bgVolume")
                btnMute.isSelected = false
            }
        }
    }
    
    @IBAction func btnMembershipOptions_Clicked(_ sender: Any) {
        let subscriptionPlanViewController: SubscriptionPlanViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionPlanViewController") as! SubscriptionPlanViewController
        self.navigationController?.pushViewController(subscriptionPlanViewController, animated: true)
    }
    
    @IBAction func btnClose1_Clicked(_ sender: Any) {
        backgroundView.isHidden = true
        scrollView.isHidden = true
    }
    
    //MARK:- Observer -
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard keyPath != nil else { // a safety precaution
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        switch keyPath! {
        case "playbackBufferEmpty" :
            if playerMeditaion.currentItem!.isPlaybackBufferEmpty {
                // do something here to inform the user that the file is buffering
            }
            
        case "playbackLikelyToKeepUp" :
            if playerMeditaion.currentItem!.isPlaybackLikelyToKeepUp {
                // remove the buffering inidcator if you added it
                if(backPress == false){
                    self.playerMeditaion.play()
                }
            }
        default :
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}



extension MPVolumeView {
    var volumeSlider:UISlider { // hacking for changing volume by programing
        var slider = UISlider()
        for subview in self.subviews {
            if subview is UISlider {
                slider = subview as! UISlider
                slider.isContinuous = false
                (subview as! UISlider).value = AVAudioSession.sharedInstance().outputVolume
                return slider
            }
        }
        return slider
    }
}
