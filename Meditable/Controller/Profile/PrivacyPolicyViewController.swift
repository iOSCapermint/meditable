//
//  PrivacyPolicyViewController.swift
//  Meditable
//
//  Created by Apple on 25/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import TTTAttributedLabel
import NVActivityIndicatorView

class PrivacyPolicyViewController: UIViewController, UIWebViewDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblPrivacyPolicy: UILabel!
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblDetails: TTTAttributedLabel!
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let url = URL(string: "http://getmeditable.com/meditable/privacy-policy.html") {
            let request = URLRequest(url: url)
            self.webView.loadRequest(request)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Privacy policy")
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        let size = CGSize(width: 75, height: 75)
        let color : UIColor = .white
        startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.stopAnimating()
    }
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        //_ = self.navigationController?.popViewController(animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
