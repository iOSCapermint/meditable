//
//  TermsAndConditionsViewController.swift
//  Meditable
//
//  Created by Apple on 25/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import TTTAttributedLabel
import NVActivityIndicatorView

class TermsAndConditionsViewController: UIViewController, UIWebViewDelegate, NVActivityIndicatorViewable {
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblTermsAndConditions: UILabel!
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblDetails: TTTAttributedLabel!
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = URL(string: "http://getmeditable.com/meditable/terms.html") {
            let request = URLRequest(url: url)
            self.webView.loadRequest(request)
            
        }
        //        lblDetails.textInsets = UIEdgeInsets.init(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
        //        lblDetails.text = "Lorem ipsum dolor summit dummy data display on the screen Lorem ipsum dolor summit dummy data display on the screen Lorem ipsum dolor summit dummy data display on the screen Lorem ipsum dolor summit dummy data display on the screen Lorem ipsum dolor summit dummy data display on the screen Lorem ipsum dolor summit dummy data display on the screen Lorem ipsum dolor summit dummy data display on the screen Lorem ipsum dolor summit dummy data display on the screen Lorem ipsum dolor summit dummy data display on the screen Lorem ipsum dolor summit dummy data display on the screen Lorem ipsum dolor summit dummy data display on the screen Lorem ipsum dolor summit dummy data display on the screen Lorem ipsum dolor summit dummy data display on the screen"
        //        lblDetails.layer.cornerRadius = 5
        //        lblDetails.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Terms and Condition")
    }

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        let size = CGSize(width: 75, height: 75)
        let color : UIColor = .white
        startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.stopAnimating()
    }
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        //_ = self.navigationController?.popViewController(animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
