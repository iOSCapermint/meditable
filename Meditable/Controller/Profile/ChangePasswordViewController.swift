//
//  ChangePasswordViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 28/04/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class ChangePasswordViewController: UIViewController, UITextFieldDelegate, NVActivityIndicatorViewable {
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblChangePassword: UILabel!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var txtCurrentPassword: CustomTextfield!
    
    @IBOutlet weak var txtNewPassword: CustomTextfield!
    
    @IBOutlet weak var txtConfirmNewPassword: CustomTextfield!
    
    @IBOutlet weak var btnUpdate: UIButton!
    
    @IBOutlet weak var btnUpdateTopConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtCurrentPassword.layer.cornerRadius = txtCurrentPassword.frame.size.height / 2
        txtNewPassword.layer.cornerRadius = txtNewPassword.frame.size.height / 2
        txtConfirmNewPassword.layer.cornerRadius = txtConfirmNewPassword.frame.size.height / 2

        btnUpdate.layer.cornerRadius = btnUpdate.frame.size.height / 2
        btnUpdate.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnUpdate.layer.borderWidth = 1.0
        
        if IS_IPHONE5 {
            btnUpdateTopConstraint.constant = 204
        }
        else if IS_IPHONE6P {
            btnUpdateTopConstraint.constant = 372
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Change Password")
    }

    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        //_ = self.navigationController!.popViewController(animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnUpdate_Clicked(_ sender: Any) {
        self.view.endEditing(true)
        
        var oldPassword = ""
        
        if defaults.value(forKey: "Password") as? String != nil {
            oldPassword = defaults.value(forKey: "Password") as! String
        }
        else {
            oldPassword = ""
        }
        
        if txtCurrentPassword.text?.characters.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please enter current password")
        }
        else if txtNewPassword.text?.characters.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please enter new password")
        }
        else if txtConfirmNewPassword.text?.characters.count == 0 {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please enter confirm new password")
        }
        else if !(txtCurrentPassword.text?.isEmpty)! && txtCurrentPassword.text != oldPassword {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Please enter correct current password")
        }
        else if !(txtNewPassword.text?.isEmpty)! && !(txtConfirmNewPassword.text?.isEmpty)! && txtNewPassword.text != txtConfirmNewPassword.text {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Password and Confirm Password doesn't match")
        }
        else {
            if appDelegate.reachable.isReachable{
                var authKey = ""
                if defaults.value(forKey: "AuthKey") as? String != nil {
                    authKey = defaults.value(forKey: "AuthKey") as! String
                }
                
                let params: Parameters = ["auth_key": authKey, "old_password": txtCurrentPassword.text!, "new_password": txtNewPassword.text!]
                
                let size = CGSize(width: 50, height: 50)
                let color : UIColor = .white
                startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
                
                Alamofire.request(baseURLWithAuth + kChangePwd , method: .post, parameters: params).validate().responseJSON { response in
                    self.stopAnimating()
                    switch response.result {
                    case .success:
                        if let json: NSDictionary = response.result.value as! NSDictionary? {
                            if json.value(forKey: "status") as? Int == 1 {
                                
                                let alertSuccess: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: json.value(forKey: "message") as? String, preferredStyle: .alert)
                                
                                let btnOk: UIAlertAction = UIAlertAction.init(title: "Ok", style: .default, handler: {
                                    _ in
                                    defaults.set("\(self.txtNewPassword.text! )", forKey: "Password")
                                    defaults.synchronize()
                                    _ = self.navigationController?.popViewController(animated: true)
                                })
                                
                                alertSuccess.addAction(btnOk)
                                self.present(alertSuccess, animated: true, completion: nil)
                            }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                                
                                let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                                alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                                { action -> Void in
                                    let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                    
                                    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                    defaults.set(deviceToken,forKey: "DeviceToken")
                                    defaults.synchronize()
                                    
                                    let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                    self.navigationController?.pushViewController(welcomeViewController, animated: true)
                                })
                                self.present(alertController, animated: true, completion: nil)
                                
                            }else {
                                UIAlertController().alertViewWithTitleAndMessage(self, message: json.value(forKey: "message") as! String)
                            }
                        }
                    case .failure(let error):
                        print (error)
                        UIAlertController().alertViewWithErrorMessage(self)
                    }
                }
            }
            else {
                UIAlertController().alertViewWithNoInternet(self)
            }
        }
    }
    
    //Text Field Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtCurrentPassword.isFirstResponder {
            txtNewPassword.becomeFirstResponder()
        }
        else if txtNewPassword.isFirstResponder {
            txtConfirmNewPassword.becomeFirstResponder()
        }
        else if txtConfirmNewPassword.isFirstResponder {
            txtConfirmNewPassword.resignFirstResponder()
        }
        
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
