//
//  ManageBackgroundTracksViewController.swift
//  Meditable
//
//  Created by capermintmini6 on 17/07/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import AVFoundation
import UICircularProgressRing

class ManageBackgroundTracksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable, URLSessionDownloadDelegate, AVAudioPlayerDelegate {

    @IBOutlet weak var tblManageBgTracks: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    var bgTrackList = [[String : AnyObject]]()
    lazy var session : URLSession = {
        let config = URLSessionConfiguration.ephemeral
        config.allowsCellularAccess = true
        let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
        return session
    }()
    var task : URLSessionTask!
    var playerBackground = AVAudioPlayer()
    var currentPlayIndex = 5000
    var isplay = false
    var isloading = false
    var isDownloading = false
    var downloadingIndex = 5000
    var isDownloadingPause = ""
    var refresher: UIRefreshControl!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tblManageBgTracks.tableFooterView = UIView(frame: .zero)
        tblManageBgTracks.rowHeight = UITableViewAutomaticDimension
        tblManageBgTracks.estimatedRowHeight = 60
        
        lblNoDataFound.isHidden = true
//        NotificationCenter.default.addObserver(self, selector: #selector(ManageBackgroundTracksViewController.refreshList), name: NSNotification.Name.init("refreshBackgroundTrackList"), object: nil)

        //RefreshControl
        refresher = UIRefreshControl()
        tblManageBgTracks!.alwaysBounceVertical = true
        refresher.tintColor = .white
        refresher.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tblManageBgTracks!.addSubview(refresher)

        self.getBackgroundTracks()
      }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        addGoogleAnalytics(screenName: "Manage Background Tracks")
    }


    //MARK:- tableview methods -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bgTrackList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "managebgTrackCell"
        
        let cell: managebgTrackCell! = tblManageBgTracks.dequeueReusableCell(withIdentifier: cellIdentifier) as! managebgTrackCell!
        cell.lblTitle.text = "\(bgTrackList[indexPath.row]["title"]!)"
        cell.btnDelete.addTarget(self, action: #selector(ManageBackgroundTracksViewController.btnDelete(sender:)), for: .touchUpInside)
        cell.btnMusic.addTarget(self, action: #selector(ManageBackgroundTracksViewController.btnMusic(sender:)), for: .touchUpInside)
        cell.btnDelete.tag = indexPath.row
        cell.btnMusic.tag = indexPath.row

        var same = false
        if(appDelegate.savedBackgroundTracks.count != 0){
            for i in 0...appDelegate.savedBackgroundTracks.count - 1{
                if("\(appDelegate.savedBackgroundTracks[i])" == "\(bgTrackList[indexPath.row]["title"]!)"){
                    same = true
                    break
                }
            }
        }
        if(same == true){
            cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadedCloud"), for: UIControlState.normal)
        }else{
            cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadCloud"), for: UIControlState.normal)
        }

        

        if(currentPlayIndex == indexPath.row){
            cell.btnMusic.isSelected = true
        }else{
            cell.btnMusic.isSelected = false
        }
        
        cell.progressView.shouldShowValueText = false
        if(downloadingIndex == indexPath.row){
            cell.progressView.isHidden = false
            cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_control_stop"), for: UIControlState.normal)
        }else{
            cell.progressView.isHidden = true
        }
        cell.backgroundColor = .clear
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    }

    //MARK:-  Refresh -
    func refreshList() {
        if(self.isDownloading == true){
            self.task.cancel()
        }
        if(self.isplay == true){
            self.playerBackground.stop()
            self.isplay = false
        }
        currentPlayIndex = 5000
        isplay = false
        isloading = false
        isDownloading = false
        downloadingIndex = 5000
        isDownloadingPause = ""
        self.refresher.endRefreshing()
        getBackgroundTracks()
    }
    
    //MARK: Button Events
    
    @IBAction func btnBack(_ sender: UIButton) {
        if(isDownloading == true){
            let alertController = UIAlertController(title: AppName, message: QuitBackgroundDownloadMessage , preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
            { action -> Void in
                if(self.isplay == true){
                    self.playerBackground.stop()
                    self.isplay = false
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.navigationController?.popViewController(animated: true)
                self.isDownloading = false
            })
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
            { action -> Void in
                
            })
            self.present(alertController, animated: true, completion: nil)
        }else{
            if(self.isplay == true){
                self.playerBackground.stop()
                self.isplay = false
            }
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnMusic(sender: UIButton) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        var same = false
        var bgName = ""
        var backgroundPath = ""
        if(appDelegate.savedBackgroundTracks.count != 0){
            for i in 0...appDelegate.savedBackgroundTracks.count - 1{
                if("\(appDelegate.savedBackgroundTracks[i])" == "\(bgTrackList[sender.tag]["title"]!)"){
                    same = true
                    let s = "\(bgTrackList[sender.tag]["audio_url"]!)"
                    let urlw = URL.init(string :s)!
                    bgName = urlw.lastPathComponent
                    
                    let bgpath = appDelegate.myDownloadBGPath
                    let url = NSURL(fileURLWithPath: bgpath)
                    let filePath = url.appendingPathComponent("\(bgName)")?.path
                    let fileManager = FileManager.default
                    if fileManager.fileExists(atPath: filePath!) {
                        backgroundPath = filePath!
                    } else {
                    }
                    break
                }
            }
        }
        
        if(same == true){
            if(sender.isSelected == false){
                isplay = true
                for indexPath in self.tblManageBgTracks.indexPathsForVisibleRows! {
                    if let cell : managebgTrackCell = tblManageBgTracks.cellForRow(at: IndexPath.init(item: indexPath.row, section: 0)) as? managebgTrackCell{
                        DispatchQueue.main.async {
                            cell.btnMusic.isSelected = false
                        }
                    }
                }
                do {
                    if(currentPlayIndex != sender.tag){
                        currentPlayIndex = sender.tag
                        playerBackground = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: backgroundPath))
                        playerBackground.delegate = self
                    }
                    playerBackground.play()
                }
                catch{
                    print(error)
                }
                DispatchQueue.main.async {
                    sender.isSelected = true
                }
            }else{
                isplay = false
                DispatchQueue.main.async {
                    sender.isSelected = false
                }
                playerBackground.pause()
            }
        }else{
            if(sender.isSelected == false){
                for indexPath in self.tblManageBgTracks.indexPathsForVisibleRows! {
                    if let cell : managebgTrackCell = tblManageBgTracks.cellForRow(at: IndexPath.init(item: indexPath.row, section: 0)) as? managebgTrackCell{
                        DispatchQueue.main.async {
                            cell.btnMusic.isSelected = false
                        }
                    }
                }
                if(currentPlayIndex != sender.tag){
                    currentPlayIndex = sender.tag
                    isloading = true
                    if(isplay == true){
                        playerBackground.stop()
                        self.isplay = false
                    }
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    DispatchQueue.global().async {
                        self.stopAnimating()
                        if let theURL = URL(string: "\(self.bgTrackList[sender.tag]["audio_url"]!)") {
                            do{
                                if let data = try? Data(contentsOf: theURL){
                                    if(self.isplay == false){
                                    self.playerBackground = try AVAudioPlayer(data: data)
                                    self.isloading = false
                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                    self.playerBackground.delegate = self
                                        self.playerBackground.play()
                                        self.isplay = true
                                    }
                                }
                            }catch{
                                
                            }
                        }
                    }
                }else{
                    self.isplay = true
                    self.playerBackground.play()
                }
                DispatchQueue.main.async {
                    sender.isSelected = true
                }
            }else{
                if(isloading != true){
                    self.isplay = false
                    DispatchQueue.main.async {
                        sender.isSelected = false
                    }
                    playerBackground.pause()
                }else{
                    UIAlertController().alertViewWithTitleAndMessage(self, message: "Please wait while meditation finishes loading")
                }
            }
            
        }
    }
    
    @IBAction func btnDelete(sender: UIButton) {
        if isplay == true{
            for indexPath in self.tblManageBgTracks.indexPathsForVisibleRows! {
                if let cell : managebgTrackCell = tblManageBgTracks.cellForRow(at: IndexPath.init(item: indexPath.row, section: 0)) as? managebgTrackCell{
                    DispatchQueue.main.async {
                        cell.btnMusic.isSelected = false
                    }
                }
            }
            playerBackground.pause()
        }
        let cell : managebgTrackCell = tblManageBgTracks.cellForRow(at: IndexPath.init(item: sender.tag, section: 0)) as! managebgTrackCell
        var same = false
        var foundedIndex = 5000
        if(appDelegate.savedBackgroundTracks.count != 0){
            for i in 0...appDelegate.savedBackgroundTracks.count - 1{
                if("\(appDelegate.savedBackgroundTracks[i])" == "\(bgTrackList[sender.tag]["title"]!)"){
                    same = true
                    foundedIndex = i
                    break
                }
            }
        }
        if(same == true && isDownloadingPause == ""){
            let alertController = UIAlertController(title: AppName, message: DeleteBackgroundTrack, preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
            { action -> Void in
                self.removeFromBundle(index: sender.tag)
                appDelegate.savedBackgroundTracks.remove(at: foundedIndex)
                self.refreshList()
                cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadCloud"), for: UIControlState.normal)
            })
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
            { action -> Void in
                
            })
            self.present(alertController, animated: true, completion: nil)
            
        }else{
            if(isDownloadingPause == "start" && self.downloadingIndex == sender.tag){
                self.task.suspend()
                cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_play"), for: UIControlState.normal)
                isDownloadingPause = "pause"
            }else if(isDownloadingPause == "pause" && self.downloadingIndex == sender.tag){
                self.task.resume()
                cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_control_stop"), for: UIControlState.normal)
                isDownloadingPause = "start"
            }else if(isDownloading == false){
                isDownloading = true
                downloadingIndex = sender.tag
                cell.progressView.isHidden = false
                isDownloadingPause = "start"
                cell.btnDownload.setImage(#imageLiteral(resourceName: "ic_control_stop"), for: UIControlState.normal)
                let s = "\(bgTrackList[sender.tag]["audio_url"]!)"
                let urlw = URL.init(string :s)!
                let req = URLRequest.init(url: urlw)
                let task = self.session.downloadTask(with: req)
                self.task = task
                self.task.taskDescription = "\(bgTrackList[sender.tag]["title"]!)/\(urlw.lastPathComponent)"
                task.resume()
            }else{
                UIAlertController().alertViewWithTitleAndMessage(self, message: "Please wait, Previous background track download is in progress")
            }
        }
    }
    
    func removeFromBundle(index : Int){
        let audioStr  = "\(bgTrackList[index]["audio_url"]!)"
        let audioLastComponent = (audioStr as NSString).lastPathComponent
        self.removeMeditation(trackTitle: "\(audioLastComponent)")
    }
    func removeMeditation(trackTitle : String) {
        let fileManager = FileManager.default
        if !FileManager.default.fileExists(atPath: appDelegate.myDownloadBGPath) {
            try! FileManager.default.createDirectory(atPath: appDelegate.myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        let path =  appDelegate.myDownloadBGPath + "/" + "\(trackTitle)"
        do {
            try fileManager.removeItem(atPath: path)
        } catch let error as NSError {
            print(error.debugDescription)
        }
    }

    //MARK:-  URLSesseionDownload Methods -
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        for indexPath in self.tblManageBgTracks.indexPathsForVisibleRows! {
            if (indexPath.row == self.downloadingIndex) {
                if let cell : managebgTrackCell = self.tblManageBgTracks.cellForRow(at: IndexPath.init(row: downloadingIndex, section: 0)) as? managebgTrackCell{
                    DispatchQueue.global().async {
                        cell.progressView.setProgress(value: CGFloat(progress * 100.0), animationDuration: 0)
                    }
                }
            }
        }
    }
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        downloadingIndex = 5000
        isDownloading = false
        isDownloadingPause = ""
        tblManageBgTracks.reloadData()
    }

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {

        isDownloading = false
        isDownloadingPause = ""

        let array = downloadTask.taskDescription!.components(separatedBy: "/")

        let destinationUrl = appDelegate.myDownloadBGPath
        let pathURL = NSURL(fileURLWithPath: destinationUrl)
        let filePath = pathURL.appendingPathComponent("\(array[1])")?.path
        if !FileManager.default.fileExists(atPath: appDelegate.myDownloadBGPath) {
            try! FileManager.default.createDirectory(atPath: appDelegate.myDownloadBGPath, withIntermediateDirectories: true, attributes: nil)
        }
        print("Finished downloading!")
        
        // Here you can move your downloaded file
        do {
            // after downloading your file you need to move it to your destination url
            try FileManager.default.moveItem(at: location, to: URL.init(fileURLWithPath: filePath!))
            appDelegate.savedBackgroundTracks.append("\(array[0])")
            print("File moved to documents folder")
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        for indexPath in self.tblManageBgTracks.indexPathsForVisibleRows! {
            if (indexPath.row == self.downloadingIndex) {
                let cell : managebgTrackCell = tblManageBgTracks.cellForRow(at: IndexPath.init(row: downloadingIndex, section: 0)) as! managebgTrackCell
                DispatchQueue.main.async {
                    cell.btnDownload.setImage(#imageLiteral(resourceName: "downloadedCloud"), for: UIControlState.normal)
                    cell.progressView.isHidden = true
                }
            }
            
        }
    }
    
    //MARK:- Background Tracks -
    func getBackgroundTracks(){
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kGetBackgroundTracks, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            self.bgTrackList = json.value(forKey: "data") as! [[String : AnyObject]]
                            
                            if self.bgTrackList.count != 0{
                                self.lblNoDataFound.isHidden = true
                                self.tblManageBgTracks.reloadData()
                            }else{
                                self.lblNoDataFound.isHidden = false
                            }
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                    }else {
//                        if let message = json["message"].string {
//                            UIAlertController().alertViewWithTitleAndMessage(self, message: message)
//                        }
                    }
                case .failure(let error):
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            lblNoDataFound.isHidden = false
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
class managebgTrackCell : UITableViewCell{
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnMusicLogo: UIButton!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var btnMusic: UIButton!
    @IBOutlet weak var progressView: UICircularProgressRingView!
}
