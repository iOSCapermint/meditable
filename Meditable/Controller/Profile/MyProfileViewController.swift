//
//  MyProfileViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 27/04/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON
import MessageUI

class MyProfileViewController: UIViewController, NVActivityIndicatorViewable, UITextFieldDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,MFMailComposeViewControllerDelegate {

    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var imgProfilePicture: UIImageView!
    
    @IBOutlet weak var btnEditProfileImage: UIButton!
    
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblUserEmail: UILabel!
    
    @IBOutlet weak var lblTrailDays: UILabel!
    
    @IBOutlet weak var btnSubscribeForMore: UIButton!
    
    @IBOutlet weak var imgArrow1: UIImageView!
    
    @IBOutlet weak var firstSeperatorView: UIView!
    
    @IBOutlet weak var btnAboutApproach: UIButton!
    
    @IBOutlet weak var imgArrow2: UIImageView!
    
    @IBOutlet weak var secondSeperatorView: UIView!
    
    @IBOutlet weak var btnChangePassword: UIButton!
    
    @IBOutlet weak var imgArrow3: UIImageView!
    
    @IBOutlet weak var thirdSeperatorView: UIView!
    
    @IBOutlet weak var btnHealthDisclaimer: UIButton!
    
    @IBOutlet weak var imgArrow4: UIImageView!
    
    @IBOutlet weak var forthSeperatorView: UIView!
    
    @IBOutlet weak var btnTermsAndConditions: UIButton!
    
    @IBOutlet weak var imgArrow5: UIImageView!
    
    @IBOutlet weak var fifthSeperatorView: UIView!
    
    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    
    @IBOutlet weak var imgArrow6: UIImageView!
    
    @IBOutlet weak var sixthSeperatorView: UIView!
    
    @IBOutlet weak var btnShareMeditable: UIButton!
    
    @IBOutlet weak var imgArrow7: UIImageView!

    @IBOutlet weak var seventhSeperatorView: UIView!
    
    @IBOutlet weak var btnLogout: UIButton!
    
    @IBOutlet weak var btnEditProfileImageTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var InnerViewSubscriptionDetail: UIView!
    @IBOutlet var SubscriptionView: UIView!
    @IBOutlet weak var lblRenewalDate: UILabel!
    @IBOutlet weak var lblRenealAmt: UILabel!
    
    @IBOutlet weak var btnManageBackgroundTracks: UIButton!
    
    @IBOutlet weak var btnSupport: UIButton!
    
    var isEditingMobileNumber = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if IS_IPHONE4s || IS_IPHONE5 {
            btnEditProfileImageTrailingConstraint.constant = 95
        }
        else if IS_IPHONE6 {
            btnEditProfileImageTrailingConstraint.constant = 125
        }
        else if IS_IPHONE6P {
            btnEditProfileImageTrailingConstraint.constant = 145
        }

        btnEditProfileImage.layer.cornerRadius = btnEditProfileImage.frame.size.height / 2
        btnEditProfileImage.layer.masksToBounds = true
        
        lblTrailDays.layer.cornerRadius = 5.0
        lblTrailDays.layer.masksToBounds = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(MyProfileViewController.tapSubscriptionDetail(sender:)))
        lblTrailDays.addGestureRecognizer(tap)
        lblTrailDays.isUserInteractionEnabled = true
        
        btnSubscribeForMore.layer.cornerRadius = 5.0
        btnSubscribeForMore.layer.borderColor = UIColor().yellowBorderColor.cgColor
        btnSubscribeForMore.layer.borderWidth = 1.0
        btnSubscribeForMore.layer.masksToBounds = true

        self.imgProfilePicture.layer.cornerRadius = self.imgProfilePicture.frame.size.height / 2
        self.imgProfilePicture.layer.borderColor = UIColor.clear.cgColor
        self.imgProfilePicture.layer.masksToBounds = true
        
        setProfileData()
       
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "My Profile")
        if(appDelegate.reachable.isReachable){
            if(appDelegate.membership_plan_id == "0" || appDelegate.membership_plan_id == "1" || appDelegate.membership_plan_id == "4"){
                btnSubscribeForMore.setTitle("Subscribe Now", for: .normal)
                if(appDelegate.membership_plan_id == "1"){
                    lblTrailDays.text = upgradeMsg
                }else{
                    lblTrailDays.text = "You have \(numberOfDaysRemaining) days left from your free trial"
                }
            }else{
                btnSubscribeForMore.setTitle("Change Plan", for: .normal)
                lblTrailDays.text = "You have \(numberOfDaysRemaining) days left till subscription renewal"
                //            btnSubscribeForMore.isEnabled = false
            }
        }else{
            btnSubscribeForMore.setTitle("Subscribe Now", for: .normal)
            lblTrailDays.text = "You have \(numberOfDaysRemaining) days left from your free trial"
        }
        
        SubscriptionView.frame = self.view.frame
        self.view.addSubview(SubscriptionView)
        SubscriptionView.isHidden = true
        SubscriptionView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        InnerViewSubscriptionDetail.layer.cornerRadius = 5.0
        InnerViewSubscriptionDetail.clipsToBounds = true
        SubscriptionView.isHidden = true
        var amt = "$0.00"
        if(appDelegate.membership_plan_id == "2"){
            amt = "$9.99"
        }else if(appDelegate.membership_plan_id == "3"){
            amt = "$77.99"
        }else if(appDelegate.membership_plan_id == "5"){
            amt = "$9.99"
        }else if(appDelegate.membership_plan_id == "6"){
            amt = "$77.99"
        }
        lblRenewalDate.text = amt
        lblRenewalDate.text = "Your subscription will expire on \(expiryDate) date."
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //Set Profile Data On Screen
    func setProfileData() {
        if let userName = defaults.value(forKey: "Name") as? String {
            lblUserName.text = userName
        }
        
        if let email = defaults.value(forKey: "Email") as? String {
            lblUserEmail.text = email
        }
        
        let profileImage: MLDownloadImageView = MLDownloadImageView.init()
        if let profileImageURL = defaults.value(forKey: "image_url") as? String {
            let imgURL = profileImageURL
            let placeHolder: UIImage = UIImage.init(named: "UserPlaceholder")!
            
            profileImage.downloadImage(with: NSURL.init(string: imgURL) as URL!, placeHolder: placeHolder, imageError: placeHolder, roundedCorners: false, block: { (image) in
                var downloadedImage = image
                downloadedImage = image?.scale(toNewSize: self.imgProfilePicture.frame.size)
                self.imgProfilePicture.image = downloadedImage
                self.imgProfilePicture.contentMode = .scaleAspectFill
                self.imgProfilePicture.clipsToBounds = true
            })
            profileImage.layer.masksToBounds = true
            profileImage.cacheEnable = true;
            profileImage.startDownload()
        }
    }

    //MARK:- Tap subscription detail
    
    func tapSubscriptionDetail(sender:UITapGestureRecognizer) {
        if(appDelegate.membership_plan_id != "0" && appDelegate.membership_plan_id != "1" && appDelegate.membership_plan_id != "4"){
            SubscriptionView.isHidden = false
        }
    }
    
    
    //MARK:- button events-
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        //_ = self.navigationController?.popViewController(animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnEditProfileImage_Clicked(_ sender: Any) {
        let actionSheet: UIAlertController = UIAlertController.init(title: AlertViewTitle, message: "Upload profile image", preferredStyle: .actionSheet)
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheet.addAction(cancelAction)
        
        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Using Camera", style: .default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.cameraDevice = .front
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(takePictureAction)
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose Existing Photo", style: .default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
                imagePicker.allowsEditing = false
                imagePicker.mediaTypes = [kUTTypeImage as String]
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(choosePictureAction)
        
        //Present the AlertController
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func btnSubscribeForMore_Clicked(_ sender: Any) {
            let subscriptionPlanViewController: SubscriptionPlanViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionPlanViewController") as! SubscriptionPlanViewController
            self.navigationController?.pushViewController(subscriptionPlanViewController, animated: true)
    }
    
    @IBAction func btnAboutApproach_Clicked(_ sender: Any) {
        let aboutTheApproachViewController: AboutTheApproachViewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutTheApproachViewController") as! AboutTheApproachViewController
        self.navigationController?.pushViewController(aboutTheApproachViewController, animated: true)
    }
    
    @IBAction func btnManageBackgroundTracks(_ sender: UIButton) {
        let manageBackgroundTracks: ManageBackgroundTracksViewController = self.storyboard?.instantiateViewController(withIdentifier: "ManageBackgroundTracksViewController") as! ManageBackgroundTracksViewController
        self.navigationController?.pushViewController(manageBackgroundTracks, animated: true)
    }
    @IBAction func btnChangePassword_Clicked(_ sender: Any) {
        let changePasswordViewController: ChangePasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        self.navigationController?.pushViewController(changePasswordViewController, animated: true)
    }
    
    @IBAction func btnHealthDisclaimer_Clicked(_ sender: Any) {
        let healthDisclaimerViewController: HealthDisclaimerViewController = self.storyboard?.instantiateViewController(withIdentifier: "HealthDisclaimerViewController") as! HealthDisclaimerViewController
        self.navigationController?.pushViewController(healthDisclaimerViewController, animated: true)
    }
    
    @IBAction func btnTermsAndConditions_Clicked(_ sender: Any) {
        let termsAndConditionsViewController: TermsAndConditionsViewController = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as! TermsAndConditionsViewController
        self.navigationController?.pushViewController(termsAndConditionsViewController, animated: true)
    }
    
    @IBAction func btnPrivacyPolicy_Clicked(_ sender: Any) {
        let privacyPolicyViewController: PrivacyPolicyViewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
        self.navigationController?.pushViewController(privacyPolicyViewController, animated: true)
    }
    
    @IBAction func btnShareMeditable_Clicked(_ sender: Any) {
        let text: String = "Download this awsome app from App Store now:"
        let url: URL = URL(string: appURL)!
        
        // set up activity view controller
        let activityViewController = UIActivityViewController(activityItems: [text, url], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [.airDrop, .addToReadingList, .assignToContact, .openInIBooks, .print, .saveToCameraRoll]

        // present the view controller
        self.present(activityViewController, animated: true)
    }
    
    @IBAction func btnLogout_Clicked(_ sender: Any) {
        
        if appDelegate.reachable.isReachable{
            
            var authKey = ""
            if defaults.value(forKey: "AuthKey") as? String != nil {
                authKey = defaults.value(forKey: "AuthKey") as! String
            }
            if defaults.value(forKey: "applyCouponCode") as? String != nil {
                defaults.set(nil, forKey: "applyCouponCode")
            }
            if defaults.value(forKey: "purchased") as? String != nil {
                defaults.set(nil, forKey: "purchased")
            }
            var params = [String : AnyObject]()
            params = ["auth_key": authKey as AnyObject]
            
            let size = CGSize(width: 50, height: 50)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kLogout , method: .post, parameters: params).validate().responseJSON { response in
                self.stopAnimating()
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as! NSDictionary? {
                        if json.value(forKey: "status") as? Int == 1 {
                            
                            let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                            
                            defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                            defaults.set(deviceToken,forKey: "DeviceToken")
                            defaults.synchronize()
                            
                            let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                            self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)

                        }else {
                            UIAlertController().alertViewWithTitleAndMessage(self, message: json.value(forKey: "message") as! String)
                        }
                    }
                case .failure(let error):
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    @IBAction func btnClosePopup(_ sender: UIButton) {
        SubscriptionView.isHidden = true
    }
    
    @IBAction func btnSupport(_ sender: UIButton) {
        if !MFMailComposeViewController.canSendMail() {
            UIAlertController().alertViewWithTitleAndMessage(self, message: "Mail services are not available")
            print("Mail services are not available")
            return
        }else{
            let emailTitle = "Meditable Support"
            let messageBody = "How meditable can help you?"
            let toRecipents = ["info@getmeditable.com"]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setMessageBody(messageBody, isHTML: false)
            mc.setToRecipients(toRecipents)
            
            self.present(mc, animated: true, completion: nil)
        }
    }
    // Delegate Mail Composer
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .cancelled:
            print("Mail cancelled")
        case .saved:
            print("Mail saved")
        case .sent:
            print("Mail sent")
        case .failed:
            print("Mail sent failure: \(error!.localizedDescription)")
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    //Update Profile Picture
    func updateProfileImage() {
        if appDelegate.reachable.isReachable{
            
            var authKey = ""
            if defaults.value(forKey: "AuthKey") as? String != nil {
                authKey = defaults.value(forKey: "AuthKey") as! String
            }
            
            var params = [String : AnyObject]()
            params = ["auth_key": authKey as AnyObject]
            
            let size = CGSize(width: 50, height: 50)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.upload(multipartFormData: {
                multipartFormData in
                //append image
                if self.imgProfilePicture != nil {
                    if let imageData = UIImageJPEGRepresentation(self.imgProfilePicture.image!, 0.6) {
                        multipartFormData.append(imageData, withName: "file", fileName: "profilepicture.png", mimeType: "image/png")
                    }
                }
                //append parameters
                for (key, value) in params {
                    multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, usingThreshold: 1, to: baseURLWithAuth + kUpdateProPic, method: .post, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON(completionHandler: {
                        response in
                        switch response.result {
                        case .success:
                            self.stopAnimating()
                            if let json: NSDictionary = response.result.value as! NSDictionary? {
                                if json.value(forKey: "status") as? Int == 1 {
                                    print("Profile Image Uploaded Successfully with received data:- \(json)")
                                    
                                    defaults.set(json.value(forKey: "image_url") as! String, forKey: "image_url")
                                    defaults.synchronize()
                                    self.setProfileData()
//                                    self.getMeters()
                                }
                                else {
                                    UIAlertController().alertViewWithTitleAndMessage(self, message: json.value(forKey: "message") as! String)
                                }
                            }
                        case .failure(let error):
                            self.stopAnimating()
                            UIAlertController().alertViewWithTitleAndMessage(self, message: error.localizedDescription)
                        }
                    })
                case .failure(let encodingError):
                    self.stopAnimating()
                    UIAlertController().alertViewWithTitleAndMessage(self, message: encodingError.localizedDescription)
                }
            })
        }
        else {
            self.stopAnimating()
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    //Image Picker Controller Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if var image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.dismiss(animated: true, completion: nil)
            image = image.scale(toNewSize: self.imgProfilePicture.frame.size)
            self.imgProfilePicture.image = image
            self.imgProfilePicture.contentMode = .scaleAspectFill
            self.imgProfilePicture.layer.masksToBounds = true
            self.updateProfileImage()
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
