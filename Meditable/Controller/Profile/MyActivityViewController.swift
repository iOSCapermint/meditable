//
//  MyActivityViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 27/04/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import FSCalendar
import AVFoundation
import AudioToolbox
import MediaPlayer
import NVActivityIndicatorView


class MyActivityViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance, NVActivityIndicatorViewable {
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var lblMyActivity: UILabel!
    
    @IBOutlet weak var btnSettings: UIButton!
    
    @IBOutlet weak var lblLatestActivity: UILabel!
    
    @IBOutlet weak var tblLatestActivity: UITableView!
    
    @IBOutlet weak var lblStats: UILabel!
    
    @IBOutlet weak var statsCalendar: FSCalendar!
    
    @IBOutlet weak var statDetailsView: UIView!
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var lblCurrentStreakDays: UILabel!
    
    @IBOutlet weak var lblCurrentStreak: UILabel!
    
    @IBOutlet weak var lblLongestStreak: UILabel!
    
    @IBOutlet weak var firstSeperator: UIView!
    
    @IBOutlet weak var lblTotalTime: UILabel!
    
    @IBOutlet weak var lblTotalTimeValue: UILabel!
    
    @IBOutlet weak var secondSeperatorView: UIView!
    
    @IBOutlet weak var lblNumberOfDays: UILabel!
    
    @IBOutlet weak var lblDaysValue: UILabel!
    
    @IBOutlet weak var tblLatestActivityHeightConstraint: NSLayoutConstraint!
    
    var playerMeditaion = AVQueuePlayer()
    var responseActivityData = [String : AnyObject]()
    var responseMeditationData = [[String : AnyObject]]()
    var responseCalenderData = [String]()
    var btnindex = Int()
    var refresher: UIRefreshControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //RefreshControl
        refresher = UIRefreshControl()
        tblLatestActivity!.alwaysBounceVertical = true
        refresher.tintColor = .white
        refresher.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tblLatestActivity!.addSubview(refresher)
        
        addGoogleAnalytics(screenName: "My Activity Screen")
        
        self.btnindex = -1
        
        
//        getActivityList(month: "\(Date().getMonth())", year: "\(getDateMonthYear(date: Date()).year)")
        
        tblLatestActivity.isScrollEnabled = false
        
        statsCalendar.delegate = self
        statsCalendar.dataSource = self
        statsCalendar.clipsToBounds = true
        addGradient()
        
        tblLatestActivityHeightConstraint.constant = 80//CGFloat(filterEventDate.count * 70)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "My Activity")
        getActivityList(month: "\(Date().getMonth())", year: "\(getDateMonthYear(date: Date()).year)")
    }
    //MARK:-  Refresh -
    func refreshList() {
        self.refresher.endRefreshing()
        getActivityList(month: "\(Date().getMonth())", year: "\(getDateMonthYear(date: Date()).year)")
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnSettings_Clicked(_ sender: Any) {
        
        let myProfileViewController: MyProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
        self.navigationController?.pushViewController(myProfileViewController, animated: true)
    }
    
    //Play Button Tapped
    func btnPlayMeditation_Clicked(sender: UIButton) {
        let indexPath: NSIndexPath = self.tblLatestActivity.indexPath(for: (sender.superview?.superview as! UITableViewCell))! as NSIndexPath
        
        if btnindex == indexPath.row{
            btnindex = -1
        }else{
            btnindex = indexPath.row
        }
        
        if(sender.isSelected == false){
            var avPlayerItem : [AVPlayerItem] = []
            let arrayList = self.responseMeditationData[indexPath.row]["track"] as! [[String : AnyObject]]
            for index in 0..<arrayList.count{
                let tmp = "\(arrayList[index]["audio_url"]!)"
                let url = URL.init(string: tmp)
                let it = AVPlayerItem.init(url: url!)
                avPlayerItem.append(it)
            }
            playerMeditaion = AVQueuePlayer.init(items: avPlayerItem)
            if #available(iOS 10.0, *) {
                playerMeditaion.automaticallyWaitsToMinimizeStalling = false
            } else {
                // Fallback on earlier versions
            }
            self.playerMeditaion.play()
            
        }else{
            self.playerMeditaion.pause()
            self.playerMeditaion.rate = 0
        }
        self.tblLatestActivity.reloadData()
    }
    
    func btnNewMeditation_Clicked(sender: UIButton) {
        if(appDelegate.reachable.isReachable){
            let customMeditationViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationViewController") as! CustomMeditationViewController
            self.navigationController?.pushViewController(customMeditationViewController, animated: true)
        }else{
            let alertController = UIAlertController(title: AppName, message: NoInternetMessage, preferredStyle:UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            { action -> Void in
                
            })
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //TableView Delegate & Data source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.responseMeditationData.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellIdentifier = "Cell"
        
        if indexPath.row == self.responseMeditationData.count{
            cellIdentifier = "ButtonCell"
        }else{
            cellIdentifier = "Cell"
        }
        
        
        let cell: UITableViewCell! = tblLatestActivity.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
        
        if indexPath.row == self.responseMeditationData.count {
            let btnNewMeditation: UIButton = cell.viewWithTag(10) as! UIButton
            
            btnNewMeditation.layer.borderWidth = 1
            btnNewMeditation.layer.borderColor = UIColor().yellowBorderColor.cgColor
            btnNewMeditation.layer.cornerRadius = btnNewMeditation.frame.size.height / 2
            btnNewMeditation.layer.masksToBounds = true
            
            btnNewMeditation.addTarget(self, action: #selector(MyActivityViewController.btnNewMeditation_Clicked(sender: )), for: .touchUpInside)
        }
        else {
            let lblMeditationName: UILabel = cell.viewWithTag(10) as! UILabel
            let lblMeditationTime: UILabel = cell.viewWithTag(11) as! UILabel
            
            if(self.responseMeditationData.count > 0){
                lblMeditationName.text = "\(self.responseMeditationData[indexPath.row]["title"]!)"
                
                let trackArray : [[String : AnyObject]] = responseMeditationData[indexPath.row]["track"] as! [[String : AnyObject]]
                var bgTime = "00:00"
                for i in 0...trackArray.count - 1 {
                    if("\(trackArray[i]["track_type"]!)" == "B"){
                        bgTime = "\(trackArray[i]["time"]!)"
                        break
                    }
                }
                let time = self.getTotalTime(totalTime: "\(self.responseMeditationData[indexPath.row]["total_meditation_time"]!)", backgroundMusicTime: bgTime)
                lblMeditationTime.text = "\(time.time)"
                
//                lblMeditationTime.text = "\(self.responseMeditationData[indexPath.row]["total_meditation_time"]!)"
            }
            let btnPlay: UIButton = cell.viewWithTag(12) as! UIButton
            btnPlay.addTarget(self, action: #selector(MyActivityViewController.btnPlayMeditation_Clicked(sender: )), for: .touchUpInside)
            if btnindex == indexPath.row{
                btnPlay.isSelected = true
                btnPlay.setImage(#imageLiteral(resourceName: "Stop"), for: UIControlState.selected)
            }else{
                btnPlay.isSelected = false
                btnPlay.setImage(#imageLiteral(resourceName: "SmallPlay"), for: UIControlState.normal)
            }
            
            btnPlay.isHidden = true
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let customMeditationPlaylistViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationPlaylistViewController") as! CustomMeditationPlaylistViewController
        customMeditationPlaylistViewController.responseArray = [self.responseMeditationData[indexPath.row]]
        customMeditationPlaylistViewController.index = indexPath.row
        customMeditationPlaylistViewController.isOriginal = "Y"//"\(self.responseMeditationData[indexPath.row]["is_original"]!)"
        
        if(self.responseMeditationData[indexPath.row]["meditation_id"] != nil){
            customMeditationPlaylistViewController.meditationID = "\(self.responseMeditationData[indexPath.row]["meditation_id"]!)"
        }else{
            customMeditationPlaylistViewController.meditationID = "\(self.responseMeditationData[indexPath.row]["custom_meditation_id"]!)"
        }
        customMeditationPlaylistViewController.fromActivityScreen = true
        let trackList = self.responseMeditationData[indexPath.row]["track"] as! [[String : AnyObject]]
        var bgTime = "00:00"
        for i in 0...trackList.count - 1 {
            if("\(trackList[i]["track_type"]!)" == "B"){
                bgTime = "\(trackList[i]["time"]!)"
                break
            }
        }
        let time = self.getTotalTime(totalTime: "\(self.responseMeditationData[indexPath.row]["total_meditation_time"]!)", backgroundMusicTime: bgTime)
        customMeditationPlaylistViewController.totalTime = "\(time.time)"
        customMeditationPlaylistViewController.backgroundSeconds = time.backgroundSeconds
        customMeditationPlaylistViewController.totalSeconds = time.totalSeconds
        
        self.navigationController?.pushViewController(customMeditationPlaylistViewController, animated: true)
        
//        let customMeditationPlaylistViewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomMeditationPlaylistViewController") as! CustomMeditationPlaylistViewController
//        print(self.responseMeditationData[indexPath.row])
//        customMeditationPlaylistViewController.responseArray = [self.responseMeditationData[indexPath.row]]
//        customMeditationPlaylistViewController.index = indexPath.section
//        customMeditationPlaylistViewController.isOriginal = "Y"//"\(self.meditationList[indexPath.section]["is_original"]!)"
//        self.navigationController?.pushViewController(customMeditationPlaylistViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    
    //FSCalendar Delegate & Data source Methods
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        return self.responseCalenderData.count
    }
    
    func calendar(_ calendar: FSCalendar, imageFor date: Date) -> UIImage? {
        if(self.responseCalenderData.count > 0){
            for i in 0...self.responseCalenderData.count - 1{
                if "\(self.responseCalenderData[i])" == "\(date.toString())"{
                    return #imageLiteral(resourceName: "SmallBaloon")
                }
            }
        }
        return UIImage.init(named: "")
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {

        if("\(date.toStringMonthYear())" != "\(Date().toStringMonthYear())"){
            if(self.responseCalenderData.count > 0){
                for i in 0...self.responseCalenderData.count - 1{
                    if "\(self.responseCalenderData[i])" == "\(date.toString())"{
                        return UIColor().lightYellowColor
                    }
                }
            }
            return UIColor.lightGray
        }
        if(self.responseCalenderData.count > 0){
            for i in 0...self.responseCalenderData.count - 1{
                if "\(self.responseCalenderData[i])" == "\(date.toString())"{
                    return UIColor().yellowBorderColor
                }
            }
        }
        return UIColor().whiteBorderColor
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        getActivityList(month: "\(calendar.currentPage.getMonth())", year: "\(getDateMonthYear(date: calendar.currentPage).year)")
    }
    
    
    
    //MARK:- GetActivity Details -
    
    func getActivityList(month : String, year : String){
        if(appDelegate.reachable.isReachable){
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)

            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            let param = ["auth_key": authKey, "month" : "\(month)", "year" : "\(year)"]
            APIManager().MyActivity(parameters: param as! [String : String]) { (response, error) in
                if(error == nil){
                    self.stopAnimating()
                    let responseArray = response as! [String : AnyObject]
                    self.responseActivityData = responseArray["data"] as! [String : AnyObject]
                    if(self.responseActivityData["latest_meditation"] != nil){
                        self.responseMeditationData = self.responseActivityData["latest_meditation"] as! [[String : AnyObject]]
                    }
                    if(self.responseActivityData["calendar_date"] != nil){
                        self.responseCalenderData = self.responseActivityData["calendar_date"] as! [String]
                    }
                    self.tblLatestActivityHeightConstraint.constant = CGFloat(80 * self.responseMeditationData.count) + 80.0
                    if self.responseActivityData["current_streak"] != nil{
                        self.lblCurrentStreakDays.text = "\(self.responseActivityData["current_streak"]!) Days"
                    }
                    if(self.responseActivityData["total_days"] != nil){
                        self.lblDaysValue.text = "\(self.responseActivityData["total_days"]!) Days"
                    }
                    if(self.responseActivityData["max_streak"] != nil){
                        self.lblLongestStreak.text = "Longest Streak: \(self.responseActivityData["max_streak"]!) Days"
                    }
                    if responseArray["total_meditation_time"] != nil {
                        let strMinute = responseArray["total_meditation_time"] as! String
                        let totalArray = strMinute.components(separatedBy: ":")
                        if(strMinute != ""){
                            if(totalArray[0] == "00"){
                                self.lblTotalTimeValue.text = "\(totalArray[1]) Minutes \(totalArray[2]) Seconds"
                            }else{
                                self.lblTotalTimeValue.text = "\(totalArray[0]) Hours \(totalArray[1]) Minutes \(totalArray[2]) Seconds"
                            }
                        }
                    } else {
                        self.lblTotalTimeValue.text = ""
                    }
                    
                    self.tblLatestActivity.reloadData()
                    self.statsCalendar.reloadData()
                }else{
                    if(error?.code == 5 || error?.code == 3){
                        let alertController = UIAlertController(title: AppName, message: "Unauthorized", preferredStyle:UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                        { action -> Void in
                            let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                            defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                            defaults.set(deviceToken,forKey: "DeviceToken")
                            defaults.synchronize()
                            let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                            self.navigationController?.pushViewController(welcomeViewController, animated: true)
                        })
                        self.present(alertController, animated: true, completion: nil)
                    }
                    self.stopAnimating()
                }
            }
        }
    }
    
    //Add Gradient To Background View
    func addGradient(){
//        self.view.backgroundColor = UIColor.init(red: 23.0/255.0, green: 15.0/255.0, blue: 43.0/255.0, alpha: 1.0)
//        let gradient:CAGradientLayer = CAGradientLayer()
//        gradient.frame.size = view.frame.size
//        gradient.colors = [UIColor.init(red: 38/255, green: 25/255, blue: 70/255, alpha: 1.0).cgColor, UIColor.init(red: 23/255, green: 15/255, blue: 43/255, alpha: 1.0).cgColor] //Or any colors
//        gradient.locations = [0.0, 1.0]
//        view.layer.insertSublayer(gradient, at: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
