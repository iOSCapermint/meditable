//
//  AboutTheApproachViewController.swift
//  Meditable
//
//  Created by Apple on 25/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON
import AVFoundation
import AudioToolbox
import ReachabilitySwift

class AboutTheApproachViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable, AVAudioPlayerDelegate {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblAboutApproach: UILabel!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var tblAbout: UITableView!
    
    var trackArray = [[String: AnyObject]]()
    var btnindex = Int()
    var audioPlayer : AVAudioPlayer!
    var player = AVPlayer()
    var isPlaying : Bool = false
    var btnPlayIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        getTrackList()
        self.btnindex = -1
        
        //        let videoURL = URL(string: "http://madeinkhadi.com/meditable/uploads/audio/about/mok4uo8750pml7as1k0y2hfxa.mp3")
        //
        //        player = AVPlayer(url:videoURL!)
        //
        //        let playerLayer = AVPlayerLayer(player: player)
        //
        //        playerLayer.frame = self.view.bounds
        //        self.view.layer.addSublayer(playerLayer)
        //
        //        player.play()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "About the Approach")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBack_Clicked(_ sender: Any) {
        //_ = self.navigationController?.popViewController(animated: true)
        player.pause()
        player.rate = 0
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    //MARK:- Play Button Tapped
    func btnPlayMeditation_Clicked(sender: UIButton) {
        player.rate = 0
        let indexPath: NSIndexPath = self.tblAbout.indexPath(for: (sender.superview?.superview as! UITableViewCell))! as NSIndexPath
        btnPlayIndex = indexPath.row
        
        if btnindex == indexPath.row{
            btnindex = -1
        }else{
            btnindex = indexPath.row
        }
        
        if var urlString: String = self.trackArray[indexPath.row]["audio_url"] as? String {
            
            urlString = urlString.replacingOccurrences(of: " ", with: "%20")
            //.replacingOccurrences(of: " ", with: "%20")
            
            let url = URL.init(string: urlString)
            //            let url = Bundle.main.url(forResource: "inTime", withExtension: "mp3")!
            
            do {
                let avPlayerItem: AVPlayerItem = AVPlayerItem.init(url: url!)
                player = AVPlayer.init(playerItem: avPlayerItem)
                
                let duration: CMTime = player.currentItem!.asset.duration
                let seconds:  Float = Float(CMTimeGetSeconds(duration))
                
                
                let cell : UITableViewCell = (self.tblAbout.cellForRow(at: indexPath as IndexPath) as UITableViewCell!)!
                let customSlider: CustomUISlider = cell.viewWithTag(11) as! CustomUISlider
                
                if(sender.isSelected == true){
                    //                if(isPlaying == true){
                    player.pause()
                    customSlider.value = 0.0
                    player.rate = 0
                    //                    isPlaying = false
                }else{
                    if #available(iOS 10.0, *) {
                        player.automaticallyWaitsToMinimizeStalling = false
                    } else {
                        // Fallback on earlier versions
                    }
                    player.play()
                    isPlaying = true
                    player.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1/30.0, Int32(NSEC_PER_SEC)), queue: nil) { time in
                        
                        //                        let cell : UITableViewCell = (self.tblAbout.cellForRow(at: indexPath as IndexPath) as UITableViewCell!)!
                        //                        let customSlider: CustomUISlider = cell.viewWithTag(11) as! CustomUISlider
                        
                        if let currentItem = self.player.currentItem {
                            let duration = currentItem.duration.seconds
                            if !(duration.isNaN || duration.isInfinite){
                                customSlider.maximumValue = Float(duration)
                                customSlider.value = Float(CMTimeGetSeconds(time))
                            }else {
                                
                            }
                        }
                    }
                }
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        self.tblAbout.reloadData()
    }
    
    func sliderValueChanged(sender: UISlider) {
        
        let indexPath: NSIndexPath = self.tblAbout.indexPath(for: (sender.superview?.superview as! UITableViewCell))! as NSIndexPath
        if(btnPlayIndex == indexPath.row){
            let timeScale: Int32 = player.currentItem!.asset.duration.timescale
            let time: CMTime = CMTimeMakeWithSeconds(Float64(sender.value), timeScale)
            player.seek(to: time)
        }else{
            sender.value = 0.0
        }
    }
    
    //TableView Delegate & Data source Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.trackArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        
        let cell: UITableViewCell! = tblAbout.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
        
        let lblMeditationName: UILabel = cell.viewWithTag(10) as! UILabel
        if let title = self.trackArray[indexPath.row]["title"] as? String {
            lblMeditationName.text = title
        }
        
        let customSlider: CustomUISlider = cell.viewWithTag(11) as! CustomUISlider
        customSlider.maximumValue = 1.0
        customSlider.minimumValue = 0.0
        customSlider.addTarget(self, action: #selector(AboutTheApproachViewController.sliderValueChanged(sender: )), for: .valueChanged)
        customSlider.value = 0.0
        let lblMeditationTime: UILabel = cell.viewWithTag(12) as! UILabel
        if let time = self.trackArray[indexPath.row]["time"] as? String {
            lblMeditationTime.text = time
        }
        
        let btnPlay: UIButton = cell.viewWithTag(13) as! UIButton
        if btnindex == indexPath.row{
            btnPlay.isSelected = true
            btnPlay.setImage(#imageLiteral(resourceName: "Stop"), for: UIControlState.selected)
        }else{
            btnPlay.isSelected = false
            btnPlay.setImage(#imageLiteral(resourceName: "SmallPlay"), for: UIControlState.normal)
        }
        
        btnPlay.addTarget(self, action: #selector(AboutTheApproachViewController.btnPlayMeditation_Clicked(sender: )), for: .touchUpInside)
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView: UIView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    
    //MARK: - About track list API
    func getTrackList() {
        if appDelegate.reachable.isReachable{
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kAboutTrack, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
                        if json.value(forKey: "status") as? Int == 1{
                            self.trackArray = json.value(forKey: "data") as! [[String : AnyObject]]
                            
                            if self.trackArray.count > 0{
                                self.tblAbout.reloadData()
                            }
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                        
                        
                    }else {
                        //                        if let message = json["message"].string {
                        //                            UIAlertController().alertViewWithTitleAndMessage(self, message: message)
                        //                        }
                    }
                case .failure(let error):
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension AVPlayer{
    var isPlaying: Bool{
        return rate != 0 && error == nil
    }
}
