//
//  CategoriesViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 03/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import YSLContainerViewController
import TPKeyboardAvoiding
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON

class CategoriesViewController: UIViewController, YSLContainerViewControllerDelegate, NVActivityIndicatorViewable {

    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblChooseYourGoal: UILabel!
    
    var performanceList = [[String : AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        // For Check Online or Offline
//        if appDelegate.reachable.isReachable{
//            getCategoryData()
//        }else if(defaults.value(forKey: kCategoryWithMeditationCount) == nil){
//            UIAlertController().alertViewWithErrorMessage(self)
//        }else{
//            let resultArray : [[String : AnyObject]] = fetchFromLocal(title: kCategoryWithMeditationCount) as [[String : AnyObject]]
//            self.loadOffline(dict: resultArray)
//        }
        
        getCategoryData()
        
        let performanceListViewController: PerformanceListViewController = self.storyboard!.instantiateViewController(withIdentifier: "PerformanceListViewController") as! PerformanceListViewController
        performanceListViewController.title = "Performance"
        performanceListViewController.view.backgroundColor = UIColor.clear
        
        let healthListViewController: HealthListViewController = self.storyboard!.instantiateViewController(withIdentifier: "HealthListViewController") as! HealthListViewController
        healthListViewController.title = "Health"
        healthListViewController.view.backgroundColor = UIColor.clear
        
        let innerBalanceListViewController: InnerBalanceListViewController = self.storyboard!.instantiateViewController(withIdentifier: "InnerBalanceListViewController") as! InnerBalanceListViewController
        innerBalanceListViewController.title = "Inner Balance"
        innerBalanceListViewController.view.backgroundColor = UIColor.clear
        
        let spiritualityViewController: SpiritualityViewController = self.storyboard!.instantiateViewController(withIdentifier: "SpiritualityViewController") as! SpiritualityViewController
        spiritualityViewController.title = "Spirituality"
        spiritualityViewController.view.backgroundColor = UIColor.clear
        
        let yslContainerViewController: YSLContainerViewController = YSLContainerViewController.init(controllers: [performanceListViewController, healthListViewController, innerBalanceListViewController, spiritualityViewController], topBarHeight: 0, parentViewController: self, selectedIndex: appDelegate.selectedTabIndex)
        yslContainerViewController.view.backgroundColor = UIColor.clear
//        yslContainerViewController.view.backgroundColor = UIColor.init(patternImage: UIImage.init(named: "Category_bg_Tab")!)
        yslContainerViewController.view.frame = CGRect(x: 0, y: 64, width: SCREEN_WIDTH, height: SCREEN_HEIGHT - 64)
        yslContainerViewController.view.contentMode = .scaleAspectFill

        var newImgThumb : UIImageView
        newImgThumb = UIImageView.init(frame: self.view.bounds)
        newImgThumb.image = #imageLiteral(resourceName: "Category_withoutmoon")
        newImgThumb.contentMode = .scaleAspectFill
        yslContainerViewController.view.addSubview(newImgThumb)
        yslContainerViewController.view.sendSubview(toBack: newImgThumb)
        
        yslContainerViewController.delegate = self
        //yslContainerViewController.menuBackGroudColor = UIColor.clear //UIColor.init(patternImage: UIImage.init(named: "CategoryBackground")!) //UIColor.init(red: 38/255.0, green: 27/255.0, blue: 64/255.0, alpha: 1.0)
        //yslContainerViewController.menuItemFont = UIFont(name: String().themeFuturaBoldFontName, size: 16.0)
        //yslContainerViewController.menuItemTitleColor = .white
        //yslContainerViewController.menuIndicatorColor = UIColor().yellowBorderColor
        
        self.view.addSubview(yslContainerViewController.view)
        self.view.bringSubview(toFront: btnBack)
        self.view.bringSubview(toFront: lblChooseYourGoal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Library's Category")
    }

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnBack_Clicked(_ sender: AnyObject) {
        appDelegate.selectedTabIndex = 0
        NotificationCenter.default.post(name: NSNotification.Name.init("DisplayCategoryView"), object: nil, userInfo: nil)
        //self.navigationController!.popViewController(animated: true)
    }

    func containerViewItemIndex(_ index: Int, currentController controller: UIViewController!) {

    }

    
    /*
    //MARK:- LoadOffline -
    
    func loadOffline(dict : [[String : AnyObject]]){
        self.performanceList = dict
        for index in 0..<self.performanceList.count{
            let catID = self.performanceList[index]["category_id"] as! Int
            if catID == 1{
                appDelegate.healthArray = self.performanceList[index]["sub_category"] as! [[String : AnyObject]]
                NotificationCenter.default.post(name: NSNotification.Name.init("HealthNotification"), object: nil, userInfo: nil)
            } else if catID == 4{
                appDelegate.performanceArray = self.performanceList[index]["sub_category"] as! [[String : AnyObject]]
                NotificationCenter.default.post(name: NSNotification.Name.init("PerformanceNotification"), object: nil, userInfo: nil)
            } else if catID == 3{
                appDelegate.spiritualArray = self.performanceList[index]["sub_category"] as! [[String : AnyObject]]
                NotificationCenter.default.post(name: NSNotification.Name.init("SpiritualNotification"), object: nil, userInfo: nil)
            } else {
                appDelegate.innerArray = self.performanceList[index]["sub_category"] as! [[String : AnyObject]]
                NotificationCenter.default.post(name: NSNotification.Name.init("InnerBalanceNotification"), object: nil, userInfo: nil)
            }
        }
    }
    */
    
    //MARK: - Get Categories list API -
    func getCategoryData() {
        if appDelegate.reachable.isReachable{
            
            var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
            if authKey == nil {
                authKey = ""
            }
            
            var params = Parameters()
            
            params = ["auth_key": authKey]
            
            
            let size = CGSize(width: 75, height: 75)
            let color : UIColor = .white
            startAnimating(size, type: NVActivityIndicatorType(rawValue: 29)!, color: color)
            
            Alamofire.request(baseURLWithAuth + kCategoryWithMeditationCount, method: .post, parameters: params).validate().responseJSON {
                response in
                self.stopAnimating()
                
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as? NSDictionary{
//                        self.saveToLocal(result: json.value(forKey: "data") as! [[String : AnyObject]], title: kCategoryWithMeditationCount)
                        if json.value(forKey: "status") as? Int == 1{
                            
                            self.performanceList = json.value(forKey: "data") as! [[String : AnyObject]]
                            for index in 0..<self.performanceList.count{
                                let catID = self.performanceList[index]["category_id"] as! Int
                                if catID == 1{
                                    appDelegate.healthArray = self.performanceList[index]["sub_category"] as! [[String : AnyObject]]
                                    NotificationCenter.default.post(name: NSNotification.Name.init("HealthNotification"), object: nil, userInfo: nil)
                                } else if catID == 4{
                                    appDelegate.performanceArray = self.performanceList[index]["sub_category"] as! [[String : AnyObject]]
                                    NotificationCenter.default.post(name: NSNotification.Name.init("PerformanceNotification"), object: nil, userInfo: nil)
                                } else if catID == 3{
                                    appDelegate.spiritualArray = self.performanceList[index]["sub_category"] as! [[String : AnyObject]]
                                    NotificationCenter.default.post(name: NSNotification.Name.init("SpiritualNotification"), object: nil, userInfo: nil)
                                } else {
                                    appDelegate.innerArray = self.performanceList[index]["sub_category"] as! [[String : AnyObject]]
                                    NotificationCenter.default.post(name: NSNotification.Name.init("InnerBalanceNotification"), object: nil, userInfo: nil)
                                }
                            }
                            if appDelegate.performanceArray.count > 0{
//                                self.tblPerformanceMeditations.reloadData()
                            }
                        }else if(json.value(forKey: "status") as? Int == 5 || json.value(forKey: "status") as? Int == 3){
                            
                            let alertController = UIAlertController(title: AppName, message: "\(json.value(forKey: "message")!)", preferredStyle:UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                            { action -> Void in
                                let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                                
                                defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                defaults.set(deviceToken,forKey: "DeviceToken")
                                defaults.synchronize()
                                
                                let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                                self.navigationController?.pushViewController(welcomeViewController, animated: true)
                            })
                            self.present(alertController, animated: true, completion: nil)
                            
                        }

                        
                        
                    }else {
                        //                        if let message = json["message"].string {
                        //                            UIAlertController().alertViewWithTitleAndMessage(self, message: message)
                        //                        }
                    }
                case .failure(let error):
                    print (error)
                    UIAlertController().alertViewWithErrorMessage(self)
                }
            }
        }
        else {
            UIAlertController().alertViewWithNoInternet(self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
