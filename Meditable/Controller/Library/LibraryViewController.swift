//
//  LibraryViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 27/04/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit

class LibraryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var categoryList: UICollectionView!
    @IBOutlet weak var categoryListTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var subscriptionView: UIView!
    @IBOutlet weak var lblSubscriptionDays: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblRemainingCount: UILabel!
    @IBOutlet weak var subscribeInnerView: UIView!
    

    let imgCategories = ["Performance", "Health", "Inner Balance", "Spirituality"]

    
       override func viewDidLoad() {
        super.viewDidLoad()
        
        self.apiForBGTracks()

        if IS_IPHONE4s || IS_IPHONE5 {
            categoryListTopConstraint.constant = 233
        }
        else if IS_IPHONE6P {
            categoryListTopConstraint.constant = 313
        }
        
        subscriptionView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(subscriptionView)
        self.view.bringSubview(toFront: subscriptionView)
//        UIApplication.shared.keyWindow!.addSubview(subscriptionView)
//        UIApplication.shared.keyWindow!.bringSubview(toFront: subscriptionView)
        subscriptionView.isHidden = true
        subscribeInnerView.layer.cornerRadius = 7.0
        subscribeInnerView.clipsToBounds = true
        subscriptionView.frame = self.view.frame
        NotificationCenter.default.addObserver(self, selector: #selector(LibraryViewController.showSubscriptionView(sender: )), name: NSNotification.Name.init("showSubscriptionView"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        addGoogleAnalytics(screenName: "Library")
    }

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnManageSubscription(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.subscriptionView.isHidden = true
        }
        let subscriptionPlanViewController: SubscriptionPlanViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionPlanViewController") as! SubscriptionPlanViewController
        self.navigationController?.pushViewController(subscriptionPlanViewController, animated: true)
    }
    
    func showSubscriptionView (sender: Notification) {
        lblRemainingCount.text = "YOU HAVE \(numberOfDaysRemaining) DAYS LEFT FROM YOUR FREE TRIAL PERIOD"
        subscriptionView.isHidden = false
    }
    //Collection View Delegate & Data Source Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if imgCategories.count > 0 {
            return imgCategories.count
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "Cell"
        
        let cell: UICollectionViewCell! = categoryList.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as UICollectionViewCell!
        
        let imgCategory: UIImageView = cell.viewWithTag(10) as! UIImageView
        imgCategory.image = UIImage.init(named: imgCategories[indexPath.item])
        
        let lblCategoryName: UILabel = cell.viewWithTag(11) as! UILabel
        lblCategoryName.text = imgCategories[indexPath.item]
        
        
        cell.layer.cornerRadius = 6
        cell.layer.borderColor = UIColor().yellowBorderColor.cgColor
        cell.layer.borderWidth = 1.0
        
        cell.layer.masksToBounds = true
        cell.backgroundColor = .clear
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        /*let categoriesViewController: CategoriesViewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoriesViewController") as! CategoriesViewController
        categoriesViewController.selectedTabIndex = indexPath.item
        self.navigationController?.pushViewController(categoriesViewController, animated: true)*/
        let info = ["SelectedCategory": indexPath.item]
        NotificationCenter.default.post(name: NSNotification.Name.init("DisplayMainCategory"), object: nil, userInfo: info)
    }

    @IBAction func btnCancel(_ sender: UIButton) {
        subscriptionView.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension LibraryViewController: UICollectionViewDelegateFlowLayout {
    fileprivate var sectionInsets: UIEdgeInsets {
        return UIEdgeInsetsMake(5, 20, 5, 20)
    }
    
    fileprivate var itemsPerRow: CGFloat {
        return 2
    }
    
    fileprivate var interitemSpace: CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let sectionPadding = sectionInsets.left * (itemsPerRow + 1)
//        let interitemPadding = max(0.0, itemsPerRow - 1) * interitemSpace
//        let availableWidth = collectionView.bounds.width - sectionPadding - interitemPadding
//        let widthPerItem = availableWidth / itemsPerRow
//        return CGSize(width: widthPerItem, height: 120)
        if(IS_IPHONE5){
            return CGSize(width: (collectionView.frame.size.width - 50.0)/2, height: 120)
        }else{
            return CGSize(width: (collectionView.frame.size.width - 60.0)/2, height: 120)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if(IS_IPHONE5){
            return 10
        }else{
            return 20
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return interitemSpace
    }
    
    func apiForBGTracks(){
        var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
        if authKey == nil {
            authKey = ""
        }
        let param = ["auth_key": authKey]
        APIManager().backGroundTrackAPI(parameters: param as! [String : String]) { (response, error) in
            if(error == nil){
                if let json = response as? NSDictionary{
                    var backgroundTrackArray = [String]()
                    var backgroundTrackNameArray = [String]()

                    let responseArray = json.value(forKey: "data") as! [[String : AnyObject]]
                    for i in 0...responseArray.count - 1 {
                        backgroundTrackArray.append("\(responseArray[i]["audio_url"]!)")
                        backgroundTrackNameArray.append("\(responseArray[i]["title"]!)")
                    }
                    self.downloadBackgroundTrack(track: backgroundTrackArray, title: backgroundTrackNameArray)
                }
            }else{
                if(error?.code == 5 || error?.code == 3){
                    let alertController = UIAlertController(title: AppName, message: "Unauthorized", preferredStyle:UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
                    { action -> Void in
                        let deviceToken = defaults.value(forKey: "DeviceToken") as! String
                        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        defaults.set(deviceToken,forKey: "DeviceToken")
                        defaults.synchronize()
                        let welcomeViewController: WelcomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                        self.navigationController?.pushViewController(welcomeViewController, animated: true)
                    })
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func downloadBackgroundTrack(track : [String], title : [String]) {
        appDelegate.savedBackgroundTracks.removeAll()
        for i in 0...track.count - 1{
            if let audioUrl = URL(string: track[i]) {
                // then lets create your document folder url
                
                let destinationUrl = appDelegate.myDownloadBGPath
                let pathURL = NSURL(fileURLWithPath: destinationUrl)
                let filePath = pathURL.appendingPathComponent("\(audioUrl.lastPathComponent)")?.path
                if !FileManager.default.fileExists(atPath: appDelegate.myDownloadBGPath) {
                    try! FileManager.default.createDirectory(atPath: appDelegate.myDownloadBGPath, withIntermediateDirectories: true, attributes: nil)
                }
                // to check if it exists before downloading it
                if FileManager.default.fileExists(atPath: filePath!) {
                    appDelegate.savedBackgroundTracks.append("\(title[i])")
                    print("The file already exists at path")
                } else {
                    // you can use NSURLSession.sharedSession to download the data asynchronously
                    URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                        guard let location = location, error == nil else {
                            return
                        }
                        do {
                            // after downloading your file you need to move it to your destination url
                            try FileManager.default.moveItem(at: location, to: URL.init(fileURLWithPath: filePath!))
                            appDelegate.savedBackgroundTracks.append("\(title[i])")
                            print("File moved to documents folder")
                            NotificationCenter.default.post(name: NSNotification.Name.init("refreshBackgroundTrackList"), object: nil, userInfo: nil)
                        } catch let error as NSError {
                            print(error.localizedDescription)
                        }
                    }).resume()
                }
            }
        }
    }
}
