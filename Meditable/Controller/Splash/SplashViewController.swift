//
//  SplashViewController.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 02/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import FLAnimatedImage
import AVKit
import AVFoundation

class SplashViewController: VideoSplashViewController, ICETutorialControllerDelegate, AVPlayerViewControllerDelegate {
    
    @IBOutlet weak var imgAnimatedSplashView: UIImageView!
    
    var isLastPageReached = false
    
    var page1 = ICETutorialPage()
    
    var page2 = ICETutorialPage()
    
    var page3 = ICETutorialPage()
    
    var page4 = ICETutorialPage()
    
    var playerController = AVPlayerViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        

        let url = URL(fileURLWithPath: Bundle.main.path(forResource: "2x", ofType: "mp4")!)
        self.videoFrame = view.frame
        self.fillMode = .resize
        self.alwaysRepeat = false
        self.sound = false
        self.startTime = 0.0
        self.duration = 3.0
        self.alpha = 0.7
        self.backgroundColor = UIColor.clear
        self.contentURL = url
        
        NotificationCenter.default.addObserver(self, selector:#selector(SplashViewController.playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func viewWillAppear(){
        super.viewWillAppear(true)
        addGoogleAnalytics(screenName: "Application Launch")
        
//        NotificationCenter.default.addObserver(self, selector:#selector(SplashViewController.playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    //Go to Tutorial ViewController
    func playerDidFinishPlaying() {
        //playerController.dismiss(animated: false, completion: nil)
        self.playerItemDidReachEnd()
        
        self.page1 = ICETutorialPage.init(title: "Inner Balance", subTitle: "Lorem ipsum dolor summit dymmy data", pictureName: "TutorialBackground", duration: 2.0)
        
        self.page2 = ICETutorialPage.init(title: "Peace Of Mind", subTitle: "Lorem ipsum dolor summit dymmy data Lorem ipsum dolor summit dymmy data Lorem ipsum dolor summit dymmy data Lorem ipsum dolor summit dymmy data", pictureName: "Category_withoutmoon", duration: 2.0)
        
        self.page3 = ICETutorialPage.init(title: "Inner Balance", subTitle: "Lorem ipsum dolor summit dymmy data Lorem ipsum dolor summit dymmy data Lorem ipsum dolor summit dymmy data", pictureName: "TutorialBackground", duration: 2.0)
        
        self.page4 = ICETutorialPage.init(title: "Let's Meditate", subTitle: "Lorem ipsum dolor summit dymmy data", pictureName: "Category_withoutmoon", duration: 2.0)
        
        let titleStyle = ICETutorialLabelStyle.init()
        titleStyle.font = UIFont.init(name: String().themeFuturaFontName, size: 30)
        titleStyle.textColor = .white
        
        let subtitleStyle = ICETutorialLabelStyle.init()
        subtitleStyle.font = UIFont.init(name: String().themeFuturaFontName, size: 14)
        subtitleStyle.textColor = .white
        
        if IS_IPHONE5 {
            titleStyle.offset = 270
            subtitleStyle.offset = 220
        }
        else {
            titleStyle.offset = 300
            subtitleStyle.offset = 250
        }
        
        ICETutorialStyle.sharedInstance().titleStyle = titleStyle
        ICETutorialStyle.sharedInstance().subTitleStyle = subtitleStyle

        if (defaults.value(forKey: "UserID") as? String != nil) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tabbarViewController : TabbarViewController = storyboard.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
//            let navigationController: UINavigationController = self.window?.rootViewController as! UINavigationController
            navigationController?.pushViewController(tabbarViewController, animated: true)
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tutorialViewController : TutorialViewController = storyboard.instantiateViewController(withIdentifier: "TutorialViewController") as! TutorialViewController
            self.navigationController?.pushViewController(tutorialViewController, animated: true)

//            let tutorialViewController = ICETutorialController.init(pages: [page1, page2, page3, page4], delegate: self)
//            self.navigationController?.pushViewController(tutorialViewController!, animated: true)
            
        }
        /*let delay = 0.1 // time in seconds
        Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(SplashViewController.gotoTutorialView(sender:)), userInfo: nil, repeats: false)*/
    }
    
    //Tutorial Delegate Methods
    func tutorialController(_ tutorialController: ICETutorialController!, scrollingFromPageIndex fromIndex: UInt, toPageIndex toIndex: UInt) {
        print("Scrolling from page \(fromIndex) fromIndex to page \(toIndex).");
    }
    
    func tutorialControllerDidReachLastPage(_ tutorialController: ICETutorialController!) {
        print("This is the last page of tutorial")
        isLastPageReached = true
    }
    
    func tutorialController(_ tutorialController: ICETutorialController!, didClickOnLeftButton sender: UIButton!) {
        print("Skip clicked")

        let welcomeViewController: WelcomeViewController = self.storyboard!.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
        self.navigationController?.pushViewController(welcomeViewController, animated: true)

        /*let navigationController: UINavigationController = UINavigationController.init(rootViewController: welcomeViewController)
        navigationController.navigationBar.isHidden = true
        navigationController.navigationBar.barStyle = .black
        appDelegate.window?.rootViewController = navigationController*/
    }
    
    func tutorialController(_ tutorialController: ICETutorialController!, didClickOnRightButton sender: UIButton!) {
        print("Next clicked")
        
        if (isLastPageReached) {
            let welcomeViewController: WelcomeViewController = self.storyboard!.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
            self.navigationController?.pushViewController(welcomeViewController, animated: true)
            
            /*let navigationController: UINavigationController = UINavigationController.init(rootViewController: welcomeViewController)
            navigationController.navigationBar.isHidden = true
            navigationController.navigationBar.barStyle = .black
            appDelegate.window?.rootViewController = navigationController*/
        }
    }
    
    func gotoTutorialView(sender: AnyObject) {
        let tutorialViewController = ICETutorialController.init(pages: [page1, page2, page3, page4], delegate: self)
        self.navigationController?.pushViewController(tutorialViewController!, animated: true)
        //appDelegate.window?.rootViewController = tutorialViewController
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
