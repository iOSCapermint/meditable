//
//  XMLReader.h
//  FreeDictionary
//
//  Created by Vivek Shah on 11/16/15.
//  Copyright © 2015 Vivek Shah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMLReader : NSObject <NSXMLParserDelegate> 

+(NSDictionary *)dictionaryForXMLData:(NSData *)data error:(NSError *)errorPointer;
+(NSDictionary *)dictionaryForXMLString:(NSString *)string error:(NSError *)errorPointer;

@end
