//
//  UIImage+iPhone5extension.m
//  Health Manager App
//
//  Created by Milan Saraiya on 12/08/13.
//  Copyright (c) 2013 Beurer. All rights reserved.
//

#import "UIImage+iPhone5extension.h"

@implementation UIImage (iPhone5extension)

+ (UIImage*)imageNamedForDevice:(NSString*)name {
    if (IS_IPHONE_5)
    {
        UIImage *tallImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@-568h.png", name]];
        if(tallImage)
            return tallImage;
    }
    return [UIImage imageNamed:[NSString stringWithFormat:@"%@", name]];
}
@end
