//
//  NSString+SpecialCharacters.h
//  Fundoo
//
//  Created by Next Technosoft on 02/09/15.
//  Copyright (c) 2015 nexttechnosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SpecialCharacters)

+(NSString *)removeSpecialCharacters:(NSString *)string;

+(NSString *)replaceSpecialCharacters:(NSString *)specialCharacterStrings;

@end
