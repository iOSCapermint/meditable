//
//  NSDateFormatter+Locale.h
//  Health Manager App
//
//  Created by mac mini2 on 12/31/13.
//  Copyright (c) 2013 Beurer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Locale)

- (id)initWithSafeLocale;

@end