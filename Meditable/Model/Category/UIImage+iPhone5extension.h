//
//  UIImage+iPhone5extension.h
//  Health Manager App
//
//  Created by Milan Saraiya on 12/08/13.
//  Copyright (c) 2013 Beurer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (iPhone5extension)

+ (UIImage*)imageNamedForDevice:(NSString*)name;

@end
