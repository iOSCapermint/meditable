//
//  NSString+SpecialCharacters.m
//  Fundoo
//
//  Created by Next Technosoft on 02/09/15.
//  Copyright (c) 2015 nexttechnosoft. All rights reserved.
//

#import "NSString+SpecialCharacters.h"

@implementation NSString (SpecialCharacters)

+(NSString *)removeSpecialCharacters:(NSString *)stringWithSpecialCharacters {
    NSString *selectedMessageText = [stringWithSpecialCharacters stringByReplacingOccurrencesOfString:@"\n\n" withString:@""];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"!" withString:@"%21"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"\"" withString:@"%22"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"#" withString:@"%23"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"$" withString:@"%24"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%" withString:@"%25"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"'" withString:@"%27"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"(" withString:@"%28"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@")" withString:@"%29"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"*" withString:@"%2A"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"-" withString:@"%2D"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"." withString:@"%2E"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"/" withString:@"%2F"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"<" withString:@"%3C"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@">" withString:@"%3E"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"?" withString:@"%3F"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"[" withString:@"%5B"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"\\" withString:@"%5C"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"]" withString:@"%5D"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"^" withString:@"%5E"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"_" withString:@"%5F"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"`" withString:@"%60"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"{" withString:@"%7B"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"|" withString:@"%7C"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"}" withString:@"%7D"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"~" withString:@"%7E"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"¡" withString:@"%A1"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"¢" withString:@"%A2"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"£" withString:@"%A3"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"¤" withString:@"%A4"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"¥" withString:@"%A5"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"¦" withString:@"%A6"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"§" withString:@"%A7"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"¨" withString:@"%A8"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"©" withString:@"%A9"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ª" withString:@"%AA"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"«" withString:@"%AB"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"¬" withString:@"%AC"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"®" withString:@"%AE"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"¯" withString:@"%AF"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"À" withString:@"%C0"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Á" withString:@"%C1"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Â" withString:@"%C2"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ã" withString:@"%C3"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ä" withString:@"%C4"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Å" withString:@"%C5"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Æ" withString:@"%C6"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ç" withString:@"%C7"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"È" withString:@"%C8"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"É" withString:@"%C9"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ê" withString:@"%CA"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ë" withString:@"%CB"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ì" withString:@"%CC"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Í" withString:@"%CD"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Î" withString:@"%CE"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ï" withString:@"%CF"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ð" withString:@"%D0"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ñ" withString:@"%D1"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ò" withString:@"%D2"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ó" withString:@"%D3"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ô" withString:@"%D4"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Õ" withString:@"%D5"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ö" withString:@"%D6"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"×" withString:@"%D7"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ø" withString:@"%D8"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ù" withString:@"%D9"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ú" withString:@"%DA"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Û" withString:@"%DB"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ü" withString:@"%DC"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ý" withString:@"%DD"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Þ" withString:@"%DE"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ß" withString:@"%DF"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"à" withString:@"%E0"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"á" withString:@"%E1"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"â" withString:@"%E2"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ã" withString:@"%E3"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ä" withString:@"%E4"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"å" withString:@"%E5"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"æ" withString:@"%E6"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ç" withString:@"%E7"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"è" withString:@"%E8"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"é" withString:@"%E9"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ê" withString:@"%EA"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ë" withString:@"%EB"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ì" withString:@"%EC"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"í" withString:@"%ED"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"î" withString:@"%EE"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ï" withString:@"%EF"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ð" withString:@"%F0"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ñ" withString:@"%F1"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ò" withString:@"%F2"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ó" withString:@"%F3"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ô" withString:@"%F4"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"õ" withString:@"%F5"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ö" withString:@"%F6"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"÷" withString:@"%F7"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ø" withString:@"%F8"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ù" withString:@"%F9"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ú" withString:@"%FA"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"û" withString:@"%FB"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ü" withString:@"%FC"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ý" withString:@"%FD"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"þ" withString:@"%FE"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ÿ" withString:@"%FF"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Œ" withString:@"%152"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"œ" withString:@"%153"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Š" withString:@"%160"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"š" withString:@"%161"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"Ÿ" withString:@"%178"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"ƒ" withString:@"%192"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"€" withString:@"%20AC"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"•" withString:@"%2022"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"\U0000fffc" withString:@""];

    return selectedMessageText;
}

+(NSString *)replaceSpecialCharacters:(NSString *)specialCharacterStrings {
    NSString *selectedMessageText = [specialCharacterStrings stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%21" withString:@"!"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%22" withString:@"\""];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%23" withString:@"#"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%24" withString:@"$"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%25" withString:@"%"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%26" withString:@"&"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%27" withString:@"'"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%28" withString:@"("];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%29" withString:@")"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%2A" withString:@"*"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%2B" withString:@"+"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%2C" withString:@","];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%2D" withString:@"-"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%2E" withString:@"."];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%2F" withString:@"/"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%3A" withString:@":"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%3B" withString:@";"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%3C" withString:@"<"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%3D" withString:@"="];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%3E" withString:@">"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%3F" withString:@"?"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%40" withString:@"@"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%5B" withString:@"["];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%5C" withString:@"\\"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%5D" withString:@"]"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%5E" withString:@"^"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%5F" withString:@"_"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%60" withString:@"`"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%7B" withString:@"{"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%7C" withString:@"|"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%7D" withString:@"}"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%7E" withString:@"~"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%A1" withString:@"¡"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%A2" withString:@"¢"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%A3" withString:@"£"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%A4" withString:@"¤"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%A5" withString:@"¥"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%A6" withString:@"¦"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%A7" withString:@"§"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%A8" withString:@"¨"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%A9" withString:@"©"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%AA" withString:@"ª"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%AB" withString:@"«"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%AC" withString:@"¬"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%AE" withString:@"®"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%AF" withString:@"¯"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%C0" withString:@"À"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%C1" withString:@"Á"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%C2" withString:@"Â"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%C3" withString:@"Ã"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%C4" withString:@"Ä"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%C5" withString:@"Å"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%C6" withString:@"Æ"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%C7" withString:@"Ç"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%C8" withString:@"È"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%C9" withString:@"É"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%CA" withString:@"Ê"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%CB" withString:@"Ë"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%CC" withString:@"Ì"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%CD" withString:@"Í"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%CE" withString:@"Î"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%CF" withString:@"Ï"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%D0" withString:@"Ð"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%D1" withString:@"Ñ"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%D2" withString:@"Ò"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%D3" withString:@"Ó"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%D4" withString:@"Ô"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%D5" withString:@"Õ"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%D6" withString:@"Ö"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%D7" withString:@"×"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%D8" withString:@"Ø"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%D9" withString:@"Ù"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%DA" withString:@"Ú"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%DB" withString:@"Û"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%DC" withString:@"Ü"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%DD" withString:@"Ý"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%DE" withString:@"Þ"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%DF" withString:@"ß"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%E0" withString:@"à"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%E1" withString:@"á"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%E2" withString:@"â"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%E3" withString:@"ã"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%E4" withString:@"ä"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%E5" withString:@"å"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%E6" withString:@"æ"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%E7" withString:@"ç"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%E8" withString:@"è"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%E9" withString:@"é"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%EA" withString:@"ê"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%EB" withString:@"ë"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%EC" withString:@"ì"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%ED" withString:@"í"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%EE" withString:@"î"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%EF" withString:@"ï"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%F0" withString:@"ð"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%F1" withString:@"ñ"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%F2" withString:@"ò"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%F3" withString:@"ó"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%F4" withString:@"ô"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%F5" withString:@"õ"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%F6" withString:@"ö"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%F7" withString:@"÷"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%F8" withString:@"ø"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%F9" withString:@"ù"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%FA" withString:@"ú"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%FB" withString:@"û"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%FC" withString:@"ü"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%FD" withString:@"ý"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%FE" withString:@"þ"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%FF" withString:@"ÿ"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%152" withString:@"Œ"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%153" withString:@"œ"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%160" withString:@"Š"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%161" withString:@"š"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%178" withString:@"Ÿ"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%192" withString:@"ƒ"];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%20AC" withString:@"€"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%2022" withString:@"•"];
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    
    selectedMessageText = [selectedMessageText stringByReplacingOccurrencesOfString:@"\\U0000fffc" withString:@""];
    return selectedMessageText;
}

@end
