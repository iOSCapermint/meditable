//
//  NSDateFormatter+Locale.m
//  Health Manager App
//
//  Created by mac mini2 on 12/31/13.
//  Copyright (c) 2013 Beurer. All rights reserved.
//

#import "NSDateFormatter+Locale.h"

@implementation NSDateFormatter (Locale)

- (id)initWithSafeLocale {
    static NSLocale* en_US_POSIX = nil;
    self = [self init];
    if (en_US_POSIX == nil) {
        en_US_POSIX = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    }
    [self setLocale:en_US_POSIX];
    return self;
}
@end