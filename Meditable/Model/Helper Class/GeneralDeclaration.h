//
//  GeneralDeclaration.h
//  VintageHubApp
//
//  Created by Mac Mini on 25/11/13.
//  Copyright (c) 2013 VintageHub. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GeneralDeclaration : NSObject

@property (nonatomic, assign) BOOL isiPhone;
@property (nonatomic, assign) CGFloat screenHeight;
@property (nonatomic, assign) CGFloat screenWidth;
@property (nonatomic, retain) NSString *storyBoardName;
@property (nonatomic, retain) NSString *dbPath;
@property (nonatomic, retain) NSString *deviceToken;

+(GeneralDeclaration *)generalDeclaration;

@end
