//
//  GeneralDeclaration.m
//  VintageHubApp
//
//  Created by Mac Mini on 25/11/13.
//  Copyright (c) 2013 VintageHub. All rights reserved.
//

#import "GeneralDeclaration.h"
#import "HelperMethod.h"

@implementation GeneralDeclaration

@synthesize isiPhone = _isiPhone;
@synthesize screenHeight = _screenHeight;
@synthesize screenWidth = _screenWidth;
@synthesize storyBoardName = _storyBoardName;
@synthesize dbPath = _dbPath;
@synthesize deviceToken = _deviceToken;

+(GeneralDeclaration *)generalDeclaration {
    static dispatch_once_t pred;
    static GeneralDeclaration *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[GeneralDeclaration alloc] init];
        shared.isiPhone = [HelperMethod IsDeviceiPhone];
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        CGFloat screenScale = [[UIScreen mainScreen] scale];
        CGSize screenSize = CGSizeMake(screenBounds.size.width * screenScale, screenBounds.size.height * screenScale);
        shared.screenWidth = screenSize.width;
        shared.screenHeight = screenSize.height;
        shared.storyBoardName = @"Main";
        
        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDir = [documentPaths objectAtIndex:0];
        shared.dbPath = [documentsDir stringByAppendingPathComponent:@"MyTravellas.sqlite"];
    });
    return shared;
}

@end
