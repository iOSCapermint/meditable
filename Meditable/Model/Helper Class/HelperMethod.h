//
//  HelperMethod.h
//  VintageHubApp
//
//  Created by Mac Mini on 22/11/13.
//  Copyright (c) 2013 VintageHub. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ResponseCompilationBlock) (BOOL isSuccess, NSInteger statusCode, id response);

@interface HelperMethod : NSObject

+(NSString *)GetLocalizeTextForKey:(NSString *)key;
+(NSString *)GetCurrentCulture;
+(NSLocale *)GetCurrentLocale;
+(NSString *)GetWebAPIBasePath;
+(NSString *)GetImageBasePath;
+(BOOL)IsDeviceiPhone;
+(NSString *)GetStoryBoardName;
+(CGFloat) GetDeviceWidth;
+(CGFloat) GetDeviceHeight;
+(UIColor *)GetTitleFontColor;
+(UIColor *)SetBackgroundColorOfView;
+(BOOL)IsValidEmail:(NSString *)emailString Strict:(BOOL)strictFilter;
+(UIColor *)GetBackgroundColorOfTableView;

//Insert Default Settings
+(void)setNavigationBarBackground:(BOOL)isLandScape;

+(void)addStatusBarToViewController:(UIViewController *)viewController;
+(UIImage *)applyFilterOnImage:(UIImage *)capturedImage withFilterTypeName:(NSString *)filterMethodName;

+(void)addImagesToScrollView:(NSArray *)imageList inScrollView:(UIScrollView *)scrollView withHeight:(NSInteger)height withTag:(NSInteger)tag;
+(BOOL)CheckIfDatabaseExists:(NSString *) dbPath;

+(void)createDeparmentTable;
+(void)insertValueInDeparmentTable:(NSString *) title withDescription:(NSString *)description withEmail:(NSString *)email withContactno:(NSString *)contact_no withDepartmentId:(NSString *)department_id;
+(void)deleteDataFromDeparmentTable;
+(NSMutableArray *)getDataFromDeparmentTable;

+(void)createCityOfficialTable;
+(void)insertValueInCityOfficialTable:(NSString *) name withDesignation:(NSString *)designation withDescription:(NSString *)description withImage_url:(NSString *)image_url withCity_official_id:(NSString *)city_official_id withOrderNumber:(NSString *)default_order;
+(void)deleteDataFromCityOfficialTable;
+(NSMutableArray *)getDataFromCityOfficialTable;

@end
