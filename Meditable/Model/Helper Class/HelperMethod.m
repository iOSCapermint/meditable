//
//  HelperMethod.m
//  VintageHubApp
//
//  Created by Mac Mini on 22/11/13.
//  Copyright (c) 2013 VintageHub. All rights reserved.
//

#import "HelperMethod.h"
#import <sys/xattr.h>

@implementation HelperMethod

+(NSString *)GetCurrentCulture {
    NSString *language = @"";
    
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSRange substr = NSMakeRange(0, 2);
    language = [language substringWithRange:substr];
    
    if([language isEqualToString:@"en"])
        language = @"en-US";
    else if([language isEqualToString:@"pt"])
        language = @"pt-PT";
    return  language;
}

+(NSLocale *)GetCurrentLocale {
    return [[NSLocale alloc] initWithLocaleIdentifier:[[self GetCurrentCulture] stringByReplacingOccurrencesOfString:@"-" withString:@"_"]];
}

+(NSString *)GetLocalizeTextForKey:(NSString*)key {
    NSString *currentLanguage = [self GetCurrentCulture];
    NSString *path;
    if ([currentLanguage isEqualToString:@"en-US"])
        path = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"];
    else if ([currentLanguage isEqualToString:@"pt-PT"])
        path = [[NSBundle mainBundle] pathForResource:@"pt-PT" ofType:@"lproj"];
    else
        path = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"];
    
    NSBundle *languageBundle = [NSBundle bundleWithPath:path];
    NSString *str = [languageBundle localizedStringForKey:key value:@"" table:nil];
    return str;
}

+(NSString *)GetWebAPIBasePath {
    return @"http://alpha-trends.com/api/index.php?method=";
}

+(NSString *)GetImageBasePath {
    return @"http://brainque.com/picbuk/adminpanel/albumphotos/";
}

+(BOOL)IsDeviceiPhone {
    UIDevice* thisDevice = [UIDevice currentDevice];
    return !(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad);
}

+(CGFloat)GetDeviceWidth {
    return [GeneralDeclaration generalDeclaration].screenWidth;
}

+(CGFloat)GetDeviceHeight {
    return [GeneralDeclaration generalDeclaration].screenHeight;
}

+(NSString *)GetStoryBoardName {
    return [GeneralDeclaration generalDeclaration].storyBoardName;
}

+(UIColor *)GetTitleFontColor {
    return [UIColor whiteColor];
}

+(UIColor *)SetBackgroundColorOfView {
    //UIColor *color = [UIColor alloc];
    return [UIColor clearColor];
}

+(UIColor *)GetBackgroundColorOfOverviewPage {
    CGFloat deviceHeight = [HelperMethod GetDeviceHeight];
    if (deviceHeight == 480)
        return [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"overview_bg"]];
    else if (deviceHeight == 568)
        return [[UIColor alloc] initWithPatternImage:[UIImage imageNamedForDevice:@"overview_bg"]];
    else
        return [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"overview_bg"]];
}

+(BOOL)IsValidEmail:(NSString *)emailString Strict:(BOOL)strictFilter {
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailString];
}

+(UIColor *)GetBackgroundColorOfTableView {
    UIColor *color;
    CGFloat deviceHeight = [self GetDeviceHeight];
    if (deviceHeight == 480)
        color = [UIColor colorWithPatternImage:[UIImage imageNamed:@"inner_line_bg"]];
    else if (deviceHeight == 568)
        color = [UIColor colorWithPatternImage:[UIImage imageNamed:@"inner_line_bg"]];
    else
        color = [UIColor colorWithPatternImage:[UIImage imageNamed:@"inner_line_bg"]];
    return color;
}

+(void)setNavigationBarBackground:(BOOL)isLandScape {
    UIImage *img = [UIImage imageNamedForDevice:@"Navigationbar.png"];
    [[UINavigationBar appearance] setBackgroundImage:img forBarMetrics:UIBarMetricsDefault];
}

+(void)addStatusBarToViewController:(UIViewController *)viewController {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [GeneralDeclaration generalDeclaration].screenWidth, 20)];
    view.backgroundColor = RGBCOLOR(233, 38, 93);
    [viewController.view addSubview:view];
}

+(UIImage *)applyFilterOnImage:(UIImage *)capturedImage withFilterTypeName:(NSString *)filterMethodName {
    UIImage *filteredImage = [[UIImage alloc] init];
    SEL _selector = NSSelectorFromString(filterMethodName);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    filteredImage = [capturedImage performSelector:_selector];
#pragma clang diagnostic pop
    return filteredImage;
}

+(void)addImagesToScrollView:(NSArray *)imageList inScrollView:(UIScrollView *)scrollView withHeight:(NSInteger)height withTag:(NSInteger)tag {
    [scrollView setContentSize:CGSizeMake(SCREEN_WIDTH * [imageList count], height)];
    
    for (int i = 0; i < [imageList count]; i++) {
        UIImageView *imgOutfit = [[UIImageView alloc] init];
        imgOutfit.frame = CGRectMake(SCREEN_WIDTH * i, 0, SCREEN_WIDTH, height);
        imgOutfit.userInteractionEnabled = true;
        imgOutfit.backgroundColor = [UIColor whiteColor];
        imgOutfit.image = [UIImage imageNamed: [imageList objectAtIndex:i]];
        if (tag == 1) {
            imgOutfit.contentMode = UIViewContentModeScaleToFill;
        }
        else {
            imgOutfit.contentMode = UIViewContentModeScaleAspectFit;
        }
        
        
//        NSString *imageURL = [imageList objectAtIndex:i];
//        
//        [imgOutfit sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:imageName(@"Splash") options:SDWebImageProgressiveDownload | SDWebImageHighPriority | SDWebImageRetryFailed completed:^(UIImage *image, NSError* error, SDImageCacheType cacheType, NSURL *imageURL) {
//            if (!error) {
//                imgOutfit.image = image;
//                if (tag == 1) {
//                    imgOutfit.contentMode = UIViewContentModeScaleToFill;
//                }
//                else {
//                    imgOutfit.contentMode = UIViewContentModeScaleAspectFit;
//                }
//            }
//        }];
        
        imgOutfit.tag = i;
        [scrollView addSubview:imgOutfit];
    }
    [scrollView scrollRectToVisible:CGRectMake(0, 0, SCREEN_WIDTH, height) animated:NO];
}

+(BOOL)CheckIfDatabaseExists:(NSString *) dbPath {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDBExists = [fileManager fileExistsAtPath:dbPath];
    return isDBExists;
}

+(void)createDeparmentTable {
    sqlite3 *db;
    NSString *dbPath = [GeneralDeclaration generalDeclaration].dbPath;
    NSLog(@"dbPath: %@", dbPath);
    
    if(![HelperMethod CheckIfDatabaseExists:dbPath]) {
        if (sqlite3_open([dbPath UTF8String], &db) == SQLITE_OK) {
            char *errMsg;
            
            const char *createQuery = "CREATE TABLE IF NOT EXISTS department(title TEXT, description TEXT, email TEXT, contact_no TEXT, department_id TEXT)";
            
            if (sqlite3_exec(db, createQuery, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog(@"Failed to create table :%s", errMsg);
            }
            else {
                NSLog(@"department table successfully created");
            }
            sqlite3_close(db);
            
            [self createCityOfficialTable];
        } else {
            NSLog(@"Failed to open/create database");
        }
    }
}

+(void)insertValueInDeparmentTable:(NSString *) title withDescription:(NSString *)description withEmail:(NSString *)email withContactno:(NSString *)contact_no withDepartmentId:(NSString *)department_id {
    sqlite3 *db;
    
    if (sqlite3_open([[GeneralDeclaration generalDeclaration].dbPath UTF8String], &db) == SQLITE_OK) {
        //create insertStatement
        sqlite3_stmt *insertStatement;
        
        //create insertQuery
        const char *insertQuery = "INSERT INTO department (title, description, email, contact_no, department_id) VALUES (?,?,?,?,?)";
        
        if(sqlite3_prepare_v2(db, insertQuery, -1, &insertStatement, NULL) != SQLITE_OK)
        {
            NSLog(@"Error while creating add statement. '%s'", sqlite3_errmsg(db));
        }
        
        // insert values
        sqlite3_bind_text(insertStatement, 1, [title UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStatement, 2, [description UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStatement, 3, [email UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStatement, 4, [contact_no UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStatement, 5, [department_id UTF8String], -1, SQLITE_TRANSIENT);
        
        if (sqlite3_step(insertStatement) == SQLITE_DONE) {
            NSLog(@"Data successfully saved : new item id is %lld",sqlite3_last_insert_rowid(db));
        }
        else{
            NSLog(@"Failed to add contact");
        }
        sqlite3_finalize(insertStatement);
    }
    sqlite3_close(db);
}

+(void)deleteDataFromDeparmentTable {
    sqlite3 *db;
    
    if (sqlite3_open([[GeneralDeclaration generalDeclaration].dbPath UTF8String], &db) == SQLITE_OK) {
        //create deleteStatement
        sqlite3_stmt *deleteStatement;
        
        //create deleteQuery
        NSString  *deleteQuery = [NSString stringWithFormat:@"delete from department"];
        const char *deleteStatement1 = [deleteQuery UTF8String];
        
        sqlite3_prepare_v2(db,deleteStatement1, -1, &deleteStatement, NULL);
        
        if(sqlite3_step(deleteStatement) == SQLITE_DONE) {
            NSLog(@"Data delete successfully..!!");
        }
        else {
            NSLog(@"Data not delete..!!");
        }
        sqlite3_finalize(deleteStatement);
        sqlite3_close(db);
    }
}

+(NSMutableArray *)getDataFromDeparmentTable {
    NSMutableArray *deparmentList = [[NSMutableArray alloc] init];
    @try {
        if ([DBHelper OpenDatabaseInReadOnlyMode]) {
            NSString *selectQuery = [NSString stringWithFormat:@"SELECT * FROM department"];
            sqlite3_stmt *sqlStatement = [DBHelper PrepareSQLiteStatementFromQuery:selectQuery];
            
            if (sqlStatement != nil) {
                while (sqlite3_step(sqlStatement) == SQLITE_ROW) {
                    NSMutableDictionary *deparmentData = [[NSMutableDictionary alloc] init];
            
                    NSString *title = sqlite3_column_text(sqlStatement, 0) ? [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 0)] : @"";
                    [deparmentData setValue:title forKey:@"title"];
                    
                    NSString *description = sqlite3_column_text(sqlStatement, 1) ? [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 1)] : @"";
                    [deparmentData setValue:description forKey:@"description"];
                    
                    NSString *email = sqlite3_column_text(sqlStatement, 2) ? [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 2)] : @"";
                    [deparmentData setValue:email forKey:@"email"];
                    
                    NSString *contact_no = sqlite3_column_text(sqlStatement, 3) ? [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 3)] : @"";
                    [deparmentData setValue:contact_no forKey:@"contact_no"];
                    
                    NSString *department_id = sqlite3_column_text(sqlStatement, 4) ? [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 4)] : @"";
                    [deparmentData setValue:department_id forKey:@"department_id"];
                    
                    [deparmentList addObject:deparmentData];
                }
            }
        }
    } @catch (NSException *exception) {
        NSLog (@"%@", exception);
    } @finally {
        [DBHelper CloseDatabaseConnection];
        return deparmentList;
    }
}

+(void)createCityOfficialTable {
    sqlite3 *db;
    NSString *dbPath = [GeneralDeclaration generalDeclaration].dbPath;
    NSLog(@"dbPath: %@", dbPath);
    
    if (sqlite3_open([dbPath UTF8String], &db) == SQLITE_OK) {
        char *errMsg;
        
        const char *createQuery = "CREATE TABLE IF NOT EXISTS cityofficial(name TEXT, designation TEXT, description TEXT, image_url TEXT, city_official_id TEXT, default_order TEXT)";
        
        if (sqlite3_exec(db, createQuery, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            NSLog(@"Failed to create table :%s", errMsg);
        }
        else {
            NSLog(@"cityofficial table successfully created");
        }
        sqlite3_close(db);
    } else {
        NSLog(@"Failed to open/create database");
    }
}

+(void)insertValueInCityOfficialTable:(NSString *) name withDesignation:(NSString *)designation withDescription:(NSString *)description withImage_url:(NSString *)image_url withCity_official_id:(NSString *)city_official_id withOrderNumber:(NSString *)default_order {
    sqlite3 *db;
    
    if (sqlite3_open([[GeneralDeclaration generalDeclaration].dbPath UTF8String], &db) == SQLITE_OK) {
        //create insertStatement
        sqlite3_stmt *insertStatement;
        
        //create insertQuery
        const char *insertQuery = "INSERT INTO cityofficial (name, designation, description, image_url, city_official_id, default_order) VALUES (?,?,?,?,?, ?)";
        
        if(sqlite3_prepare_v2(db, insertQuery, -1, &insertStatement, NULL) != SQLITE_OK)
        {
            NSLog(@"Error while creating add statement. '%s'", sqlite3_errmsg(db));
        }
        
        // insert values
        sqlite3_bind_text(insertStatement, 1, [name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStatement, 2, [designation UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStatement, 3, [description UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStatement, 4, [image_url UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStatement, 5, [city_official_id UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStatement, 6, [default_order UTF8String], -1, SQLITE_TRANSIENT);
        
        if (sqlite3_step(insertStatement) == SQLITE_DONE) {
            NSLog(@"Data successfully saved : new item id is %lld",sqlite3_last_insert_rowid(db));
        }
        else{
            NSLog(@"Failed to add contact");
        }
        sqlite3_finalize(insertStatement);
    }
    sqlite3_close(db);
}

+(void)deleteDataFromCityOfficialTable {
    sqlite3 *db;
    
    if (sqlite3_open([[GeneralDeclaration generalDeclaration].dbPath UTF8String], &db) == SQLITE_OK) {
        //create deleteStatement
        sqlite3_stmt *deleteStatement;
        
        //create deleteQuery
        NSString  *deleteQuery = [NSString stringWithFormat:@"delete from cityofficial"];
        const char *deleteStatement1 = [deleteQuery UTF8String];
        
        sqlite3_prepare_v2(db,deleteStatement1, -1, &deleteStatement, NULL);
        
        if(sqlite3_step(deleteStatement) == SQLITE_DONE) {
            NSLog(@"Data delete successfully..!!");
        }
        else {
            NSLog(@"Data not delete..!!");
        }
        sqlite3_finalize(deleteStatement);
        sqlite3_close(db);
    }
}

+(NSMutableArray *)getDataFromCityOfficialTable {
    NSMutableArray *cityOfficialList = [[NSMutableArray alloc] init];
    @try {
        if ([DBHelper OpenDatabaseInReadOnlyMode]) {
            NSString *selectQuery = [NSString stringWithFormat:@"SELECT * FROM cityofficial"];
            sqlite3_stmt *sqlStatement = [DBHelper PrepareSQLiteStatementFromQuery:selectQuery];
            
            if (sqlStatement != nil) {
                while (sqlite3_step(sqlStatement) == SQLITE_ROW) {
                    NSMutableDictionary *cityOfficialData = [[NSMutableDictionary alloc] init];
                    
                    NSString *name = sqlite3_column_text(sqlStatement, 0) ? [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 0)] : @"";
                    [cityOfficialData setValue:name forKey:@"name"];
                    
                    NSString *designation = sqlite3_column_text(sqlStatement, 1) ? [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 1)] : @"";
                    [cityOfficialData setValue:designation forKey:@"designation"];
                    
                    NSString *description = sqlite3_column_text(sqlStatement, 2) ? [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 2)] : @"";
                    [cityOfficialData setValue:description forKey:@"description"];
                    
                    NSString *image_url = sqlite3_column_text(sqlStatement, 3) ? [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 3)] : @"";
                    [cityOfficialData setValue:image_url forKey:@"image_url"];
                    
                    NSString *city_official_id = sqlite3_column_text(sqlStatement, 4) ? [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 4)] : @"";
                    [cityOfficialData setValue:city_official_id forKey:@"city_official_id"];
                    
                    NSString *defaultOrderOfCityOfficial = sqlite3_column_text(sqlStatement, 5) ? [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 5)] : @"";
                    [cityOfficialData setValue:defaultOrderOfCityOfficial forKey:@"default_order"];
                    
                    [cityOfficialList addObject:cityOfficialData];
                }
            }
        }
    } @catch (NSException *exception) {
        NSLog (@"%@", exception);
    } @finally {
        [DBHelper CloseDatabaseConnection];
        return cityOfficialList;
    }
}

@end
