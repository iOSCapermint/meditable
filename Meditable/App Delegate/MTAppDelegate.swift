//
//  MTAppDelegate.swift
//  MyTravella
//
//  Created by Capermint Mini 2 on 31/01/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import SlideMenuControllerSwift
import UserNotifications
import SwiftyJSON
import Firebase
import FirebaseMessaging
import FirebaseCrash
import FacebookCore
import FacebookLogin
import Bolts
import AVFoundation
import ReachabilitySwift
import MZDownloadManager
import Fabric
import Crashlytics


@UIApplicationMain

class MTAppDelegate: UIResponder, UIApplicationDelegate, NVActivityIndicatorViewable, SlideMenuControllerDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var storyboard: UIStoryboard!
    var indicatorView: NVActivityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: SCREEN_WIDTH / 2, y: SCREEN_HEIGHT / 2, width: 50, height: 50))
    var selectedTabIndex = 0
    var performanceArray = [[String: AnyObject]]()
    var healthArray = [[String: AnyObject]]()
    var spiritualArray = [[String: AnyObject]]()
    var innerArray = [[String: AnyObject]]()
    var performanceTracksArray = [[String: AnyObject]]()
    var healthTracksArray = [[String: AnyObject]]()
    var spiritualTracksArray = [[String: AnyObject]]()
    var innerTracksArray = [[String: AnyObject]]()
    var subCatPerformanceArray = [[String: AnyObject]]()
    var subCatHealthArray = [[String: AnyObject]]()
    var subCatSpiritualArray = [[String: AnyObject]]()
    var subCatInnerArray = [[String: AnyObject]]()
    let reachable = Reachability()!
    var downloadedMeditations = [[String : AnyObject]]()
    var backgroundSessionCompletionHandler : (() -> Void)?
    var startDownloading : Bool = false
    let myDownloadPath = MZUtility.baseFilePath + "/Default folder"
    let myDownloadBGPath = MZUtility.baseFilePath + "/Background Tracks"
    var savedBackgroundTracks = [String]()
    var TotalMeditationTime = ""
    var playFromLibrary = true
    var membership_plan_id = ""
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UserDefaults.standard.set(false, forKey: "startDownloading")
        UserDefaults.standard.synchronize()

        storyboard = UIStoryboard(name: "Main", bundle: nil)

        //Override point for customization after application launch.
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        else {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound]
            let setting: UIUserNotificationSettings = UIUserNotificationSettings.init(types: type, categories: nil)
            application.registerUserNotificationSettings(setting)
            application.registerForRemoteNotifications()
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
        }
        catch {
            
        }
        
        
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.applicationIconBadgeNumber = 0

        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachable)
        do{
            try reachable.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }

        //UINavigationBar.appearance().backgroundColor = UIColor().backgroundColor
        
        FIRApp.configure()

        let fcmToken = FIRInstanceID.instanceID().token()
        if fcmToken == nil {
            NotificationCenter.default.post(name: Notification.Name.firInstanceIDTokenRefresh, object: nil)
        }
        
        connectToFcm()
        
        NotificationCenter.default.addObserver(self, selector: #selector(tokenRefreshNotification(notification:)), name: NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
        
        autoLogin()
        self.window?.makeKeyAndVisible()

        guard let gai = GAI.sharedInstance() else {
            assert(false, "Google Analytics not configured correctly")
            return true
        }
        gai.tracker(withTrackingId: googleTrackID)
        // Optional: automatically report uncaught exceptions.
        gai.trackUncaughtExceptions = true
        
        // Optional: set Logger to VERBOSE for debug information.
        // Remove before app release.
        gai.logger.logLevel = .verbose;
        
        
        Fabric.with([Crashlytics.self])
        
        return SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    func reachabilityChanged(note: NSNotification) {
        
        let reachable = note.object as! Reachability
        
        if reachable.isReachable {
            if reachable.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
            APIManager().autoLoginForSave()
        } else {
            displayAlert(title: AppName, message: "Sorry, no Internet connectivity detected. Please reconnect and try again")
        }
    }
    
    func displayAlert(title : String, message : String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil));
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func checkPurchasedProduct(){
        let isFourColorPurchased = MKStoreKit.shared().isProductPurchased("product_id")
        let isMovesPurchased = MKStoreKit.shared().isProductPurchased("product_id")
        
        if(isFourColorPurchased == true){
            //Default.set(true, forKey: FourColorPurchased)
        }else{
            //Default.set(false, forKey: FourColorPurchased)
        }
        if(isMovesPurchased == true){
           // Default.set(true, forKey: MovesPurchased)
        }else{
            //Default.set(false, forKey: MovesPurchased)
        }
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        AppEventsLogger.activate(application)
        
        //Connect to FCM here
        connectToFcm()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //App Return URLs from Social Platforms
    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if (url.scheme == facebookScheme) {
            if #available(iOS 9.0, *) {
                return SDKApplicationDelegate.shared.application(app, open: url, options: options)
            }
            else {
                return true
            }
        }
        return true
    }
    
    /*public func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
     if url.scheme == facebookScheme {
     return SDKApplicationDelegate.shared.application(application, open: url as URL!, sourceApplication: sourceApplication,
     annotation: annotation)
     }
     else if url.scheme == googleScheme {
     return GIDSignIn.sharedInstance().handle(url as URL!, sourceApplication: sourceApplication, annotation: annotation)
     }
     else {
     return true
     }
     }*/
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var deviceTokenString: String = deviceToken.hexString()
        deviceTokenString = deviceTokenString.replacingOccurrences(of: "\"", with: "")
        defaults.set("Y", forKey: "isOptedForNotifications")
        defaults.synchronize()
        
        //Need to use this when swizzling is disabled. We have disabled swizzling and so we need to make a request to Firebase to set APNS token. Also, always check the APNS envioronment and set type accordingly
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .prod)
        print("*********************deviceToken---\(deviceToken)**********************")
        print("This is Firebase Instance APNS Token \(String(describing: FIRInstanceID.instanceID().token()))")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        defaults.set("N", forKey: "isOptedForNotifications")
        defaults.synchronize()
    }
    
    func tokenRefreshNotification(notification: NSNotification) {
        // NOTE: It can be nil here
        var refreshedToken = ""
        if FIRInstanceID.instanceID().token() != nil {
            refreshedToken = FIRInstanceID.instanceID().token()!
            print("Firebase InstanceID token: \(refreshedToken)")
            
            defaults.set(refreshedToken, forKey: "DeviceToken")
            defaults.synchronize()
            
            connectToFcm()
        }
    }
    
    func connectToFcm() {
        FIRMessaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(String(describing: error))")
            }
            else {
                print("Connected to FCM.")
            }
        }
    }
    
    //Push Notification Delegate Methods
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        var typeOfNotifications = ""
        
        let data: Dictionary<String, AnyObject> = response.notification.request.content.userInfo["aps"] as! Dictionary<String, AnyObject>
        
        if data["type"] as? String != nil {
            typeOfNotifications = data["type"] as! String
        }
        else {
            typeOfNotifications = "general"
        }
    }
    
    //This method will call when app is in Foreground
    /*@available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler(
            [UNNotificationPresentationOptions.alert,
             UNNotificationPresentationOptions.sound,
             UNNotificationPresentationOptions.badge])
    }*/
    
    /*func getInitialData() {
        if HelperMethod.checkInternetStatus() {
            self.window?.rootViewController?.view.bringSubview(toFront: indicatorView)
            indicatorView.color = UIColor.white
            indicatorView.type = NVActivityIndicatorType(rawValue: 29)!
            indicatorView.type = NVActivityIndicatorType.ballClipRotate
            indicatorView.startAnimating()
            
            Alamofire.request(baseURLWithoutAuth + "generalist" , method: .post, parameters: nil).validate().responseJSON { response in
                self.indicatorView.stopAnimating()
                switch response.result {
                case .success:
                    if let json: NSDictionary = response.result.value as! NSDictionary? {
                        if json.value(forKey: "status") as? Int == 1 {
                            print("Login Successful with received data:- \(json)")
                            
                            if json.value(forKey: "country") as? [[String: AnyObject]] != nil {
                                self.countriesList = json.value(forKey: "country") as! [[String: AnyObject]]
                            }
                            
                            if json.value(forKey: "accommodation") as? [[String: AnyObject]] != nil {
                                self.accomodationsList = json.value(forKey: "accommodation") as! [[String: AnyObject]]
                            }
                            
                            if json.value(forKey: "language") as? [[String: AnyObject]] != nil {
                                self.languagesList = json.value(forKey: "language") as! [[String: AnyObject]]
                            }
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SetInitialData"), object: nil)
                        }
                    }
                case .failure(let error):
                    print (error)
                }
            }
        }
    }*/
    
    func autoLogin() {
        if (defaults.value(forKey: "UserID") as? String != nil) {
            storyboard = UIStoryboard(name: "Main", bundle: nil)
            
//            if appDelegate.reachable.isReachable{
            
                var authKey: String! = UserDefaults.standard.value(forKey: "AuthKey") as! String
                if authKey == nil {
                    authKey = ""
                }
                if authKey != "" {
//                    let tabbarViewController : TabbarViewController = storyboard.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
//                    let navigationController: UINavigationController = self.window?.rootViewController as! UINavigationController
//                    navigationController.pushViewController(tabbarViewController, animated: true)
                }
//            }
//            else {
//                UIAlertController().alertViewWithNoInternet((self.window?.rootViewController)!)
//            }
        }
        /*if defaults.value(forKey: "AuthKey") as? String != nil {
            if defaults.value(forKey: "Email") as? String != nil {
                DispatchQueue.main.async {
                    let email = defaults.value(forKey: "Email") as! String
                    FIRAuth.auth()?.signIn(withEmail: email, password: email, completion: { (user, error) in
                        print("User Logged in Successfully to Firebase")
                        if user != nil {
                            print(user!)
                            UserLoggedIn(LOGIN_EMAIL)
                            let notificationName = Notification.Name(NOTIFICATION_USER_LOGGED_IN)
                            NotificationCenter.default.post(name: notificationName, object: nil)
                        }
                    })
                }
                
                DispatchQueue.main.async {
                    var credentails = ""
                    
                    if let userEmail = defaults.value(forKey: "Email") as? String {
                        credentails = userEmail
                    }
                    
                    QBRequest.logIn(withUserEmail: credentails, password: credentails, successBlock: { (response, user) in
                        print("User logged in successfully")
                        UserDefault.setObject(String(user!.id), forKey: QBID)
                        UserLoggedIn(LOGIN_EMAIL)
                        
                        QBCore.instance().add(self)
                        QBProfile().synchronize(withUserData: user!)
                        user!.password = credentails
                        
                        QBChat.instance().connect(with: user!, completion: { (error) in
                            if (error != nil) {
                                print(error!.localizedDescription)
                            }
                            else {
                                print("User connected to chat successfully")
                            }
                        })
                        
                    }, errorBlock: { (response) in
                        print(response.description)
                    })
                }
            }

            if defaults.value(forKey: "isNewSocialUser") as? String != nil {
                if defaults.value(forKey: "isNewSocialUser") as! String == "Y" {
                    let completeSocialLoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompleteSocialLoginViewController") as! CompleteSocialLoginViewController
                    completeSocialLoginViewController.isFromAppDelegate = true
                    
                    let navigationController: UINavigationController = UINavigationController.init(rootViewController: completeSocialLoginViewController)
                    navigationController.navigationBar.barStyle = .black
                    navigationController.navigationBar.isTranslucent = false
                    navigationController.navigationBar.isHidden = true
                    
                    self.window?.rootViewController = navigationController
                }
                else {
                    if defaults.value(forKey: "isUserVerified") as! String == "Y" && defaults.value(forKey: "isProfileCompleted") as! String == "Y"  {
                        let homeViewController: HomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        let menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
                        menuViewController.homeViewController = homeViewController
                        
                        let slideMenuController = ExSlideMenuController(mainViewController: homeViewController, leftMenuViewController: menuViewController)
                        slideMenuController.automaticallyAdjustsScrollViewInsets = true
                        slideMenuController.delegate = self
                        
                        let navigationController: UINavigationController = UINavigationController.init(rootViewController: slideMenuController)
                        navigationController.navigationBar.barStyle = .black
                        navigationController.navigationBar.isTranslucent = false
                        navigationController.navigationBar.isHidden = true
                        
                        self.window?.rootViewController = navigationController
                    }
                    else if defaults.value(forKey: "isUserVerified") as! String == "N" {
                        let verificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "VerificationViewController") as! VerificationViewController
                        verificationViewController.isFromAppDelegate = true
                        
                        let navigationController: UINavigationController = UINavigationController.init(rootViewController: verificationViewController)
                        navigationController.navigationBar.barStyle = .black
                        navigationController.navigationBar.isTranslucent = false
                        navigationController.navigationBar.isHidden = true
                        
                        self.window?.rootViewController = navigationController
                    }
                    else if defaults.value(forKey: "isProfileCompleted") as! String == "N" {
                        let completeProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompleteProfileViewController") as! CompleteProfileViewController
                        
                        let navigationController: UINavigationController = UINavigationController.init(rootViewController: completeProfileViewController)
                        navigationController.navigationBar.barStyle = .black
                        navigationController.navigationBar.isTranslucent = false
                        navigationController.navigationBar.isHidden = true
                        
                        self.window?.rootViewController = navigationController
                    }
                }
            }
            else {
                if defaults.value(forKey: "isUserVerified") as? String != nil && defaults.value(forKey: "isProfileCompleted") as? String != nil {
                    if defaults.value(forKey: "isUserVerified") as! String == "Y" && defaults.value(forKey: "isProfileCompleted") as! String == "Y"  {
                        let homeViewController: HomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        let menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
                        menuViewController.homeViewController = homeViewController
                        
                        let slideMenuController = ExSlideMenuController(mainViewController: homeViewController, leftMenuViewController: menuViewController)
                        slideMenuController.automaticallyAdjustsScrollViewInsets = true
                        slideMenuController.delegate = self
                        
                        let navigationController: UINavigationController = UINavigationController.init(rootViewController: slideMenuController)
                        navigationController.navigationBar.barStyle = .black
                        navigationController.navigationBar.isTranslucent = false
                        navigationController.navigationBar.isHidden = true
                        
                        self.window?.rootViewController = navigationController
                    }
                    else if defaults.value(forKey: "isUserVerified") as! String == "N" {
                        let verificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "VerificationViewController") as! VerificationViewController
                        verificationViewController.isFromAppDelegate = true
                        
                        let navigationController: UINavigationController = UINavigationController.init(rootViewController: verificationViewController)
                        navigationController.navigationBar.barStyle = .black
                        navigationController.navigationBar.isTranslucent = false
                        navigationController.navigationBar.isHidden = true
                        
                        self.window?.rootViewController = navigationController
                    }
                    else if defaults.value(forKey: "isProfileCompleted") as! String == "N" {
                        let completeProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompleteProfileViewController") as! CompleteProfileViewController
                        
                        let navigationController: UINavigationController = UINavigationController.init(rootViewController: completeProfileViewController)
                        navigationController.navigationBar.barStyle = .black
                        navigationController.navigationBar.isTranslucent = false
                        navigationController.navigationBar.isHidden = true
                        
                        self.window?.rootViewController = navigationController
                    }
                }
            }
        }
        self.window?.makeKeyAndVisible()*/
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        backgroundSessionCompletionHandler = completionHandler
    }
}
