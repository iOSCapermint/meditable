//
//  UIImage+fixOrientation.h
//  VintageHubApp
//
//  Created by mac mini2 on 12/25/13.
//  Copyright (c) 2013 VintageHub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;
- (UIImage *)imageRotatedByRadians:(CGFloat)radians;
- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;
- (UIImage *)cropImageInFrame:(CGRect)croppingFrame;
- (UIImage *)cropImageForCorrectAspectRatio;
- (UIImage *)scaleImageToNewSize:(CGSize)newImageSize;
- (UIImage *)scaleImage:(UIImage *)image;

@end
