//
//  TimeLineTableViewCell.swift
//  Meditable
//
//  Created by Capermint Mini 2 on 17/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

import UIKit

class TimeLineTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTrackName: UILabel!
    
    @IBOutlet weak var btnEditTrackName: UIButton!
    
    @IBOutlet weak var lblTrackTime: UILabel!
    
    @IBOutlet weak var btnInfo: UIButton!
    
    @IBOutlet weak var btnPlay: UIButton!
    
    @IBOutlet weak var topTitleConstraint: NSLayoutConstraint!
    
    var allRows = Int()

    var currentIndexPath = IndexPath()
    
    let offSet:CGFloat = 15.0
    
    let circleRadius:CGFloat = 5.0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func draw(_ rect: CGRect) {
        
        //creating a circle
        /*let circlePath = UIBezierPath(arcCenter: CGPoint(x: offSet, y: (circleRadius * 2 + 10)), radius: CGFloat(circleRadius), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        //shapeLayer.strokeColor = UIColor().yellowBorderColor.cgColor
        shapeLayer.fillColor = UIColor().yellowBorderColor.cgColor
        //shapeLayer.borderColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 2.0
        shapeLayer.strokeStart = 0.0
        shapeLayer.strokeEnd = 1.0
        shapeLayer.masksToBounds = true
        UIColor.white.setStroke()
        UIColor().yellowBorderColor.setFill()
        circlePath.stroke()*/
        
        let view = UIView.init(frame: CGRect.init(x: 10.0, y: (circleRadius * 2) + 5, width: 10, height: 10))
        view.backgroundColor = UIColor().yellowBorderColor
        view.layer.cornerRadius = view.frame.size.height / 2
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 1
        self.addSubview(view)
        
        //creating a line with dashed pattern
        let dashPath = UIBezierPath()
        var startPoint = CGPoint()
        
        if currentIndexPath.row == 0 {
            startPoint = CGPoint(x:offSet, y:(circleRadius * 2 + 10))
        }
        else {
            startPoint = CGPoint(x:offSet, y:0)
        }
        dashPath.move(to: startPoint)
        
        var endPoint = CGPoint()
        if currentIndexPath.row == allRows - 1 {
            endPoint = CGPoint(x:offSet, y:(circleRadius * 2 + 10))
        }
        else {
            endPoint = CGPoint(x:offSet, y:self.bounds.maxY)
        }
        dashPath.addLine(to: endPoint)
        
        let dashes: [CGFloat] = [0, 0] //line with dash pattern of 4 thick and i unit space
        dashPath.setLineDash(dashes, count: dashes.count, phase: 0)
        dashPath.lineWidth = 1.0
        dashPath.lineCapStyle = .butt
        UIColor().whiteBorderColor.set()
        //UIColor.black.set()
        dashPath.stroke()
    }

}
