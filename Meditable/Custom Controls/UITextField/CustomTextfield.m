//
//  CustomTextField.m
//  VintageHubApp
//
//  Created by mac mini2 on 12/24/13.
//  Copyright (c) 2013 KatNap. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextfield

-(CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset([super textRectForBounds:bounds], 20.f, 0.f);
}

-(CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset([super textRectForBounds:bounds], 20.f, 0.f);
}

@end
