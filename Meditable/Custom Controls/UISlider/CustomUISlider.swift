//
//  CustomUISlider.swift
//  Meditable
//
//  Created by Apple on 20/05/17.
//  Copyright © 2017 capermint. All rights reserved.
//

class CustomUISlider : UISlider {
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        
        //keeps original origin and width, changes height, you get the idea
        let customBounds = CGRect(origin: bounds.origin, size: CGSize(width: bounds.size.width, height: 5.0))
        super.trackRect(forBounds: customBounds)
        return customBounds
    }
    
//    //while we are here, why not change the image here as well? (bonus material)
    override func awakeFromNib() {
        self.setThumbImage(#imageLiteral(resourceName: "ThumbOfBar"), for: .normal)
        self.setThumbImage(#imageLiteral(resourceName: "ThumbOfBar"), for: .highlighted)
        super.awakeFromNib()
    }
}
