//  LabeledPickerView.h
//  LabeledPickerView

#import <UIKit/UIKit.h>

@interface LabeledPickerView : UIPickerView

{
    NSMutableDictionary *labels;
}

/** Adds the label for the given component. */
-(void)addLabel:(NSString *)labeltext forComponent:(NSUInteger)component forLongestString:(NSString *)longestString;
@end